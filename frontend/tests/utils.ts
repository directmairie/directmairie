import { expect, Page, test as base } from '@playwright/test';
import AxeBuilder from '@axe-core/playwright';

// throws if there are any errors in the console when running the application
export const test = base.extend({
  page: async ({ page }, use) => {
    const messages: Array<string> = [];
    page.on('console', msg => {
      if (msg.type() === 'error') {
        messages.push(`${msg.text()}`);
      }
    });
    await use(page);
    expect(messages).toEqual([]);
  }
});

export const currentUser = {
  id: 1,
  email: 'john@mail.com',
  firstName: 'John',
  lastName: 'Doe',
  phone: '0612345678',
  issueTransitionNotificationActivated: false,
  superAdmin: false,
  poolingOrganizationAdmin: false,
  governmentAdmin: false
};

export async function checkA11y(
  page: Page,
  options: { excludedSelectors: Array<string> } = { excludedSelectors: [] }
) {
  const accessibilityScanResults = await new AxeBuilder({ page })
    .exclude(options.excludedSelectors)
    .analyze();
  expect(accessibilityScanResults.violations).toEqual([]);
}

export async function setTokenInLocalStorage(page: Page) {
  await page.addInitScript(() =>
    window.localStorage.setItem('dm-token', JSON.stringify({ token: 'fake-token' }))
  );
}
