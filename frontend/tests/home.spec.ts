import { checkA11y, currentUser, setTokenInLocalStorage, test } from './utils';
import { expect } from '@playwright/test';
import { IssueModel, IssueTransitionModel, PictureModel } from '../src/app/models/issue.model';

const issueInParis = {
  id: 42,
  creationInstant: '2019-01-08T15:00:00Z',
  status: 'REJECTED',
  description: 'Tower fell down',
  coordinates: {
    latitude: 48.858624,
    longitude: 2.294537
  },
  category: {
    label: 'Other'
  },
  transitions: [
    {
      transitionInstant: '2019-01-09T15:00:00Z',
      beforeStatus: 'CREATED',
      afterStatus: 'REJECTED',
      message: 'No problem found'
    }
  ],
  pictures: [] as Array<PictureModel>
} as IssueModel;
const issueInLyon = {
  id: 43,
  creationInstant: '2019-01-09T15:00:00Z',
  status: 'RESOLVED',
  description: null,
  coordinates: {
    latitude: 45.7573654,
    longitude: 4.8427945
  },
  category: {
    label: 'Broken lamp'
  },
  transitions: [
    {
      transitionInstant: '2019-01-10T15:00:00Z',
      beforeStatus: 'CREATED',
      afterStatus: 'RESOLVED',
      message: 'Fixed'
    }
  ],
  pictures: [] as Array<PictureModel>
} as IssueModel;
const issue2InLyon = {
  id: 44,
  creationInstant: '2019-01-09T15:00:00Z',
  status: 'CREATED',
  description: null,
  coordinates: {
    latitude: 45.7573654,
    longitude: 4.8427945
  },
  category: {
    label: 'Broken lamp'
  },
  transitions: [] as Array<IssueTransitionModel>,
  pictures: [] as Array<PictureModel>
} as IssueModel;

test.describe('Home', () => {
  test('should display issues', async ({ page }) => {
    await setTokenInLocalStorage(page);
    await page.route('**/api/users/me', route => route.fulfill({ json: currentUser }));

    await page.route('**/api/issues/mine', async route => {
      await route.fulfill({
        json: {
          content: [issueInParis, issueInLyon, issue2InLyon],
          size: 3,
          number: 0,
          totalElements: 3,
          totalPages: 1
        }
      });
    });

    await page.goto('/');
    await expect(page.locator('h2')).toHaveText('Mes dernières remontées');
    await expect(page.locator('.list-group-item')).toHaveCount(3);

    await expect(page.locator('.category').first()).toHaveText('Other');
    await expect(page.locator('.description').first()).toHaveText('Tower fell down');

    await checkA11y(page);

    // expand the first issue
    await page.locator('.description').first().click();
    await expect(page.locator('.leaflet-marker-icon')).toHaveCount(1);
  });
});
