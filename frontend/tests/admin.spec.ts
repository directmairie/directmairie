import { currentUser, setTokenInLocalStorage, test } from './utils';
import { expect, test as baseTest } from '@playwright/test';
import { IssueModel, IssueTransitionModel, PictureModel } from '../src/app/models/issue.model';

const issueInParis = {
  id: 42,
  creationInstant: '2019-01-08T15:00:00Z',
  status: 'CREATED',
  description: 'Tower fell down',
  coordinates: {
    latitude: 48.858624,
    longitude: 2.294537
  },
  category: {
    label: 'Other'
  },
  transitions: [
    {
      transitionInstant: '2019-01-09T15:00:00Z',
      beforeStatus: 'CREATED',
      afterStatus: 'REJECTED',
      message: 'No problem found'
    }
  ],
  pictures: [] as Array<PictureModel>
} as IssueModel;
const issueInLyon = {
  id: 43,
  creationInstant: '2019-01-09T15:00:00Z',
  status: 'CREATED',
  description: null,
  coordinates: {
    latitude: 45.7573654,
    longitude: 4.8427945
  },
  category: {
    label: 'Broken lamp'
  },
  transitions: [] as Array<IssueTransitionModel>,
  pictures: [] as Array<PictureModel>
} as IssueModel;

test.describe('Admin', () => {
  test('should display issues', async ({ page }) => {
    await setTokenInLocalStorage(page);

    await page.route('**/api/users/me', route => route.fulfill({ json: currentUser }));

    await page.route('**/api/issues?page=0&status=CREATED&status=RESOLVED&status=REJECTED', route =>
      route.fulfill({
        json: {
          content: [issueInParis, issueInLyon],
          size: 20,
          number: 0,
          totalElements: 42,
          totalPages: 3
        }
      })
    );

    await page.goto('/admin/issues');

    await expect(page.locator('h1')).toContainText("Remontées d'information");
    await expect(page.locator('.card-header')).toHaveCount(2);
    await expect(page.locator('.card-header').first()).toContainText('Other');
    await expect(page.locator('.description').first()).toContainText('Tower fell down');

    await expect(page.locator('.leaflet-marker-icon')).toHaveCount(2);
    await expect(page.locator('#map')).toHaveScreenshot('issues-list.png', {
      maxDiffPixelRatio: 0.05
    });

    await page.route('**/api/issues?page=1&status=CREATED&status=RESOLVED&status=REJECTED', route =>
      route.fulfill({
        json: {
          content: [issueInParis, issueInLyon],
          size: 20,
          number: 0,
          totalElements: 42,
          totalPages: 3
        }
      })
    );
    // clicking on another page
    await page.locator('.page-link', { hasText: '2' }).click();

    // wait a short instant for the view transition
    await page.waitForTimeout(200);
    // should refresh the URL
    await expect(page.url()).toContain('/admin/issues?page=2');
  });

  test('should allow to update status', async ({ page }) => {
    await setTokenInLocalStorage(page);

    await page.route('**/api/users/me', route => route.fulfill({ json: currentUser }));

    await page.route('**/api/issues?page=0&status=CREATED&status=RESOLVED&status=REJECTED', route =>
      route.fulfill({
        json: {
          content: [issueInParis, issueInLyon],
          size: 20,
          number: 0,
          totalElements: 42,
          totalPages: 3
        }
      })
    );

    await page.goto('/admin/issues');

    // update the status of the first issue from CREATED to RESOLVED
    await expect(page.locator('#statusDropdown-43')).toContainText('Créée');
    await page.locator('#statusDropdown-43').click();
    await page.getByRole('button', { name: 'Résolue' }).click();

    // and trigger a new API request
    await page.route('**/api/issues/43/status', async route => {
      if (route.request().method() === 'PUT') {
        await route.fulfill({});
      }
    });
    await page.route('**/api/issues/43', route =>
      route.fulfill({
        json: {
          ...issueInLyon,
          status: 'RESOLVED',
          transitions: [
            {
              transitionInstant: '2019-01-10T15:00:00Z',
              beforeStatus: 'CREATED',
              afterStatus: 'RESOLVED',
              message: null
            }
          ]
        }
      })
    );
    await page.locator('#confirm').click();

    // should update the status on success
    await expect(page.locator('#statusDropdown-43')).toContainText('Résolue');
  });
});

baseTest.describe('Admin', () => {
  // we use the base test from Playwright to avoid the check of the console logs after the test
  baseTest('should display an error if fetching fails', async ({ page }) => {
    await setTokenInLocalStorage(page);

    await page.route('**/api/users/me', route => route.fulfill({ json: currentUser }));

    await page.route('**/api/issues?page=0&status=CREATED&status=RESOLVED&status=REJECTED', route =>
      route.fulfill({
        status: 504
      })
    );

    await page.goto('/admin/issues');

    await expect(page.locator('h1')).toContainText("Remontées d'information");
    await expect(page.locator('.card-header')).toHaveCount(0);
    await expect(page.locator('.leaflet-marker-icon')).toHaveCount(0);
    await expect(page.locator('.toast-body')).toContainText('504');
  });
});
