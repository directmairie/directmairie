import { checkA11y, test } from './utils';
import { expect } from '@playwright/test';
import { IssueModel, PictureModel } from '../src/app/models/issue.model';

const issue = {
  id: 1,
  coordinates: { latitude: 48.858624, longitude: 2.294537 },
  status: 'DRAFT',
  pictures: [] as Array<PictureModel>
} as IssueModel;

test.describe('DirectMairie', () => {
  test('should display title on home page', async ({ page }) => {
    await page.goto('/');
    await expect(page.locator('h1')).toHaveText('Bienvenue dans DirectMairie !');
    await expect(page.locator('.btn-primary')).toHaveText('Remonter une information');
    await expect(page.locator('.btn-primary')).toHaveAttribute('href', '/issue');
    await checkA11y(page);
  });

  test('should display an issue creation page', async ({ page }) => {
    await page.goto('/issue');

    await expect(page.locator('#category')).toBeHidden();
    await expect(page.locator('#description')).toBeHidden();
    await expect(page.locator('#submit')).toBeHidden();

    await page.fill('#location', 'Paris');
    await page.route(/https:\/\/nominatim\.openstreetmap\.org\/search(.*)/, route =>
      route.fulfill({
        json: [
          {
            lat: '48.858624',
            lon: '2.294537',
            display_name: 'Tour Eiffel, Paris, France',
            address: {},
            boundingbox: [45, 50, 0, 10]
          },
          {
            lat: '48.886909',
            lon: '2.343126',
            display_name: 'Sacré coeur, Paris, France',
            address: {},
            boundingbox: [45, 50, 0, 10]
          }
        ]
      })
    );
    await page.click('#search');

    await page.route('**/api/issue-groups?latitude=48.858624&longitude=2.294537', route =>
      route.fulfill({
        json: [
          {
            id: 2,
            label: 'Lighting',
            categories: [
              {
                id: 2,
                label: 'Light broken',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ]
      })
    );

    await page.route('**/api/issues', async route => {
      if (route.request().method() === 'POST') {
        await route.fulfill({
          json: issue
        });
      }
    });
    await page.locator('.searchResult').first().click();

    await page.locator('#category').selectOption({ label: 'Other' });
    await page.locator('#description-acnowledgment-button').click();
    await page.locator('#description').fill('Tower fell down');
    await expect(page.locator('#map')).toHaveScreenshot('issue-creation.png', {
      maxDiffPixelRatio: 0.05
    });
    await checkA11y(page, {
      // the picture container fails the contrast check, but it's not a real issue
      excludedSelectors: ['.card-img-top']
    });

    await page.route('**/api/issues', async route => {
      if (route.request().method() === 'PUT') {
        await route.fulfill({
          json: issue
        });
      }
    });

    await page.route('**/api/issues/1', async route => {
      await route.fulfill({
        json: {
          ...issue,
          assignedGovernment: 1
        }
      });
    });
    await page.click('#submit');
    await expect(page.url()).toContain('/issue;id=1');
    await expect(page.locator('h1')).toHaveText('Merci !');
  });
});
