import { checkA11y, currentUser, setTokenInLocalStorage, test } from './utils';
import { expect } from '@playwright/test';

test.describe('Account', () => {
  test('should display account, change it, change the password, delete it', async ({ page }) => {
    await setTokenInLocalStorage(page);

    // api call to get the user
    await page.route('**/api/users/me', route => route.fulfill({ json: currentUser }));
    // api call to get the issues of the user
    await page.route('**/api/issues/mine', route => route.fulfill({ json: [] }));

    await page.goto('/');

    // go to the account
    await page.locator('.navbar-toggler').click();
    await expect(page.locator('#navbarAccount')).toContainText('John Doe');
    await page.locator('#navbarAccount').click();
    await checkA11y(page);

    await expect(page.locator('#email')).toContainText('john@mail');

    // update the info
    await page.getByRole('link', { name: 'Modifier' }).click();

    await page.locator('#current-password').fill('password');
    await page.locator('#first-name').fill('Jane');
    await checkA11y(page);

    await page.route('**/api/users/me', route =>
      route.fulfill({ body: JSON.stringify({ ...currentUser, firstName: 'Jane' }) })
    );
    await page.getByText('Enregistrer').click();
    await expect(page.getByRole('alert')).toContainText('Vos informations ont été enregistrées');
    await expect(page.getByRole('alert')).not.toBeVisible();
    await expect(page.locator('#navbarAccount')).toContainText('Jane Doe');

    // update password
    await page.getByRole('link', { name: 'Changer mon mot de passe' }).click();
    await page.locator('#current-password').fill('password');
    await page.locator('#new-password').fill('new-password');
    await page.locator('#password-confirmation').fill('new-password');
    await page.getByRole('button', { name: 'Enregistrer' }).click();
    await expect(page.getByRole('alert')).toContainText('Votre mot de passe a été modifié');
    await expect(page.getByRole('alert')).not.toBeVisible();
    await checkA11y(page);

    await expect(page.locator('h1')).toContainText('Mon compte');

    // delete account
    await page.getByRole('link', { name: 'Supprimer mon compte' }).click();
    await page.locator('#password').fill('password');
    await checkA11y(page);
    // api call to delete the user
    await page.route('**/api/users/me/deletion', route => route.fulfill({ json: {} }));
    await page.getByRole('button', { name: 'Supprimer mon compte' }).click();
    await expect(page.locator('h1')).toContainText('Bienvenue dans DirectMairie');
  });
});
