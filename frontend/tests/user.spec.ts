import { checkA11y, currentUser, test } from './utils';
import { expect } from '@playwright/test';

test.describe('User registration', () => {
  test('should allow to register', async ({ page }) => {
    await page.goto('/');

    await page.click('.navbar-toggler');
    await expect(page.locator('#navbarLogin')).toHaveText("S'identifier");
    await expect(page.locator('#navbarRegister')).toHaveText("S'inscrire");

    await page.click('#navbarRegister');
    await checkA11y(page);

    await expect(page.locator('h1')).toHaveText('Inscription');

    await page.locator('#email').fill('nicolas@adullact.fr');
    await page.locator('#password').fill('password');
    await page.locator('#passwordConfirmation').fill('password');

    await page.route('**/api/users', async route => {
      if (route.request().method() === 'POST') {
        await route.fulfill({ json: currentUser });
      }
    });
    await page.locator('#register').click();

    await expect(page.locator('#successSection')).toContainText(
      "Un courriel vous a été envoyé à l'adresse john@mail.com. Consultez votre boîte aux lettres."
    );
  });
});
