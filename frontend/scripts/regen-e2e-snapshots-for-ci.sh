#/bin/sh

# since esbuild is platform specific, we need to run a pnpm install to have a linux esbuild
# before running the tests
# Once done, we execute pnpm locally to get back to a local (MacOS) esbuild

PLAYWRIGHT_VERSION=$(cat package.json | jq --raw-output '.devDependencies["@playwright/test"]')
rm -rf ./node_modules
docker run --rm --network host -v $(pwd):/work/ -w /work/ -it mcr.microsoft.com/playwright:v${PLAYWRIGHT_VERSION} sh -c "corepack pnpm install --frozen-lockfile; corepack pnpm e2e --update-snapshots --retries=3"

rm -rf ./node_modules
pnpm install
