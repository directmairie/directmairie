// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-mocha-reporter'),
      require('karma-coverage'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    proxies: {
      // avoid 404 errors on images in tests
      '/api/issues/1/pictures/42/bytes': '',
      '/api/issues/1/pictures/43/bytes': '',
      '/api/issues/43/pictures/1/bytes': '',
      '/api/issues/43/pictures/12/bytes': '',
      '/api/governments/1/logo/12/bytes': '',
      '/api/governments/42/logo/76/bytes': '',
      '/api/governments/42/logo/245/bytes': '',
      '/api/governments/42/logo/354/bytes': '',
      '/api/issues/43/pictures/2/bytes': '',
      '/api/governments/1000/logo/3456/bytes': '',
      '/api/governments/1000/logo/97654/bytes': ''
    },
    jasmineHtmlReporter: {
      suppressAll: true // removes the duplicated traces
    },
    coverageReporter: {
      dir: require('path').join(__dirname, './coverage/directmairie'),
      subdir: '.',
      reporters: [{ type: 'html' }, { type: 'text-summary' }]
    },
    reporters: process.env.CI === 'true' ? ['dots'] : ['mocha', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadlessNoSandbox'],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: 'ChromeHeadless',
        flags: ['--no-sandbox']
      }
    },
    singleRun: false
  });
};
