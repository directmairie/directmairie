# DirectMairie Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Uses the production configuration by default.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

This project contains the end-to-end tests for the project.
It uses [Playwright](https://playwright.dev) to run the tests on the running application.

### Launch tests during development

Run `pnpm e2e`

### Snapshot tests

You can regenerate the snapshot tests by running

    pnpm e2e --update-snapshots

Or for the CI, run the script

    ./scripts/regen-e2e-snapshots-for-ci.sh

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
