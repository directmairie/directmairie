/// <reference types="@angular/localize" />

import { AppComponent } from './app/app.component';
import { routes } from './app/app.routes';
import { provideRouter, withViewTransitions } from '@angular/router';
import { bootstrapApplication } from '@angular/platform-browser';
import { errorInterceptor } from './app/error-interceptor.service';
import { authenticationInterceptor } from './app/authentication-interceptor.service';
import { provideHttpClient, withInterceptors, withNoXsrfProtection } from '@angular/common/http';
import { isDevMode, LOCALE_ID, provideZoneChangeDetection } from '@angular/core';
import { provideServiceWorker } from '@angular/service-worker';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr);
bootstrapApplication(AppComponent, {
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' },
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideServiceWorker('ngsw-worker.js', { enabled: !isDevMode() }),
    provideRouter(routes, withViewTransitions({ skipInitialTransition: true })),
    provideHttpClient(
      withInterceptors([errorInterceptor, authenticationInterceptor]),
      withNoXsrfProtection()
    )
  ]
  // eslint-disable-next-line no-console
}).catch(err => console.error(err));
