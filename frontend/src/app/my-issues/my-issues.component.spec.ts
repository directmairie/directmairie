import { TestBed } from '@angular/core/testing';

import { MyIssuesComponent } from './my-issues.component';
import { ComponentTester, createMock, TestButton } from 'ngx-speculoos';
import { IssueService } from '../issue.service';
import { createPage } from '../models/page.model.spec';
import { IssueModel, IssueTransitionModel, PictureModel } from '../models/issue.model';
import { Subject } from 'rxjs';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';
import { IssueCategoryModel } from '../models/issue-category.model';
import { LOCALE_ID } from '@angular/core';
import { PageModel } from '../models/page.model';
import { IssueTransitionsComponent } from '../issue-transitions/issue-transitions.component';
import { IssueStatusComponent } from '../issue-status/issue-status.component';

class MyIssuesComponentTester extends ComponentTester<MyIssuesComponent> {
  constructor() {
    super(MyIssuesComponent);
  }

  get title() {
    return this.element('h2');
  }

  get issues() {
    return this.elements('.issue') as Array<TestButton>;
  }

  get description() {
    return this.element('.description');
  }

  get transitionsComponents() {
    return this.components(IssueTransitionsComponent);
  }

  get status() {
    return this.token('.issue-status', IssueStatusComponent)!;
  }

  get creationInstant() {
    return this.element('.creation-instant');
  }

  get mapMarkers() {
    return this.nativeElement.querySelectorAll('.leaflet-marker-icon');
  }

  get pictures() {
    return this.elements('.picture');
  }
}

describe('MyIssuesComponent', () => {
  let tester: MyIssuesComponentTester;
  let issueService: jasmine.SpyObj<IssueService>;
  const issuesSubject = new Subject<PageModel<IssueModel>>();

  const issueInLyon: IssueModel = {
    id: 43,
    status: 'RESOLVED',
    creationInstant: '2019-01-09T15:00:00Z',
    description:
      'A long description of more than 120 characters to check if it properly truncated and is displayed completely when the issue is expanded',
    coordinates: {
      latitude: 45.7573654,
      longitude: 4.8427945
    },
    category: {
      id: 2,
      label: 'lighting',
      descriptionRequired: false
    },
    pictures: [{ id: 12, creationInstant: '2019-01-09T15:00:00Z' }],
    assignedGovernment: null,
    transitions: [
      {
        beforeStatus: 'CREATED',
        afterStatus: 'RESOLVED',
        transitionInstant: '2019-01-10T15:00:00Z',
        message: 'we replaced the light bulb'
      }
    ]
  };
  const issueInParis: IssueModel = {
    id: 42,
    creationInstant: '2019-01-08T15:00:00Z',
    status: 'CREATED',
    description: 'Tower fell down',
    coordinates: {
      latitude: 48.858624,
      longitude: 2.294537
    },
    category: {
      label: 'Other'
    } as IssueCategoryModel,
    pictures: [] as Array<PictureModel>,
    transitions: [] as Array<IssueTransitionModel>
  } as IssueModel;

  beforeEach(async () => {
    issueService = createMock(IssueService);

    TestBed.configureTestingModule({
      providers: [
        { provide: IssueService, useValue: issueService },
        { provide: LOCALE_ID, useValue: 'fr' },
        { provide: NgbConfig, useValue: { animation: false } as NgbConfig }
      ]
    });

    issueService.listMine.and.returnValue(issuesSubject);

    tester = new MyIssuesComponentTester();
    await tester.change();
  });

  it('should not display anything if no issues', async () => {
    issuesSubject.next(createPage([], 0, 0, 0));
    await tester.change();
    expect(tester.title).toBeNull();
  });

  it('should display title and issues', async () => {
    issuesSubject.next(createPage([issueInLyon, issueInParis], 1, 1, 1));
    await tester.change();
    expect(tester.title).toHaveText('Mes dernières remontées');

    expect(tester.issues.length).toBe(2);

    // the first issue should have a short description
    expect(tester.description).toHaveText(
      'A long description of more than 120 characters to check if it properly truncated and is displayed completely when the is...'
    );
    expect(tester.status.status()).toBe('RESOLVED');
    expect(tester.creationInstant).toContainText('il y a ');

    // expand the first issue
    await tester.issues[0].click();

    // the first issue should have a complete description
    expect(tester.description).toHaveText(issueInLyon.description!);
    // and a map
    expect(tester.mapMarkers.length).toBe(1);
    // and transitions
    expect(tester.transitionsComponents.length).toBe(1);
    expect(tester.transitionsComponents[0].issue()).toBe(issueInLyon);
    // and pictures
    expect(tester.pictures.length).toBe(1);
    expect(tester.pictures[0].element('img')!.attr('src')).toBe('/api/issues/43/pictures/12/bytes');
    expect(tester.pictures[0].element('img')!.attr('alt')).toContain('Photo créée il y a');

    // expand the second issue
    await tester.issues[1].click();
    // should collapse the first one
    expect(tester.description).toHaveText(
      'A long description of more than 120 characters to check if it properly truncated and is displayed completely when the is...'
    );

    // click again on the second issue collapses it
    await tester.issues[1].click();
    // so no map
    expect(tester.mapMarkers.length).toBe(0);
  });
});
