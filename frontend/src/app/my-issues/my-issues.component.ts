import { ChangeDetectionStrategy, Component, inject, LOCALE_ID, Signal } from '@angular/core';
import { IssueModel, PictureModel } from '../models/issue.model';
import { IssueService } from '../issue.service';
import { DateTime } from 'luxon';
import { featureGroup, LatLngBounds, MapOptions, marker, Marker } from 'leaflet';
import { defaultMapOptions, defaultMarkerOptions } from '../map';
import { map, Observable, scan, startWith, Subject, switchMap } from 'rxjs';
import { LeafletModule } from '@bluehalo/ngx-leaflet';
import { NgbCollapse } from '@ng-bootstrap/ng-bootstrap';
import { IssueTransitionsComponent } from '../issue-transitions/issue-transitions.component';
import { IssueStatusComponent } from '../issue-status/issue-status.component';
import { toSignal } from '@angular/core/rxjs-interop';

const descriptionLengthThreshold = 120;

interface ViewModel {
  /**
   * The last issues of the current user
   */
  issues: Array<IssueModel> | null;
  /**
   * The currently focused issue.
   * When clicking on an issue, the panel will expand to display:
   * - the full description
   * - the map centered on the issue
   * - the pictures associated with the issue
   */
  expandedIssue: IssueModel | null;
  /**
   * The expanded version of an issue displays a map,
   * containing only one marker: the one representing the expanded issue.
   */
  mapMarkers: Array<Marker>;
  /**
   * The map options are always the same
   */
  mapOptions: MapOptions;
  /**
   * The bounds of the map are computed based on the markers.
   */
  mapFitBounds: LatLngBounds | null;
}

@Component({
  selector: 'dm-my-issues',
  templateUrl: './my-issues.component.html',
  styleUrl: './my-issues.component.scss',
  imports: [NgbCollapse, LeafletModule, IssueTransitionsComponent, IssueStatusComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MyIssuesComponent {
  readonly vm: Signal<ViewModel | undefined>;

  /**
   * Emits when an issue is toggled
   */
  readonly toggleSubject = new Subject<IssueModel>();

  private readonly locale = inject(LOCALE_ID);

  constructor() {
    const issueService = inject(IssueService);
    const vm$: Observable<ViewModel | undefined> = issueService.listMine().pipe(
      // we only display the issues if there are some
      map(page =>
        page && page.totalElements
          ? {
              issues: page.content,
              expandedIssue: null,
              mapFitBounds: null,
              mapMarkers: [] as Array<Marker>,
              mapOptions: defaultMapOptions()
            }
          : undefined
      )
    );

    // we change the ViewModel when an issue is toggled to expand or collapse it
    this.vm = toSignal(
      vm$.pipe(
        switchMap(vm =>
          this.toggleSubject.pipe(
            scan((vm, action) => this.toggleIssue(vm!, action), vm),
            startWith(vm)
          )
        )
      )
    );
  }

  /**
   * Returns the description,
   * or the truncated description if too long and the issue is not expanded,
   * or a default placeholder if there is none.
   */
  getTruncatedDescription(issue: IssueModel) {
    if (issue.description) {
      return issue.description.length > descriptionLengthThreshold
        ? issue.description.substring(0, descriptionLengthThreshold) + '...'
        : issue.description;
    }
    return 'Pas de description';
  }

  /**
   * Builds the url for the picture of an issue
   */
  url(issue: IssueModel, picture: PictureModel) {
    return `/api/issues/${issue.id}/pictures/${picture.id}/bytes`;
  }

  /**
   * Returns a relative time for the creation instant of an issue,
   * for example 'il y a 2 heures'.
   */
  fromNow(creationInstant: string) {
    return DateTime.fromISO(creationInstant).toRelative({ locale: this.locale });
  }

  /**
   * Toggle an issue.
   * If not expanded, then it expands it to display the details (so we need to compute the map details).
   * If already expanded, it collapses it (by setting `expandedIssue` to null) .
   */
  toggleIssue(vm: ViewModel, issue: IssueModel): ViewModel {
    if (vm.expandedIssue !== issue) {
      const mapMarkers = [
        marker(
          [issue.coordinates.latitude, issue.coordinates.longitude],
          defaultMarkerOptions(issue.description ?? '')
        )
      ];
      const mapFitBounds = featureGroup(mapMarkers).getBounds().pad(0.05);
      return {
        ...vm,
        expandedIssue: issue,
        mapMarkers,
        mapFitBounds
      };
    }
    return {
      ...vm,
      expandedIssue: null
    };
  }
}
