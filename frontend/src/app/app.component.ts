import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { VersionEvent } from '@angular/service-worker';

import { SwUpdateService } from './sw-update.service';
import { DefaultErrorMessagesComponent } from './default-error-messages/default-error-messages.component';
import { FooterComponent } from './footer/footer.component';
import { RouterOutlet } from '@angular/router';
import { SuccessComponent } from './success/success.component';
import { ErrorComponent } from './error/error.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CurrentUserService } from './current-user.service';
import { toSignal } from '@angular/core/rxjs-interop';

/**
 * The root application component, displaying the navbar, the success and error components, and the routed component
 */
@Component({
  selector: 'dm-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
  imports: [
    NavbarComponent,
    ErrorComponent,
    SuccessComponent,
    RouterOutlet,
    FooterComponent,
    DefaultErrorMessagesComponent
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  readonly updatesAvailable: Signal<VersionEvent | undefined>;

  constructor() {
    const swUpdateService = inject(SwUpdateService);
    const currentUserService = inject(CurrentUserService);
    // subscribe to new versions available
    this.updatesAvailable = toSignal(swUpdateService.updatesAvailable());
    currentUserService.init();
  }
}
