import { TestBed } from '@angular/core/testing';

import { NavbarComponent } from './navbar.component';
import {
  ComponentTester,
  createMock,
  provideAutomaticChangeDetection,
  TestHtmlElement
} from 'ngx-speculoos';
import { CurrentUserService } from '../current-user.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { CurrentUserModel } from '../models/current-user.model';
import { provideRouter, Router } from '@angular/router';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';
import { GovernmentModel } from '../admin/models/government.model';
import { BrandingService } from '../branding.service';

class NavbarComponentTester extends ComponentTester<NavbarComponent> {
  constructor() {
    super(NavbarComponent);
  }

  get navbarContent() {
    return this.element('#navbarContent');
  }

  get userName() {
    return this.element('#navbarUserName');
  }

  get issuesDropdown() {
    return this.element('#navbarIssuesDropdown');
  }

  get issues() {
    return this.element('#navbarIssues');
  }

  get issuesExport() {
    return this.element('#navbarIssuesExport');
  }

  get notification() {
    return this.element('#navbarNotification');
  }

  get governments() {
    return this.element('#navbarGovernments');
  }

  get poolingOrganizations() {
    return this.element('#navbarPoolingOrganizations');
  }

  get register() {
    return this.element('#navbarRegister');
  }

  get login() {
    return this.element('#navbarLogin');
  }

  get logout() {
    return this.element('#navbarLogout') as TestHtmlElement<HTMLAnchorElement>;
  }

  get toggler() {
    return this.button('.navbar-toggler')!;
  }

  get governmentLogo() {
    return this.element('#government-logo')!;
  }
}

describe('NavbarComponent', () => {
  let tester: NavbarComponentTester;
  let currentUser$: BehaviorSubject<CurrentUserModel | null>;

  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let brandingService: jasmine.SpyObj<BrandingService>;
  let governmentSubject: Subject<GovernmentModel>;
  let router: Router;

  beforeEach(async () => {
    currentUserService = createMock(CurrentUserService);
    brandingService = createMock(BrandingService);

    TestBed.configureTestingModule({
      providers: [
        provideAutomaticChangeDetection(),
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: BrandingService, useValue: brandingService },
        { provide: NgbConfig, useValue: { animation: false } as NgbConfig }
      ]
    });

    currentUser$ = new BehaviorSubject<CurrentUserModel | null>(null);
    currentUserService.getUserChanges.and.returnValue(currentUser$);
    router = TestBed.inject(Router);

    governmentSubject = new Subject<GovernmentModel>();
    brandingService.getBrandingGoverment.and.returnValue(governmentSubject);

    tester = new NavbarComponentTester();
    await tester.change();
  });

  it('should display register and login links when no user', () => {
    expect(tester.login).not.toBeNull();
    expect(tester.register).not.toBeNull();
    expect(tester.issuesDropdown).toBeNull();
    expect(tester.userName).toBeNull();
    expect(tester.logout).toBeNull();
  });

  it('should display logout and user name links when user', async () => {
    currentUser$.next({ email: 'john@mail.com' } as CurrentUserModel);
    await tester.change();

    expect(tester.login).toBeNull();
    expect(tester.register).toBeNull();
    expect(tester.issuesDropdown).toBeNull();
    expect(tester.governments).toBeNull();
    expect(tester.poolingOrganizations).toBeNull();
    expect(tester.notification).toBeNull();
    expect(tester.userName).toContainText('john');
    expect(tester.logout).not.toBeNull();

    currentUser$.next({ email: 'john@mail.com', firstName: 'John' } as CurrentUserModel);
    await tester.change();
    expect(tester.userName).toContainText('John');

    currentUser$.next({
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe'
    } as CurrentUserModel);
    await tester.change();
    expect(tester.userName).toContainText('John\u00a0Doe');
  });

  it('should display issues dropdown and its items link only, and notifications when government administrator', async () => {
    currentUser$.next({ email: 'john@mail.com', governmentAdmin: true } as CurrentUserModel);
    await tester.change();

    expect(tester.issuesDropdown).not.toBeNull();
    expect(tester.issues).not.toBeNull();
    expect(tester.issuesExport).not.toBeNull();
    expect(tester.notification).not.toBeNull();

    expect(tester.governments).toBeNull();
    expect(tester.poolingOrganizations).toBeNull();
  });

  it('should display issues and governments and notifications links when pooling organization administrator', async () => {
    currentUser$.next({
      email: 'john@mail.com',
      poolingOrganizationAdmin: true
    } as CurrentUserModel);
    await tester.change();

    expect(tester.notification).not.toBeNull();
    expect(tester.issuesDropdown).not.toBeNull();
    expect(tester.governments).not.toBeNull();
    expect(tester.poolingOrganizations).toBeNull();
  });

  it('should display issues link, governments link, pooling organizations link and notifications link when super administrator', async () => {
    currentUser$.next({ email: 'john@mail.com', superAdmin: true } as CurrentUserModel);
    await tester.change();

    expect(tester.notification).not.toBeNull();
    expect(tester.issuesDropdown).not.toBeNull();
    expect(tester.governments).not.toBeNull();
    expect(tester.poolingOrganizations).not.toBeNull();
  });

  it('should toggle the content visibility', async () => {
    expect(tester.navbarContent).not.toHaveClass('show');

    await tester.toggler.click();

    expect(tester.navbarContent).toHaveClass('show');

    await tester.toggler.click();

    expect(tester.navbarContent).not.toHaveClass('show');
  });

  it('should log out and navigate to home page', async () => {
    spyOn(router, 'navigate');
    currentUser$.next({ email: 'john@mail.com', superAdmin: true } as CurrentUserModel);
    await tester.change();

    await tester.logout.click();

    expect(currentUserService.remove).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should display government logo depending on branding government', async () => {
    expect(tester.governmentLogo).toBeNull();

    governmentSubject.next({ id: 42, logo: { id: 245 } } as GovernmentModel);
    await tester.change();
    expect(tester.governmentLogo.attr('src')).toBe('/api/governments/42/logo/245/bytes');

    governmentSubject.next({ id: 42, logo: null } as GovernmentModel);
    await tester.change();
    expect(tester.governmentLogo).toBeNull();
  });
});
