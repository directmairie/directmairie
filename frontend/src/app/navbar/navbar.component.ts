import { ChangeDetectionStrategy, Component, inject, Signal, signal } from '@angular/core';
import * as icons from '../icons';
import { CurrentUserModel } from '../models/current-user.model';
import { CurrentUserService } from '../current-user.service';
import { Router, RouterLink } from '@angular/router';
import { combineLatest, map, startWith } from 'rxjs';
import { BrandingService } from '../branding.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {
  NgbCollapse,
  NgbDropdown,
  NgbDropdownToggle,
  NgbDropdownMenu
} from '@ng-bootstrap/ng-bootstrap';
import { toSignal } from '@angular/core/rxjs-interop';

interface ViewModel {
  currentUser: CurrentUserModel | null;
  userName: string | null;
  atLeastGovernmentAdmin: boolean;
  atLeastPoolingOrganizationAdmin: boolean;
  superAdmin: boolean;
  brandingLogoUrl: string | null;
}

/**
 * The main responsive navigation bar, displaying menus based on the roles of the current user (if any)
 */
@Component({
  selector: 'dm-navbar',
  templateUrl: './navbar.component.html',
  styleUrl: './navbar.component.scss',
  imports: [
    RouterLink,
    FontAwesomeModule,
    NgbCollapse,
    NgbDropdown,
    NgbDropdownToggle,
    NgbDropdownMenu
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent {
  private readonly currentUserService = inject(CurrentUserService);
  private readonly router = inject(Router);

  readonly expanded = signal(false);
  readonly icons = icons;
  readonly vm: Signal<ViewModel | undefined>;

  constructor() {
    const brandingService = inject(BrandingService);
    this.vm = toSignal(
      combineLatest([
        this.currentUserService.getUserChanges(),
        brandingService.getBrandingGoverment().pipe(startWith(null))
      ]).pipe(
        map(([currentUser, government]) => ({
          currentUser,
          userName: this.getUsername(currentUser),
          governmentAdmin: currentUser?.governmentAdmin ?? false,
          atLeastGovernmentAdmin: this.atLeastGovernmentAdmin(currentUser),
          atLeastPoolingOrganizationAdmin: this.atLeastPoolingOrganizationAdmin(currentUser),
          superAdmin: this.superAdmin(currentUser),
          brandingLogoUrl:
            government && government.logo
              ? `/api/governments/${government.id}/logo/${government.logo.id}/bytes`
              : null
        }))
      )
    );
  }

  logout() {
    this.currentUserService.remove();
    this.router.navigate(['/']);
  }

  toggle() {
    this.expanded.update(expanded => !expanded);
  }

  collapse() {
    this.expanded.set(false);
  }

  private atLeastGovernmentAdmin(user: CurrentUserModel | null): boolean {
    return !!(user && (user.governmentAdmin || this.atLeastPoolingOrganizationAdmin(user)));
  }

  private atLeastPoolingOrganizationAdmin(user: CurrentUserModel | null): boolean {
    return !!(user && (user.poolingOrganizationAdmin || this.superAdmin(user)));
  }

  private superAdmin(user: CurrentUserModel | null) {
    return user?.superAdmin ?? false;
  }

  private getUsername(user: CurrentUserModel | null) {
    if (user) {
      if (user.firstName && user.lastName) {
        return `${user.firstName}\u00a0${user.lastName}`;
      } else if (user.firstName) {
        return `${user.firstName}`;
      } else {
        return user.email.substring(0, user.email.indexOf('@'));
      }
    } else {
      return null;
    }
  }
}
