import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

/**
 * Utility functions used in tests
 */
function functionalErrorResponse(errorCode: string): HttpErrorResponse {
  return new HttpErrorResponse({
    status: 400,
    error: {
      functionalError: errorCode
    }
  });
}

export function functionalErrorObservable<T>(errorCode: string): Observable<T> {
  return throwError(() => functionalErrorResponse(errorCode));
}
