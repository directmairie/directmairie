import { inject } from '@angular/core';
import { HttpInterceptorFn } from '@angular/common/http';
import { CurrentUserStorageService } from './current-user-storage.service';

/**
 * An HTTP interceptor used to send the authentication token as an Authorization header with all the requests to the
 * DirectMairie backend API.
 */
export const authenticationInterceptor: HttpInterceptorFn = (req, next) => {
  const currentUserStorage = inject(CurrentUserStorageService);
  let request = req;
  const authenticatedUser = currentUserStorage.loadAuthenticatedUser();
  if (!req.url.startsWith('http') && authenticatedUser) {
    request = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authenticatedUser.token}`
      }
    });
  }
  return next(request);
};
