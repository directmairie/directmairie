import { Directive, inject, input, contentChildren } from '@angular/core';
import { RouterLinkActive } from '@angular/router';

let counter = 0;

/**
 * Directives allowing to display navs (tabs, pills) that navigate using the router, using routerLink and
 * routerLinkActive.
 * These directive main goal is to add the accessibility attributes (role, aria) automatically, as well as the
 * standard bootstrap classes (nav and nav-link).
 *
 * Usage:
 *
 * ```
 * <ul class="nav-tabs" dmRouterNav #nav="dmRouterNav">
 *   <li class="nav-item">
 *     <a routerLink="/foo" routerLinkActive="active" dmRouterNavLink>Foo</a>
 *   </li>
 *   <li class="nav-item">
 *     <a routerLink="/bar" routerLinkActive="active" dmRouterNavLink="bar">Bar</a>
 *   </li>
 * </ul>
 *
 * <div [dmRouterNavPanel]="nav">
 *   <router-outlet></router-outlet>
 * </div>
 * ```
 */
@Directive({
  selector: '[dmRouterNavLink]',
  host: {
    '[attr.id]': 'id',
    '[class.nav-link]': 'true',
    '[attr.aria-current]': 'ariaCurrent'
  }
})
export class RouterNavLinkDirective {
  private readonly routerLinkActive = inject(RouterLinkActive);
  readonly nav = inject(RouterNavDirective);

  /**
   * The ID of the link. If not specified, it's auto-generated
   */
  readonly itemId = input<string | null>(null, { alias: 'dmRouterNavLink' });

  private readonly domId = 'router-nav-link-' + counter++;

  get selected() {
    return this.routerLinkActive.isActive;
  }

  get id() {
    return this.itemId() || this.domId;
  }

  get ariaCurrent() {
    return this.selected ? 'page' : null;
  }
}

@Directive({
  selector: '[dmRouterNavPanel]',
  host: {
    '[attr.role]': `'tabpanel'`,
    '[attr.aria-labelledby]': 'nav().selectedId'
  }
})
export class RouterNavPanelDirective {
  /**
   * The reference to the router nav directive.
   */
  readonly nav = input.required<RouterNavDirective>({ alias: 'dmRouterNavPanel' });
}

@Directive({
  selector: '[dmRouterNav]',
  exportAs: 'dmRouterNav',
  host: {
    '[class.nav]': 'true'
  }
})
export class RouterNavDirective {
  readonly navItems = contentChildren(RouterNavLinkDirective, { descendants: true });

  // cannot be a computed because it depends on a getter of the RouterLinkActive directive which is not reactive
  get selectedId() {
    return this.navItems().find(item => item.nav === this && item.selected)?.id;
  }
}
