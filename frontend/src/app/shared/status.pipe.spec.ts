import { StatusPipe } from './status.pipe';

describe('StatusPipe', () => {
  it('translate to french', () => {
    const pipe = new StatusPipe();
    expect(pipe.transform('DRAFT')).toBe('Brouillon');
    expect(pipe.transform('CREATED')).toBe('Créée');
    expect(pipe.transform('REJECTED')).toBe('Rejetée');
    expect(pipe.transform('RESOLVED')).toBe('Résolue');
  });
});
