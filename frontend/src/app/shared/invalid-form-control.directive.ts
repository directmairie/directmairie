/* eslint-disable @angular-eslint/directive-selector */
import { Directive, inject } from '@angular/core';
import { NgControl } from '@angular/forms';
import { ValdemortConfig } from 'ngx-valdemort';

/**
 * Directive that dynamically adds/removes the CSS class is-invalid, used by Bootstrap, on
 * elements which already have the form-control Bootstrap CSS class, based on the
 * validity of the NgControl directive also present on this element and on the rules
 * configured by the ValdemortConfig service to decide when the validation errors should be displayed.
 */
@Directive({
  selector: '.form-control',
  host: {
    '[class.is-invalid]': 'isInvalid'
  }
})
export class InvalidFormControlDirective {
  /**
   * the NgControl directive (if any), which can in fact be the NgModel directive, or the formControlName
   * directive, or any other directive that extends NgControl
   */
  private ngControl = inject(NgControl, { optional: true });

  /**
   * the ValdemortConfig service, used to know when the is-invalid class must be added
   */
  private config = inject(ValdemortConfig);

  get isInvalid() {
    return (
      this.ngControl &&
      this.ngControl.invalid &&
      this.config.shouldDisplayErrors(
        this.ngControl.control!,
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        (this.ngControl as any).formDirective
      )
    );
  }
}
