import { ChangeDetectionStrategy, Component } from '@angular/core';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

/**
 * Component allowing to mark a form field as required by displaying an asterisk in front of its label (typically)
 * and by adding a screen-reader only message indicating that the field is required.
 * All required fields are supposed to be marked this way.
 *
 * Usages:
 * ```
 *   <label dmRequired>Name</label>
 *   <dm-required>Sélectionner la catégorie de problème</dm-required>
 * ```
 */
@Component({
  selector: 'dm-required, [dmRequired]',
  templateUrl: './required.component.html',
  styleUrl: './required.component.scss',
  imports: [FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequiredComponent {
  readonly icons = icons;
}
