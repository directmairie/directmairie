import { TestBed } from '@angular/core/testing';

import { ComponentTester } from 'ngx-speculoos';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { RequiredComponent } from './required.component';

@Component({
  selector: 'dm-test',
  template: `
    @if (mode() === 'element') {
      <dm-required>Hello 1</dm-required>
    }
    @if (mode() === 'attribute') {
      <!-- eslint-disable-next-line @angular-eslint/template/label-has-associated-control -->
      <label dmRequired>Hello 2</label>
    }
  `,
  imports: [RequiredComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly mode = signal<'element' | 'attribute'>('element');
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get icon() {
    return this.element('fa-icon');
  }

  get message() {
    return this.element('.visually-hidden');
  }
}

describe('RequiredComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({});

    tester = new TestComponentTester();
    await tester.stable();
  });

  it('should display an icon and a message for screen readers in element mode', () => {
    expect(tester.icon).not.toBeNull();
    expect(tester.message).toContainText('Champ obligatoire');
    expect(tester.testElement).toHaveText('Hello 1 Champ obligatoire');
  });

  it('should display an icon and a message for screen readers in attribute mode', async () => {
    tester.componentInstance.mode.set('attribute');
    await tester.stable();

    expect(tester.icon).not.toBeNull();
    expect(tester.message).toContainText('Champ obligatoire');
    expect(tester.testElement).toHaveText('Hello 2 Champ obligatoire');
  });
});
