import { Pipe, PipeTransform } from '@angular/core';
import { IssueStatus } from '../models/issue.model';

export function translateStatus(value: IssueStatus) {
  if (!value) {
    return '';
  }
  switch (value) {
    case 'DRAFT':
      return 'Brouillon';
    case 'CREATED':
      return 'Créée';
    case 'REJECTED':
      return 'Rejetée';
    case 'RESOLVED':
      return 'Résolue';
  }
}

/**
 * Pipe transforming an IssueStatus into a French label
 */
@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {
  transform(value: IssueStatus): string {
    return translateStatus(value);
  }
}
