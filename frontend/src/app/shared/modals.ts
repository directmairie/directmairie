import { EMPTY, map, Observable, of, race, switchMap, take } from 'rxjs';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

export function fromModal<T>(modalRef: NgbModalRef): Observable<T> {
  const closed$ = modalRef.closed.pipe(map(result => ({ type: 'closed', result })));
  const dismissed$ = modalRef.dismissed.pipe(map(() => ({ type: 'dismissed', result: undefined })));

  return race(closed$, dismissed$).pipe(
    take(1),
    switchMap(({ type, result }) => (type === 'closed' ? of(result) : EMPTY))
  );
}
