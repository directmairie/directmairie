import { TestBed } from '@angular/core/testing';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ComponentTester } from 'ngx-speculoos';
import { provideRouter, Router, RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import {
  RouterNavDirective,
  RouterNavLinkDirective,
  RouterNavPanelDirective
} from './router-nav.directive';

@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
class PlaceholderComponent {}

@Component({
  template: `
    <ul class="nav-tabs" dmRouterNav #nav="dmRouterNav">
      @if (true) {
        <li class="nav-item">
          <a routerLink="/foo" routerLinkActive="active" dmRouterNavLink>Foo</a>
        </li>
      }
      <li class="nav-item">
        <a routerLink="/bar" routerLinkActive="active" dmRouterNavLink="bar">Bar</a>
      </li>
    </ul>

    <div [dmRouterNavPanel]="nav">
      <router-outlet />
    </div>
  `,
  imports: [
    RouterLink,
    RouterLinkActive,
    RouterOutlet,
    RouterNavDirective,
    RouterNavPanelDirective,
    RouterNavLinkDirective
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get tabList() {
    return this.element('ul');
  }

  get tabLinks() {
    return this.elements('a');
  }

  get tabPanel() {
    return this.element('div')!;
  }
}

describe('Router nav directives', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [
        provideRouter([
          { path: 'foo', component: PlaceholderComponent },
          { path: 'bar', component: PlaceholderComponent }
        ])
      ]
    });

    tester = new TestComponentTester();
    TestBed.inject(Router).initialNavigation();

    await tester.stable();
  });

  it('should add class and accessibility attributes', async () => {
    TestBed.inject(Router).navigate(['/foo']);
    await tester.stable();

    expect(tester.tabList).toHaveClass('nav');

    expect(tester.tabLinks[0]).toHaveClass('nav-link');
    expect(tester.tabLinks[0].nativeElement.id).toBeTruthy();
    expect(tester.tabLinks[0].attr('aria-current')).toBe('page');

    expect(tester.tabLinks[1]).toHaveClass('nav-link');
    expect(tester.tabLinks[1].nativeElement.id).toBe('bar');
    expect(tester.tabLinks[1].attr('aria-current')).toBeNull();

    expect(tester.tabPanel.attr('role')).toBe('tabpanel');
    expect(tester.tabPanel.attr('aria-labelledby')).toBe(tester.tabLinks[0].nativeElement.id);

    TestBed.inject(Router).navigate(['/bar']);
    await tester.stable();

    expect(tester.tabLinks[0].attr('aria-current')).toBeNull();
    expect(tester.tabLinks[1].attr('aria-current')).toBe('page');
    expect(tester.tabPanel.attr('aria-labelledby')).toBe('bar');
  });
});
