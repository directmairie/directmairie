import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RequiredComponent } from '../required/required.component';

/**
 * Component displaying a message indicating that form fields marked with an asterisk are required.
 * All forms containing at least one required field are supposed to display this message at the top of the form.
 */
@Component({
  selector: 'dm-required-fields-message',
  templateUrl: './required-fields-message.component.html',
  styleUrl: './required-fields-message.component.scss',
  imports: [RequiredComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequiredFieldsMessageComponent {}
