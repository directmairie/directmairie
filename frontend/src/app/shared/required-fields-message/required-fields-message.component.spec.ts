import { TestBed } from '@angular/core/testing';

import { RequiredFieldsMessageComponent } from './required-fields-message.component';
import { ComponentTester } from 'ngx-speculoos';
import { RequiredComponent } from '../required/required.component';

describe('RequiredFieldsMessageComponent', () => {
  let tester: ComponentTester<RequiredFieldsMessageComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({});

    tester = new ComponentTester(RequiredFieldsMessageComponent);
  });

  it('should display a message', () => {
    expect(tester.testElement).toContainText(`Les champs marqués d'un`);
    expect(tester.testElement).toContainText('sont obligatoires');
    expect(tester.element(RequiredComponent)).not.toBeNull();
  });
});
