import { CurrentUserStorageService } from './current-user-storage.service';
import { AuthenticationResultModel } from './models/authentication-result.model';

describe('CurrentUserStorageService', () => {
  it('should store the authenticated user', () => {
    const service = new CurrentUserStorageService();
    spyOn(Storage.prototype, 'setItem');

    const user: AuthenticationResultModel = { token: 'abc' };
    service.storeAuthenticatedUser(user);

    expect(Storage.prototype.setItem).toHaveBeenCalledWith('dm-token', JSON.stringify(user));
  });

  it('should retrieve a user if one is stored', () => {
    const service = new CurrentUserStorageService();

    const user: AuthenticationResultModel = { token: 'abc' };
    spyOn(Storage.prototype, 'getItem').and.returnValue(JSON.stringify(user));

    expect(service.loadAuthenticatedUser()).toEqual({ token: 'abc' });
  });

  it('should remove the authenticated user is null is stored', () => {
    const service = new CurrentUserStorageService();
    spyOn(Storage.prototype, 'removeItem');

    service.storeAuthenticatedUser(null);

    expect(Storage.prototype.removeItem).toHaveBeenCalledWith('dm-token');
  });
});
