import { TestBed } from '@angular/core/testing';
import { SwUpdate, VersionEvent } from '@angular/service-worker';

import { SwUpdateService } from './sw-update.service';
import { Observable, of } from 'rxjs';

describe('SwUpdateService', () => {
  const swUpdate = jasmine.createSpyObj<SwUpdate>('SwUpdate', [], ['versionUpdates']);
  const versionUpdates = Object.getOwnPropertyDescriptor(swUpdate, 'versionUpdates')!
    .get as jasmine.Spy<() => Observable<VersionEvent>>;
  versionUpdates.and.returnValue(of({} as VersionEvent));

  let service: SwUpdateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: SwUpdate, useValue: swUpdate }]
    });
    service = TestBed.inject(SwUpdateService);
  });

  it('should emit available updates', () => {
    let updateReceived: VersionEvent;
    service.updatesAvailable().subscribe(update => (updateReceived = update));

    expect(updateReceived!).not.toBeNull();
  });
});
