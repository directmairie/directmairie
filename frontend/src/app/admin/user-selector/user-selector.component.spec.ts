import { TestBed } from '@angular/core/testing';

import { UserSelectorComponent } from './user-selector.component';
import { UserService } from '../user.service';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { UserModel } from '../../registration/models/registration.model';
import { Subject } from 'rxjs';
import { PageModel } from '../../models/page.model';
import { createPage } from '../../models/page.model.spec';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';

@Component({
  selector: 'dm-test',
  template: '<dm-user-selector [(users)]="admins" />',
  imports: [UserSelectorComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly admins = signal<Array<UserModel>>([]);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get adminEmail() {
    return this.input('#adminEmail')!;
  }

  get results(): NodeListOf<HTMLButtonElement> {
    // Based on the typeahead test itself
    // see https://github.com/ng-bootstrap/ng-bootstrap/blob/master/src/typeahead/typeahead.spec.ts
    // The dropdown is appended to the body, not to this element, so we can't unfortunately return an array of
    // TestButton, but only DOM elements
    return document.querySelectorAll('ngb-typeahead-window.dropdown-menu button.dropdown-item');
  }

  get badges() {
    return this.elements('.user');
  }
}

describe('UserSelectorComponent', () => {
  let userService: jasmine.SpyObj<UserService>;
  let tester: TestComponentTester;

  const user1 = { id: 1, email: 'user1@rhone.fr' } as UserModel;
  const user2 = { id: 2, email: 'user2@rhone.fr' } as UserModel;

  beforeEach(async () => {
    userService = createMock(UserService);
    TestBed.configureTestingModule({
      providers: [{ provide: UserService, useValue: userService }]
    });

    tester = new TestComponentTester();
    await tester.stable();

    jasmine.clock().install();
    jasmine.clock().mockDate();
  });

  afterEach(() => jasmine.clock().uninstall());

  it('should display the selected users as bagdes', async () => {
    // given admins selected
    const admins: Array<UserModel> = [user1, user2];
    const component = tester.componentInstance;
    component.admins.set(admins);

    // when displaying the component
    await tester.stable();

    // then it should have several removable badges
    expect(tester.badges.length).toBe(2);
    expect(tester.badges[0]).toContainText(user1.email);
    expect(tester.badges[0].button('button')).not.toBeNull();
    expect(tester.badges[1]).toContainText(user2.email);
    expect(tester.badges[1].button('button')).not.toBeNull();
  });

  it('should add/remove users and update badges', async () => {
    const users$ = new Subject<PageModel<UserModel>>();
    userService.searchUserByEmail.and.returnValue(users$);

    const component = tester.componentInstance;
    expect(tester.adminEmail).toHaveValue('');
    expect(tester.badges.length).toBe(0);

    // when a value is entered
    await tester.adminEmail.fillWith('user1');
    jasmine.clock().tick(400);
    expect(userService.searchUserByEmail).toHaveBeenCalledWith('user1');
    users$.next(createPage([user1], 1, 1, 1));

    // results should appear
    expect(tester.results.length).toBe(1);
    expect(tester.results[0].textContent).toBe('user1@rhone.fr');

    // when the result is selected
    await tester.results[0].click();
    await tester.stable();

    // the user is added to the admins
    expect(component.admins()).toEqual([user1]);

    // the input is emptied
    expect(tester.adminEmail).toHaveValue('');

    // the focus is given back to the input
    expect(document.activeElement).toBe(tester.adminEmail.nativeElement);

    // and a badge should appear
    expect(tester.badges.length).toBe(1);
    expect(tester.badges[0]).toContainText('user1@rhone.fr');

    // when another value is entered
    await tester.adminEmail.fillWith('user');
    jasmine.clock().tick(400);
    expect(userService.searchUserByEmail).toHaveBeenCalledWith('user');
    users$.next(createPage([user1, user2], 1, 1, 1));

    // results should appear and should not contain already select user1
    expect(tester.results.length).toBe(1);
    expect(tester.results[0].textContent).toBe('user2@rhone.fr');

    // when the result is selected
    await tester.results[0].click();
    await tester.stable();

    // another admin is added
    expect(tester.adminEmail).toHaveValue('');
    expect(component.admins()).toEqual([user1, user2]);

    // the focus is given back to the input
    expect(document.activeElement).toBe(tester.adminEmail.nativeElement);

    // and another badge should appear
    expect(tester.badges.length).toBe(2);
    expect(tester.badges[0]).toContainText('user1@rhone.fr');
    expect(tester.badges[1]).toContainText('user2@rhone.fr');

    // when a badge is removed
    await tester.badges[0].button('button')!.click();

    // the admin is removed
    expect(tester.adminEmail).toHaveValue('');
    expect(component.admins()).toEqual([user2]);

    // and the badge should disappear
    expect(tester.badges.length).toBe(1);
    expect(tester.badges[0]).toContainText('user2@rhone.fr');
  });
});
