import {
  Component,
  ElementRef,
  inject,
  model,
  ChangeDetectionStrategy,
  viewChild
} from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { debounceTime, distinctUntilChanged, filter, map, Observable, switchMap } from 'rxjs';
import {
  NgbTypeaheadSelectItemEvent,
  NgbHighlight,
  NgbTypeahead
} from '@ng-bootstrap/ng-bootstrap';

import { UserService } from '../user.service';
import { UserModel } from '../../registration/models/registration.model';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';

/**
 * Component allowing to select multiple administrators (to add to a government or a pooling organisation)
 * by entering part of their email into a typeahead
 */
@Component({
  selector: 'dm-user-selector',
  templateUrl: './user-selector.component.html',
  styleUrl: './user-selector.component.scss',
  imports: [
    NgbHighlight,
    ReactiveFormsModule,
    NgbTypeahead,
    InvalidFormControlDirective,
    FontAwesomeModule
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserSelectorComponent {
  private readonly userService = inject(UserService);

  readonly icons = icons;

  readonly users = model.required<Array<UserModel>>();

  readonly typeahead = viewChild.required<ElementRef<HTMLInputElement>>('typeahead');

  readonly control = new FormControl<string | null>(null);

  readonly searchUserByEmail$ = (text$: Observable<string>): Observable<Array<UserModel>> =>
    text$.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      filter(query => query.length > 2),
      switchMap(query => this.userService.searchUserByEmail(query)),
      map(paginatedUsers =>
        paginatedUsers.content.filter(foundUser => !this.users().some(u => foundUser.id === u.id))
      )
    );

  removeUser(user: UserModel) {
    this.users.update(users => users.filter(u => u !== user));
    this.typeahead().nativeElement.focus();
  }

  selectUser(event: NgbTypeaheadSelectItemEvent) {
    event.preventDefault();
    const selected: UserModel = event.item;
    this.users.set([...this.users(), selected]);
    this.control.setValue('');
    this.typeahead().nativeElement.focus();
  }
}
