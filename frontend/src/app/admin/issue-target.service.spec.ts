import { TestBed } from '@angular/core/testing';
import { createPage } from '../models/page.model.spec';
import { IssueTarget, IssueTargetService } from './issue-target.service';
import { PoolingOrganizationService } from './pooling-organization.service';
import { GovernmentService } from './government.service';
import { CurrentUserService } from '../current-user.service';
import { of } from 'rxjs';
import { CurrentUserModel } from '../models/current-user.model';
import { GovernmentDetailedModel, GovernmentModel } from './models/government.model';
import {
  PoolingOrganizationDetailedModel,
  PoolingOrganizationModel
} from './models/pooling-organization.model';
import { createMock } from 'ngx-speculoos';

describe('IssueTargetService', () => {
  let service: IssueTargetService;

  let poolingOrganizationService: jasmine.SpyObj<PoolingOrganizationService>;
  let governmentService: jasmine.SpyObj<GovernmentService>;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;

  const lorette = {
    id: 1,
    name: 'Lorette'
  } as GovernmentModel;
  const loire = {
    id: 2,
    name: 'Loire'
  } as PoolingOrganizationModel;

  beforeEach(() => {
    poolingOrganizationService = createMock(PoolingOrganizationService);
    governmentService = createMock(GovernmentService);
    currentUserService = createMock(CurrentUserService);

    TestBed.configureTestingModule({
      providers: [
        { provide: PoolingOrganizationService, useValue: poolingOrganizationService },
        { provide: GovernmentService, useValue: governmentService },
        { provide: CurrentUserService, useValue: currentUserService }
      ]
    });

    service = TestBed.inject(IssueTargetService);
  });

  it('should search targets when user is government admin only', () => {
    currentUserService.getUserChanges.and.returnValue(
      of({ governmentAdmin: true } as CurrentUserModel)
    );

    const query = 'lo';
    governmentService.search.and.returnValue(of(createPage([lorette], 10, 1, 0)));

    let actualTargets: Array<IssueTarget> | null = null;
    service.searchTargets(query).subscribe(response => (actualTargets = response));

    expect(actualTargets!).toEqual([{ id: lorette.id, name: lorette.name, type: 'government' }]);
  });

  it('should search targets when user is pooling organization admin only', () => {
    currentUserService.getUserChanges.and.returnValue(
      of({ poolingOrganizationAdmin: true } as CurrentUserModel)
    );

    const query = 'lo';
    poolingOrganizationService.search.and.returnValue(of([loire]));
    governmentService.search.and.returnValue(of(createPage([lorette], 10, 1, 0)));

    let actualTargets: Array<IssueTarget> | null = null;
    service.searchTargets(query).subscribe(response => (actualTargets = response));

    expect(actualTargets!).toEqual([
      { id: loire.id, name: loire.name, type: 'pooling-organization' },
      { id: lorette.id, name: lorette.name, type: 'government' }
    ]);
  });

  it('should search targets when user is pooling organization admin and government admin', () => {
    currentUserService.getUserChanges.and.returnValue(
      of({ poolingOrganizationAdmin: true, governmentAdmin: true } as CurrentUserModel)
    );

    const query = 'lo';
    poolingOrganizationService.search.and.returnValue(of([loire]));
    governmentService.search.and.returnValue(of(createPage([lorette], 10, 1, 0)));

    let actualTargets: Array<IssueTarget> | null = null;
    service.searchTargets(query).subscribe(response => (actualTargets = response));

    expect(actualTargets!).toEqual([
      { id: loire.id, name: loire.name, type: 'pooling-organization' },
      { id: lorette.id, name: lorette.name, type: 'government' }
    ]);
  });

  it('should search targets when user is superadmin', () => {
    currentUserService.getUserChanges.and.returnValue(of({ superAdmin: true } as CurrentUserModel));

    const query = 'lo';
    poolingOrganizationService.search.and.returnValue(of([loire]));
    governmentService.search.and.returnValue(of(createPage([lorette], 10, 1, 0)));

    let actualTargets: Array<IssueTarget> | null = null;
    service.searchTargets(query).subscribe(response => (actualTargets = response));

    expect(actualTargets!).toEqual([
      { id: loire.id, name: loire.name, type: 'pooling-organization' },
      { id: lorette.id, name: lorette.name, type: 'government' }
    ]);
  });

  it('should load targets', () => {
    poolingOrganizationService.get.and.returnValue(of(loire as PoolingOrganizationDetailedModel));
    governmentService.get.and.returnValue(of(lorette as GovernmentDetailedModel));

    let actualTargets: Array<IssueTarget> | null = null;
    service
      .loadIssueTargets([loire.id], [lorette.id])
      .subscribe(response => (actualTargets = response));

    expect(actualTargets!).toEqual([
      { id: loire.id, name: loire.name, type: 'pooling-organization' },
      { id: lorette.id, name: lorette.name, type: 'government' }
    ]);
  });
});
