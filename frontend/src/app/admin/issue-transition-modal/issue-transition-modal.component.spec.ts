import { TestBed } from '@angular/core/testing';

import { IssueTransitionModalComponent } from './issue-transition-modal.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { IssueStatusComponent } from '../../issue-status/issue-status.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IssueModel } from '../../models/issue.model';

class IssueTransitionModalComponentTester extends ComponentTester<IssueTransitionModalComponent> {
  constructor() {
    super(IssueTransitionModalComponent);
  }

  get fromStatus() {
    return this.components(IssueStatusComponent)[0];
  }

  get toStatus() {
    return this.components(IssueStatusComponent)[1];
  }

  get message() {
    return this.textarea('#message')!;
  }

  get confirm() {
    return this.button('#confirm')!;
  }
}

describe('IssueTransitionModalComponent', () => {
  let tester: IssueTransitionModalComponentTester;
  let activeModal: jasmine.SpyObj<NgbActiveModal>;

  beforeEach(async () => {
    activeModal = createMock(NgbActiveModal);

    TestBed.configureTestingModule({
      providers: [{ provide: NgbActiveModal, useValue: activeModal }]
    });

    tester = new IssueTransitionModalComponentTester();
    tester.componentInstance.prepare(
      {
        status: 'CREATED'
      } as IssueModel,
      'RESOLVED'
    );
    await tester.stable();
  });

  it('should ask for a confirmation with a message', () => {
    expect(tester.fromStatus.status()).toBe('CREATED');
    expect(tester.toStatus.status()).toBe('RESOLVED');
    expect(tester.message).toHaveValue('');
  });

  it('should confirm with null if message is blank', async () => {
    await tester.message.fillWith('  ');
    await tester.confirm.click();
    expect(activeModal.close).toHaveBeenCalledWith(null);
  });

  it('should confirm with trimmed message', async () => {
    await tester.message.fillWith(' Hello ');
    await tester.confirm.click();
    expect(activeModal.close).toHaveBeenCalledWith('Hello');
  });
});
