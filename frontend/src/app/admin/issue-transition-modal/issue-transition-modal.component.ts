import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { IssueModel, IssueStatus } from '../../models/issue.model';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { IssueStatusComponent } from '../../issue-status/issue-status.component';

@Component({
  selector: 'dm-issue-transition-modal',
  imports: [ReactiveFormsModule, IssueStatusComponent],
  templateUrl: './issue-transition-modal.component.html',
  styleUrl: './issue-transition-modal.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueTransitionModalComponent {
  readonly activeModal = inject(NgbActiveModal);

  readonly vm = signal<{ issue: IssueModel; newStatus: IssueStatus } | undefined>(undefined);

  readonly form = inject(NonNullableFormBuilder).group({
    message: ''
  });

  prepare(issue: IssueModel, newStatus: IssueStatus) {
    this.vm.set({ issue, newStatus });
  }

  close() {
    this.activeModal.close(this.form.value.message!.trim() || null);
  }
}
