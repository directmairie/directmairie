import { Injectable, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { UserModel } from '../registration/models/registration.model';
import { PageModel } from '../models/page.model';

/**
 * Service allowing to search for users by a part of their email
 */
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private http = inject(HttpClient);

  searchUserByEmail(query: string, page = 0): Observable<PageModel<UserModel>> {
    return this.http.get<PageModel<UserModel>>('/api/users', {
      params: {
        query,
        page: String(page)
      }
    });
  }
}
