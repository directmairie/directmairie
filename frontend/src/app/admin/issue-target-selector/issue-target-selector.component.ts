import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  inject,
  input,
  output,
  viewChild
} from '@angular/core';
import { debounceTime, distinctUntilChanged, filter, map, Observable, switchMap } from 'rxjs';
import {
  NgbHighlight,
  NgbTypeahead,
  NgbTypeaheadSelectItemEvent
} from '@ng-bootstrap/ng-bootstrap';
import * as icons from '../../icons';
import { IssueTarget, IssueTargetService } from '../issue-target.service';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';

/**
 * Component used to select the targets of a list of issues. Targets are either governments or pooling organizations.
 * The component consists of a typeahead allowing to search for targets, and of pills showing the currently selected targets.
 */
@Component({
  selector: 'dm-issue-target-selector',
  templateUrl: './issue-target-selector.component.html',
  styleUrl: './issue-target-selector.component.scss',
  imports: [
    FontAwesomeModule,
    NgbHighlight,
    ReactiveFormsModule,
    NgbTypeahead,
    InvalidFormControlDirective
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueTargetSelectorComponent {
  private readonly issueTargetService = inject(IssueTargetService);

  readonly targetCtrl = new FormControl('');
  readonly targetTypeahead: (text$: Observable<string>) => Observable<Array<IssueTarget>>;
  readonly targetFormatter = (target: IssueTarget) => target.name;
  readonly selectedTargets = input<Array<IssueTarget>>([]);
  readonly selectedTargetsChange = output<Array<IssueTarget>>();

  readonly typeahead = viewChild.required<ElementRef<HTMLInputElement>>('typeahead');

  readonly icons = icons;

  constructor() {
    this.targetTypeahead = text$ =>
      text$.pipe(
        map(text => text.trim()),
        filter(text => text.length > 1),
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(text => this.issueTargetService.searchTargets(text)),
        map(targets =>
          targets.filter(
            target =>
              !this.selectedTargets().some(t => t.id === target.id && t.type === target.type)
          )
        )
      );
  }

  selectTarget(event: NgbTypeaheadSelectItemEvent<IssueTarget>) {
    event.preventDefault();
    this.targetCtrl.setValue('');
    const newTarget = event.item;
    const selectedTargets = this.selectedTargets();
    if (
      !selectedTargets.some(target => target.type === newTarget.type && target.id === newTarget.id)
    ) {
      this.selectedTargetsChange.emit([...selectedTargets, newTarget]);
    }
    this.typeahead().nativeElement.focus();
  }

  removeTarget(target: IssueTarget) {
    this.selectedTargetsChange.emit(this.selectedTargets().filter(t => t !== target));
    this.typeahead().nativeElement.focus();
  }
}
