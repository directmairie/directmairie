import { TestBed } from '@angular/core/testing';

import { IssueTargetSelectorComponent } from './issue-target-selector.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { IssueTarget, IssueTargetService } from '../issue-target.service';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { Subject } from 'rxjs';

@Component({
  template: '<dm-issue-target-selector [(selectedTargets)]="selectedTargets" />',
  imports: [IssueTargetSelectorComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly selectedTargets = signal<Array<IssueTarget>>([]);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get targetInput() {
    return this.input('input')!;
  }

  get results(): NodeListOf<HTMLButtonElement> {
    // Based on the typeahead test itself
    // see https://github.com/ng-bootstrap/ng-bootstrap/blob/master/src/typeahead/typeahead.spec.ts
    // The dropdown is appended to the body, not to this element, so we can't unfortunately return an array of
    // TestButton, but only DOM elements
    return document.querySelectorAll('ngb-typeahead-window.dropdown-menu button.dropdown-item');
  }

  get badges() {
    return this.elements('.issue-target');
  }
}

describe('IssueTargetSelectorComponent', () => {
  let issueTargetService: jasmine.SpyObj<IssueTargetService>;
  let tester: TestComponentTester;

  const target1: IssueTarget = { id: 1, type: 'pooling-organization', name: 'Org Loire' };
  const target2: IssueTarget = { id: 1, type: 'government', name: 'Gov Loire' };
  const target3: IssueTarget = { id: 2, type: 'pooling-organization', name: 'Org Rhone' };
  const target4: IssueTarget = { id: 3, type: 'government', name: 'Gov Rhone' };

  beforeEach(async () => {
    issueTargetService = createMock(IssueTargetService);

    TestBed.configureTestingModule({
      providers: [{ provide: IssueTargetService, useValue: issueTargetService }]
    });

    tester = new TestComponentTester();
    await tester.stable();

    jasmine.clock().install();
    jasmine.clock().mockDate();
  });

  afterEach(() => jasmine.clock().uninstall());

  it('should display the selected targets as bagdes', async () => {
    // given admins selected
    const targets: Array<IssueTarget> = [target1, target2];
    const component = tester.componentInstance;
    component.selectedTargets.set(targets);

    // when displaying the component
    await tester.stable();

    // then it should have several removable badges
    expect(tester.badges.length).toBe(2);
    expect(tester.badges[0]).toContainText(target1.name);
    expect(tester.badges[0].button('button')).not.toBeNull();
    expect(tester.badges[1]).toContainText(target2.name);
    expect(tester.badges[1].button('button')).not.toBeNull();
  });

  it('should add/remove targets and update badges', async () => {
    const targets$ = new Subject<Array<IssueTarget>>();
    issueTargetService.searchTargets.and.returnValue(targets$);

    const component = tester.componentInstance;
    component.selectedTargets.set([target1, target2]);

    await tester.stable();

    expect(tester.targetInput).toHaveValue('');
    expect(tester.badges.length).toBe(2);

    // when a value is entered
    await tester.targetInput.fillWith('rho');
    jasmine.clock().tick(400);
    expect(issueTargetService.searchTargets).toHaveBeenCalledWith('rho');
    targets$.next([target3, target4]);

    // results should appear
    expect(tester.results.length).toBe(2);
    expect(tester.results[0].textContent).toBe(target3.name);
    expect(tester.results[1].textContent).toBe(target4.name);

    // when the result is selected
    await tester.results[0].click();
    await tester.stable();

    // the user is added to the admins
    expect(component.selectedTargets()).toEqual([target1, target2, target3]);

    // the input is emptied
    expect(tester.targetInput).toHaveValue('');

    // the focus is given back to the input
    expect(document.activeElement).toBe(tester.targetInput.nativeElement);

    // and a new badge should appear
    expect(tester.badges.length).toBe(3);
    expect(tester.badges[2]).toContainText(target3.name);

    // when another value is entered
    await tester.targetInput.fillWith('rho');
    jasmine.clock().tick(400);
    targets$.next([target3, target4]);

    // results should appear and should not contain already selected target1
    expect(tester.results.length).toBe(1);
    expect(tester.results[0].textContent).toBe(target4.name);

    // when the result is selected
    await tester.results[0].click();
    await tester.stable();

    // another target is added
    expect(tester.targetInput).toHaveValue('');
    expect(component.selectedTargets()).toEqual([target1, target2, target3, target4]);

    // the focus is given back to the input
    expect(document.activeElement).toBe(tester.targetInput.nativeElement);

    // and another badge should appear
    expect(tester.badges.length).toBe(4);
    expect(tester.badges[3]).toContainText(target4.name);

    // when a badge is removed
    await tester.badges[0].button('button')!.click();

    // the target is removed
    expect(component.selectedTargets()).toEqual([target2, target3, target4]);

    // and the badge should disappear
    expect(tester.badges.length).toBe(3);
    expect(tester.badges[0]).toContainText(target2.name);
  });
});
