import { TestBed } from '@angular/core/testing';

import { GovernmentComponent } from './government.component';
import { ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { GovernmentService } from '../government.service';
import { ActivatedRoute } from '@angular/router';
import { of, Subject, throwError } from 'rxjs';
import { GovernmentDetailedModel } from '../models/government.model';
import { DateTime } from 'luxon';

class GovernmentComponentTester extends ComponentTester<GovernmentComponent> {
  constructor() {
    super(GovernmentComponent);
  }

  get title() {
    return this.element('h1');
  }

  get code() {
    return this.element('#code');
  }

  get url() {
    return this.element('#url');
  }

  get osmId() {
    return this.element('#osmId');
  }

  get administrators() {
    return this.elements('.administrator-item');
  }

  get groups() {
    return this.elements('.group-item');
  }

  get categories() {
    return this.elements('.category-item');
  }

  get contacts() {
    return this.elements('.contact-item');
  }

  get contactEmail() {
    return this.element('#contactEmail');
  }

  get logo() {
    return this.element('#logo');
  }

  get noLogo() {
    return this.element('#noLogo');
  }

  get uploadLogo() {
    return this.button('#uploadLogo');
  }

  get deleteLogo() {
    return this.button('#deleteLogo')!;
  }

  get tooLargeLogo() {
    return this.element('#tooLargeLogo');
  }
}

describe('GovernmentComponent', () => {
  let tester: GovernmentComponentTester;
  let service: jasmine.SpyObj<GovernmentService>;

  let government: GovernmentDetailedModel;

  beforeEach(() => {
    service = createMock(GovernmentService);

    const route = stubRoute({
      params: { governmentId: 1000 }
    });

    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: GovernmentService, useValue: service }
      ]
    });

    government = {
      poolingOrganizationId: 1,
      id: 1000,
      name: 'Loire',
      code: '123',
      url: 'https://loire.fr',
      osmId: 321,
      contactEmail: 'contact@loire.fr',
      administrators: [
        {
          id: 1,
          email: 'jane@mail.com'
        },
        {
          id: 2,
          email: 'john@mail.com'
        }
      ],
      issueGroups: [
        {
          id: 2,
          label: 'Lighting',
          categories: [
            {
              id: 21,
              label: 'Broken lamp',
              descriptionRequired: false
            }
          ]
        },
        {
          id: 1,
          label: 'Other',
          categories: [
            {
              id: 1,
              label: 'Other',
              descriptionRequired: true
            }
          ]
        }
      ],
      contacts: [
        {
          issueGroup: {
            id: 2,
            label: 'Lighting'
          },
          email: 'lighting@loire.fr'
        },
        {
          issueGroup: {
            id: 1,
            label: 'Other'
          },
          email: 'other@loire.fr'
        }
      ],
      logo: {
        id: 3456,
        creationInstant: '2019-02-26T12:13:14Z'
      }
    } as GovernmentDetailedModel;
  });

  it('should display the government once available', async () => {
    const government$ = new Subject<GovernmentDetailedModel>();

    service.get.and.returnValue(government$);

    tester = new GovernmentComponentTester();
    await tester.stable();
    expect(tester.title).toBeNull();

    government$.next(government);
    await tester.stable();

    expect(tester.title).toContainText('Collectivité Loire');
    expect(tester.code).toContainText('123');
    expect(tester.url).toContainText('https://loire.fr');
    expect(tester.osmId).toContainText('321');
    expect(tester.administrators.length).toBe(2);
    expect(tester.administrators[0]).toContainText('jane@mail.com');
    expect(tester.administrators[1].element('a')!.attr('href')).toBe('mailto:john@mail.com');
    expect(tester.groups.length).toBe(2);
    expect(tester.groups[0]).toContainText('Lighting');
    expect(tester.groups[0]).toContainText('Broken lamp');
    expect(tester.categories.length).toBe(2);
    expect(tester.categories[0]).toContainText('Broken lamp');
    expect(tester.contacts.length).toBe(2);
    expect(tester.contacts[0]).toContainText('Lighting\u00a0: lighting@loire.fr');
    expect(tester.contactEmail).toContainText('contact@loire.fr');
    expect(tester.logo).not.toBeNull();
    expect(tester.noLogo).toBeNull();
    expect(tester.uploadLogo).toHaveText('Changer le logo');
    expect(tester.deleteLogo).not.toBeNull();
    expect(tester.tooLargeLogo).toBeNull();
  });

  it('should display the right stuff when there is no logo', async () => {
    government.logo = null;
    service.get.and.returnValue(of(government));

    tester = new GovernmentComponentTester();
    await tester.stable();
    expect(tester.logo).toBeNull();
    expect(tester.noLogo).not.toBeNull();
    expect(tester.uploadLogo).toHaveText('Choisir le logo');
    expect(tester.deleteLogo).toBeNull();
    expect(tester.tooLargeLogo).toBeNull();
  });

  it('should delete logo', async () => {
    service.get.and.returnValue(of(government));
    service.deleteLogo.and.returnValue(of(undefined));

    const logoId = government.logo!.id;

    tester = new GovernmentComponentTester();
    await tester.stable();

    await tester.deleteLogo.click();

    expect(tester.logo).toBeNull();
    expect(tester.noLogo).not.toBeNull();

    expect(service.deleteLogo).toHaveBeenCalledWith(government.id, logoId);
  });

  it('should upload logo', async () => {
    government.logo = null;
    service.get.and.returnValue(of(government));
    tester = new GovernmentComponentTester();
    await tester.stable();
    tester.componentInstance.logoTooLarge.set(true);
    await tester.stable();

    const originalFile = {} as File;
    service.validateLogo.and.returnValue(of(originalFile));
    service.uploadLogo.and.returnValue(
      of({
        id: 97654,
        creationInstant: DateTime.local().toISO()!
      })
    );

    tester.componentInstance.uploadLogo({
      target: {
        files: [originalFile]
      }
    } as unknown as Event);
    await tester.stable();

    expect(tester.logo).not.toBeNull();
    expect(tester.noLogo).toBeNull();
    expect(tester.tooLargeLogo).toBeNull();

    expect(service.validateLogo).toHaveBeenCalledWith(originalFile);
    expect(service.uploadLogo).toHaveBeenCalledWith(government.id, originalFile);
  });

  it('should not upload logo', async () => {
    government.logo = null;
    service.get.and.returnValue(of(government));
    tester = new GovernmentComponentTester();
    await tester.stable();

    const originalFile = {} as File;
    service.validateLogo.and.returnValue(throwError(() => 'too large'));

    tester.componentInstance.uploadLogo({
      target: {
        files: [originalFile]
      }
    } as unknown as Event);
    await tester.stable();

    expect(tester.logo).toBeNull();
    expect(tester.noLogo).not.toBeNull();
    expect(tester.tooLargeLogo).not.toBeNull();

    expect(service.validateLogo).toHaveBeenCalledWith(originalFile);
    expect(service.uploadLogo).not.toHaveBeenCalled();
  });
});
