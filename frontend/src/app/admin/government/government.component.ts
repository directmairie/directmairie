import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  computed,
  inject,
  linkedSignal,
  signal,
  Signal
} from '@angular/core';
import { GovernmentService, MAX_LOGO_SIZE } from '../government.service';
import { catchError, EMPTY, switchMap } from 'rxjs';
import { GovernmentDetailedModel } from '../models/government.model';
import { ActivatedRoute, RouterLink } from '@angular/router';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DecimalPipe } from '@angular/common';
import { toSignal } from '@angular/core/rxjs-interop';

/**
 * The component used to display a detailed govenment
 */
@Component({
  selector: 'dm-government',
  templateUrl: './government.component.html',
  styleUrl: './government.component.scss',
  imports: [FontAwesomeModule, RouterLink, DecimalPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GovernmentComponent {
  private readonly governmentService = inject(GovernmentService);
  private readonly route = inject(ActivatedRoute);
  private readonly changeDetector = inject(ChangeDetectorRef);

  readonly originalGovernment: Signal<GovernmentDetailedModel | undefined> = toSignal(
    this.governmentService.get(parseInt(this.route.snapshot.paramMap.get('governmentId')!))
  );
  readonly government = linkedSignal(this.originalGovernment);

  readonly icons = icons;

  readonly logoTooLarge = signal(false);
  readonly maxLogoSize = MAX_LOGO_SIZE;

  readonly logoUrl = computed(() => {
    const government = this.government();
    return government && government.logo
      ? `/api/governments/${government.id}/logo/${government.logo.id}/bytes`
      : undefined;
  });

  uploadLogo(event: Event) {
    this.logoTooLarge.set(false);
    const fileInput = event.target as HTMLInputElement;
    const file = fileInput.files![0];

    const government = this.government()!;
    this.governmentService
      .validateLogo(file)
      .pipe(
        catchError(() => {
          this.logoTooLarge.set(true);
          return EMPTY;
        }),
        switchMap(blob => this.governmentService.uploadLogo(government.id, blob))
      )
      .subscribe(logo => {
        this.government.set({ ...government, logo });
      });

    fileInput.value = '';
  }

  deleteLogo() {
    this.logoTooLarge.set(false);
    const government = this.government()!;
    this.governmentService.deleteLogo(government.id, government.logo!.id).subscribe(() => {
      this.government.set({ ...government, logo: null });
      this.changeDetector.detectChanges();
    });
  }
}
