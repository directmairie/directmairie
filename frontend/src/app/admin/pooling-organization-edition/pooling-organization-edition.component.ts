import {
  ChangeDetectionStrategy,
  Component,
  effect,
  inject,
  signal,
  Signal,
  WritableSignal
} from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { UserModel } from '../../registration/models/registration.model';
import {
  PoolingOrganizationCommand,
  PoolingOrganizationDetailedModel
} from '../models/pooling-organization.model';
import { PoolingOrganizationService } from '../pooling-organization.service';
import { SuccessService } from '../../success.service';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { map, Observable } from 'rxjs';
import { UserSelectorComponent } from '../user-selector/user-selector.component';
import { ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { toSignal } from '@angular/core/rxjs-interop';

interface ViewModel {
  mode: 'create' | 'update';
  editedOrganization: PoolingOrganizationDetailedModel | null;
  admins: WritableSignal<Array<UserModel>>;
}

/**
 * Component used to create or update a pooling organization
 */
@Component({
  selector: 'dm-pooling-organization-edition',
  templateUrl: './pooling-organization-edition.component.html',
  styleUrl: './pooling-organization-edition.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    UserSelectorComponent,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PoolingOrganizationEditionComponent {
  private readonly poolingOrganizationService = inject(PoolingOrganizationService);
  private readonly router = inject(Router);
  private readonly successService = inject(SuccessService);

  readonly form = inject(NonNullableFormBuilder).group({
    name: ['', Validators.required]
  });

  readonly vm: Signal<ViewModel | undefined>;

  constructor() {
    const route = inject(ActivatedRoute);
    const organizationId = route.snapshot.paramMap.get('organizationId');
    this.vm = organizationId
      ? toSignal(
          this.poolingOrganizationService.get(parseInt(organizationId)).pipe(
            map(organization => ({
              mode: 'update' as const,
              editedOrganization: organization,
              admins: signal(organization.administrators)
            }))
          )
        )
      : signal({
          mode: 'create' as const,
          editedOrganization: null,
          admins: signal<Array<UserModel>>([])
        }).asReadonly();
    effect(() => {
      const vm = this.vm();
      if (vm && vm.editedOrganization) {
        this.form.setValue({
          name: vm.editedOrganization.name
        });
      }
    });
  }

  save() {
    if (this.form.invalid) {
      return;
    }
    const vm = this.vm()!;

    const command: PoolingOrganizationCommand = {
      name: this.form.value.name!,
      administratorIds: vm.admins().map(admin => admin.id)
    };

    let observable: Observable<number>;
    let message: string;

    if (vm.mode === 'create') {
      observable = this.poolingOrganizationService
        .create(command)
        .pipe(map(organization => organization.id));
      message = 'Le mutualisant a été créé.';
    } else {
      observable = this.poolingOrganizationService
        .update(vm.editedOrganization!.id, command)
        .pipe(map(() => vm.editedOrganization!.id));
      message = 'Le mutualisant a été modifié.';
    }

    observable.subscribe(id => {
      this.router.navigate(['/admin/pooling-organizations', id]);
      this.successService.success(message);
    });
  }
}
