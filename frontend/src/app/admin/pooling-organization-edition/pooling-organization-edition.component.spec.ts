import { TestBed } from '@angular/core/testing';

import { PoolingOrganizationEditionComponent } from './pooling-organization-edition.component';
import { ActivatedRouteStub, ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { UserSelectorComponent } from '../user-selector/user-selector.component';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { PoolingOrganizationService } from '../pooling-organization.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SuccessService } from '../../success.service';
import { of } from 'rxjs';
import { UserModel } from '../../registration/models/registration.model';
import {
  PoolingOrganizationCommand,
  PoolingOrganizationDetailedModel
} from '../models/pooling-organization.model';
import { UserService } from '../user.service';

class PoolingOrganizationEditionComponentTester extends ComponentTester<PoolingOrganizationEditionComponent> {
  constructor() {
    super(PoolingOrganizationEditionComponent);
  }

  get title() {
    return this.element('h1');
  }

  get name() {
    return this.input('#name')!;
  }

  get adminSelector() {
    return this.component(UserSelectorComponent);
  }

  get save() {
    return this.button('#save')!;
  }
}

describe('PoolingOrganizationEditionComponent', () => {
  let tester: PoolingOrganizationEditionComponentTester;
  let service: jasmine.SpyObj<PoolingOrganizationService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;
  let route: ActivatedRouteStub;

  beforeEach(() => {
    service = createMock(PoolingOrganizationService);
    successService = createMock(SuccessService);
    route = stubRoute();

    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: PoolingOrganizationService, useValue: service },
        { provide: SuccessService, useValue: successService },
        { provide: UserService, useValue: createMock(UserService) }
      ]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    router = TestBed.inject(Router);
    spyOn(router, 'navigate');
  });

  describe('in create mode', () => {
    beforeEach(async () => {
      tester = new PoolingOrganizationEditionComponentTester();
      await tester.stable();
    });

    it('should have a title', () => {
      expect(tester.title).toHaveText('Créer un mutualisant');
    });

    it('should display an empty form', () => {
      expect(tester.name).toHaveValue('');
      expect(tester.adminSelector.users()).toEqual([]);
    });

    it('should validate the form', async () => {
      await tester.save.click();

      expect(tester.testElement).toContainText('Le nom est obligatoire');

      await tester.name.fillWith('bla');
      expect(tester.testElement).not.toContainText('Le nom est obligatoire');

      expect(service.create).not.toHaveBeenCalled();
    });

    it('should create the organization', async () => {
      service.create.and.returnValue(of({ id: 42 } as PoolingOrganizationDetailedModel));

      await tester.name.fillWith('bla');
      tester.adminSelector.users.set([{ id: 1 }, { id: 2 }] as Array<UserModel>);

      await tester.save.click();

      expect(service.create).toHaveBeenCalledWith({
        name: 'bla',
        administratorIds: [1, 2]
      } as PoolingOrganizationCommand);
      expect(router.navigate).toHaveBeenCalledWith(['/admin/pooling-organizations', 42]);
      expect(successService.success).toHaveBeenCalledWith('Le mutualisant a été créé.');
    });
  });

  describe('in update mode', () => {
    let editedOrganization: PoolingOrganizationDetailedModel;

    beforeEach(async () => {
      editedOrganization = {
        id: 42,
        name: 'Org 1',
        administrators: [
          {
            id: 1,
            email: 'john@mail.com'
          }
        ]
      } as PoolingOrganizationDetailedModel;

      route.setParam('organizationId', editedOrganization.id.toString());

      service.get.and.returnValue(of(editedOrganization));

      tester = new PoolingOrganizationEditionComponentTester();
      await tester.stable();
    });

    it('should have a title', () => {
      expect(tester.title).toContainText('Modifier le mutualisant Org 1');
    });

    it('should display a populated form', () => {
      expect(tester.name).toHaveValue('Org 1');
      expect(tester.adminSelector.users()).toEqual(editedOrganization.administrators);
    });

    it('should update the organization', async () => {
      service.update.and.returnValue(of(undefined));

      await tester.name.fillWith('bla');
      tester.adminSelector.users.set([{ id: 1 }, { id: 2 }] as Array<UserModel>);

      await tester.save.click();

      expect(service.update).toHaveBeenCalledWith(editedOrganization.id, {
        name: 'bla',
        administratorIds: [1, 2]
      } as PoolingOrganizationCommand);
      expect(router.navigate).toHaveBeenCalledWith(['/admin/pooling-organizations', 42]);
      expect(successService.success).toHaveBeenCalledWith('Le mutualisant a été modifié.');
    });
  });
});
