import { Routes } from '@angular/router';

import { IssueListComponent } from './issue-list/issue-list.component';
import { GovernmentEditionComponent } from './government-edition/government-edition.component';
import { PoolingOrganizationListComponent } from './pooling-organization-list/pooling-organization-list.component';
import { PoolingOrganizationComponent } from './pooling-organization/pooling-organization.component';
import { GovernmentListComponent } from './government-list/government-list.component';
import { GovernmentComponent } from './government/government.component';
import { PoolingOrganizationEditionComponent } from './pooling-organization-edition/pooling-organization-edition.component';
import { ExportComponent } from './export/export.component';
import { NotificationEditionComponent } from './notification-edition/notification-edition.component';

export const adminRoutes: Routes = [
  { path: 'issues', component: IssueListComponent },
  { path: 'pooling-organizations', component: PoolingOrganizationListComponent },
  { path: 'pooling-organizations/new', component: PoolingOrganizationEditionComponent },
  { path: 'pooling-organizations/:organizationId', component: PoolingOrganizationComponent },
  {
    path: 'pooling-organizations/:organizationId/edit',
    component: PoolingOrganizationEditionComponent
  },
  { path: 'governments', component: GovernmentListComponent },
  { path: 'governments/new', component: GovernmentEditionComponent },
  { path: 'governments/:governmentId', component: GovernmentComponent },
  { path: 'governments/:governmentId/edit', component: GovernmentEditionComponent },
  { path: 'export', component: ExportComponent },
  { path: 'notification', component: NotificationEditionComponent }
];
