import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { PoolingOrganizationDetailedModel } from '../models/pooling-organization.model';
import { PoolingOrganizationService } from '../pooling-organization.service';
import { ActivatedRoute, RouterLink } from '@angular/router';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { toSignal } from '@angular/core/rxjs-interop';

/**
 * Component used to display a detailed pooling organization
 */
@Component({
  selector: 'dm-pooling-organization',
  templateUrl: './pooling-organization.component.html',
  styleUrl: './pooling-organization.component.scss',
  imports: [FontAwesomeModule, RouterLink],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PoolingOrganizationComponent {
  readonly organization: Signal<PoolingOrganizationDetailedModel | undefined>;
  readonly icons = icons;

  constructor() {
    const route = inject(ActivatedRoute);
    const organizationId = +route.snapshot.paramMap.get('organizationId')!;
    const poolingOrganizationService = inject(PoolingOrganizationService);
    this.organization = toSignal(poolingOrganizationService.get(organizationId));
  }
}
