import { TestBed } from '@angular/core/testing';

import { PoolingOrganizationComponent } from './pooling-organization.component';
import { ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { PoolingOrganizationService } from '../pooling-organization.service';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs';
import { PoolingOrganizationDetailedModel } from '../models/pooling-organization.model';

class PoolingOrganizationComponentTester extends ComponentTester<PoolingOrganizationComponent> {
  constructor() {
    super(PoolingOrganizationComponent);
  }

  get title() {
    return this.element('h1');
  }

  get administrators() {
    return this.elements('.administrator-item');
  }
}

describe('PoolingOrganizationComponent', () => {
  let tester: PoolingOrganizationComponentTester;
  let service: jasmine.SpyObj<PoolingOrganizationService>;

  beforeEach(() => {
    const route = stubRoute({
      params: {
        organizationId: '1000'
      }
    });
    service = createMock(PoolingOrganizationService);

    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: PoolingOrganizationService, useValue: service }
      ]
    });
  });

  it('should display the organization once available', async () => {
    const organization$ = new Subject<PoolingOrganizationDetailedModel>();

    service.get.and.returnValue(organization$);

    tester = new PoolingOrganizationComponentTester();
    await tester.stable();
    expect(tester.title).toBeNull();

    organization$.next({
      id: 1000,
      name: 'Adullact',
      administrators: [
        {
          id: 1,
          email: 'jane@mail.com'
        },
        {
          id: 2,
          email: 'john@mail.com'
        }
      ]
    } as PoolingOrganizationDetailedModel);
    await tester.stable();

    expect(tester.title).toContainText('Mutualisant Adullact');
    expect(tester.administrators.length).toBe(2);
    expect(tester.administrators[0]).toContainText('jane@mail.com');
    expect(tester.administrators[1].element('a')!.attr('href')).toBe('mailto:john@mail.com');
  });
});
