import { TestBed } from '@angular/core/testing';

import { I18nDateParserFormatterService } from './i18n-date-parser-formatter.service';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { provideDirectMairieDatepicker } from './direct-mairie-datepicker.providers';

describe('I18nDateParserFormatterService', () => {
  let service: NgbDateParserFormatter;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideDirectMairieDatepicker()]
    });
    service = TestBed.inject(NgbDateParserFormatter);
  });

  it('should be properly provided', () => {
    expect(service instanceof I18nDateParserFormatterService).toBe(true);
  });

  it('should format', () => {
    expect(service.format(null)).toBe('');
    expect(service.format(undefined as unknown as null)).toBe('');
    expect(
      service.format({
        year: 2018,
        month: 2,
        day: 14
      })
    ).toBe('14/02/2018');
  });

  it('should parse', () => {
    expect(service.parse(null as unknown as string)).toBeNull();
    expect(service.parse(undefined as unknown as string)).toBeNull();
    expect(service.parse('')).toBeNull();
    expect(service.parse('02/14/2018')).toBeNull();
    expect(service.parse('14/02/18')).toBeNull();
    expect(service.parse('abcd-ef-gh')).toBeNull();
    expect(service.parse('14/02/2018')).toEqual({
      year: 2018,
      month: 2,
      day: 14
    });
  });
});
