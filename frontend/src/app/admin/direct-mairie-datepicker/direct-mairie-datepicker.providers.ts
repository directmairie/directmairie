import { NgbDateAdapter, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { IsoDateAdapterService } from './iso-date-adapter.service';
import { I18nDateParserFormatterService } from './i18n-date-parser-formatter.service';

/**
 * Custom services and components used to customize the NgbDatepicker.
 */
export function provideDirectMairieDatepicker() {
  return [
    { provide: NgbDateAdapter, useClass: IsoDateAdapterService },
    { provide: NgbDateParserFormatter, useClass: I18nDateParserFormatterService }
  ];
}
