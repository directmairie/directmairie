import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { TestBed } from '@angular/core/testing';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { NgbDatepicker, NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { ComponentTester } from 'ngx-speculoos';
import { DatepickerContainerComponent } from './datepicker-container.component';
import { provideDirectMairieDatepicker } from '../direct-mairie-datepicker.providers';

@Component({
  template: `
    <dm-datepicker-container class="foo">
      <input class="form-control" [formControl]="dateCtrl" ngbDatepicker />
    </dm-datepicker-container>
  `,
  imports: [DatepickerContainerComponent, ReactiveFormsModule, NgbInputDatepicker],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  dateCtrl = new FormControl<string>('');
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get toggler() {
    return this.button('.date-picker-toggler')!;
  }

  get inputDatepicker() {
    return this.input(NgbInputDatepicker);
  }

  get datepicker() {
    return this.element(NgbDatepicker);
  }

  get container() {
    return this.element('dm-datepicker-container');
  }
}

describe('DatepickerContainerComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [provideDirectMairieDatepicker()]
    });

    tester = new TestComponentTester();
    await tester.stable();
  });

  it('should display an input, a toggle button, and toggle the datepicker', async () => {
    expect(tester.toggler).not.toBeNull();
    expect(tester.inputDatepicker).not.toBeNull();
    expect(tester.datepicker).toBeNull();

    await tester.toggler.click();
    expect(tester.datepicker).not.toBeNull();

    await tester.toggler.click();
    expect(tester.datepicker).toBeNull();
  });

  it('should have the input-group class in addition to its original class', () => {
    expect(tester.container).toHaveClass('input-group');
    expect(tester.container).toHaveClass('foo');
  });
});
