import { ChangeDetectionStrategy, Component, contentChild } from '@angular/core';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import * as icons from '../../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

/**
 * Component used to simplify the markup of a datepicker in a popup. It wraps the input which has the
 * ngbDatepicker directive, and prepends the toggle button to it.
 *
 * Example usage:
 *
 * <dm-datepicker-container class="col-sm-9">
 *   <input class="form-control" formControlName="date" ngbDatepicker />
 * </dm-datepicker-container>
 */
@Component({
  selector: 'dm-datepicker-container',
  templateUrl: './datepicker-container.component.html',
  host: {
    class: 'input-group'
  },
  imports: [FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DatepickerContainerComponent {
  readonly icons = icons;

  readonly datePicker = contentChild.required(NgbInputDatepicker);

  toggle() {
    this.datePicker().toggle();
  }
}
