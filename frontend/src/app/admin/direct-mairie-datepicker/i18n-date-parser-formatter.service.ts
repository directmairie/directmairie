import { Injectable } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { DateTime } from 'luxon';

/**
 * Service used to transform the strings entered in a date picker into NgbDateStruct instances used internally
 * by the date picker, and vice-versa. It uses a strict French pattern, but could be made more generic by
 * using a pattern based on the i18n locale.
 */
@Injectable()
export class I18nDateParserFormatterService extends NgbDateParserFormatter {
  // this pattern is hard-coded for now, but could be easily changed or be made dynamic later
  private pattern = 'dd/MM/yyyy';

  parse(value: string): NgbDateStruct | null {
    if (value) {
      const date = DateTime.fromFormat(value, this.pattern);
      if (!date.isValid) {
        return null;
      }
      return {
        year: date.year,
        month: date.month,
        day: date.day
      };
    }
    return null;
  }

  format(date: NgbDateStruct): string {
    return date ? DateTime.fromObject(date).toFormat(this.pattern) : '';
  }
}
