import { TestBed } from '@angular/core/testing';
import { NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { IsoDateAdapterService } from './iso-date-adapter.service';
import { provideDirectMairieDatepicker } from './direct-mairie-datepicker.providers';

describe('IsoDateAdapterService', () => {
  let service: NgbDateAdapter<string>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideDirectMairieDatepicker()]
    });
    service = TestBed.inject(NgbDateAdapter);
  });

  it('should be properly provided', () => {
    expect(service instanceof IsoDateAdapterService).toBe(true);
  });

  it('should transform string to NgbDateStruct', () => {
    expect(service.fromModel('2017-01-01')).toEqual({ year: 2017, month: 1, day: 1 });
    expect(service.fromModel('2017-12-31')).toEqual({ year: 2017, month: 12, day: 31 });
    expect(service.fromModel('2017-12-1')).toBeNull();
    expect(service.fromModel('')).toBeNull();
    expect(service.fromModel(null)).toBeNull();
    expect(service.fromModel(undefined as unknown as null)).toBeNull();
  });

  it('should transform NgbDateStruct to string', () => {
    expect(service.toModel({ year: 2017, month: 1, day: 1 })).toBe('2017-01-01');
    expect(service.toModel({ year: 2017, month: 12, day: 31 })).toBe('2017-12-31');
    expect(service.toModel(null)).toBeNull();
    expect(service.toModel(undefined as unknown as null)).toBeNull();
  });
});
