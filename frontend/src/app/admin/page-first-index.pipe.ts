import { Pipe, PipeTransform } from '@angular/core';
import { PageModel } from '../models/page.model';

/**
 * Pipe used to extract the index of the first element of a page of result, i.e. the number M in
 * "elements M to N on a total of T"
 */
@Pipe({
  name: 'pageFirstIndex'
})
export class PageFirstIndexPipe<T> implements PipeTransform {
  transform(page: PageModel<T>): number {
    return page.number * page.size + 1;
  }
}
