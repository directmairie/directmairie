import { PageFirstIndexPipe } from './page-first-index.pipe';
import { PageModel } from '../models/page.model';

describe('PageFirstIndexPipe', () => {
  it('should return the first index of a page, for display', () => {
    const pipe = new PageFirstIndexPipe();
    const page = {
      size: 10,
      number: 0,
      content: ['a', 'b', 'c']
    } as PageModel<string>;

    expect(pipe.transform(page)).toBe(1);

    page.number = 2;

    expect(pipe.transform(page)).toBe(21);
  });
});
