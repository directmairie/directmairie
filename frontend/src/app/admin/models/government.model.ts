import { IssueGroupModel } from '../../models/issue-category.model';
import { UserModel } from '../../registration/models/registration.model';
import { PictureModel } from '../../models/issue.model';

export interface GovernmentModel {
  poolingOrganizationId: number;
  id: number;
  name: string;
  code: string;
  osmId: number;
  logo: PictureModel | null;
  url: string | null;
}

export interface GovernmentContactCommand {
  issueGroupId: number;
  email: string;
}

export interface GovernmentCommand {
  osmId: number;
  name: string;
  code: string;
  url: string | null;
  issueCategoryIds: Array<number>;
  contactEmail: string;
  contacts: Array<GovernmentContactCommand>;
  administratorIds: Array<number>;
  poolingOrganizationId: number;
}

export interface GovernmentDetailedModel extends GovernmentModel {
  contactEmail: string;
  administrators: Array<UserModel>;
  issueGroups: Array<IssueGroupModel>;
  contacts: Array<GovernmentContactModel>;
}

interface GovernmentContactModel {
  issueGroup: IssueGroupRefModel;
  email: string;
}

interface IssueGroupRefModel {
  id: number;
  label: string;
}
