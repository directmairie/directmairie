import { UserModel } from '../../registration/models/registration.model';

export interface PoolingOrganizationModel {
  id: number;
  name: string;
}

export interface PoolingOrganizationDetailedModel extends PoolingOrganizationModel {
  administrators: Array<UserModel>;
}

export interface PoolingOrganizationCommand {
  name: string;
  administratorIds: Array<number>;
}
