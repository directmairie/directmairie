import { PageLastIndexPipe } from './page-last-index.pipe';
import { PageModel } from '../models/page.model';

describe('PageLastIndexPipe', () => {
  it('should return the last index of a page, for display', () => {
    const pipe = new PageLastIndexPipe();
    const page = {
      size: 10,
      number: 0,
      content: ['a', 'b', 'c']
    } as PageModel<string>;

    expect(pipe.transform(page)).toBe(3);

    page.number = 2;

    expect(pipe.transform(page)).toBe(23);
  });
});
