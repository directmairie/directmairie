import { TestBed } from '@angular/core/testing';

import { NotificationEditionComponent } from './notification-edition.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { SuccessService } from '../../success.service';
import { GovernmentService } from '../government.service';
import { NotificationService } from './notification.service';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { of, Subject } from 'rxjs';
import { createPage } from '../../models/page.model.spec';
import { GovernmentModel } from '../models/government.model';
import { NotificationCommand } from './notification.model';

class NotificationEditionComponentTester extends ComponentTester<NotificationEditionComponent> {
  constructor() {
    super(NotificationEditionComponent);
  }

  get governmentTypeahead() {
    return this.input('#government')!;
  }

  get governmentResults() {
    return this.elements<HTMLButtonElement>(
      'ngb-typeahead-window.dropdown-menu button.dropdown-item'
    );
  }

  get subject() {
    return this.input('#subject')!;
  }

  get message() {
    return this.textarea('#message')!;
  }

  get url() {
    return this.input('#url')!;
  }

  get send() {
    return this.button('#send')!;
  }

  get errors() {
    return this.elements('val-errors div');
  }
}

describe('NotificationEditionComponent', () => {
  let tester: NotificationEditionComponentTester;

  let successService: jasmine.SpyObj<SuccessService>;
  let notificationService: jasmine.SpyObj<NotificationService>;
  let governmentService: jasmine.SpyObj<GovernmentService>;

  beforeEach(() => {
    successService = createMock(SuccessService);
    notificationService = createMock(NotificationService);
    governmentService = createMock(GovernmentService);

    TestBed.configureTestingModule({
      providers: [
        { provide: SuccessService, useValue: successService },
        { provide: NotificationService, useValue: notificationService },
        { provide: GovernmentService, useValue: governmentService }
      ]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();
  });

  describe('with unique government', () => {
    beforeEach(async () => {
      governmentService.search.and.returnValue(
        of(createPage([{ id: 42, name: 'Loire' } as GovernmentModel], 20, 1, 0))
      );

      tester = new NotificationEditionComponentTester();
      await tester.stable();
    });

    it('should not display government typeahead if unique government', () => {
      expect(tester.governmentTypeahead).toBeNull();
      expect(tester.testElement).toContainText('Envoyer une alerte aux abonnés de Loire');
    });
  });

  describe('with many governments', () => {
    beforeEach(async () => {
      governmentService.search.and.returnValue(
        of(
          createPage(
            [
              { id: 42, name: 'Loire' },
              { id: 69, name: 'Lyon' }
            ] as Array<GovernmentModel>,
            20,
            2,
            0
          )
        )
      );

      tester = new NotificationEditionComponentTester();
      await tester.stable();

      jasmine.clock().install();
      jasmine.clock().mockDate();
    });

    afterEach(() => jasmine.clock().uninstall());

    it('should display empty form', () => {
      expect(tester.governmentTypeahead).toHaveValue('');
      expect(tester.subject).toHaveValue('');
      expect(tester.message).toHaveValue('');
      expect(tester.url).toHaveValue('');
      expect(tester.send.disabled).toBeFalse();
    });

    it('should validate', async () => {
      expect(tester.testElement).toContainText('0 / 250');

      await tester.send.click();

      // government, subject, message are required
      expect(tester.errors.length).toBe(3);

      await tester.url.fillWith('not a url');

      // government, subject, message are required, url is invalid
      expect(tester.errors.length).toBe(4);
      expect(tester.testElement).toContainText(`L'URL est invalide`);

      await tester.message.fillWith('0123456789'.repeat(30));

      // government, subject are required, url is invalid, message is too long
      expect(tester.errors.length).toBe(4);
      expect(tester.testElement).toContainText('Le message ne peut dépasser 250 caractères');
      expect(tester.testElement).toContainText('300 / 250');

      expect(notificationService.send).not.toHaveBeenCalled();
    });

    it('should send message', async () => {
      await tester.governmentTypeahead.fillWith('L');
      jasmine.clock().tick(400);
      expect(tester.governmentResults.length).toBe(2);
      expect(tester.governmentResults[0]).toHaveTrimmedText('Loire');
      expect(governmentService.search).toHaveBeenCalledWith('L', 0);
      await tester.governmentResults[0].click();
      expect(tester.governmentTypeahead).toHaveValue('Loire');

      await tester.subject.fillWith('Test');
      await tester.message.fillWith('Test message');
      await tester.url.fillWith('https://loire.fr');

      const sentSubject = new Subject<void>();
      notificationService.send.and.returnValue(sentSubject);
      await tester.send.click();

      const expectedCommand: NotificationCommand = {
        governmentId: 42,
        subject: 'Test',
        message: 'Test message',
        url: 'https://loire.fr'
      };
      expect(notificationService.send).toHaveBeenCalledWith(expectedCommand);

      expect(tester.send.disabled).toBeTrue();
      sentSubject.next();
      await tester.stable();

      expect(successService.success).toHaveBeenCalled();
      expect(tester.send).toBeNull();
      expect(tester.testElement).toContainText(
        `L'alerte est en cours d'envoi à tous les abonnés de Loire`
      );
    });
  });
});
