import { TestBed } from '@angular/core/testing';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';
import { NotificationService } from './notification.service';
import { NotificationCommand } from './notification.model';

describe('NotificationService', () => {
  let http: HttpTestingController;
  let service: NotificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NotificationService);
  });

  it('should send a notification', () => {
    let done = false;
    const command: NotificationCommand = {
      governmentId: 42,
      subject: 'Test',
      message: 'Message',
      url: 'https://loire.fr'
    };
    service.send(command).subscribe(() => (done = true));

    const request = http.expectOne({
      method: 'POST',
      url: '/api/notifications'
    });
    expect(request.request.body).toEqual(command);
    request.flush(null);

    expect(done).toBeTrue();
  });
});
