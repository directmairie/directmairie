import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { GovernmentService } from '../government.service';
import {
  BehaviorSubject,
  catchError,
  combineLatest,
  debounceTime,
  distinctUntilChanged,
  map,
  OperatorFunction,
  switchMap,
  tap
} from 'rxjs';
import { GovernmentModel } from '../models/government.model';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { DecimalPipe } from '@angular/common';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import * as icons from '../../icons';
import { NotificationService } from './notification.service';
import { NotificationCommand } from './notification.model';
import { SuccessService } from '../../success.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RequiredComponent } from '../../shared/required/required.component';
import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { toSignal } from '@angular/core/rxjs-interop';

type State = 'editing' | 'sending' | 'sent';

interface ViewModel {
  uniqueGovernment: GovernmentModel | null;
  state: State;
}

@Component({
  selector: 'dm-notification-edition',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    ValidationErrorsComponent,
    DecimalPipe,
    InvalidFormControlDirective,
    ValidationErrorDirective,
    FontAwesomeModule,
    RequiredComponent,
    NgbTypeahead
  ],
  templateUrl: './notification-edition.component.html',
  styleUrl: './notification-edition.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationEditionComponent {
  private readonly governmentService = inject(GovernmentService);
  private readonly notificationService = inject(NotificationService);
  private readonly successService = inject(SuccessService);

  readonly maxMessageLength = 250;
  readonly icons = icons;

  readonly stateSubject = new BehaviorSubject<'editing' | 'sending' | 'sent'>('editing');

  readonly governmentTypeahead: OperatorFunction<string, Array<GovernmentModel>> = text$ =>
    text$.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(query =>
        this.governmentService.search(query, 0).pipe(
          map(page => page.content),
          catchError(() => [])
        )
      )
    );
  readonly governmentFormatter = (government: GovernmentModel) => government.name;

  readonly vm: Signal<ViewModel | undefined>;
  private readonly fb = inject(NonNullableFormBuilder);
  readonly form = this.fb.group({
    government: this.fb.control<GovernmentModel | null>(null, Validators.required),
    subject: ['', [Validators.required, Validators.maxLength(80)]],
    message: ['', [Validators.required, Validators.maxLength(this.maxMessageLength)]],
    url: ['', [Validators.pattern(/^http(s?):\/\/(\S+)/)]]
  });

  constructor() {
    this.vm = toSignal(
      combineLatest({
        uniqueGovernment: this.governmentService.search(null, 0).pipe(
          map(page => (page.content.length === 1 ? page.content[0] : null)),
          tap(uniqueGovernment => this.form.controls.government.setValue(uniqueGovernment))
        ),
        state: this.stateSubject
      })
    );
  }

  send() {
    if (!this.form.valid) {
      return;
    }

    this.stateSubject.next('sending');
    const formValue = this.form.value;
    const command: NotificationCommand = {
      governmentId: formValue.government!.id,
      subject: formValue.subject!,
      message: formValue.message!,
      url: formValue.url!.trim() || null
    };
    this.notificationService.send(command).subscribe({
      next: () => {
        this.successService.success(`Notification en cours d'envoi`);
        this.stateSubject.next('sent');
      },
      error: () => {
        this.stateSubject.next('editing');
      }
    });
  }
}
