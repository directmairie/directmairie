import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { NotificationCommand } from './notification.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private http = inject(HttpClient);

  send(command: NotificationCommand): Observable<void> {
    return this.http.post<void>('/api/notifications', command);
  }
}
