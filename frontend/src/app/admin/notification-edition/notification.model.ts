export interface NotificationCommand {
  governmentId: number;
  subject: string;
  message: string;
  url: string | null;
}
