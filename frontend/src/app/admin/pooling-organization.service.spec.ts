import { PoolingOrganizationService } from './pooling-organization.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {
  PoolingOrganizationDetailedModel,
  PoolingOrganizationModel
} from './models/pooling-organization.model';
import { provideHttpClient } from '@angular/common/http';

describe('PoolingOrganizationService', () => {
  let service: PoolingOrganizationService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(PoolingOrganizationService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should list pooling organizations', () => {
    let orgsReceived: Array<PoolingOrganizationModel> = [];
    service.search().subscribe(response => (orgsReceived = response));

    const request = http.expectOne('/api/pooling-organizations');
    const org: PoolingOrganizationModel = {
      id: 1,
      name: 'Rhone'
    };
    request.flush([org]);

    expect(orgsReceived).toEqual([org]);
  });

  [
    { query: 'foo', expectedUrl: '/api/pooling-organizations?query=foo' },
    { query: '', expectedUrl: '/api/pooling-organizations' },
    { query: null, expectedUrl: '/api/pooling-organizations' }
  ].forEach(data => {
    it(`should search organizations with query ${data.query}`, () => {
      const expectedOrganizations = [
        {
          id: 1000
        }
      ] as Array<PoolingOrganizationModel>;

      let actualOrganizations: Array<PoolingOrganizationModel> | null = null;

      service.search(data.query).subscribe(organizations => (actualOrganizations = organizations));

      http
        .expectOne({
          method: 'GET',
          url: data.expectedUrl
        })
        .flush(expectedOrganizations);

      expect(actualOrganizations!).toEqual(expectedOrganizations);
    });
  });

  it('should get organization', () => {
    const expectedOrganization = {
      id: 1000
    } as PoolingOrganizationDetailedModel;

    let actualOrganization: PoolingOrganizationDetailedModel | null = null;

    service.get(1000).subscribe(organization => (actualOrganization = organization));

    http
      .expectOne({
        method: 'GET',
        url: '/api/pooling-organizations/1000'
      })
      .flush(expectedOrganization);

    expect(actualOrganization!).toEqual(expectedOrganization);
  });
});
