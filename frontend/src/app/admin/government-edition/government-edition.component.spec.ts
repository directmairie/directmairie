import { TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ActivatedRouteStub,
  ComponentTester,
  createMock,
  provideAutomaticChangeDetection,
  stubRoute,
  TestElement,
  TestInput
} from 'ngx-speculoos';
import { of, Subject, throwError } from 'rxjs';

import { GovernmentService } from '../government.service';
import { GovernmentEditionComponent } from './government-edition.component';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { GeolocationService } from '../../geolocation.service';
import { UserModel } from '../../registration/models/registration.model';
import { PoolingOrganizationService } from '../pooling-organization.service';
import {
  GovernmentCommand,
  GovernmentDetailedModel,
  GovernmentModel
} from '../models/government.model';
import { PoolingOrganizationModel } from '../models/pooling-organization.model';
import { IssueCategoryService } from '../../issue-category.service';
import { OTHER_CATEGORY_ID, OTHER_ISSUE_GROUP_ID } from '../../models/issue-category.model';
import { SuccessService } from '../../success.service';
import { UserSelectorComponent } from '../user-selector/user-selector.component';
import { UserService } from '../user.service';

class GovernmentEditionTester extends ComponentTester<GovernmentEditionComponent> {
  constructor() {
    super(GovernmentEditionComponent);
  }

  get title() {
    return this.element('h1');
  }

  get osmId() {
    return this.input('#osm-id')!;
  }

  get name() {
    return this.input('#name')!;
  }

  get nameHelp() {
    return this.element('#name-help');
  }

  get code() {
    return this.input('#code')!;
  }

  get url() {
    return this.input('#url')!;
  }

  get poolingOrgForm() {
    return this.element('#pooling-organization-group') as TestElement<HTMLDivElement>;
  }

  get poolingOrg() {
    return this.select('#pooling-organization')!;
  }

  get contactEmail() {
    return this.input('#contactEmail')!;
  }

  get submit() {
    return this.button('#submit')!;
  }

  get adminSelector() {
    return this.component(UserSelectorComponent);
  }

  get categories() {
    return this.elements('.category') as Array<TestInput>;
  }

  get barrierCategoryCheckbox() {
    return this.categories[0];
  }

  get holeCategoryCheckbox() {
    return this.categories[1];
  }

  get contacts() {
    return this.elements('.contact') as Array<TestInput>;
  }
}

describe('GovernmentEditionComponent', () => {
  let service: jasmine.SpyObj<GovernmentService>;
  let geolocationService: jasmine.SpyObj<GeolocationService>;
  let poolingOrganizationService: jasmine.SpyObj<PoolingOrganizationService>;
  let issueCategoryService: jasmine.SpyObj<IssueCategoryService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;
  let route: ActivatedRouteStub;
  let tester: GovernmentEditionTester;

  const user1 = { id: 1, email: 'user1@rhone.fr' } as UserModel;
  const user2 = { id: 2, email: 'user2@rhone.fr' } as UserModel;
  const otherCategoryGroup = {
    id: OTHER_ISSUE_GROUP_ID,
    label: 'Other',
    categories: [{ id: OTHER_CATEGORY_ID, label: 'Other', descriptionRequired: true }]
  };
  const barrierCategory = { id: 11, label: 'Barrier', descriptionRequired: true };
  const potholeCategory = { id: 12, label: 'Pothole', descriptionRequired: true };
  const highwayCategoryGroup = {
    id: 2,
    label: 'Highways',
    categories: [barrierCategory, potholeCategory]
  };

  beforeEach(() => {
    service = createMock(GovernmentService);
    geolocationService = createMock(GeolocationService);
    poolingOrganizationService = createMock(PoolingOrganizationService);
    issueCategoryService = createMock(IssueCategoryService);
    successService = createMock(SuccessService);
    route = stubRoute();

    TestBed.configureTestingModule({
      providers: [
        provideAutomaticChangeDetection(),
        { provide: ActivatedRoute, useValue: route },
        { provide: GovernmentService, useValue: service },
        { provide: GeolocationService, useValue: geolocationService },
        { provide: PoolingOrganizationService, useValue: poolingOrganizationService },
        { provide: IssueCategoryService, useValue: issueCategoryService },
        { provide: SuccessService, useValue: successService },
        { provide: UserService, useValue: createMock(UserService) }
      ]
    });
    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();
    poolingOrganizationService.search.and.returnValue(
      of([
        {
          id: 1,
          name: 'pool'
        }
      ])
    );
    issueCategoryService.getAllCategories.and.returnValue(
      of([highwayCategoryGroup, otherCategoryGroup])
    );
    router = TestBed.inject(Router);
    spyOn(router, 'navigate');
  });

  describe('in create mode', () => {
    it('should have a title', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      expect(tester.title).toHaveText('Créer une collectivité');
    });

    it('should display an empty form', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      expect(tester.osmId).toHaveValue('');
      expect(tester.name).toHaveValue('');
      expect(tester.code).toHaveValue('');
      expect(tester.contactEmail).toHaveValue('');
      expect(tester.adminSelector.users()).toEqual([]);
    });

    it('should validate and submit the form', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      service.create.and.returnValue(
        of({
          id: 1234
        } as GovernmentModel)
      );
      const nominatimResult = new Subject<string>();
      geolocationService.findGovernmentInfo.and.returnValue(nominatimResult);
      await tester.url.fillWith('loire.fr'); // invalid URL
      await tester.submit.click();

      expect(tester.testElement).toContainText(`L'identifiant Open Street Map est obligatoire`);
      expect(tester.testElement).toContainText('Le nom est obligatoire');
      expect(tester.testElement).toContainText('Le SIREN est obligatoire');
      expect(tester.testElement).toContainText("L'URL doit être valide (et commencer");
      expect(tester.testElement).toContainText('Le courriel de contact par défaut');

      expect(service.create).not.toHaveBeenCalled();

      await tester.contactEmail.fillWith('blabla');
      expect(tester.testElement).toContainText(
        'Le courriel de contact par défaut doit être une adresse courriel valide'
      );

      await tester.osmId.fillWith('42');
      await tester.osmId.dispatchEvent(new Event('blur'));
      // submit is unavailable during validation
      expect(tester.submit.disabled).toBe(true);
      nominatimResult.next('Loire');
      nominatimResult.complete();
      await tester.change();
      expect(tester.submit.disabled).toBe(false);
      await tester.name.fillWith('Loire');
      await tester.code.fillWith('42D');
      await tester.url.fillWith('https://loire.fr');
      await tester.contactEmail.fillWith('contact@loire.fr');

      await tester.submit.click();
      expect(service.create).toHaveBeenCalledWith({
        osmId: 42,
        name: 'Loire',
        code: '42D',
        url: 'https://loire.fr',
        contactEmail: 'contact@loire.fr',
        issueCategoryIds: [],
        contacts: [],
        administratorIds: [],
        poolingOrganizationId: 1
      });

      expect(router.navigate).toHaveBeenCalledWith(['/admin/governments', 1234]);
      expect(successService.success).toHaveBeenCalledWith('La collectivité a été créée.');
    });

    it('should validate with Nominatim and use info if available', async () => {
      geolocationService.findGovernmentInfo.and.returnValues(
        // 43
        throwError(() => new Error('unable to geocode')),
        // 44
        of('Rhone'),
        // 45
        of('Loire'),
        // 43 again
        throwError(() => new Error('unable to geocode'))
      );

      tester = new GovernmentEditionTester();
      await tester.change();

      // emulate empty field
      await tester.osmId.dispatchEvent(new Event('blur'));
      expect(geolocationService.findGovernmentInfo).not.toHaveBeenCalled();
      expect(tester.nameHelp).toBeNull();

      // emulate a non existing code
      await tester.osmId.fillWith('43');
      await tester.osmId.dispatchEvent(new Event('blur'));

      expect(geolocationService.findGovernmentInfo).toHaveBeenCalledWith(43);
      await tester.change();

      expect(tester.testElement).toContainText(`L'identifiant Open Street Map est invalide`);
      expect(tester.componentInstance.governmentForm.get('osmId')!.invalid).toBe(true);
      expect(tester.nameHelp).toBeNull();

      // emulate an existing code
      await tester.osmId.fillWith('44');
      await tester.osmId.dispatchEvent(new Event('blur'));

      expect(geolocationService.findGovernmentInfo).toHaveBeenCalledWith(44);
      await tester.change();

      expect(tester.testElement).not.toContainText(`L'identifiant Open Street Map est invalide`);
      expect(tester.componentInstance.governmentForm.get('osmId')!.invalid).toBe(false);

      // should have filled the name input
      expect(tester.name).toHaveValue('Rhone');

      // emulate another existing code
      await tester.osmId.fillWith('45');
      await tester.osmId.dispatchEvent(new Event('blur'));

      expect(geolocationService.findGovernmentInfo).toHaveBeenCalledWith(45);
      await tester.change();

      expect(tester.testElement).not.toContainText(`L'identifiant Open Street Map est invalide`);
      expect(tester.componentInstance.governmentForm.controls.osmId.invalid).toBe(false);
      expect(tester.componentInstance.lastSearchedOsmName()).toBe('Loire');
      expect(tester.componentInstance.lastSearchedOsmId()).toBe(45);

      // should not have filled the name input because it was already filled
      // should just display a warning below the field
      expect(tester.name).toHaveValue('Rhone');
      expect(tester.nameHelp).toContainText(
        `Le nom associé à l'identifiant Open Street Map 45 est Loire`
      );

      // emulate another non existing code
      await tester.osmId.fillWith('43');
      await tester.osmId.dispatchEvent(new Event('blur'));

      expect(geolocationService.findGovernmentInfo).toHaveBeenCalledWith(43);
      await tester.change();

      expect(tester.testElement).toContainText(`L'identifiant Open Street Map est invalide`);
      expect(tester.componentInstance.governmentForm.controls.osmId.invalid).toBe(true);

      // should not have filled the name input because it was already filled
      // should not display a warning below the field
      expect(tester.name).toHaveValue('Rhone');
      expect(tester.nameHelp).toBeNull();
    });

    it('should get the selected admins from the user selector component and submit the form', async () => {
      service.create.and.returnValue(of({} as GovernmentModel));

      tester = new GovernmentEditionTester();
      await tester.change();
      tester.adminSelector.users.set([user1, user2]);
      await tester.change();
      expect(tester.componentInstance.admins()).toEqual([user1, user2]);

      // fill the rest of the form
      await tester.osmId.fillWith('42');
      await tester.name.fillWith('Loire');
      await tester.code.fillWith('42D');
      await tester.url.fillWith('https://loire.fr');
      await tester.contactEmail.fillWith('contact@loire.fr');

      // submit with the selected admin id
      await tester.submit.click();
      expect(service.create).toHaveBeenCalledWith({
        osmId: 42,
        name: 'Loire',
        code: '42D',
        url: 'https://loire.fr',
        contactEmail: 'contact@loire.fr',
        issueCategoryIds: [],
        contacts: [],
        administratorIds: [1, 2],
        poolingOrganizationId: 1
      });
    });

    it('should display the pooling select if there are several ones', async () => {
      // given several pooling orgs
      const orgs: Array<PoolingOrganizationModel> = [
        { id: 1, name: 'pool 1' },
        { id: 2, name: 'pool 2' }
      ];

      poolingOrganizationService.search.and.returnValue(of(orgs));

      // when displaying the component
      tester = new GovernmentEditionTester();
      await tester.change();

      // then it should display the select
      expect(tester.poolingOrgForm).not.toHaveClass('d-none');
      expect(tester.poolingOrg.optionLabels).toEqual(['', 'pool 1', 'pool 2']);
      await tester.poolingOrg.selectIndex(0);

      await tester.submit.click();
      expect(tester.testElement).toContainText('Le mutualisant est obligatoire');

      await tester.poolingOrg.selectIndex(1);
      expect(tester.testElement).not.toContainText('Le mutualisant est obligatoire');
    });

    it('should display the categories and allow to select them with a contact email', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      // given 2 groups with categories
      expect(issueCategoryService.getAllCategories).toHaveBeenCalled();
      expect(tester.categories.length).toBe(3);

      // the "normal" categories are not selected and enabled
      expect(tester.barrierCategoryCheckbox.checked).toBe(false);
      expect(tester.barrierCategoryCheckbox.disabled).toBe(false);
      expect(tester.holeCategoryCheckbox.checked).toBe(false);
      expect(tester.holeCategoryCheckbox.disabled).toBe(false);

      // the "other" category should be already selected and disabled
      const otherCategory = tester.categories[2];
      expect(otherCategory.checked).toBe(true);
      expect(otherCategory.disabled).toBe(true);

      // with only one contact field for the "other" category
      expect(tester.contacts.length).toBe(1);

      // when checking one "normal" category
      await tester.holeCategoryCheckbox.check();

      // then we should have another contact field
      expect(tester.contacts.length).toBe(2);

      // should still be here if checking another category of the group
      await tester.barrierCategoryCheckbox.check();
      expect(tester.contacts.length).toBe(2);

      // stay if we remove all but one
      await tester.holeCategoryCheckbox.uncheck();
      expect(tester.contacts.length).toBe(2);

      // and disappear if we uncheck the last one
      await tester.barrierCategoryCheckbox.uncheck();
      expect(tester.contacts.length).toBe(1);

      // the email should be valid
      const otherEmailContact = tester.contacts[0];
      await otherEmailContact.fillWith('invalid');
      await otherEmailContact.dispatchEvent(new Event('blur'));
      expect(tester.testElement).toContainText(
        `L'adresse courriel de contact doit être une adresse courriel valide`
      );

      await otherEmailContact.fillWith('jane@mail.com');
      await otherEmailContact.dispatchEvent(new Event('blur'));
      expect(tester.testElement).not.toContainText(
        `L'adresse courriel de contact doit être une adresse courriel valide`
      );

      // check again a category, but with no email filled
      await tester.barrierCategoryCheckbox.check();

      // the value send to the server should contain the selected category ids and the contact emails
      await tester.osmId.fillWith('42');
      tester.componentInstance.validatingOsmId.set(false);
      await tester.name.fillWith('Loire');
      await tester.code.fillWith('42D');
      await tester.url.fillWith('https://loire.fr');
      await tester.contactEmail.fillWith('contact@loire.fr');

      service.create.and.returnValue(of({} as GovernmentModel));
      await tester.submit.click();

      expect(service.create).toHaveBeenCalledWith({
        osmId: 42,
        name: 'Loire',
        code: '42D',
        url: 'https://loire.fr',
        contactEmail: 'contact@loire.fr',
        // the id of "Other" is not sent (because it is disabled and automatically added server-side)
        issueCategoryIds: [barrierCategory.id],
        // contact for group "Other"
        contacts: [{ email: 'jane@mail.com', issueGroupId: OTHER_ISSUE_GROUP_ID }],
        administratorIds: [],
        poolingOrganizationId: 1
      });
    });
  });

  describe('in update mode', () => {
    let editedGovernment: GovernmentDetailedModel;

    beforeEach(() => {
      editedGovernment = {
        poolingOrganizationId: 1,
        id: 12,
        name: 'Rhône',
        code: '69',
        osmId: 7378,
        issueGroups: [
          otherCategoryGroup,
          {
            id: highwayCategoryGroup.id,
            label: highwayCategoryGroup.label,
            categories: [barrierCategory]
          }
        ],
        contacts: [{ email: 'highway@rhone.fr', issueGroup: highwayCategoryGroup }],
        contactEmail: 'default@rhone.fr',
        administrators: [user1],
        logo: null,
        url: 'https://rhone.fr'
      };

      route.setParam('governmentId', editedGovernment.id.toString());

      service.get.and.returnValue(of(editedGovernment));
    });

    it('should have a title', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      expect(tester.title).toContainText('Modifier la collectivité Rhône');
    });

    it('should display a populated form', async () => {
      tester = new GovernmentEditionTester();
      await tester.change();

      expect(tester.poolingOrgForm).toHaveClass('d-none');
      expect(tester.name).toHaveValue('Rhône');
      expect(tester.code).toHaveValue('69');
      expect(tester.contactEmail).toHaveValue('default@rhone.fr');
      expect(tester.adminSelector.users()).toEqual(editedGovernment.administrators);
      expect(tester.barrierCategoryCheckbox).toBeChecked();
      expect(tester.holeCategoryCheckbox).not.toBeChecked();
    });

    it('should update the organization', async () => {
      service.update.and.returnValue(of(undefined as unknown as GovernmentModel));

      tester = new GovernmentEditionTester();
      await tester.change();

      await tester.name.fillWith('Rhône updated');
      await tester.code.fillWith('69 updated');
      await tester.url.fillWith('https://mon-rhone.fr');
      await tester.contactEmail.fillWith('default-updated@rhone.fr');
      tester.adminSelector.users.set([{ id: 1 }, { id: 2 }] as Array<UserModel>);
      await tester.barrierCategoryCheckbox.uncheck();
      await tester.holeCategoryCheckbox.check();

      await tester.submit.click();

      const expectedCommand: GovernmentCommand = {
        poolingOrganizationId: 1,
        name: 'Rhône updated',
        code: '69 updated',
        url: 'https://mon-rhone.fr',
        osmId: 7378,
        // the id of "Other" is not sent (because it is disabled and automatically added server-side)
        issueCategoryIds: [potholeCategory.id],
        contactEmail: 'default-updated@rhone.fr',
        contacts: [{ email: 'highway@rhone.fr', issueGroupId: highwayCategoryGroup.id }],
        administratorIds: [1, 2]
      };
      expect(service.update).toHaveBeenCalledWith(editedGovernment.id, expectedCommand);
      expect(router.navigate).toHaveBeenCalledWith(['/admin/governments', 12]);
      expect(successService.success).toHaveBeenCalledWith('La collectivité a été modifiée.');
    });
  });
});
