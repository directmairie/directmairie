import { ChangeDetectionStrategy, Component, inject, Signal, signal } from '@angular/core';
import {
  FormArray,
  FormControl,
  FormGroup,
  NonNullableFormBuilder,
  ReactiveFormsModule,
  Validators
} from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { combineLatest, finalize, map, Observable, of, tap } from 'rxjs';

import { GovernmentService } from '../government.service';
import { GeolocationService } from '../../geolocation.service';
import { UserModel } from '../../registration/models/registration.model';
import { PoolingOrganizationModel } from '../models/pooling-organization.model';
import { PoolingOrganizationService } from '../pooling-organization.service';
import {
  GovernmentCommand,
  GovernmentContactCommand,
  GovernmentDetailedModel
} from '../models/government.model';
import { IssueCategoryService } from '../../issue-category.service';
import { IssueGroupModel, OTHER_CATEGORY_ID } from '../../models/issue-category.model';
import { SuccessService } from '../../success.service';
import { UserSelectorComponent } from '../user-selector/user-selector.component';
import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { toSignal } from '@angular/core/rxjs-interop';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';

interface ViewModel {
  mode: 'create' | 'update';
  editedGovernment: GovernmentDetailedModel | null;
  poolingOrganizations: Array<PoolingOrganizationModel>;
  issueGroups: Array<IssueGroupModel>;
}

/**
 * The component used to create or update a government
 */
@Component({
  selector: 'dm-government-edition',
  templateUrl: './government-edition.component.html',
  styleUrl: './government-edition.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    UserSelectorComponent,
    RouterLink,
    InvalidFormControlDirective
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GovernmentEditionComponent {
  private readonly router = inject(Router);
  private readonly governmentService = inject(GovernmentService);
  private readonly geolocationService = inject(GeolocationService);
  private readonly successService = inject(SuccessService);

  private readonly fb = inject(NonNullableFormBuilder);
  readonly issueGroupsFormArray = this.fb.array<
    FormGroup<{ categories: FormArray<FormControl<boolean>>; contact?: FormControl<string> }>
  >([]);
  readonly governmentForm = this.fb.group({
    poolingOrganizationId: [null as number | null, Validators.required],
    osmId: [null as number | null, Validators.required],
    name: [null as string | null, Validators.required],
    url: [null as string | null, Validators.pattern(/^http(s?):\/\/(\S+)/)],
    code: [null as string | null, Validators.required],
    contactEmail: [null as string | null, [Validators.required, Validators.email]],
    issueGroupsFormArray: this.issueGroupsFormArray
  });

  readonly validatingOsmId = signal(false);
  readonly lastSearchedOsmName = signal<string | null>(null);
  readonly lastSearchedOsmId = signal<number | null>(null);

  readonly vm: Signal<ViewModel | undefined>;
  readonly admins = signal<Array<UserModel>>([]);

  constructor() {
    const route = inject(ActivatedRoute);
    const governmentId = route.snapshot.paramMap.get('governmentId');
    const poolingOrganizationService = inject(PoolingOrganizationService);
    const issueCategoryService = inject(IssueCategoryService);

    // fetch the government edited if update mode
    // and fetch all categories
    // and add them in the form, with one email contact field per group,
    // if at least one category of the group is selected

    const government$: Observable<GovernmentDetailedModel | null> = governmentId
      ? this.governmentService.get(parseInt(governmentId))
      : of(null);
    const issueGroups$: Observable<Array<IssueGroupModel>> =
      issueCategoryService.getAllCategories();
    const poolingOrganizations$: Observable<Array<PoolingOrganizationModel>> =
      poolingOrganizationService.search();

    const data$ = combineLatest({
      editedGovernment: government$,
      issueGroups: issueGroups$,
      poolingOrganizations: poolingOrganizations$
    });

    const vm$: Observable<ViewModel> = data$.pipe(
      map(({ editedGovernment, issueGroups, poolingOrganizations }) => {
        return {
          editedGovernment,
          mode: editedGovernment ? ('update' as const) : ('create' as const),
          poolingOrganizations,
          issueGroups
        };
      }),
      tap(vm => this.initializeForm(vm))
    );
    this.vm = toSignal(vm$);
  }

  save() {
    if (this.governmentForm.invalid || this.validatingOsmId()) {
      return;
    }
    const vm = this.vm()!;

    // build the issue categories selected ids
    // and the contact associated to the group if it exists
    const issueCategoryIds: Array<number> = [];
    const contacts: Array<GovernmentContactCommand> = [];
    this.issueGroupsFormArray.value.forEach((groupValue, groupIndex) => {
      const issueGroup = vm.issueGroups[groupIndex];
      const categories = issueGroup.categories;
      categories.forEach((category, categoryIndex) => {
        // selected category
        if (groupValue.categories && groupValue.categories[categoryIndex]) {
          issueCategoryIds.push(category.id);
        }
      });
      if (groupValue.contact) {
        contacts.push({
          email: groupValue.contact,
          issueGroupId: issueGroup.id
        });
      }
    });
    const governmentFormValue = this.governmentForm.value;
    const governmentCommand: GovernmentCommand = {
      osmId: governmentFormValue.osmId!,
      name: governmentFormValue.name!,
      code: governmentFormValue.code!,
      url: governmentFormValue.url!,
      issueCategoryIds,
      contactEmail: governmentFormValue.contactEmail!,
      contacts,
      administratorIds: this.admins().map(user => user.id),
      poolingOrganizationId: governmentFormValue.poolingOrganizationId!
    };
    let observable: Observable<number>;
    let message: string;

    if (vm.mode === 'create') {
      observable = this.governmentService
        .create(governmentCommand)
        .pipe(map(government => government.id));
      message = 'La collectivité a été créée.';
    } else {
      observable = this.governmentService
        .update(vm.editedGovernment!.id, governmentCommand)
        .pipe(map(() => vm.editedGovernment!.id));
      message = 'La collectivité a été modifiée.';
    }

    observable.subscribe(id => {
      this.router.navigate(['/admin/governments', id]);
      this.successService.success(message);
    });
  }

  /**
   * If the name field is not already filled,
   * we help the user by fetching the info from nominatim when the OSM id is entered (on focus loss)
   */
  searchInfoFromOsmId() {
    const osmIdControl = this.governmentForm.controls.osmId;
    const osmId = osmIdControl.value;
    if (osmId) {
      this.lastSearchedOsmId.set(osmId);
      this.validatingOsmId.set(true);
      this.geolocationService
        .findGovernmentInfo(osmId)
        .pipe(finalize(() => this.validatingOsmId.set(false)))
        .subscribe({
          next: label => {
            // the osm id is correct
            // we remove the error if there was one
            if (osmIdControl.hasError('invalidOsmId')) {
              const errors = { ...osmIdControl.errors };
              delete errors.invalidOsmId;
              // reset errors to make sure of the validity of the field
              osmIdControl.setErrors(errors);
            }
            // we fill the name field if it is empty
            this.lastSearchedOsmName.set(label);
            const nameControl = this.governmentForm.controls.name;
            if (!nameControl.value) {
              nameControl.setValue(label);
            }
          },
          error: () => {
            // lookup failed
            // we set an error
            osmIdControl.setErrors({
              invalidOsmId: true
            });
          }
        });
    }
  }

  /**
   * Display help below the name field if the osmId field is valid,
   * and the name currently in the name input is different than the name returned by Nominatim
   * Only displayed if the current osmId is the same than the last searched
   */
  isNameHelpDisplayed() {
    return (
      this.governmentForm.controls.osmId.valid &&
      this.lastSearchedOsmId &&
      this.governmentForm.controls.osmId.value === this.lastSearchedOsmId() &&
      this.lastSearchedOsmName() &&
      this.lastSearchedOsmName() !== this.governmentForm.controls.name.value &&
      !this.validatingOsmId()
    );
  }

  shouldDisplayContact(groupIndex: number) {
    return this.issueGroupsFormArray.at(groupIndex).controls.contact;
  }

  private initializeForm(vm: ViewModel) {
    const selectedCategoryIds = new Set<number>();

    if (vm.editedGovernment) {
      this.admins.set(vm.editedGovernment.administrators);
      this.initializeGovernmentControls(vm.editedGovernment);

      // build the selected categories
      vm.editedGovernment.issueGroups.forEach(group =>
        group.categories.forEach(category => selectedCategoryIds.add(category.id))
      );
    }
    this.initializeIssueGroupControls(
      vm.issueGroups,
      selectedCategoryIds,
      vm.mode,
      vm.editedGovernment
    );
    this.initializePoolingOrganizationControls(vm.poolingOrganizations, vm.mode);
  }

  private initializeGovernmentControls(government: GovernmentDetailedModel) {
    this.governmentForm.controls.poolingOrganizationId.setValue(government.poolingOrganizationId);
    this.governmentForm.controls.name.setValue(government.name);
    this.governmentForm.controls.code.setValue(government.code);
    this.governmentForm.controls.url.setValue(government.url);
    this.governmentForm.controls.osmId.setValue(government.osmId);
    this.governmentForm.controls.contactEmail.setValue(government.contactEmail);
  }

  private initializeIssueGroupControls(
    issueGroups: Array<IssueGroupModel>,
    selectedCategoryIds: Set<number>,
    mode: 'create' | 'update',
    editedGovernment: GovernmentDetailedModel | null
  ) {
    // we build one FormControl for each group to enter the contact email
    const contactEmailControlsMap = new Map<number, FormControl<string>>(); // issueGroupId -> contactFormControl
    // we build one FormGroup per issue category group
    // containing a FormArray of categories (one FormControl per category => a checkbox in the template)
    // and one FormControl for the contact (an input for the email in the template)
    const issueGroupFormGroups = issueGroups.map((group, groupIndex) => {
      // when we update the government, we populate the field with the existing contact email
      // if there is one
      const issueGroup = issueGroups[groupIndex];
      let contactValue = '';
      if (mode === 'update' && editedGovernment!.contacts) {
        const existingContact = editedGovernment!.contacts.find(
          contact => contact.issueGroup.id === issueGroup.id
        );
        contactValue = existingContact ? existingContact.email : '';
      }
      contactEmailControlsMap.set(issueGroup.id, this.fb.control(contactValue, [Validators.email]));
      return this.fb.group<{
        categories: FormArray<FormControl<boolean>>;
        contact?: FormControl<string>;
      }>({
        categories: this.fb.array(
          group.categories.map(category => {
            // special case for the Other category of the Other group (category with id OTHER_CATEGORY_ID)
            // it should always be selected and disabled
            if (category.id === OTHER_CATEGORY_ID) {
              // select the other category and disable it
              const otherCategoryControl = this.fb.control(true);
              otherCategoryControl.disable();
              return otherCategoryControl;
            }
            return this.fb.control(selectedCategoryIds.has(category.id));
          })
        )
      });
    });

    issueGroupFormGroups.forEach((group, groupIndex) => {
      // associated issue group
      const issueGroup = issueGroups[groupIndex];
      // we subscribe to the value changes in each group
      // to dynamically add or remove the contact field
      const categoriesArray = group.controls.categories;
      categoriesArray.valueChanges.subscribe(categories => {
        // check if at least one is selected
        const atLeastOneSelected = categories.some((category: boolean) => category);
        if (atLeastOneSelected) {
          // add the FormControl created previously, with the previous value if there was one
          group.addControl('contact', contactEmailControlsMap.get(issueGroup.id)!);
        } else {
          group.removeControl('contact');
        }
      });
      this.issueGroupsFormArray.push(group);
      // manually trigger an update
      // to make sure that the valueChanges observable emits and the contact fields are added
      categoriesArray.updateValueAndValidity({ onlySelf: true, emitEvent: true });
    });
  }

  private initializePoolingOrganizationControls(
    poolingOrganizations: Array<PoolingOrganizationModel>,
    mode: 'create' | 'update'
  ) {
    // in creation mode, if there is only one, manually set it
    if (mode === 'create' && poolingOrganizations.length === 1) {
      this.governmentForm.controls.poolingOrganizationId.setValue(poolingOrganizations[0].id);
    }
  }
}
