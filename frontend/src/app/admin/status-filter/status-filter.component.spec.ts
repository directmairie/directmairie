import { TestBed } from '@angular/core/testing';
import { ComponentTester, TestInput } from 'ngx-speculoos';

import { StatusFilterComponent } from './status-filter.component';
import { IssueStatus, LISTABLE_STATUSES } from '../../models/issue.model';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';

@Component({
  template: `<dm-status-filter [(statusFilter)]="statusFilter" />`,
  imports: [StatusFilterComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly statusFilter = signal<Array<IssueStatus>>([]);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get checkboxes() {
    return this.elements('input[type=checkbox]');
  }

  get createdCheckbox(): TestInput {
    return this.elements<HTMLInputElement>('input[type=checkbox]')[0];
  }

  get resolvedCheckbox(): TestInput {
    return this.elements<HTMLInputElement>('input[type=checkbox]')[1];
  }

  get rejectedCheckbox(): TestInput {
    return this.elements<HTMLInputElement>('input[type=checkbox]')[2];
  }
}

describe('StatusFilterComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({});
    tester = new TestComponentTester();
    await tester.stable();
  });

  it('should display all status', () => {
    expect(tester.checkboxes.length).toBe(LISTABLE_STATUSES.length);
  });

  it('should select statuses given in input', async () => {
    tester.componentInstance.statusFilter.set(['CREATED', 'REJECTED']);
    await tester.stable();

    expect(tester.createdCheckbox).toBeChecked();
    expect(tester.rejectedCheckbox).toBeChecked();
    expect(tester.resolvedCheckbox).not.toBeChecked();
  });

  it('should emit statuses selected when checking one', async () => {
    tester.componentInstance.statusFilter.set(['CREATED', 'REJECTED']);
    await tester.stable();

    await tester.resolvedCheckbox.check();
    expect(tester.componentInstance.statusFilter()).toEqual(['CREATED', 'REJECTED', 'RESOLVED']);
  });

  it('should emit statuses selected when unchecking one', async () => {
    tester.componentInstance.statusFilter.set(['CREATED', 'REJECTED']);
    await tester.stable();

    await tester.createdCheckbox.uncheck();
    expect(tester.componentInstance.statusFilter()).toEqual(['REJECTED']);
  });
});
