import { Component, output, input, ChangeDetectionStrategy } from '@angular/core';
import { LISTABLE_STATUSES, IssueStatus } from '../../models/issue.model';
import { StatusPipe } from '../../shared/status.pipe';

/**
 * Component used by the issue list component to apply a status filter allowing to query for issues by their status
 */
@Component({
  selector: 'dm-status-filter',
  templateUrl: './status-filter.component.html',
  styleUrl: './status-filter.component.scss',
  imports: [StatusPipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StatusFilterComponent {
  allStatuses: Array<IssueStatus> = LISTABLE_STATUSES;
  readonly statusFilter = input.required<Array<IssueStatus>>();
  readonly statusFilterChange = output<Array<IssueStatus>>();

  isStatusSelected(status: IssueStatus) {
    return this.statusFilter().includes(status);
  }

  /**
   * Adds or remove a status from the status filter
   * and outputs an event
   */
  selectStatus(status: IssueStatus) {
    if (this.isStatusSelected(status)) {
      this.statusFilterChange.emit(this.statusFilter().filter(s => status !== s));
    } else {
      this.statusFilterChange.emit([...this.statusFilter(), status]);
    }
  }
}
