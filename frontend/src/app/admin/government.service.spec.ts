import { TestBed } from '@angular/core/testing';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';

import { GovernmentService } from './government.service';
import {
  GovernmentCommand,
  GovernmentDetailedModel,
  GovernmentModel
} from './models/government.model';
import { PageModel } from '../models/page.model';
import { PictureModel } from '../models/issue.model';
import { logo1x257, logo256x256, logo257x1 } from '../images.spec';
import { provideHttpClient } from '@angular/common/http';

describe('GovernmentService', () => {
  let http: HttpTestingController;
  let service: GovernmentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(GovernmentService);
  });

  it('should create a government', () => {
    const command: GovernmentCommand = {
      osmId: 4850451,
      name: 'Rhone',
      code: '69D',
      url: 'https://rhone.fr',
      contactEmail: 'contact@rhone.fr',
      issueCategoryIds: [],
      contacts: [],
      administratorIds: [],
      poolingOrganizationId: 1
    };

    let governmentReceived: GovernmentModel | null = null;
    service.create(command).subscribe(response => (governmentReceived = response));

    const request = http.expectOne({
      method: 'POST',
      url: '/api/governments'
    });
    expect(request.request.body).toBe(command);

    const government: GovernmentModel = {
      poolingOrganizationId: 1,
      id: 1,
      osmId: 4850451,
      name: 'Rhone',
      code: '69D',
      logo: null,
      url: 'https://rhone.fr'
    };
    request.flush(government);

    expect(governmentReceived!).toBe(government);
  });

  it('should update a government', () => {
    const command: GovernmentCommand = {
      osmId: 4850451,
      name: 'Rhone',
      code: '69D',
      url: 'https://rhone.fr',
      contactEmail: 'contact@rhone.fr',
      issueCategoryIds: [],
      contacts: [],
      administratorIds: [],
      poolingOrganizationId: 1
    };

    let governmentReceived: GovernmentModel | null = null;
    service.update(12, command).subscribe(response => (governmentReceived = response));

    const request = http.expectOne({
      method: 'PUT',
      url: '/api/governments/12'
    });
    expect(request.request.body).toBe(command);

    const government: GovernmentModel = {
      poolingOrganizationId: 1,
      id: 1,
      osmId: 4850451,
      name: 'Rhone',
      code: '69D',
      logo: null,
      url: 'https://rhone.fr'
    };
    request.flush(government);

    expect(governmentReceived!).toBe(government);
  });

  [
    { query: 'foo', page: 1, expectedUrl: '/api/governments?query=foo&page=1' },
    { query: '', page: 0, expectedUrl: '/api/governments?page=0' },
    { query: null, page: 0, expectedUrl: '/api/governments?page=0' }
  ].forEach(data => {
    it(`should search governments with query ${data.query} and page ${data.page}`, () => {
      const expectedGovernments = {
        content: [{ id: 1000 }]
      } as PageModel<GovernmentModel>;

      let actualGovernments: PageModel<GovernmentModel> | null = null;

      service
        .search(data.query, data.page)
        .subscribe(governments => (actualGovernments = governments));

      http
        .expectOne({
          method: 'GET',
          url: data.expectedUrl
        })
        .flush(expectedGovernments);

      expect(actualGovernments!).toEqual(expectedGovernments);
    });
  });

  it('should get government', () => {
    const expectedGovernment = {
      id: 1000
    } as GovernmentDetailedModel;

    let actualGovernment: GovernmentDetailedModel | null = null;

    service.get(1000).subscribe(government => (actualGovernment = government));

    http
      .expectOne({
        method: 'GET',
        url: '/api/governments/1000'
      })
      .flush(expectedGovernment);

    expect(actualGovernment!).toEqual(expectedGovernment);
  });

  it('should upload the logo of a government', () => {
    const expectedPicture = { id: 4567 } as PictureModel;

    let actualPicture: PictureModel | null = null;

    const file = new Blob();
    const formData = new FormData();
    formData.set('file', file);

    service.uploadLogo(1000, file).subscribe(picture => (actualPicture = picture));

    const testRequest = http.expectOne({
      method: 'POST',
      url: '/api/governments/1000/logo'
    });
    expect(testRequest.request.body).toEqual(formData);

    testRequest.flush(expectedPicture);
    expect(actualPicture!).toEqual(expectedPicture);
  });

  it('should delete the logo of a government', () => {
    let deleted = false;

    service.deleteLogo(1000, 1042).subscribe(() => (deleted = true));

    http
      .expectOne({
        method: 'DELETE',
        url: '/api/governments/1000/logo/1042'
      })
      .flush(204);
    expect(deleted).toBe(true);
  });

  it('should validate a logo', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(logo256x256);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    const originalFile = {} as File;
    service.validateLogo(originalFile).subscribe(blob => {
      expect(blob).toBe(originalFile);
      done();
    });
  });

  it('should not validate a logo that is too large', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(logo257x1);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    const originalFile = {} as File;
    service.validateLogo(originalFile).subscribe({ error: () => done() });
    expect().nothing();
  });

  it('should not validate a logo that is too tall', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(logo1x257);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    const originalFile = {} as File;
    service.validateLogo(originalFile).subscribe({ error: () => done() });
    expect().nothing();
  });
});
