import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { map, switchMap } from 'rxjs';
import { PageModel } from '../../models/page.model';
import { GovernmentModel } from '../models/government.model';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { GovernmentService } from '../government.service';
import * as icons from '../../icons';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { PageLastIndexPipe } from '../page-last-index.pipe';
import { PageFirstIndexPipe } from '../page-first-index.pipe';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DecimalPipe } from '@angular/common';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { toSignal } from '@angular/core/rxjs-interop';

interface SearchCriteria {
  query: string;
  page: number;
}

/**
 * The component used to query and display a paginated list of governments
 */
@Component({
  selector: 'dm-government-list',
  templateUrl: './government-list.component.html',
  styleUrl: './government-list.component.scss',
  imports: [
    ReactiveFormsModule,
    InvalidFormControlDirective,
    RouterLink,
    FontAwesomeModule,
    NgbPagination,
    DecimalPipe,
    PageFirstIndexPipe,
    PageLastIndexPipe
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GovernmentListComponent {
  private readonly governmentService = inject(GovernmentService);
  private readonly router = inject(Router);

  readonly form = inject(NonNullableFormBuilder).group({
    query: ''
  });
  readonly governmentPage: Signal<PageModel<GovernmentModel> | undefined>;
  readonly icons = icons;
  readonly maxPaginationSize = 5;

  constructor() {
    const route = inject(ActivatedRoute);
    this.form.setValue({
      query: (route.snapshot.queryParamMap.get('query') || '').trim()
    });

    this.governmentPage = toSignal(
      route.queryParamMap.pipe(
        map((params): SearchCriteria => {
          const pageParam = params.get('page');
          const page = pageParam ? parseInt(pageParam, 10) - 1 : 0;
          return {
            query: params.get('query') || '',
            page
          };
        }),
        switchMap(searchCriteria =>
          this.governmentService.search(searchCriteria.query.trim(), searchCriteria.page)
        )
      )
    );
  }

  navigateToPage(page: number) {
    this.router.navigate([], {
      queryParams: { page },
      queryParamsHandling: 'merge'
    });
  }

  search() {
    this.router.navigate([], {
      queryParams: {
        query: this.form.value.query!.trim(),
        page: 1
      }
    });
  }
}
