import { TestBed } from '@angular/core/testing';

import { GovernmentListComponent } from './government-list.component';
import { ActivatedRouteStub, ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { of } from 'rxjs';
import { GovernmentService } from '../government.service';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { createPage } from '../../models/page.model.spec';
import { GovernmentModel } from '../models/government.model';

class GovernmentListComponentTester extends ComponentTester<GovernmentListComponent> {
  constructor() {
    super(GovernmentListComponent);
  }

  get query() {
    return this.input('#query')!;
  }

  get search() {
    return this.button('#search')!;
  }

  get resume() {
    return this.element('#resume');
  }

  get items() {
    return this.elements('.government-item');
  }

  get pagination() {
    return this.component(NgbPagination);
  }
}

describe('GovernmentListComponent', () => {
  let tester: GovernmentListComponentTester;
  let service: jasmine.SpyObj<GovernmentService>;
  let router: Router;
  let route: ActivatedRouteStub;

  const loire: GovernmentModel = {
    poolingOrganizationId: 1,
    id: 1,
    name: 'Loire',
    code: '123',
    osmId: 321,
    logo: null,
    url: 'https://loire.fr'
  };
  const rhone: GovernmentModel = {
    poolingOrganizationId: 1,
    id: 2,
    name: 'Rhône',
    code: '456',
    osmId: 654,
    logo: null,
    url: 'https://rhone.fr'
  };
  const loiret: GovernmentModel = {
    poolingOrganizationId: 1,
    id: 3,
    name: 'Loiret',
    code: '789',
    osmId: 987,
    logo: null,
    url: 'https://loiret.fr'
  };
  const basRhin: GovernmentModel = {
    poolingOrganizationId: 1,
    id: 4,
    name: 'Bas-Rhin',
    code: '148',
    osmId: 841,
    logo: null,
    url: 'https://bas-rhin.fr'
  };
  const loirEtCher: GovernmentModel = {
    poolingOrganizationId: 1,
    id: 1,
    name: 'Loir et Cher',
    code: '379',
    osmId: 973,
    logo: null,
    url: 'https://loir-et-cher.fr'
  };

  beforeEach(() => {
    route = stubRoute();
    service = createMock(GovernmentService);
    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: GovernmentService, useValue: service }
      ]
    });

    router = TestBed.inject(Router);
  });

  it('should display an empty form and list first page initially if no query', async () => {
    service.search.and.returnValue(of(createPage<GovernmentModel>([loire, rhone], 2, 3, 0)));

    tester = new GovernmentListComponentTester();
    await tester.stable();

    expect(tester.query).toHaveValue('');
    expect(tester.resume).toContainText('Résultats 1 à 2 sur 3');
    expect(tester.items.length).toBe(2);
    expect(tester.items[0].element('a')).toContainText('Loire');
    expect(tester.items[0]).toContainText('123');
    expect(tester.items[0]).toContainText('osm id 321');
    expect(tester.pagination.page).toBe(1);
    expect(tester.pagination.pageCount).toBe(2);
    expect(service.search).toHaveBeenCalledWith('', 0);
  });

  it('should not display a pagination if there is a single page', async () => {
    service.search.and.returnValue(of(createPage<GovernmentModel>([loire, rhone], 2, 2, 0)));

    tester = new GovernmentListComponentTester();
    await tester.stable();

    expect(tester.resume).toContainText('Résultats 1 à 2 sur 2');
    expect(tester.items.length).toBe(2);
    expect(tester.pagination).toBeNull();
  });

  it('should display a form pre-populated with the query in the URL and list the matching governments', async () => {
    route.setQueryParams({
      query: ' Loi',
      page: '2'
    });
    service.search.and.returnValue(of(createPage<GovernmentModel>([loiret], 2, 3, 1)));

    tester = new GovernmentListComponentTester();
    await tester.stable();

    expect(tester.query).toHaveValue('Loi');
    expect(tester.items.length).toBe(1);
    expect(tester.items[0].element('a')).toContainText('Loiret');
    expect(tester.pagination.page).toBe(2);
    expect(tester.pagination.pageCount).toBe(2);
    expect(service.search).toHaveBeenCalledWith('Loi', 1);
  });

  it('should navigate when searching', async () => {
    route.setQueryParams({
      page: '2'
    });
    const pageBeforeSearch = createPage<GovernmentModel>([loiret], 2, 3, 1);
    const pageAfterSearch = createPage([basRhin, rhone], 2, 3, 0);

    service.search.and.returnValues(of(pageBeforeSearch), of(pageAfterSearch));
    spyOn(router, 'navigate').and.callFake((_path: unknown, extra: NavigationExtras) => {
      route.setQueryParams(extra.queryParams!);
      return Promise.resolve(true);
    });
    tester = new GovernmentListComponentTester();
    await tester.stable();

    expect(tester.items.length).toBe(1);
    expect(tester.pagination.page).toBe(2);

    await tester.query.fillWith('  Rh  ');
    await tester.search.click();

    expect(tester.items.length).toBe(2);
    expect(tester.items[0].element('a')).toContainText('Bas-Rhin');
    expect(tester.pagination.page).toBe(1);

    expect(service.search).toHaveBeenCalledWith('Rh', 0);
  });

  it('should navigate when changing page', async () => {
    route.setQueryParams({
      query: 'Loi',
      page: '2'
    });
    const pageBeforePageChange = createPage([loire, loiret], 2, 3, 0);
    const pageAfterPageChange = createPage([loirEtCher], 2, 3, 1);

    service.search.and.returnValues(of(pageBeforePageChange), of(pageAfterPageChange));
    spyOn(router, 'navigate').and.callFake((_path: unknown, extra: NavigationExtras) => {
      route.setQueryParams(extra.queryParams!);
      return Promise.resolve(true);
    });
    tester = new GovernmentListComponentTester();
    await tester.stable();

    expect(tester.items.length).toBe(2);
    expect(tester.pagination.page).toBe(1);

    tester.pagination.pageChange.emit(2);
    await tester.stable();

    expect(tester.items.length).toBe(1);
    expect(tester.items[0].element('a')).toContainText('Loir et Cher');
    expect(tester.pagination.page).toBe(2);

    expect(service.search).toHaveBeenCalledWith('Loi', 1);
  });
});
