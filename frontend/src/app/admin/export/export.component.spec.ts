import { TestBed } from '@angular/core/testing';

import { ExportComponent, validDateRange } from './export.component';
import { ComponentTester, createMock, TestHtmlElement } from 'ngx-speculoos';
import { IssueService } from '../../issue.service';
import { FormControl, FormGroup } from '@angular/forms';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { Subject } from 'rxjs';
import { provideDirectMairieDatepicker } from '../direct-mairie-datepicker/direct-mairie-datepicker.providers';

class ExportComponentTester extends ComponentTester<ExportComponent> {
  constructor() {
    super(ExportComponent);
  }

  get from() {
    return this.input('#from')!;
  }

  get to() {
    return this.input('#to')!;
  }

  get export() {
    return this.button('#export')!;
  }

  get exportingSpinner() {
    return this.element('#exportingSpinner');
  }

  get downloadLink() {
    return this.element('#downloadLink') as TestHtmlElement<HTMLAnchorElement>;
  }
}

describe('ExportComponent', () => {
  let tester: ExportComponentTester;
  let issueService: jasmine.SpyObj<IssueService>;

  beforeEach(async () => {
    issueService = createMock(IssueService);
    TestBed.configureTestingModule({
      providers: [
        provideDirectMairieDatepicker(),
        { provide: IssueService, useValue: issueService }
      ]
    });
    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new ExportComponentTester();
    await tester.stable();
  });

  it('should display an empty form', () => {
    expect(tester.from).toHaveText('');
    expect(tester.to).toHaveText('');
    expect(tester.exportingSpinner).toBeNull();
  });

  it('should validate the form', async () => {
    await tester.export.click();

    expect(tester.testElement).toContainText('La date de début est obligatoire');
    expect(tester.testElement).toContainText('La date de fin est obligatoire');
    expect(issueService.export).not.toHaveBeenCalled();

    await tester.from.fillWith('abc');
    expect(tester.testElement).toContainText(
      'La date de début doit être une date valide, au format jj/MM/aaaa'
    );

    await tester.from.fillWith('01/01/2019');
    await tester.to.fillWith('01/01/2018');
    expect(tester.testElement).toContainText(
      'La date de fin doit être ultérieure ou égale à la date de début'
    );
  });

  it('should export', async () => {
    const exportResult = new Subject<Blob>();

    issueService.export.and.returnValue(exportResult);
    spyOn(tester.downloadLink.nativeElement, 'click');

    await tester.from.fillWith('01/01/2019');
    await tester.to.fillWith('31/01/2019');
    await tester.export.click();

    expect(tester.exportingSpinner).not.toBeNull();

    const csvBlob = new Blob();
    exportResult.next(csvBlob);
    exportResult.complete();
    await tester.stable();

    expect(tester.exportingSpinner).toBeNull();
    expect(tester.downloadLink.nativeElement.click).toHaveBeenCalled();
    expect(tester.downloadLink.attr('download')).toBe('export_2019-01-01_2019-01-31.csv');
    expect(tester.downloadLink.attr('href')).toMatch(/blob:.*/);
  });

  describe('validRange Validator', () => {
    let formGroup: FormGroup<{ from: FormControl<string>; to: FormControl<string> }>;

    beforeEach(() => {
      formGroup = tester.componentInstance.form;
    });

    it('should return null if any date is empty', () => {
      const value = {
        from: '',
        to: ''
      };

      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();

      value.from = '2019-01-01';
      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();

      value.from = '';
      value.to = '2019-01-01';
      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();
    });

    it('should return null if any date is invalid', () => {
      const value = {
        from: 'invalid',
        to: 'invalid'
      };

      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();

      value.from = '2019-01-01';
      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();

      value.from = 'invalid';
      value.to = '2019-01-01';
      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();
    });

    it('should return null if from <= to', () => {
      const value = {
        from: '2019-01-01',
        to: '2019-01-02'
      };

      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();

      value.from = '2019-01-02';
      formGroup.setValue(value);
      expect(validDateRange(formGroup)).toBeNull();
    });

    it('should return error if from > to', () => {
      const value = {
        from: '2019-01-02',
        to: '2019-01-01'
      };

      formGroup.setValue(value);
      expect(validDateRange(formGroup)).not.toBeNull();
      expect(validDateRange(formGroup)!.dateRange).toBeTruthy();
    });
  });
});
