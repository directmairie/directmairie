import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  inject,
  signal,
  viewChild
} from '@angular/core';
import { IssueService } from '../../issue.service';
import {
  AbstractControl,
  NonNullableFormBuilder,
  ValidationErrors,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import { finalize } from 'rxjs';
import * as icons from '../../icons';
import { DateTime } from 'luxon';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ValidationErrorsComponent, ValidationErrorDirective } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { NgbInputDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { DatepickerContainerComponent } from '../direct-mairie-datepicker/datepicker-container/datepicker-container.component';

export function validDateRange(formGroup: AbstractControl): ValidationErrors | null {
  const formValue = formGroup.value;
  if (!formValue.from || !formValue.to) {
    return null;
  }

  const from = DateTime.fromISO(formValue.from);
  const to = DateTime.fromISO(formValue.to);

  if (!from.isValid || !to.isValid) {
    return null;
  }

  if (from > to) {
    return { dateRange: true };
  }
  return null;
}

/**
 * The component used to display the page allowing to enter a date range and export issues as CSV
 */
@Component({
  selector: 'dm-export',
  templateUrl: './export.component.html',
  styleUrl: './export.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    DatepickerContainerComponent,
    NgbInputDatepicker,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    FontAwesomeModule
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExportComponent {
  private readonly issueService = inject(IssueService);

  readonly icons = icons;
  readonly form = inject(NonNullableFormBuilder).group(
    {
      from: ['', Validators.required], // date in ISO format
      to: ['', Validators.required] // date in ISO format
    },
    {
      validators: validDateRange
    }
  );
  readonly exporting = signal(false);

  readonly downloadLink = viewChild.required<ElementRef<HTMLAnchorElement>>('downloadLink');

  /**
   * If we simply submitted a form like in the old days, without using JavaScript, the browser would
   * download the file returned by the server all by itself.
   * But the server needs to check that the current user is allowed to do that.
   * So we need to pass the Authorization header in the request, and we can only do that using an AJAX request.
   * There are thus two solutions:
   * 1. change the server to accept the token in a cookie, store the auth token in a cookie in the browser,
   * and use an old-fashioned form, or set window.location to point to the download URL, to let the server send the
   * request with the cookie, and download the CSV
   * 2. Use AJAX to send the request and get back the CSV as a Blob. We then need to trigger the "download" of this
   * Blob.
   *
   * We chose the second solution.
   * To trigger the download, we need to generate a URL for that blob (which will look like `blob:..../<uuid>`),
   * then set the `href` of a link in the DOM to that URL and click the link.
   * And in order to set the file name, we simply set the `download` attribute of that link.
   * Then we can programmatically click on that link.
   * Finally, at the end, we revoke the URL to let the browser know that this blob can be discarded.
   */
  export() {
    if (this.form.invalid) {
      return;
    }

    this.exporting.set(true);
    const formValue = this.form.value;

    this.issueService
      .export(formValue.from!, formValue.to!)
      .pipe(
        finalize(() => {
          this.exporting.set(false);
        })
      )
      .subscribe(blob => {
        const downloadUrl = URL.createObjectURL(blob);
        const link = this.downloadLink().nativeElement;
        link.href = downloadUrl;
        link.download = `export_${formValue.from}_${formValue.to}.csv`;
        link.click();

        URL.revokeObjectURL(downloadUrl);
      });
  }
}
