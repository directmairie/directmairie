import { Injectable, inject } from '@angular/core';
import { GovernmentService } from './government.service';
import { PoolingOrganizationService } from './pooling-organization.service';
import { filter, first, forkJoin, map, Observable, of, switchMap } from 'rxjs';
import { PoolingOrganizationModel } from './models/pooling-organization.model';
import { GovernmentModel } from './models/government.model';
import { CurrentUserModel } from '../models/current-user.model';
import { CurrentUserService } from '../current-user.service';

export interface IssueTarget {
  id: number;
  type: 'government' | 'pooling-organization';
  name: string;
}

function organizationToTarget(org: PoolingOrganizationModel): IssueTarget {
  return {
    id: org.id,
    name: org.name,
    type: 'pooling-organization'
  };
}

function governmentToTarget(gov: GovernmentModel): IssueTarget {
  return {
    id: gov.id,
    name: gov.name,
    type: 'government'
  };
}

@Injectable({
  providedIn: 'root'
})
export class IssueTargetService {
  private currentUserService = inject(CurrentUserService);
  private governmentService = inject(GovernmentService);
  private poolingOrganizationService = inject(PoolingOrganizationService);

  loadIssueTargets(
    poolingOrganizationIds: Array<number>,
    governmentIds: Array<number>
  ): Observable<Array<IssueTarget>> {
    const poolingOrganizations$ =
      poolingOrganizationIds.length === 0
        ? of([])
        : forkJoin(
            poolingOrganizationIds.map(organizationId =>
              this.loadPoolingOrganizationTarget(organizationId)
            )
          );
    const governments$ =
      governmentIds.length === 0
        ? of([])
        : forkJoin(governmentIds.map(governmentId => this.loadGovernmentTarget(governmentId)));

    return forkJoin([poolingOrganizations$, governments$]).pipe(
      map(([poolingOrganizations, governments]) => [...poolingOrganizations, ...governments])
    );
  }

  searchTargets(text: string): Observable<Array<IssueTarget>> {
    return this.currentUserService.getUserChanges().pipe(
      first(),
      filter(
        (currentUser: CurrentUserModel | null): currentUser is CurrentUserModel => !!currentUser
      ),
      switchMap(currentUser => this.searchTargetsForUser(text, currentUser))
    );
  }

  private searchTargetsForUser(
    text: string,
    currentUser: CurrentUserModel
  ): Observable<Array<IssueTarget>> {
    const poolingOrganizations$: Observable<Array<IssueTarget>> =
      currentUser.poolingOrganizationAdmin || currentUser.superAdmin
        ? this.poolingOrganizationService
            .search(text)
            .pipe(map(organizations => organizations.map(organizationToTarget)))
        : of([]);
    const governments$: Observable<Array<IssueTarget>> = this.governmentService
      .search(text, 0)
      .pipe(map(governments => governments.content.map(governmentToTarget)));

    return forkJoin([poolingOrganizations$, governments$]).pipe(
      map(([organizations, governments]) => [...organizations, ...governments])
    );
  }

  private loadPoolingOrganizationTarget(organizationId: number): Observable<IssueTarget> {
    return this.poolingOrganizationService.get(organizationId).pipe(map(organizationToTarget));
  }

  private loadGovernmentTarget(governmentId: number): Observable<IssueTarget> {
    return this.governmentService.get(governmentId).pipe(map(governmentToTarget));
  }
}
