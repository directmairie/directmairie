import { TestBed } from '@angular/core/testing';

import { PoolingOrganizationListComponent } from './pooling-organization-list.component';
import { ActivatedRouteStub, ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { PoolingOrganizationService } from '../pooling-organization.service';
import { of } from 'rxjs';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';

class PoolingOrganizationListComponentTester extends ComponentTester<PoolingOrganizationListComponent> {
  constructor() {
    super(PoolingOrganizationListComponent);
  }

  get query() {
    return this.input('#query')!;
  }

  get search() {
    return this.button('#search')!;
  }

  get items() {
    return this.elements('.organization-item');
  }
}

describe('PoolingOrganizationListComponent', () => {
  let tester: PoolingOrganizationListComponentTester;
  let service: jasmine.SpyObj<PoolingOrganizationService>;
  let router: Router;
  let route: ActivatedRouteStub;

  beforeEach(() => {
    route = stubRoute();
    service = createMock(PoolingOrganizationService);
    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: PoolingOrganizationService, useValue: service }
      ]
    });

    router = TestBed.inject(Router);
  });

  it('should display an empty form and list everything initially if no query', async () => {
    service.search.and.returnValue(
      of([
        {
          id: 1,
          name: 'Adullact'
        },
        {
          id: 2,
          name: 'Bretagne'
        }
      ])
    );

    tester = new PoolingOrganizationListComponentTester();
    await tester.stable();

    expect(tester.query).toHaveValue('');
    expect(tester.items.length).toBe(2);
    expect(tester.items[1].element('a')).toContainText('Bretagne');
    expect(service.search).toHaveBeenCalledWith('');
  });

  it('should display a form pre-populated with the query in the URL and list the matching organizations', async () => {
    route.setQueryParams({
      query: ' Bre'
    });
    service.search.and.returnValue(
      of([
        {
          id: 2,
          name: 'Bretagne'
        }
      ])
    );

    tester = new PoolingOrganizationListComponentTester();
    await tester.stable();

    expect(tester.query).toHaveValue('Bre');
    expect(tester.items.length).toBe(1);
    expect(tester.items[0].element('a')).toContainText('Bretagne');
    expect(service.search).toHaveBeenCalledWith('Bre');
  });

  it('should navigate when searching', async () => {
    service.search.and.returnValues(
      of([]),
      of([
        {
          id: 2,
          name: 'Bretagne'
        }
      ])
    );
    spyOn(router, 'navigate').and.callFake((_path: unknown, extra: NavigationExtras) => {
      route.setQueryParams(extra.queryParams!);
      return Promise.resolve(true);
    });

    tester = new PoolingOrganizationListComponentTester();
    await tester.stable();

    expect(tester.items.length).toBe(0);

    await tester.query.fillWith('  Bre  ');
    await tester.search.click();

    expect(tester.items.length).toBe(1);
    expect(tester.items[0].element('a')).toContainText('Bretagne');

    expect(service.search).toHaveBeenCalledWith('Bre');
  });
});
