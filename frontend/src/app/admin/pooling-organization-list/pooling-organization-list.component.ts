import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { map, switchMap } from 'rxjs';
import { PoolingOrganizationModel } from '../models/pooling-organization.model';
import { PoolingOrganizationService } from '../pooling-organization.service';
import * as icons from '../../icons';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { toSignal } from '@angular/core/rxjs-interop';

/**
 * Component used to query and display a paginated list of pooling organizations
 */
@Component({
  selector: 'dm-pooling-organization-list',
  templateUrl: './pooling-organization-list.component.html',
  styleUrl: './pooling-organization-list.component.scss',
  imports: [ReactiveFormsModule, InvalidFormControlDirective, RouterLink, FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PoolingOrganizationListComponent {
  private readonly router = inject(Router);

  readonly form = inject(NonNullableFormBuilder).group({
    query: ''
  });
  readonly organizations: Signal<Array<PoolingOrganizationModel> | undefined>;
  readonly icons = icons;

  constructor() {
    const route = inject(ActivatedRoute);
    this.form.setValue({
      query: (route.snapshot.queryParamMap.get('query') || '').trim()
    });

    const poolingOrganizationService = inject(PoolingOrganizationService);
    this.organizations = toSignal(
      route.queryParamMap.pipe(
        map(params => params.get('query') ?? ''),
        switchMap(query => poolingOrganizationService.search(query.trim()))
      )
    );
  }

  search() {
    this.router.navigate([], {
      queryParams: {
        query: this.form.value.query!.trim() || undefined
      }
    });
  }
}
