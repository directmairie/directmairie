import {
  ChangeDetectionStrategy,
  Component,
  TemplateRef,
  inject,
  output,
  input,
  signal,
  computed
} from '@angular/core';
import { finalize, switchMap } from 'rxjs';

import { IssueModel, IssueStatus, LISTABLE_STATUSES, PictureModel } from '../../models/issue.model';
import { IssueService } from '../../issue.service';
import { StatusPipe } from '../../shared/status.pipe';
import { DecimalPipe, DatePipe, NgPlural, NgPluralCase } from '@angular/common';
import {
  NgbDropdown,
  NgbDropdownToggle,
  NgbDropdownMenu,
  NgbCollapse,
  NgbModal
} from '@ng-bootstrap/ng-bootstrap';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ConfirmationService } from '../../confirmation-modal/confirmation.service';
import { SuccessService } from '../../success.service';
import { IssueTransitionsComponent } from '../../issue-transitions/issue-transitions.component';
import { IssueTransitionModalComponent } from '../issue-transition-modal/issue-transition-modal.component';
import { fromModal } from '../../shared/modals';

const descriptionLengthThreshold = 120;

/**
 * The component used to display a "card" in the issues administration page, displaying the information about
 * one of the issues of the paginated list
 */
@Component({
  selector: 'dm-issue',
  templateUrl: './issue.component.html',
  styleUrl: './issue.component.scss',
  imports: [
    NgbDropdown,
    NgbDropdownToggle,
    NgbDropdownMenu,
    NgbCollapse,
    DecimalPipe,
    DatePipe,
    StatusPipe,
    FontAwesomeModule,
    IssueTransitionsComponent,
    NgPlural,
    NgPluralCase
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueComponent {
  private readonly issueService = inject(IssueService);
  private readonly confirmationService = inject(ConfirmationService);
  private readonly ngbModal = inject(NgbModal);
  private readonly successService = inject(SuccessService);

  readonly icons = icons;

  readonly issue = input.required<IssueModel>();

  readonly selected = input(false);

  readonly statusUpdated = output<IssueStatus>();
  readonly deleted = output<void>();

  readonly listableStatuses = LISTABLE_STATUSES;

  /**
   * Is the issue displaying details
   */
  readonly expanded = signal(false);

  /**
   * Can the issue display details (true if description is too long, or if it has pictures)
   */
  readonly expandable = computed(() => {
    const issue = this.issue();
    return (
      (issue.description && issue.description.length > descriptionLengthThreshold) ||
      issue.pictures.length > 0
    );
  });
  readonly waitingForStatusUpdate = signal(false);

  /**
   * Returns the description,
   * or the truncated description if too long and not expanded,
   * or a default placeholder if there is none.
   */
  readonly description = computed(() => {
    const description = this.issue().description;
    if (description) {
      return description.length > descriptionLengthThreshold && !this.expanded()
        ? description.substring(0, descriptionLengthThreshold) + '...'
        : description;
    }
    return 'Pas de description';
  });

  /**
   * Toggles the expansion of the issue
   */
  toggle(event: Event) {
    event.preventDefault();
    this.expanded.update(expanded => !expanded);
  }

  /**
   * Builds the url for a picture
   */
  url(picture: PictureModel) {
    return `/api/issues/${this.issue().id}/pictures/${picture.id}/bytes`;
  }

  updateStatus(status: IssueStatus) {
    const modalRef = this.ngbModal.open(IssueTransitionModalComponent, {
      ariaLabelledBy: 'issue-transition-modal-title',
      size: 'lg'
    });
    const modalComponent: IssueTransitionModalComponent = modalRef.componentInstance;
    modalComponent.prepare(this.issue(), status);
    return fromModal<string | null>(modalRef)
      .pipe(
        switchMap(message => {
          this.waitingForStatusUpdate.set(true);
          return this.issueService.updateStatus(this.issue().id, { status, message }).pipe(
            finalize(() => {
              this.waitingForStatusUpdate.set(false);
            })
          );
        })
      )
      .subscribe(() => this.statusUpdated.emit(status));
  }

  deleteIssue(confirmationTemplate: TemplateRef<unknown>) {
    this.confirmationService
      .confirm({
        message: confirmationTemplate
      })
      .pipe(switchMap(() => this.issueService.delete(this.issue().id)))
      .subscribe(() => {
        this.successService.success(`La remontée d'information a été supprimée`);
        this.deleted.emit();
      });
  }
}
