import { TestBed } from '@angular/core/testing';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { EMPTY, of, Subject } from 'rxjs';

import { IssueComponent } from './issue.component';
import { IssueModel, IssueStatus, PictureModel } from '../../models/issue.model';
import { IssueService } from '../../issue.service';
import { ChangeDetectionStrategy, Component, LOCALE_ID, signal } from '@angular/core';
import { NgbConfig, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { SuccessService } from '../../success.service';
import { ConfirmationService } from '../../confirmation-modal/confirmation.service';
import { IssueTransitionsComponent } from '../../issue-transitions/issue-transitions.component';
import { IssueTransitionModalComponent } from '../issue-transition-modal/issue-transition-modal.component';

@Component({
  template: `@if (issue(); as issue) {
    <dm-issue
      [issue]="issue"
      [selected]="selected()"
      (statusUpdated)="newStatus.set($event)"
      (deleted)="deleted.set(true)"
    />
  }`,
  imports: [IssueComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly issue = signal<IssueModel | null>(null);
  readonly newStatus = signal<IssueStatus | null>(null);
  readonly selected = signal(false);
  readonly deleted = signal(false);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get header() {
    return this.element('.card-header');
  }

  get status() {
    return this.element('#statusDropdown-43');
  }

  get statusSpinner() {
    return this.element('#statusSpinner-43');
  }

  get description() {
    return this.element('.description');
  }

  get transitions() {
    return this.component(IssueTransitionsComponent);
  }

  get pictures() {
    return this.element('.pictures');
  }

  get toggle() {
    return this.button('.card-link')!;
  }

  get delete() {
    return this.button('.delete-button')!;
  }

  get footer() {
    return this.element('.card-footer');
  }
}

describe('IssueComponent', () => {
  let issueInLyon: IssueModel;
  let issueService: jasmine.SpyObj<IssueService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let confirmationService: jasmine.SpyObj<ConfirmationService>;
  let ngbModal: jasmine.SpyObj<NgbModal>;
  let tester: TestComponentTester;

  beforeEach(() => {
    issueService = createMock(IssueService);
    successService = createMock(SuccessService);
    confirmationService = createMock(ConfirmationService);
    ngbModal = createMock(NgbModal);

    TestBed.configureTestingModule({
      providers: [
        { provide: LOCALE_ID, useValue: 'fr' },
        { provide: NgbConfig, useValue: { animation: false } as NgbConfig },
        { provide: SuccessService, useValue: successService },
        { provide: ConfirmationService, useValue: confirmationService },
        { provide: NgbModal, useValue: ngbModal },
        { provide: IssueService, useValue: issueService }
      ]
    });

    issueInLyon = {
      id: 43,
      status: 'RESOLVED',
      creationInstant: '2019-01-09T15:00:00Z',
      description: Array(140).fill('a').join(''),
      coordinates: {
        latitude: 45.7573654,
        longitude: 4.8427945
      },
      category: {
        id: 2,
        label: 'lighting',
        descriptionRequired: false
      },
      pictures: [],
      assignedGovernment: null,
      transitions: [
        {
          beforeStatus: 'CREATED',
          afterStatus: 'RESOLVED',
          transitionInstant: '2019-01-10T15:00:00Z',
          message: 'we replaced the light bulb'
        }
      ]
    };

    tester = new TestComponentTester();
  });

  it('should display an issue category, status, creation date, coordinates and transitions', async () => {
    tester.componentInstance.issue.set(issueInLyon);
    await tester.stable();
    expect(tester.header).toContainText('#43 - lighting');
    expect(tester.status).toContainText('Résolue');
    expect(tester.transitions.issue()).toBe(issueInLyon);
    expect(tester.footer).toContainText('9 janv. 2019');
    expect(tester.footer).toContainText('45.7573654, 4.8427945');
  });

  it('should not display transitions if absent', async () => {
    tester.componentInstance.issue.set({ ...issueInLyon, transitions: [] });
    await tester.stable();
    expect(tester.transitions).toBeNull();
  });

  it('should truncate the description if too long', async () => {
    tester.componentInstance.issue.set(issueInLyon);
    await tester.stable();

    // short description with ... at the end
    expect(tester.description).toHaveText(Array(120).fill('a').join('') + '...');

    // click on details
    await tester.toggle.click();

    // long description
    expect(tester.description).toHaveText(Array(140).fill('a').join(''));

    // click on collapse (that replaced details)
    await tester.toggle.click();

    // short description again
    expect(tester.description).toHaveText(Array(120).fill('a').join('') + '...');
  });

  it('should allow to see one picture', async () => {
    const picture1: PictureModel = {
      id: 1,
      creationInstant: '2019-01-09T15:00:00Z'
    };
    tester.componentInstance.issue.set({ ...issueInLyon, pictures: [picture1] });
    await tester.stable();

    // link has changed
    expect(tester.toggle).toContainText('Détails (1 photo)');

    // no pictures displayed at the beginning
    // we can't test the presence of pictures because they are always there
    // and animated via CSS to be shown/hidden
    expect(tester.pictures).not.toBeVisible();

    // click on details
    await tester.toggle.click();

    // show pictures
    expect(tester.pictures).toBeVisible();

    // click on collapse (that replaced details)
    await tester.toggle.click();

    // no pictures again
    expect(tester.pictures).not.toBeVisible();
  });

  it('should allow to see several pictures', async () => {
    const picture1: PictureModel = {
      id: 1,
      creationInstant: '2019-01-09T15:00:00Z'
    };
    const picture2: PictureModel = {
      id: 2,
      creationInstant: '2019-01-09T15:00:00Z'
    };
    tester.componentInstance.issue.set({ ...issueInLyon, pictures: [picture1, picture2] });
    await tester.stable();

    // link has changed
    expect(tester.toggle).toContainText('Détails (2 photos)');
  });

  it('should disable the details link if no details', async () => {
    // when there is no picture and the description is short
    tester.componentInstance.issue.set({ ...issueInLyon, description: 'short' });
    await tester.stable();

    // then the link is disabled
    expect(tester.toggle).toContainText('Détails (Pas de photo)');
    expect(tester.toggle.disabled).toBe(true);
  });

  it('should update the status of an issue', async () => {
    tester.componentInstance.issue.set({ ...issueInLyon });
    const statusSubject = new Subject<IssueModel>();
    issueService.updateStatus.and.returnValue(statusSubject);
    await tester.stable();

    const mockModalComponent = createMock(IssueTransitionModalComponent);
    const closed = of('the message');
    const dismissed = EMPTY;
    const modalRef = {
      componentInstance: mockModalComponent,
      closed,
      dismissed
    } as unknown as NgbModalRef;

    ngbModal.open.and.returnValue(modalRef);

    // select the status CREATED
    await tester.button('.dropdown-item')!.click();

    expect(mockModalComponent.prepare).toHaveBeenCalledWith(
      tester.componentInstance.issue()!,
      'CREATED'
    );

    // display a spinner while updating
    expect(tester.statusSpinner).not.toBeNull();

    // update success
    statusSubject.next({} as IssueModel);
    statusSubject.complete();
    await tester.stable();

    // then the spinner disappears
    // and the status is set to the new status
    expect(tester.componentInstance.newStatus()).toBe('CREATED');
    expect(tester.statusSpinner).toBeNull();
  });

  it('should do nothing if delete wihout confirmation', async () => {
    tester.componentInstance.issue.set(issueInLyon);
    await tester.stable();
    confirmationService.confirm.and.returnValue(EMPTY);

    await tester.delete.click();

    expect(issueService.delete).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
    expect(tester.componentInstance.deleted()).toBeFalse();
  });

  it('should delete after confirmation', async () => {
    tester.componentInstance.issue.set(issueInLyon);
    await tester.stable();
    confirmationService.confirm.and.returnValue(of(undefined));
    issueService.delete.and.returnValue(of(undefined));

    await tester.delete.click();

    expect(issueService.delete).toHaveBeenCalledWith(issueInLyon.id);
    expect(successService.success).toHaveBeenCalled();
    expect(tester.componentInstance.deleted()).toBeTrue();
  });
});
