import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  PoolingOrganizationCommand,
  PoolingOrganizationDetailedModel,
  PoolingOrganizationModel
} from './models/pooling-organization.model';

/**
 * Service used for the CRUD of pooling organizations
 */
@Injectable({
  providedIn: 'root'
})
export class PoolingOrganizationService {
  private http = inject(HttpClient);

  search(query: string | null = null): Observable<Array<PoolingOrganizationModel>> {
    const params: Record<string, string> = query ? { query } : {};
    return this.http.get<Array<PoolingOrganizationModel>>('/api/pooling-organizations', { params });
  }

  get(organizationId: number): Observable<PoolingOrganizationDetailedModel> {
    return this.http.get<PoolingOrganizationDetailedModel>(
      `/api/pooling-organizations/${organizationId}`
    );
  }

  create(command: PoolingOrganizationCommand): Observable<PoolingOrganizationDetailedModel> {
    return this.http.post<PoolingOrganizationDetailedModel>('/api/pooling-organizations', command);
  }

  update(organizationId: number, command: PoolingOrganizationCommand): Observable<void> {
    return this.http.put<void>(`/api/pooling-organizations/${organizationId}`, command);
  }
}
