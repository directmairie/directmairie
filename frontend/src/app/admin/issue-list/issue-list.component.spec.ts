import { TestBed } from '@angular/core/testing';
import { LOCALE_ID } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { ActivatedRouteStub, ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { of } from 'rxjs';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';

import { IssueListComponent } from './issue-list.component';
import { IssueService } from '../../issue.service';
import { IssueModel, IssueStatus } from '../../models/issue.model';
import { PageModel } from '../../models/page.model';
import { createPage } from '../../models/page.model.spec';
import { StatusFilterComponent } from '../status-filter/status-filter.component';
import { IssueComponent } from '../issue/issue.component';
import { IssueTargetSelectorComponent } from '../issue-target-selector/issue-target-selector.component';
import { latLng } from 'leaflet';
import { IssueTargetService } from '../issue-target.service';

registerLocaleData(localeFr);

class IssueListComponentTester extends ComponentTester<IssueListComponent> {
  constructor() {
    super(IssueListComponent);
  }

  get title() {
    return this.element('h1');
  }

  get issues() {
    return this.elements(IssueComponent);
  }

  get issueComponents() {
    return this.components(IssueComponent);
  }

  get mapMarkers() {
    return this.nativeElement.querySelectorAll('.leaflet-marker-icon');
  }

  get noIssues() {
    return this.element('#no-issues');
  }

  get pagination() {
    return this.component(NgbPagination);
  }

  get statusFilter() {
    return this.component(StatusFilterComponent);
  }

  get issueTargetSelector() {
    return this.component(IssueTargetSelectorComponent);
  }
}

describe('IssueListComponent', () => {
  const issueInParis: IssueModel = {
    id: 42,
    status: 'REJECTED',
    creationInstant: '2019-01-08T15:00:00Z',
    description: 'Tower fell down',
    coordinates: {
      latitude: 48.858624,
      longitude: 2.294537
    },
    category: {
      id: 1,
      label: 'Other',
      descriptionRequired: true
    },
    pictures: [],
    assignedGovernment: null,
    transitions: []
  };
  const issueInLyon: IssueModel = {
    id: 43,
    status: 'RESOLVED',
    creationInstant: '2019-01-09T15:00:00Z',
    description: null,
    coordinates: {
      latitude: 45.7573654,
      longitude: 4.8427945
    },
    category: {
      id: 2,
      label: 'lighting',
      descriptionRequired: false
    },
    pictures: [],
    assignedGovernment: null,
    transitions: []
  };
  const issues: PageModel<IssueModel> = createPage([issueInParis, issueInLyon], 2, 20, 0);
  let service: jasmine.SpyObj<IssueService>;
  let issueTargetService: jasmine.SpyObj<IssueTargetService>;
  let tester: IssueListComponentTester;
  let route: ActivatedRouteStub;

  beforeEach(() => {
    route = stubRoute();
    service = createMock(IssueService);
    service.list.and.returnValue(of(issues));
    issueTargetService = createMock(IssueTargetService);
    issueTargetService.loadIssueTargets.and.returnValue(of([]));

    TestBed.configureTestingModule({
      providers: [
        { provide: LOCALE_ID, useValue: 'fr' },
        { provide: ActivatedRoute, useValue: route },
        { provide: IssueService, useValue: service },
        { provide: IssueTargetService, useValue: issueTargetService }
      ]
    });
  });

  it('should list issues', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();

    expect(service.list).toHaveBeenCalledWith({
      page: 1,
      statusFilter: [],
      poolingOrganizationIds: [],
      governmentIds: []
    });
    expect(tester.issues.length).toBe(2);
  });

  it('should list issues for the requested page, status government and organization ids', async () => {
    const page = '10';
    const status: Array<IssueStatus> = ['RESOLVED'];
    const org: Array<string> = ['1000'];
    const gov: Array<string> = ['1001', '1002'];

    route.setQueryParams({ page, status, org, gov });

    tester = new IssueListComponentTester();
    await tester.stable();

    expect(service.list).toHaveBeenCalledWith({
      page: 10,
      statusFilter: ['RESOLVED'],
      poolingOrganizationIds: [1000],
      governmentIds: [1001, 1002]
    });
  });

  it('should paginate issues', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();

    expect(tester.pagination).not.toBeNull();
    expect(tester.pagination.page).toBe(1);
    expect(tester.pagination.pageCount).toBe(10);
  });

  it('should navigate to requested page when pagination is used', async () => {
    // given a component
    issueTargetService.loadIssueTargets.and.returnValue(of([]));

    const router = TestBed.inject(Router);
    spyOn(router, 'navigate');

    const status: Array<IssueStatus> = ['RESOLVED'];
    const org: Array<string> = ['1000'];
    const gov: Array<string> = ['1001', '1002'];
    route.setQueryParams({ status, org, gov });

    tester = new IssueListComponentTester();
    await tester.stable();

    // when navigating
    tester.pagination.pageChange.emit(2);
    await tester.stable();

    // then it should redirect to the list with correct parameters
    expect(router.navigate).toHaveBeenCalledWith([], {
      queryParams: { page: '2', status, org, gov }
    });
  });

  it('should trigger a new search when the status filter changes', async () => {
    // given a component
    issueTargetService.loadIssueTargets.and.returnValue(of([]));

    const router = TestBed.inject(Router) as Router;
    spyOn(router, 'navigate');

    const page = '3';
    const org: Array<string> = ['1000'];
    const gov: Array<string> = ['1001', '1002'];
    route.setQueryParams({ page, org, gov });

    tester = new IssueListComponentTester();
    await tester.stable();

    // when navigating
    tester.statusFilter.statusFilterChange.emit(['CREATED', 'RESOLVED']);

    // then it should redirect to the list with correct parameters
    expect(router.navigate).toHaveBeenCalledWith([], {
      queryParams: { page: undefined, status: ['CREATED', 'RESOLVED'], org, gov }
    });
  });

  it('should trigger a new search when the target filter changes', async () => {
    // given a component
    issueTargetService.loadIssueTargets.and.returnValue(of([]));
    const router = TestBed.inject(Router) as Router;
    spyOn(router, 'navigate');

    const page = '3';
    const status: Array<IssueStatus> = ['CREATED'];
    route.setQueryParams({ page, status });

    tester = new IssueListComponentTester();
    await tester.stable();

    // when navigating
    tester.issueTargetSelector.selectedTargetsChange.emit([
      {
        id: 1001,
        type: 'pooling-organization',
        name: 'p1'
      },
      {
        id: 1002,
        type: 'government',
        name: 'g1'
      }
    ]);

    // then it should redirect to the list with correct parameters
    expect(router.navigate).toHaveBeenCalledWith([], {
      queryParams: { page: undefined, status: ['CREATED'], org: ['1001'], gov: ['1002'] }
    });
  });

  it('should refresh the list when an issue is deleted', async () => {
    // given a component
    issueTargetService.loadIssueTargets.and.returnValue(of([]));

    const router = TestBed.inject(Router) as Router;
    spyOn(router, 'navigate');

    const page = '3';
    const status: Array<IssueStatus> = ['CREATED'];
    const org: Array<string> = ['1000'];
    const gov: Array<string> = ['1001', '1002'];
    route.setQueryParams({ page, org, gov, status });

    tester = new IssueListComponentTester();
    await tester.stable();

    service.list.calls.reset();

    // when deleting an issue
    tester.issueComponents[0].deleted.emit();

    // then it should refresh the list with the existing parameters
    expect(service.list).toHaveBeenCalledWith({
      page: 3,
      statusFilter: ['CREATED'],
      poolingOrganizationIds: [1000],
      governmentIds: [1001, 1002]
    });
  });

  it('should say that there is no issue if there is none', async () => {
    service.list.and.returnValue(of(createPage([], 20, 0, 0)));

    tester = new IssueListComponentTester();
    await tester.stable();

    expect(tester.issues.length).toBe(0);
    expect(tester.noIssues).toContainText("Pas de remontées d'information");
  });

  it('should compute markers', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();
    const vm = tester.componentInstance.vm;

    expect(vm()!.allMapMarkers.length).toBe(2);
    expect(vm()!.allMapMarkers[0].getLatLng()).toEqual(latLng(48.858624, 2.294537));
    expect(vm()!.allMapMarkers[1].getLatLng()).toEqual(latLng(45.7573654, 4.8427945));
    const tooltipContent = (vm()!.allMapMarkers[0].getTooltip()!.getContent() as HTMLElement)
      .innerText;
    expect(tooltipContent).toContain('#42 - Other');
    expect(tooltipContent).toContain('Rejetée');
    expect(tooltipContent).toContain('8 janv. 2019');
    expect(vm()!.allMapMarkers[0].hasEventListeners('click')).toBe(true);
    expect(vm()!.allMapMarkers[0].getIcon().options.className).toBe('rejected');

    const fakeElement = {
      scrollIntoView: () => {
        // do nothing
      }
    } as Element;
    spyOn(fakeElement, 'scrollIntoView');
    spyOn(document, 'querySelector').and.returnValue(fakeElement);

    // handle click event
    vm()!.allMapMarkers[0].fireEvent('click');

    // because only one overload is deprecated, and we're not using it:
    // eslint-disable-next-line @typescript-eslint/no-deprecated
    expect(document.querySelector).toHaveBeenCalledWith('#issue-42');
    expect(fakeElement.scrollIntoView).toHaveBeenCalled();
    expect(vm()!.selectedIssue!).toBe(issues.content[0]);

    // handle tooltip close
    vm()!.allMapMarkers[0].fireEvent('tooltipclose');
    expect(vm()!.selectedIssue).toBeNull();

    // handle tooltip open
    vm()!.allMapMarkers[0].fireEvent('tooltipopen');
    expect(vm()!.selectedIssue!).toBe(issues.content[0]);
  });

  it('should compute bounds', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();
    const vm = tester.componentInstance.vm;

    await tester.stable();

    expect(vm()!.mapFitBounds.getSouthWest()).toEqual(latLng(45.60230247, 2.167124125));
    expect(vm()!.mapFitBounds.getNorthEast()).toEqual(latLng(49.01368693, 4.970207375));
  });

  it('should display a map with markers', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();

    expect(tester.mapMarkers.length).toBe(2);
  });

  it('should update the map with only one marker for the issue', async () => {
    tester = new IssueListComponentTester();
    await tester.stable();
    expect(tester.issueComponents[0].selected()).toBe(false);

    // mouse over an issue
    tester.issues[0].nativeElement.dispatchEvent(new Event('mouseenter', { bubbles: true }));
    await tester.stable();

    expect(tester.mapMarkers.length).toBe(1);
    expect(tester.issueComponents[0].selected()).toBe(true);

    // mouse out of an issue
    tester.issues[0].nativeElement.dispatchEvent(new Event('mouseleave', { bubbles: true }));
    await tester.stable();

    expect(tester.mapMarkers.length).toBe(2);
    expect(tester.issueComponents[0].selected()).toBe(false);
  });

  it('should reload the issue and update the issues and the markers when issue status changes', async () => {
    issueTargetService.loadIssueTargets.and.returnValue(of([]));

    tester = new IssueListComponentTester();
    await tester.stable();

    const vm = tester.componentInstance.vm;

    const firstIssue = issues.content[0];
    service.get.and.returnValue(of({ ...firstIssue, status: 'CREATED' }));

    tester.issueComponents[0].statusUpdated.emit('CREATED');
    await tester.stable();

    expect(vm()!.issues.content[0].status).toBe('CREATED');
    expect((vm()!.allMapMarkers![0].getTooltip()!.getContent() as HTMLElement).innerText).toContain(
      'Créée'
    );
    expect((vm()!.mapMarkers![0].getTooltip()!.getContent() as HTMLElement).innerText).toContain(
      'Créée'
    );
    expect(vm()!.mapMarkers![0].getIcon().options.className).toBe('created');
  });
});
