import {
  ChangeDetectionStrategy,
  Component,
  inject,
  LOCALE_ID,
  NgZone,
  Signal
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DecimalPipe, formatDate } from '@angular/common';
import {
  combineLatest,
  concatWith,
  first,
  map,
  merge,
  Observable,
  of,
  startWith,
  Subject,
  switchMap,
  switchScan
} from 'rxjs';
import {
  featureGroup,
  latLng,
  latLngBounds,
  LatLngBounds,
  MapOptions,
  Marker,
  marker,
  MarkerOptions
} from 'leaflet';

import { IssueService } from '../../issue.service';
import { IssueModel, IssueStatus } from '../../models/issue.model';
import { defaultMapOptions, defaultMarkerOptions } from '../../map';
import { PageModel } from '../../models/page.model';
import { translateStatus } from '../../shared/status.pipe';
import { IssueTarget, IssueTargetService } from '../issue-target.service';
import { PageLastIndexPipe } from '../page-last-index.pipe';
import { PageFirstIndexPipe } from '../page-first-index.pipe';
import { LeafletModule } from '@bluehalo/ngx-leaflet';
import { IssueComponent } from '../issue/issue.component';
import { NgbPagination } from '@ng-bootstrap/ng-bootstrap';
import { StatusFilterComponent } from '../status-filter/status-filter.component';
import { IssueTargetSelectorComponent } from '../issue-target-selector/issue-target-selector.component';
import { takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';

type MarkerOptionsWithIssueId = MarkerOptions & { issueId: number };

/**
 * The immutable model of the view. It's replaced by a new one whenever it needs to change.
 */
interface ViewModel {
  readonly issues: PageModel<IssueModel>;
  readonly selectedIssue: IssueModel | null;
  readonly selectedTargets: Array<IssueTarget>;
  readonly statusFilter: Array<IssueStatus>;
  readonly allMapMarkers: Array<Marker>;
  readonly mapMarkers: Array<Marker>;
  readonly mapFitBounds: LatLngBounds;
}

interface BaseAction {
  type: 'issue-selected' | 'issue-marked' | 'issue-status-updated';
}

interface IssueSelected extends BaseAction {
  type: 'issue-selected';
  issue: IssueModel | null;
}

interface IssueMarked extends BaseAction {
  type: 'issue-marked';
  issue: IssueModel | null;
}

interface IssueStatusUpdated extends BaseAction {
  type: 'issue-status-updated';
  issue: IssueModel;
  newStatus: IssueStatus;
}

type Action = IssueSelected | IssueMarked | IssueStatusUpdated;

/**
 * The component used to query (by status) and display a paginated list of issues, along with a map showing where
 * those issues are located.
 */
@Component({
  selector: 'dm-issue-list',
  templateUrl: './issue-list.component.html',
  styleUrl: './issue-list.component.scss',
  imports: [
    IssueTargetSelectorComponent,
    StatusFilterComponent,
    NgbPagination,
    IssueComponent,
    LeafletModule,
    DecimalPipe,
    PageFirstIndexPipe,
    PageLastIndexPipe
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueListComponent {
  private readonly route = inject(ActivatedRoute);
  private readonly router = inject(Router);
  private readonly issueService = inject(IssueService);
  private readonly issueTargetService = inject(IssueTargetService);
  private readonly zone = inject(NgZone);

  readonly vm: Signal<ViewModel | undefined>;
  readonly mapOptions: MapOptions = defaultMapOptions();
  readonly maxSize = 5;

  /**
   * Emits when the selected targets change
   */
  private readonly selectedTargetsSubject = new Subject<Array<IssueTarget>>();

  /**
   * Emits when the selected statuses change
   */
  private readonly selectedStatusesSubject = new Subject<Array<IssueStatus>>();

  /**
   * Emits when an issue is deleted
   */
  private readonly issueDeletedSubject = new Subject<void>();

  /**
   * Emits when an action is made on the displayed issues
   */
  private readonly actionSubject = new Subject<Action>();

  private readonly locale = inject(LOCALE_ID);

  constructor() {
    // navigate when the selected targets change
    this.selectedTargetsSubject.pipe(takeUntilDestroyed()).subscribe(selectedTargets => {
      const queryParams = { ...this.route.snapshot.queryParams };
      queryParams.gov = selectedTargets
        .filter(t => t.type === 'government')
        .map(t => t.id.toString());
      queryParams.org = selectedTargets
        .filter(t => t.type === 'pooling-organization')
        .map(t => t.id.toString());
      // reset the page
      queryParams.page = undefined;

      this.router.navigate([], { queryParams });
    });

    // navigate when the selected statuses change
    this.selectedStatusesSubject.pipe(takeUntilDestroyed()).subscribe(statusFilter => {
      const queryParams = { ...this.route.snapshot.queryParams };
      queryParams.status = statusFilter.length ? statusFilter : undefined;
      // reset the page
      queryParams.page = undefined;

      this.router.navigate([], { queryParams });
    });

    // whenever the parameters change or an issue is deleted, the page of issues is refreshed
    const issues$ = merge(
      this.route.queryParamMap,
      this.issueDeletedSubject.pipe(map(() => this.route.snapshot.queryParamMap))
    ).pipe(
      // extract parameters
      switchMap(params => {
        const page = this.extractPageFromParameters(params);
        const statusFilter = this.extractStatusesFromParameters(params);
        const poolingOrganizationIds = this.extractPoolingOrganizationIdsFromParameters(params);
        const governmentIds = this.extractGovernmentIdsFromParameters(params);
        // launch the search
        return this.issueService.list({
          page,
          statusFilter,
          poolingOrganizationIds,
          governmentIds
        });
      })
    );

    // whenever the issues change, the markers are recreated
    const issuesWithMarkers$ = issues$.pipe(
      map(issues => {
        const allMapMarkers = this.computeMarkers(issues.content);
        const mapFitBounds = this.computeBounds(allMapMarkers);
        return {
          issues,
          allMapMarkers,
          mapFitBounds,
          mapMarkers: allMapMarkers,
          selectedIssue: null
        };
      })
    );

    const selectedTargets$ = this.route.queryParamMap.pipe(
      first(),
      switchMap(params => this.initializeSelectedTargets(params)),
      concatWith(this.selectedTargetsSubject)
    );

    const statusFilter$: Observable<Array<IssueStatus>> = this.route.queryParamMap.pipe(
      first(),
      map(params => this.extractStatusesFromParameters(params)),
      concatWith(this.selectedStatusesSubject)
    );

    // the complete model is created by combining the issues and their markers with the selected targets and the status filter
    const vm$ = combineLatest([issuesWithMarkers$, selectedTargets$, statusFilter$]).pipe(
      map(([issuesWithSelectedIssueAndMarkers, selectedTargets, statusFilter]) => ({
        ...issuesWithSelectedIssueAndMarkers,
        selectedTargets,
        statusFilter
      }))
    );

    // and this model is changed whenever an action is performed
    this.vm = toSignal(
      vm$.pipe(
        switchMap(vm =>
          this.actionSubject.pipe(
            switchScan((vm, action) => this.updateVmWithAction(vm, action), vm),
            startWith(vm)
          )
        )
      )
    );
  }

  /**
   * Called when the user adds or remove a status from the status filter
   * and triggers a navigation with the new filter and without a page.
   */
  selectStatus(statusFilter: Array<IssueStatus>) {
    this.selectedStatusesSubject.next(statusFilter);
  }

  /**
   * Display only the marker for the issue
   */
  markIssue(issue: IssueModel) {
    this.actionSubject.next({
      type: 'issue-marked',
      issue
    });
  }

  /**
   * Resets the markers of the map
   */
  resetMarkers() {
    this.actionSubject.next({
      type: 'issue-marked',
      issue: null
    });
  }

  /**
   * Method called when the user navigates to a different page using the pagination. It uses the current query
   * and navigates to the requested page.
   */
  navigateToPage(requestedPage: number) {
    const queryParams = { ...this.route.snapshot.queryParams };
    queryParams.page = requestedPage.toString();
    this.router.navigate([], { queryParams });
  }

  selectedTargetsChanged(selectedTargets: Array<IssueTarget>) {
    this.selectedTargetsSubject.next(selectedTargets);
  }

  statusUpdated(issue: IssueModel, newStatus: IssueStatus) {
    this.actionSubject.next({
      type: 'issue-status-updated',
      issue,
      newStatus
    });
  }

  issueDeleted() {
    this.issueDeletedSubject.next();
  }

  private extractPageFromParameters(params: ParamMap): number {
    let page = 1;
    const pageParam = params.get('page');
    if (pageParam) {
      page = +pageParam;
    }
    return page;
  }

  private extractStatusesFromParameters(params: ParamMap): Array<IssueStatus> {
    return params.getAll('status') as Array<IssueStatus>;
  }

  /**
   * Computes the fit bounds of the map,
   * by computing the min/max latitude and min/max longitude of the issues
   * and building the most south west and north east points.
   */
  private computeBounds(markers: Array<Marker>): LatLngBounds {
    if (markers.length === 0) {
      // TODO center on collectivity
      // for now centers on France
      const southWestOfFrance = latLng(41.7034247, -4.9488961);
      const northEastOfFrance = latLng(51.390841, 9.935934);
      return latLngBounds(southWestOfFrance, northEastOfFrance);
    }
    return featureGroup(markers).getBounds().pad(0.05);
  }

  /**
   * Computes an array of map markers,
   * one for each issue coordinate.
   */
  private computeMarkers(issues: Array<IssueModel>): Array<Marker> {
    return issues.map(issue => this.createMapMarker(issue));
  }

  private getTooltipContent(issue: IssueModel) {
    const creationDate = formatDate(issue.creationInstant, 'medium', this.locale);
    return `#${issue.id} - ${issue.category!.label} - ${translateStatus(
      issue.status
    )} - ${creationDate}`;
  }

  private getHtmlTooltipContent(issue: IssueModel) {
    const creationDate = formatDate(issue.creationInstant, 'medium', this.locale);
    const span = document.createElement('span');
    span.append(`#${issue.id} - ${issue.category!.label}`);
    span.append(document.createElement('br'));
    span.append(translateStatus(issue.status));
    span.append(document.createElement('br'));
    span.append(creationDate);
    return span;
  }

  private extractPoolingOrganizationIdsFromParameters(params: ParamMap): Array<number> {
    return params.getAll('org').map(id => parseInt(id, 10));
  }

  private extractGovernmentIdsFromParameters(params: ParamMap): Array<number> {
    return params.getAll('gov').map(id => parseInt(id, 10));
  }

  private initializeSelectedTargets(params: ParamMap): Observable<Array<IssueTarget>> {
    const poolingOrganizationIds = this.extractPoolingOrganizationIdsFromParameters(params);
    const governmentIds = this.extractGovernmentIdsFromParameters(params);
    return this.issueTargetService.loadIssueTargets(poolingOrganizationIds, governmentIds);
  }

  private createMapMarker(issue: IssueModel): Marker {
    const mapMarker = marker([issue.coordinates.latitude, issue.coordinates.longitude], {
      ...defaultMarkerOptions(issue.description!, issue.status.toLowerCase()),
      alt: this.getTooltipContent(issue),
      issueId: issue.id
    } as MarkerOptionsWithIssueId);
    // add a tooltip
    mapMarker.bindTooltip(this.getHtmlTooltipContent(issue));
    // handle a click on the marker
    mapMarker.on('click', () => {
      // and scroll the corresponding issue into view
      document.querySelector(`#issue-${issue.id}`)!.scrollIntoView();
      this.zone.run(() => this.selectIssue(issue));
    });
    mapMarker.on('tooltipopen', () => {
      this.zone.run(() => this.selectIssue(issue));
    });
    mapMarker.on('tooltipclose', () => {
      this.zone.run(() => this.selectIssue(null));
    });
    return mapMarker;
  }

  private selectIssue(issue: IssueModel | null) {
    this.actionSubject.next({
      type: 'issue-selected',
      issue
    });
  }

  private updateVmWithAction(vm: ViewModel, action: Action): Observable<ViewModel> {
    switch (action.type) {
      case 'issue-status-updated':
        return this.updateVmWithIssueStatusUpdated(vm, action);
      case 'issue-selected':
        return of(this.updateVmWithIssueSelected(vm, action));
      case 'issue-marked':
        return of(this.updateVmWithIssueMarked(vm, action));
    }
  }

  private updateVmWithIssueSelected(vm: ViewModel, action: IssueSelected): ViewModel {
    return {
      ...vm,
      selectedIssue: action.issue!
    };
  }

  private updateVmWithIssueMarked(vm: ViewModel, action: IssueMarked): ViewModel {
    return {
      ...vm,
      selectedIssue: action.issue!,
      mapMarkers:
        action.issue === null
          ? vm.allMapMarkers
          : [
              vm.allMapMarkers.find(
                mapMarker =>
                  (mapMarker.options as MarkerOptionsWithIssueId).issueId === action.issue!.id
              )!
            ]
    };
  }

  private updateVmWithIssueStatusUpdated(
    vm: ViewModel,
    action: IssueStatusUpdated
  ): Observable<ViewModel> {
    return this.issueService.get(action.issue.id).pipe(
      map(newIssue => {
        const newIssues = {
          ...vm.issues,
          content: vm.issues.content.map(issue => (issue.id === newIssue.id ? newIssue : issue))
        };
        const oldIssueMarker = vm.allMapMarkers.find(
          marker => (marker.options as MarkerOptionsWithIssueId).issueId === newIssue.id
        );
        const newIssueMarker = this.createMapMarker(newIssue);
        const newAllMapMarkers = vm.allMapMarkers.map(marker =>
          marker === oldIssueMarker ? newIssueMarker : marker
        );
        const newMapMarkers = vm.mapMarkers.map(marker =>
          marker === oldIssueMarker ? newIssueMarker : marker
        );
        return {
          ...vm,
          issues: newIssues,
          allMapMarkers: newAllMapMarkers,
          mapMarkers: newMapMarkers
        };
      })
    );
  }
}
