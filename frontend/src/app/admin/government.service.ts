import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';

import { PageModel } from '../models/page.model';
import {
  GovernmentCommand,
  GovernmentDetailedModel,
  GovernmentModel
} from './models/government.model';
import { PictureModel } from '../models/issue.model';

export const MAX_LOGO_SIZE = 256;

/**
 * Service used for the CRUD of governments
 */
@Injectable({
  providedIn: 'root'
})
export class GovernmentService {
  private http = inject(HttpClient);

  search(query: string | null, page: number): Observable<PageModel<GovernmentModel>> {
    const params: Record<string, string | number> = query ? { query } : {};
    params.page = page;
    return this.http.get<PageModel<GovernmentModel>>('/api/governments', { params });
  }

  create(command: GovernmentCommand): Observable<GovernmentModel> {
    return this.http.post<GovernmentModel>('/api/governments', command);
  }

  get(governmentId: number): Observable<GovernmentDetailedModel> {
    return this.http.get<GovernmentDetailedModel>(`/api/governments/${governmentId}`);
  }

  update(governmentId: number, command: GovernmentCommand) {
    return this.http.put<GovernmentModel>(`/api/governments/${governmentId}`, command);
  }

  uploadLogo(governmentId: number, file: Blob): Observable<PictureModel> {
    const formData = new FormData();
    formData.set('file', file);
    return this.http.post<PictureModel>(`/api/governments/${governmentId}/logo`, formData);
  }

  deleteLogo(governmentId: number, logoId: number): Observable<void> {
    return this.http.delete<void>(`/api/governments/${governmentId}/logo/${logoId}`);
  }

  /**
   * Validates that the logo has a width and a height that are <= 512px. If not, the returned observeble emits an error.
   * If valid, it emits the original file
   */
  validateLogo(file: File): Observable<Blob> {
    return new Observable((observer: Subscriber<Blob>) => {
      const img = document.createElement('img');
      this._readFile(file, image => {
        img.src = image;
        img.onload = () => {
          if (img.width > MAX_LOGO_SIZE || img.height > MAX_LOGO_SIZE) {
            observer.error('the logo file is too large');
          } else {
            observer.next(file);
            observer.complete();
          }
        };
      });
    });
  }

  /**
   * For the sake of unit testing
   */
  _readFile(file: File, callback: (image: string) => void) {
    const reader = new FileReader();
    reader.onload = () => callback(reader.result as string);
    reader.readAsDataURL(file);
  }
}
