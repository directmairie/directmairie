import { Pipe, PipeTransform } from '@angular/core';
import { PageModel } from '../models/page.model';

/**
 * Pipe used to extract the index of the last element of a page of result, i.e. the number N in
 * "elements M to N on a total of T"
 */
@Pipe({
  name: 'pageLastIndex'
})
export class PageLastIndexPipe<T> implements PipeTransform {
  transform(page: PageModel<T>): number {
    return page.number * page.size + page.content.length;
  }
}
