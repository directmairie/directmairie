import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { UserModel } from '../registration/models/registration.model';
import { PageModel } from '../models/page.model';
import { createPage } from '../models/page.model.spec';
import { provideHttpClient } from '@angular/common/http';

describe('UserService', () => {
  let http: HttpTestingController;
  let service: UserService;
  const user1: UserModel = {
    id: 1,
    email: 'user1@rhone.fr'
  } as UserModel;
  const user2: UserModel = {
    id: 2,
    email: 'user2@rhone.fr'
  } as UserModel;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UserService);
  });

  it('should search users by email', () => {
    const query = 'user1';

    let usersReceived: PageModel<UserModel> | null = null;
    service.searchUserByEmail(query).subscribe(response => (usersReceived = response));

    const request = http.expectOne('/api/users?query=user1&page=0');
    const users = createPage([user1, user2], 20, 1, 0);
    request.flush(users);

    expect(usersReceived!).toEqual(users);
  });
});
