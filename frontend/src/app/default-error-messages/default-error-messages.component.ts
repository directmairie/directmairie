import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import {
  ValdemortConfig,
  DefaultValidationErrorsDirective,
  ValidationErrorDirective
} from 'ngx-valdemort';

/**
 * Component used to include the default validation error messages displayed by ngx-valdemort. This component
 * can also be created inside a unit-test in order to configure ngx-valdemort the same way as in the actual
 * application and thus test the correct validation of a component.
 */
@Component({
  selector: 'dm-default-error-messages',
  templateUrl: './default-error-messages.component.html',
  styleUrl: './default-error-messages.component.scss',
  imports: [DefaultValidationErrorsDirective, ValidationErrorDirective],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultErrorMessagesComponent {
  constructor() {
    const valdemortConfig = inject(ValdemortConfig);
    // style form error with Bootstrap class
    valdemortConfig.errorsClasses = 'invalid-feedback';
  }
}
