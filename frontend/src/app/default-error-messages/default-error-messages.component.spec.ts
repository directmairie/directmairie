import { TestBed } from '@angular/core/testing';

import { DefaultErrorMessagesComponent } from './default-error-messages.component';
import { ValdemortConfig } from 'ngx-valdemort';

describe('DefaultErrorMessagesComponent', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should configure valdemort config', () => {
    const fixture = TestBed.createComponent(DefaultErrorMessagesComponent);
    const component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();

    const valdemortConfig = TestBed.inject(ValdemortConfig);
    expect(valdemortConfig.errorsClasses).toBe('invalid-feedback');
  });
});
