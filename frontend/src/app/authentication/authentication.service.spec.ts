import { TestBed } from '@angular/core/testing';

import { AuthenticationService } from './authentication.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { AuthenticationCommand } from './models/authentication.model';
import { provideHttpClient } from '@angular/common/http';
import { AuthenticationResultModel } from '../models/authentication-result.model';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(AuthenticationService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should login', () => {
    const expectedResult: AuthenticationResultModel = {
      token: 'jwt'
    };
    let actualResult: AuthenticationResultModel | null = null;

    const command: AuthenticationCommand = { email: 'john@mail.com', password: 'passw0rd' };
    service.login(command).subscribe(result => (actualResult = result));

    const testRequest = http.expectOne({
      url: '/api/authentications',
      method: 'POST'
    });
    expect(testRequest.request.body).toBe(command);
    testRequest.flush(expectedResult);

    expect(actualResult!).toEqual(expectedResult);
  });
});
