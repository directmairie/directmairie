import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationCommand } from './models/authentication.model';
import { Observable } from 'rxjs';
import { AuthenticationResultModel } from '../models/authentication-result.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private http = inject(HttpClient);

  requestedUrl: string | null = null;

  login(command: AuthenticationCommand): Observable<AuthenticationResultModel> {
    return this.http.post<AuthenticationResultModel>('/api/authentications', command);
  }
}
