import { Routes } from '@angular/router';
import { AuthenticationComponent } from './authentication/authentication.component';

export const authenticationRoutes: Routes = [
  {
    path: '',
    component: AuthenticationComponent
  }
];
