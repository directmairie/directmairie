export interface AuthenticationCommand {
  email: string;
  password: string;
}
