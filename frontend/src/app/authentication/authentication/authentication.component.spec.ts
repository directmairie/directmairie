import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { provideRouter, Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { ComponentTester, createMock } from 'ngx-speculoos';

import { AuthenticationComponent } from './authentication.component';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { AuthenticationResultModel } from '../../models/authentication-result.model';
import { AuthenticationService } from '../authentication.service';
import { AuthenticationCommand } from '../models/authentication.model';
import { CurrentUserService } from '../../current-user.service';
import { CurrentUserModel } from '../../models/current-user.model';
import { SuccessService } from '../../success.service';
import { MessageService } from '../../message.service';
import { DeanonimizedIssues, IssueService } from '../../issue.service';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';
import { SubscriptionService } from '../../account/subscriptions/subscription.service';

class AuthenticationComponentTester extends ComponentTester<AuthenticationComponent> {
  constructor() {
    super(AuthenticationComponent);
  }

  get email() {
    return this.input('#email')!;
  }

  get password() {
    return this.input('#password')!;
  }

  get login() {
    return this.button('#login')!;
  }

  get authenticationFailed() {
    return this.element('#authenticationFailed');
  }

  get subscriptionComponent() {
    return this.component(SubscriptionAfterSubmissionComponent);
  }
}

describe('AuthenticationComponent', () => {
  let tester: AuthenticationComponentTester;
  let router: Router;
  let authenticationService: jasmine.SpyObj<AuthenticationService>;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let issueService: jasmine.SpyObj<IssueService>;
  let messageService: jasmine.SpyObj<MessageService>;
  let successService: jasmine.SpyObj<SuccessService>;

  beforeEach(async () => {
    authenticationService = createMock(AuthenticationService);
    currentUserService = createMock(CurrentUserService);
    issueService = createMock(IssueService);
    messageService = createMock(MessageService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: AuthenticationService, useValue: authenticationService },
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: IssueService, useValue: issueService },
        { provide: MessageService, useValue: messageService },
        { provide: SuccessService, useValue: successService },
        { provide: SubscriptionService, useValue: createMock(SubscriptionService) }
      ]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();
    tester = new AuthenticationComponentTester();
    await tester.stable();

    router = TestBed.inject(Router);
  });

  it('should display an empty form', () => {
    expect(tester.email).toHaveValue('');
    expect(tester.password).toHaveValue('');
    expect(tester.authenticationFailed).toBeNull();
    expect(tester.subscriptionComponent).toBeNull();
  });

  it('should validate the form', async () => {
    await tester.login.click();

    expect(tester.testElement).toContainText(`L'adresse courriel est obligatoire`);
    expect(tester.testElement).toContainText('Le mot de passe est obligatoire');

    await tester.email.fillWith('bla');

    expect(tester.testElement).toContainText(
      `L'adresse courriel doit être une adresse courriel valide`
    );

    await tester.login.click();
    expect(authenticationService.login).not.toHaveBeenCalled();
  });

  it('should login and navigate to requested URL if no suggested subscription government returned', async () => {
    const expectedCommand: AuthenticationCommand = {
      email: 'john@mail.com',
      password: 'passw0rd'
    };

    const result: AuthenticationResultModel = {
      token: 'jwt'
    };
    authenticationService.login.and.returnValue(of(result));
    issueService.deanonymizeIssues.and.returnValue(
      of({ count: 2, suggestedSubscriptionGovernment: null })
    );
    messageService.getDeanonymizedSuccessMessage.and.returnValue('2 remontées.');
    currentUserService.storeAndRefresh.and.returnValue(
      of({ id: 1000, firstName: 'John', email: expectedCommand.email } as CurrentUserModel)
    );
    spyOn(router, 'navigateByUrl');
    authenticationService.requestedUrl = '/issues';

    tester.componentInstance.authenticationFailed.set(true);
    await tester.email.fillWith(expectedCommand.email);
    await tester.password.fillWith(expectedCommand.password);
    await tester.login.click();

    expect(tester.subscriptionComponent).toBeNull();
    expect(authenticationService.login).toHaveBeenCalledWith(expectedCommand);
    expect(tester.authenticationFailed).toBeNull();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/issues');
    expect(currentUserService.storeAndRefresh).toHaveBeenCalledWith(result);
    expect(authenticationService.requestedUrl).toBeNull();
    expect(issueService.deanonymizeIssues).toHaveBeenCalled();
    expect(messageService.getDeanonymizedSuccessMessage).toHaveBeenCalledWith(2);
    expect(successService.success).toHaveBeenCalledWith('Bienvenue John\u00a0! 2 remontées.');
  });

  it('should login and navigate to requested URL after subscription done if government returned', async () => {
    const expectedCommand: AuthenticationCommand = {
      email: 'john@mail.com',
      password: 'passw0rd'
    };

    const result: AuthenticationResultModel = {
      token: 'jwt'
    };
    authenticationService.login.and.returnValue(of(result));
    issueService.deanonymizeIssues.and.returnValue(
      of({
        count: 2,
        suggestedSubscriptionGovernment: { id: 42, name: 'Loire' }
      } as DeanonimizedIssues)
    );
    messageService.getDeanonymizedSuccessMessage.and.returnValue('2 remontées.');
    currentUserService.storeAndRefresh.and.returnValue(
      of({ id: 1000, firstName: 'John', email: expectedCommand.email } as CurrentUserModel)
    );
    spyOn(router, 'navigateByUrl');
    authenticationService.requestedUrl = '/issues';

    tester.componentInstance.authenticationFailed.set(true);
    await tester.email.fillWith(expectedCommand.email);
    await tester.password.fillWith(expectedCommand.password);
    await tester.login.click();

    expect(tester.subscriptionComponent).not.toBeNull();
    expect(authenticationService.login).toHaveBeenCalledWith(expectedCommand);
    expect(tester.authenticationFailed).toBeNull();
    expect(currentUserService.storeAndRefresh).toHaveBeenCalledWith(result);
    expect(router.navigateByUrl).not.toHaveBeenCalled();
    expect(authenticationService.requestedUrl).not.toBeNull();
    expect(issueService.deanonymizeIssues).toHaveBeenCalled();
    expect(messageService.getDeanonymizedSuccessMessage).toHaveBeenCalledWith(2);
    expect(successService.success).toHaveBeenCalledWith('Bienvenue John\u00a0! 2 remontées.');

    tester.subscriptionComponent.done.emit();
    await tester.stable();

    expect(router.navigateByUrl).toHaveBeenCalledWith('/issues');
    expect(authenticationService.requestedUrl).toBeNull();
  });

  it('should display an authentication failed message if necessary', async () => {
    authenticationService.login.and.returnValue(
      throwError(() => new HttpErrorResponse({ status: 401 }))
    );
    spyOn(router, 'navigate');

    await tester.email.fillWith('john@mail.com');
    await tester.password.fillWith('passw0rd');
    await tester.login.click();

    expect(tester.authenticationFailed).not.toBeNull();
    expect(currentUserService.storeAndRefresh).not.toHaveBeenCalled();
    expect(issueService.deanonymizeIssues).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
  });
});
