import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, RouterLink } from '@angular/router';
import { forkJoin, switchMap } from 'rxjs';

import { AuthenticationService } from '../authentication.service';
import { AuthenticationCommand } from '../models/authentication.model';
import * as icons from '../../icons';
import { CurrentUserService } from '../../current-user.service';
import { SuccessService } from '../../success.service';
import { IssueService } from '../../issue.service';
import { MessageService } from '../../message.service';
import { welcomeName } from '../../models/current-user.model';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { GovernmentModel } from '../../admin/models/government.model';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';

/**
 * The component displaying the authentication form
 */
@Component({
  selector: 'dm-authentication',
  templateUrl: './authentication.component.html',
  styleUrl: './authentication.component.scss',
  imports: [
    RouterLink,
    ReactiveFormsModule,
    FontAwesomeModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    SubscriptionAfterSubmissionComponent
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthenticationComponent {
  private readonly authenticationService = inject(AuthenticationService);
  private readonly router = inject(Router);
  private readonly currentUserService = inject(CurrentUserService);
  private readonly issueService = inject(IssueService);
  private readonly messageService = inject(MessageService);
  private readonly successService = inject(SuccessService);

  readonly form = inject(NonNullableFormBuilder).group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]]
  });
  readonly authenticationFailed = signal(false);
  readonly icons = icons;

  readonly status = signal<'authenticating' | 'subscribing'>('authenticating');
  readonly suggestedSubscriptionGovernment = signal<GovernmentModel | null>(null);

  login() {
    this.authenticationFailed.set(false);
    if (this.form.invalid) {
      return;
    }

    const value = this.form.value;
    const command: AuthenticationCommand = {
      email: value.email!,
      password: value.password!
    };
    this.authenticationService
      .login(command)
      .pipe(
        switchMap(authenticatedUser =>
          forkJoin([
            // get the current user
            this.currentUserService.storeAndRefresh(authenticatedUser),
            // and try to attach previously created issue to the user
            this.issueService.deanonymizeIssues()
          ])
        )
      )
      .subscribe({
        next: ([currentUser, deanonymizedIssues]) => {
          const message = this.messageService.getDeanonymizedSuccessMessage(
            deanonymizedIssues.count
          );
          this.successService.success(`Bienvenue ${welcomeName(currentUser)}\u00a0! ${message}`);
          if (deanonymizedIssues.suggestedSubscriptionGovernment) {
            this.suggestedSubscriptionGovernment.set(
              deanonymizedIssues.suggestedSubscriptionGovernment
            );
            this.status.set('subscribing');
          } else {
            this.subscriptionDone();
          }
        },
        error: (error: unknown) => {
          if (error instanceof HttpErrorResponse && error.status === 401) {
            this.authenticationFailed.set(true);
          }
        }
      });
  }

  subscriptionDone() {
    const url = this.authenticationService.requestedUrl ?? '/';
    this.authenticationService.requestedUrl = null;
    this.router.navigateByUrl(url);
  }
}
