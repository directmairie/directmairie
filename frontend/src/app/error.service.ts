import { Injectable, isDevMode } from '@angular/core';
import { HttpError } from './error-interceptor.service';
import { HttpErrorResponse } from '@angular/common/http';

export const INVALID_REGISTRATION_TOKEN = 'INVALID_REGISTRATION_TOKEN';
export const ALREADY_REGISTERED = 'ALREADY_REGISTERED';
export const UNKNOWN_EMAIL = 'UNKNOWN_EMAIL';
export const INVALID_PASSWORD_MODIFICATION_TOKEN = 'INVALID_PASSWORD_MODIFICATION_TOKEN';
export const EMAIL_ALREADY_USED = 'EMAIL_ALREADY_USED';
export const INCORRECT_PASSWORD = 'INCORRECT_PASSWORD';
export const INVALID_EMAIL_MODIFICATION_TOKEN = 'INVALID_EMAIL_MODIFICATION_TOKEN';

interface FunctionalErrorInfo {
  /**
   * If true, then the standard error component ignores this error, and the component that called the service should
   * handle it
   */
  customHandling: boolean;

  /**
   * The translation. Must only be undefined if the error is handled by the component
   */
  translation?: string;
}

/**
 * Service used by the error component to extract information about http errors emitted by the interceptor service,
 * and by components which need to handle functional errors in a custom way.
 *
 * This service also contains the configuration of all the functional errors, i.e. whether they are handled in a custom
 * way, and what is the actual, translated error message to be displayed in the standard error component if not.
 */
@Injectable({
  providedIn: 'root'
})
export class ErrorService {
  private functionalErrorInfos = new Map<string, FunctionalErrorInfo>();

  constructor() {
    this.functionalErrorInfos.set(INVALID_REGISTRATION_TOKEN, { customHandling: true });
    this.functionalErrorInfos.set(ALREADY_REGISTERED, { customHandling: true });
    this.functionalErrorInfos.set(UNKNOWN_EMAIL, { customHandling: true });
    this.functionalErrorInfos.set(INVALID_PASSWORD_MODIFICATION_TOKEN, { customHandling: true });
    this.functionalErrorInfos.set(EMAIL_ALREADY_USED, { customHandling: true });
    this.functionalErrorInfos.set(INCORRECT_PASSWORD, { customHandling: true });
    this.functionalErrorInfos.set(INVALID_EMAIL_MODIFICATION_TOKEN, { customHandling: true });

    this.functionalErrorInfos.set('POOLING_ORGANIZATION_WITH_SAME_NAME_ALREADY_EXISTS', {
      customHandling: false,
      translation: 'Un mutualisant du même nom existe déjà.'
    });
    this.functionalErrorInfos.set('GOVERNMENT_WITH_SAME_OSM_ID_ALREADY_EXISTS', {
      customHandling: false,
      translation: 'Un gouvernement avec le même identifiant Open Street Map existe déjà.'
    });
    this.functionalErrorInfos.set('GOVERNMENT_WITH_SAME_CODE_ALREADY_EXISTS', {
      customHandling: false,
      translation: 'Un gouvernement avec le même SIREN existe déjà.'
    });
    if (isDevMode()) {
      this.functionalErrorInfos.set('__TEST__', {
        customHandling: false,
        translation: 'test translation'
      });
    }
  }

  hasCustomHandling(error: HttpError): boolean {
    if (!error.functionalError) {
      return false;
    }
    const functionalErrorInfo = this.functionalErrorInfos.get(error.functionalError);
    if (!functionalErrorInfo) {
      return false;
    }
    return functionalErrorInfo.customHandling;
  }

  getMessageToDisplay(error: HttpError): string {
    if (!error.functionalError) {
      return error.message;
    }
    const functionalErrorInfo = this.functionalErrorInfos.get(error.functionalError);
    if (!functionalErrorInfo) {
      return error.message;
    }
    return functionalErrorInfo.translation || error.message;
  }

  isFunctionalErrorWithCode(error: unknown, errorCode: string) {
    if (!(error instanceof HttpErrorResponse)) {
      return false;
    }
    if (error.status !== 400) {
      return false;
    }
    const body = error.error;
    return body && body.functionalError === errorCode;
  }
}
