import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

/**
 * A service used to signal a successful operation. This service simply emits all the messages from an observable, and
 * the main success message observes them to display them when they're emitted.
 */
@Injectable({
  providedIn: 'root'
})
export class SuccessService {
  private messages$ = new Subject<string>();

  getMessage(): Observable<string> {
    return this.messages$.asObservable();
  }

  success(message: string) {
    this.messages$.next(message);
  }
}
