import { TestBed } from '@angular/core/testing';

import { authenticationInterceptor } from './authentication-interceptor.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { HttpClient, provideHttpClient, withInterceptors } from '@angular/common/http';
import { CurrentUserStorageService } from './current-user-storage.service';
import { createMock } from 'ngx-speculoos';

describe('AuthenticationInterceptorService', () => {
  let httpClient: HttpClient;
  let http: HttpTestingController;
  let currentUserStorageService: jasmine.SpyObj<CurrentUserStorageService>;

  beforeEach(() => {
    currentUserStorageService = createMock(CurrentUserStorageService);
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(withInterceptors([authenticationInterceptor])),
        provideHttpClientTesting(),
        { provide: CurrentUserStorageService, useValue: currentUserStorageService }
      ]
    });

    httpClient = TestBed.inject(HttpClient);
    http = TestBed.inject(HttpTestingController);
  });

  it('should not add any header to request to outside world', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue({ token: 'abc' });

    const url = 'http://nominatim.openstreetmap.org/api';
    httpClient.get(url).subscribe();

    const testRequest = http.expectOne(url);
    expect(testRequest.request.headers.has('Authorization')).toBe(false);
  });

  it('should not add any header to request to backend if no current user', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue(null);

    const url = '/api/users/me';
    httpClient.get(url).subscribe();

    const testRequest = http.expectOne(url);
    expect(testRequest.request.headers.has('Authorization')).toBe(false);
  });

  it('should add Authorization header to request to backend if current user', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue({ token: 'abc' });

    const url = '/api/users/me';
    httpClient.get(url).subscribe();

    const testRequest = http.expectOne(url);
    expect(testRequest.request.headers.get('Authorization')).toBe('Bearer abc');
  });
});
