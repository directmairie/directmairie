import {
  faAsterisk,
  faAt,
  faBan,
  faCalendarDay,
  faCamera,
  faCheckCircle,
  faCity,
  faComment,
  faCompass,
  faDownload,
  faEdit,
  faEllipsisV,
  faEnvelope,
  faEnvelopeOpenText,
  faExclamationCircle,
  faExclamationTriangle,
  faFileUpload,
  faFrown,
  faImage,
  faInfoCircle,
  faKey,
  faMapMarkerAlt,
  faNetworkWired,
  faPaperPlane,
  faPlus,
  faPowerOff,
  faSearch,
  faSearchLocation,
  faShareAlt,
  faThumbsUp,
  faTrash,
  faUpload,
  faUserCircle,
  faUserShield
} from '@fortawesome/free-solid-svg-icons';

export const locateMe = faCompass;
export const uploadPicture = faCamera;
export const trash = faTrash;
export const frown = faFrown;
export const searchLocation = faSearchLocation;
export const submitIssue = faUpload;
export const error = faExclamationCircle;
export const emailSent = faEnvelope;
export const registered = faThumbsUp;
export const thumbsUp = faThumbsUp;
export const sandwich = faEllipsisV;
export const currentUser = faUserCircle;
export const logout = faPowerOff;
export const view = faSearch;
export const edit = faEdit;
export const administrator = faUserShield;
export const issueGroup = faInfoCircle;
export const issueCategory = faComment;
export const contactEmail = faAt;
export const add = faPlus;
export const required = faAsterisk;
export const download = faDownload;
export const datepicker = faCalendarDay;
export const noLogo = faImage;
export const uploadLogo = faFileUpload;
export const government = faCity;
export const poolingOrganization = faNetworkWired;
export const issue = faMapMarkerAlt;
export const password = faKey;
export const warning = faExclamationTriangle;
export const optedIn = faCheckCircle;
export const optedOut = faBan;
export const subscriptions = faEnvelopeOpenText;
export const notification = faShareAlt;
export const send = faPaperPlane;
