import { TestBed } from '@angular/core/testing';

import { LostPasswordService } from './lost-password.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { AuthenticationResultModel } from '../models/authentication-result.model';
import { LostPasswordCommand, PasswordModificationCommand } from './models/lost-password.model';
import { provideHttpClient } from '@angular/common/http';

describe('LostPasswordService', () => {
  let service: LostPasswordService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(LostPasswordService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should signal a lost password', () => {
    let ok = false;

    const command: LostPasswordCommand = { email: 'john@mail.com' };
    service.passwordLost(command).subscribe(() => (ok = true));

    const testRequest = http.expectOne({
      url: '/api/passwords/lost',
      method: 'POST'
    });
    expect(testRequest.request.body).toBe(command);
    testRequest.flush(null);

    expect(ok).toBe(true);
  });

  it('should change password', () => {
    const expectedResult: AuthenticationResultModel = {
      token: 'jwt'
    };
    let actualResult: AuthenticationResultModel | null = null;

    const command: PasswordModificationCommand = { token: 'abcd', password: 'passw0rd' };
    service.changePassword(1000, command).subscribe(result => (actualResult = result));

    const testRequest = http.expectOne({
      url: '/api/passwords/1000/modifications',
      method: 'POST'
    });
    expect(testRequest.request.body).toBe(command);
    testRequest.flush(expectedResult);

    expect(actualResult!).toEqual(expectedResult);
  });
});
