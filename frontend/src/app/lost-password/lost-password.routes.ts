import { Routes } from '@angular/router';
import { LostPasswordComponent } from './lost-password/lost-password.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

export const lostPasswordRoutes: Routes = [
  { path: '', component: LostPasswordComponent },
  { path: ':userId', component: ConfirmationComponent }
];
