import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { NonNullableFormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import * as icons from '../../icons';
import { ErrorService, UNKNOWN_EMAIL } from '../../error.service';
import { LostPasswordService } from '../lost-password.service';
import { LostPasswordCommand } from '../models/lost-password.model';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';

/**
 * Component allowing a user to signal that he/she lost his/her password, and receive an email allowing to reset the
 * password
 */
@Component({
  selector: 'dm-lost-password',
  templateUrl: './lost-password.component.html',
  styleUrl: './lost-password.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    FontAwesomeModule
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LostPasswordComponent {
  private readonly lostPasswordService = inject(LostPasswordService);
  private readonly errorService = inject(ErrorService);

  readonly form = inject(NonNullableFormBuilder).group({
    email: ['', [Validators.required, Validators.email]]
  });
  readonly emailSent = signal(false);
  readonly unknownEmail = signal(false);
  readonly icons = icons;

  send() {
    this.unknownEmail.set(false);
    if (this.form.invalid) {
      return;
    }

    const command: LostPasswordCommand = {
      email: this.form.value.email!
    };
    this.lostPasswordService.passwordLost(command).subscribe({
      next: () => this.emailSent.set(true),
      error: (error: unknown) =>
        this.unknownEmail.set(this.errorService.isFunctionalErrorWithCode(error, UNKNOWN_EMAIL))
    });
  }
}
