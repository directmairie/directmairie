import { TestBed } from '@angular/core/testing';

import { LostPasswordComponent } from './lost-password.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { LostPasswordService } from '../lost-password.service';
import { LostPasswordCommand } from '../models/lost-password.model';
import { of } from 'rxjs';
import { functionalErrorObservable } from '../../utils.spec';
import { UNKNOWN_EMAIL } from '../../error.service';

class LostPasswordComponentTester extends ComponentTester<LostPasswordComponent> {
  constructor() {
    super(LostPasswordComponent);
  }

  get formSection() {
    return this.element('#formSection');
  }

  get email() {
    return this.input('#email')!;
  }

  get send() {
    return this.button('#send')!;
  }

  get unknownEmail() {
    return this.element('#unknownEmail');
  }

  get successSection() {
    return this.element('#successSection');
  }

  get retry() {
    return this.button('#retry')!;
  }
}

describe('LostPasswordComponent', () => {
  let tester: LostPasswordComponentTester;
  let lostPasswordService: jasmine.SpyObj<LostPasswordService>;

  beforeEach(async () => {
    lostPasswordService = createMock(LostPasswordService);
    TestBed.configureTestingModule({
      providers: [{ provide: LostPasswordService, useValue: lostPasswordService }]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new LostPasswordComponentTester();
    await tester.stable();
  });

  it('should display an empty form and no success', () => {
    expect(tester.formSection).not.toBeNull();
    expect(tester.successSection).toBeNull();

    expect(tester.email).toHaveValue('');
  });

  it('should validate the form', async () => {
    await tester.send.click();

    expect(tester.formSection).toContainText(`L'adresse courriel est obligatoire`);

    await tester.email.fillWith('bla');

    expect(tester.formSection).toContainText(
      `L'adresse courriel doit être une adresse courriel valide`
    );

    await tester.send.click();
    expect(lostPasswordService.passwordLost).not.toHaveBeenCalled();
  });

  it('should send the request, display a success message, and retry', async () => {
    const expectedCommand: LostPasswordCommand = {
      email: 'john@mail.com'
    };

    lostPasswordService.passwordLost.and.returnValue(of(undefined));

    tester.componentInstance.unknownEmail.set(true);

    await tester.email.fillWith(expectedCommand.email);
    await tester.send.click();

    expect(lostPasswordService.passwordLost).toHaveBeenCalledWith(expectedCommand);
    expect(tester.unknownEmail).toBeNull();
    expect(tester.formSection).toBeNull();
    expect(tester.successSection).not.toBeNull();
    expect(tester.successSection).toContainText(
      `Un courriel vous a été envoyé à l'adresse ${expectedCommand.email}`
    );

    await tester.retry.click();

    expect(tester.formSection).not.toBeNull();
    expect(tester.successSection).toBeNull();
  });

  it('should display unknown email error message if the service says so', async () => {
    lostPasswordService.passwordLost.and.returnValue(functionalErrorObservable(UNKNOWN_EMAIL));

    await tester.email.fillWith('john@mail.com');
    await tester.send.click();

    expect(tester.unknownEmail).not.toBeNull();
  });
});
