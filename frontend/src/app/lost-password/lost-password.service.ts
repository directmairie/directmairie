import { Injectable, inject } from '@angular/core';
import { LostPasswordCommand, PasswordModificationCommand } from './models/lost-password.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationResultModel } from '../models/authentication-result.model';

@Injectable({
  providedIn: 'root'
})
export class LostPasswordService {
  private http = inject(HttpClient);

  passwordLost(command: LostPasswordCommand): Observable<void> {
    return this.http.post<void>('/api/passwords/lost', command);
  }

  changePassword(
    userId: number,
    command: PasswordModificationCommand
  ): Observable<AuthenticationResultModel> {
    return this.http.post<AuthenticationResultModel>(
      `/api/passwords/${userId}/modifications`,
      command
    );
  }
}
