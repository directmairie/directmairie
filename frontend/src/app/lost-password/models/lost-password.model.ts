export interface LostPasswordCommand {
  email: string;
}

export interface PasswordModificationCommand {
  password: string;
  token: string;
}
