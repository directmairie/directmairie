import { TestBed } from '@angular/core/testing';

import { ConfirmationComponent } from './confirmation.component';
import { ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserService } from '../../current-user.service';
import { functionalErrorObservable } from '../../utils.spec';
import { INVALID_PASSWORD_MODIFICATION_TOKEN } from '../../error.service';
import { LostPasswordService } from '../lost-password.service';
import { SuccessService } from '../../success.service';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { of } from 'rxjs';
import { AuthenticationResultModel } from '../../models/authentication-result.model';
import { PasswordModificationCommand } from '../models/lost-password.model';
import { CurrentUserModel } from '../../models/current-user.model';

class ConfirmationComponentTester extends ComponentTester<ConfirmationComponent> {
  constructor() {
    super(ConfirmationComponent);
  }

  get password() {
    return this.input('#password')!;
  }

  get passwordConfirmation() {
    return this.input('#passwordConfirmation')!;
  }

  get save() {
    return this.button('#save')!;
  }

  get invalidToken() {
    return this.element('#invalidToken');
  }
}

describe('ConfirmationComponent', () => {
  let tester: ConfirmationComponentTester;
  let route: ActivatedRoute;
  let lostPasswordService: jasmine.SpyObj<LostPasswordService>;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;

  beforeEach(async () => {
    lostPasswordService = createMock(LostPasswordService);
    currentUserService = createMock(CurrentUserService);
    successService = createMock(SuccessService);

    route = stubRoute({
      params: {
        userId: '1000'
      },
      queryParams: {
        token: 'abcdef'
      }
    });

    TestBed.configureTestingModule({
      providers: [
        { provide: ActivatedRoute, useValue: route },
        { provide: LostPasswordService, useValue: lostPasswordService },
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new ConfirmationComponentTester();
    await tester.stable();
    router = TestBed.inject(Router);
    spyOn(router, 'navigate');
  });

  it('should display an empty form', () => {
    expect(tester.password).toHaveValue('');
    expect(tester.passwordConfirmation).toHaveValue('');
    expect(tester.invalidToken).toBeNull();
  });

  it('should validate the form', async () => {
    await tester.save.click();

    expect(tester.testElement).toContainText(`Le mot de passe est obligatoire`);
    expect(tester.testElement).toContainText(`La confirmation du mot de passe est obligatoire`);

    await tester.password.fillWith('bla');
    await tester.passwordConfirmation.fillWith('blo');

    expect(tester.testElement).not.toContainText(`Le mot de passe est obligatoire`);
    expect(tester.testElement).not.toContainText(`La confirmation du mot de passe est obligatoire`);
    expect(tester.testElement).toContainText(`Les deux mots de passe ne sont pas identiques`);

    await tester.passwordConfirmation.fillWith('bla');
    expect(tester.testElement).not.toContainText(`Les deux mots de passe ne sont pas identiques`);

    expect(lostPasswordService.changePassword).not.toHaveBeenCalled();
  });

  it('should display an invalid token error if the service says so', async () => {
    lostPasswordService.changePassword.and.returnValue(
      functionalErrorObservable(INVALID_PASSWORD_MODIFICATION_TOKEN)
    );

    await tester.password.fillWith('bla');
    await tester.passwordConfirmation.fillWith('bla');
    await tester.save.click();

    expect(tester.invalidToken).not.toBeNull();
    expect(currentUserService.storeAndRefresh).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();
  });

  it('should change the password', async () => {
    const authenticationResult: AuthenticationResultModel = {
      token: 'jwt'
    };
    lostPasswordService.changePassword.and.returnValue(of(authenticationResult));
    currentUserService.storeAndRefresh.and.returnValue(
      of({
        email: 'john@mail.com'
      } as CurrentUserModel)
    );

    await tester.password.fillWith('bla');
    await tester.passwordConfirmation.fillWith('bla');
    await tester.save.click();

    expect(lostPasswordService.changePassword).toHaveBeenCalledWith(1000, {
      token: 'abcdef',
      password: 'bla'
    } as PasswordModificationCommand);
    expect(currentUserService.storeAndRefresh).toHaveBeenCalledWith(authenticationResult);
    expect(successService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });
});
