import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { LostPasswordService } from '../lost-password.service';
import { CurrentUserService } from '../../current-user.service';
import { ErrorService, INVALID_PASSWORD_MODIFICATION_TOKEN } from '../../error.service';
import * as icons from '../../icons';
import { SuccessService } from '../../success.service';
import { NonNullableFormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { PasswordModificationCommand } from '../models/lost-password.model';
import { switchMap } from 'rxjs';
import { passwordsMatch } from '../../custom-validators';
import { welcomeName } from '../../models/current-user.model';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ValidationErrorsComponent, ValidationErrorDirective } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';

/**
 * Component displayed when a user clicks the link to confirm a password modification in the email sent after a
 * password lost request
 */
@Component({
  selector: 'dm-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrl: './confirmation.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    FontAwesomeModule,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationComponent {
  private readonly route = inject(ActivatedRoute);
  private readonly lostPasswordService = inject(LostPasswordService);
  private readonly currentUserService = inject(CurrentUserService);
  private readonly errorService = inject(ErrorService);
  private readonly successService = inject(SuccessService);
  private readonly router = inject(Router);

  readonly icons = icons;
  readonly form = inject(NonNullableFormBuilder).group(
    {
      password: ['', [Validators.required]],
      passwordConfirmation: ['', [Validators.required]]
    },
    {
      validators: passwordsMatch
    }
  );

  readonly invalidToken = signal(false);

  changePassword() {
    if (!this.form.valid) {
      return;
    }

    this.invalidToken.set(false);
    const userId = parseInt(this.route.snapshot.paramMap.get('userId')!);
    const token = this.route.snapshot.queryParamMap.get('token')!;
    const command: PasswordModificationCommand = {
      token,
      password: this.form.value.password!
    };

    this.lostPasswordService
      .changePassword(userId, command)
      .pipe(
        switchMap(authenticationResult =>
          this.currentUserService.storeAndRefresh(authenticationResult)
        )
      )
      .subscribe({
        next: user => {
          this.router.navigate(['/']);
          this.successService.success(
            `Votre mot de passe a été modifié. Bienvenue ${welcomeName(user)}\u00a0!`
          );
        },
        error: (error: unknown) => {
          if (
            this.errorService.isFunctionalErrorWithCode(error, INVALID_PASSWORD_MODIFICATION_TOKEN)
          ) {
            this.invalidToken.set(true);
          }
        }
      });
  }
}
