import { TestBed } from '@angular/core/testing';

import { BrandingService } from './branding.service';
import { CurrentUserService } from './current-user.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { Subject } from 'rxjs';
import { CurrentUserModel } from './models/current-user.model';
import { GovernmentModel } from './admin/models/government.model';
import { provideHttpClient } from '@angular/common/http';
import { createMock } from 'ngx-speculoos';

describe('BrandingService', () => {
  let brandingService: BrandingService;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let http: HttpTestingController;
  let userSubject: Subject<CurrentUserModel | null>;

  beforeEach(() => {
    currentUserService = createMock(CurrentUserService);
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideHttpClientTesting(),
        { provide: CurrentUserService, useValue: currentUserService }
      ]
    });

    userSubject = new Subject<CurrentUserModel | null>();
    currentUserService.getUserChanges.and.returnValue(userSubject);

    brandingService = TestBed.inject(BrandingService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should get branding government based on current user', () => {
    let brandingGovernment: GovernmentModel | null = null;
    brandingService.getBrandingGoverment().subscribe(g => (brandingGovernment = g));

    userSubject.next(null);
    expect(brandingGovernment).toBeNull();
    http.verify();

    userSubject.next({ governmentAdmin: false } as CurrentUserModel);
    expect(brandingGovernment).toBeNull();
    http.verify();

    const government = {
      id: 42
    } as GovernmentModel;

    userSubject.next({ governmentAdmin: true } as CurrentUserModel);
    http.expectOne({ method: 'get', url: '/api/branding' }).flush({ government });
    expect(brandingGovernment!).toBe(government);
    http.verify();

    brandingService.getBrandingGoverment().subscribe();
    http.verify();
  });
});
