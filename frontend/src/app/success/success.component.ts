import { ChangeDetectionStrategy, Component, inject, Signal, viewChild } from '@angular/core';
import { SuccessService } from '../success.service';
import { concat, map, of, switchMap, tap, timer } from 'rxjs';
import { NgbToast } from '@ng-bootstrap/ng-bootstrap';
import { toSignal } from '@angular/core/rxjs-interop';

/**
 * Component displaying a success message for a short amount of time (also known as a "SnackBar" in the Angular Material
 * terminology).
 * It displays all the success messages emitted by the success service.
 */
@Component({
  selector: 'dm-success',
  templateUrl: './success.component.html',
  styleUrl: './success.component.scss',
  imports: [NgbToast],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SuccessComponent {
  readonly message: Signal<string | undefined>;

  readonly ngbToast = viewChild(NgbToast);

  constructor() {
    const successService = inject(SuccessService);
    this.message = toSignal(
      successService.getMessage().pipe(
        switchMap(message =>
          concat(
            of(message),
            timer(4000).pipe(
              tap(() => this.ngbToast()!.hide()),
              switchMap(() => this.ngbToast()!.hidden),
              map(() => undefined)
            )
          )
        )
      )
    );
  }
}
