import { TestBed } from '@angular/core/testing';

import { SuccessComponent } from './success.component';
import { ComponentTester } from 'ngx-speculoos';
import { SuccessService } from '../success.service';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';

class SuccessComponentTester extends ComponentTester<SuccessComponent> {
  constructor() {
    super(SuccessComponent);
  }

  get successContainer() {
    return this.element('.success-container');
  }

  get message() {
    return this.element('.toast-body');
  }
}

describe('SuccessComponent', () => {
  let tester: SuccessComponentTester;
  let successService: SuccessService;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [{ provide: NgbConfig, useValue: { animation: true } as NgbConfig }]
      // keep the animation because the auto-hiding logic relies on it
    });

    tester = new SuccessComponentTester();
    successService = TestBed.inject(SuccessService);
    await tester.stable();

    jasmine.clock().install();
  });

  afterEach(() => jasmine.clock().uninstall());

  it('should not display initially', () => {
    expect(tester.successContainer).toBeNull();
  });

  it('should display a success message and hide it after some seconds', async () => {
    successService.success('hello');
    jasmine.clock().tick(500);
    await tester.stable();

    expect(tester.successContainer).not.toBeNull();
    expect(tester.message).toContainText('hello');

    jasmine.clock().tick(5000);
    await tester.stable();
    expect(tester.successContainer).toBeNull();
  });

  it('should replace success message by new one if new one comes before auto-hide delay', async () => {
    successService.success('hello');
    jasmine.clock().tick(500);
    await tester.stable();

    expect(tester.successContainer).not.toBeNull();
    jasmine.clock().tick(3000);
    await tester.stable();
    successService.success('world');
    jasmine.clock().tick(3000);
    await tester.stable();

    expect(tester.successContainer).not.toBeNull();
    expect(tester.message).toContainText('world');

    jasmine.clock().tick(2000);
  });
});
