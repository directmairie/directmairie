import { TestBed } from '@angular/core/testing';

import { MessageService } from './message.service';

describe('MessageService', () => {
  it('should build an i18n message for deanonymized issues', () => {
    const service = TestBed.inject(MessageService);

    const message = service.getDeanonymizedSuccessMessage(2);
    expect(message).toBe("2 remontées d'information ont été associées à votre compte.");

    const messageForOneIssue = service.getDeanonymizedSuccessMessage(1);
    expect(messageForOneIssue).toBe("Une remontée d'information a été associée à votre compte.");

    const messageForNoIssue = service.getDeanonymizedSuccessMessage(0);
    expect(messageForNoIssue).toBe('');
  });
});
