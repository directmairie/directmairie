import { ALREADY_REGISTERED, ErrorService } from './error.service';
import { HttpError } from './error-interceptor.service';
import { HttpErrorResponse } from '@angular/common/http';

describe('ErrorService', () => {
  let service: ErrorService;

  beforeEach(() => (service = new ErrorService()));

  it('should not have a custom handling if technical error', () => {
    const error = {
      functionalError: null
    } as HttpError;

    expect(service.hasCustomHandling(error)).toBe(false);
  });

  it('should not have a custom handling if functional error with unknown code', () => {
    const error = {
      functionalError: 'unknown'
    } as HttpError;

    expect(service.hasCustomHandling(error)).toBe(false);
  });

  it('should not have a custom handling if functional error with known code configured without custom handling', () => {
    const error = {
      functionalError: '__TEST__'
    } as HttpError;

    expect(service.hasCustomHandling(error)).toBe(false);
  });

  it('should have a custom handling if functional error with known code configured with custom handling', () => {
    const error = {
      functionalError: ALREADY_REGISTERED
    } as HttpError;

    expect(service.hasCustomHandling(error)).toBe(true);
  });

  it('should get server message as message to display if technical error', () => {
    const error = {
      functionalError: null,
      message: 'foo'
    } as HttpError;

    expect(service.getMessageToDisplay(error)).toBe('foo');
  });

  it('should get server message as message to display if unknown functional error', () => {
    const error = {
      functionalError: 'unknown',
      message: 'foo'
    } as HttpError;

    expect(service.getMessageToDisplay(error)).toBe('foo');
  });

  it('should get translation as message to display if known functional error', () => {
    const error = {
      functionalError: '__TEST__',
      message: 'foo'
    } as HttpError;

    expect(service.getMessageToDisplay(error)).toBe('test translation');
  });

  it('should tell that an error is not functional with code when not an HttpErrorResponse', () => {
    expect(service.isFunctionalErrorWithCode('hello', ALREADY_REGISTERED)).toBe(false);
  });

  it('should tell that an error is not functional with code when technical error', () => {
    const response = new HttpErrorResponse({
      status: 500
    });
    expect(service.isFunctionalErrorWithCode(response, ALREADY_REGISTERED)).toBe(false);
  });

  it('should tell that an error is not functional with code when functional error with other code', () => {
    const response = new HttpErrorResponse({
      status: 400,
      error: {
        functionalError: 'foo'
      }
    });
    expect(service.isFunctionalErrorWithCode(response, ALREADY_REGISTERED)).toBe(false);
  });

  it('should tell that an error is functional with code when functional error with same code', () => {
    const response = new HttpErrorResponse({
      status: 400,
      error: {
        functionalError: ALREADY_REGISTERED
      }
    });
    expect(service.isFunctionalErrorWithCode(response, ALREADY_REGISTERED)).toBe(true);
  });
});
