import { TestBed } from '@angular/core/testing';

import { IssueStatusComponent } from './issue-status.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { IssueStatus } from '../models/issue.model';
import { ComponentTester } from 'ngx-speculoos';

@Component({
  imports: [IssueStatusComponent],
  template: `<dm-issue-status [status]="status()" />`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly status = signal<IssueStatus>('CREATED');
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get issueStatus() {
    return this.element(IssueStatusComponent);
  }
}

describe('IssueStatusComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({});

    tester = new TestComponentTester();
    await tester.stable();
  });

  const testCases: Array<{ status: IssueStatus; expectedText: string; expectedClass: string }> = [
    { status: 'CREATED', expectedText: 'Créée', expectedClass: 'text-bg-secondary' },
    { status: 'RESOLVED', expectedText: 'Résolue', expectedClass: 'text-bg-success' },
    { status: 'REJECTED', expectedText: 'Rejetée', expectedClass: 'text-bg-danger' }
  ];

  testCases.forEach(testCase => {
    it(`should display status ${testCase.status}`, async () => {
      tester.componentInstance.status.set(testCase.status);
      await tester.stable();

      expect(tester.issueStatus).toHaveTrimmedText(testCase.expectedText);
      expect(tester.issueStatus).toHaveClass('badge');
      expect(tester.issueStatus).toHaveClass('rounded-pill');
      expect(tester.issueStatus).toHaveClass(testCase.expectedClass);
    });
  });
});
