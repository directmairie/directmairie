import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { IssueStatus } from '../models/issue.model';
import { StatusPipe } from '../shared/status.pipe';

const MAPPING: Record<IssueStatus, string> = {
  DRAFT: 'secondary',
  CREATED: 'secondary',
  REJECTED: 'danger',
  RESOLVED: 'success'
};

@Component({
  selector: 'dm-issue-status',
  imports: [StatusPipe],
  templateUrl: './issue-status.component.html',
  styleUrl: './issue-status.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  host: {
    '[class]': 'classes()'
  }
})
export class IssueStatusComponent {
  readonly status = input.required<IssueStatus>();
  readonly classes = computed(() => ['badge', 'rounded-pill', `text-bg-${MAPPING[this.status()]}`]);
}
