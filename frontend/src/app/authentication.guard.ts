import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivateFn,
  Router,
  RouterStateSnapshot,
  UrlTree
} from '@angular/router';
import { CurrentUserService } from './current-user.service';
import { AuthenticationService } from './authentication/authentication.service';

/**
 * A guard which navigates to the authentication page if the user tries to access a route which needs to be
 * authenticated. It remembers the requested URL so that the user goes to the requested route after login.
 */
export const authenticationGuard: CanActivateFn = (
  _next: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): boolean | UrlTree => {
  const currentUserService = inject(CurrentUserService);
  const authenticationService = inject(AuthenticationService);
  const router = inject(Router);
  if (currentUserService.isAuthenticated()) {
    return true;
  }
  authenticationService.requestedUrl = state.url;
  return router.parseUrl('/authentication');
};
