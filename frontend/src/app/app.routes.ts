import { Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { authenticationGuard } from './authentication.guard';
import { provideDirectMairieDatepicker } from './admin/direct-mairie-datepicker/direct-mairie-datepicker.providers';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'issue',
    loadChildren: () =>
      import('./issue-edition/issue-edition.routes').then(m => m.issueEditionRoutes)
  },
  {
    path: 'registration',
    loadChildren: () => import('./registration/registration.routes').then(m => m.registrationRoutes)
  },
  {
    path: 'authentication',
    loadChildren: () =>
      import('./authentication/authentication.routes').then(m => m.authenticationRoutes)
  },
  {
    path: 'lost-password',
    loadChildren: () =>
      import('./lost-password/lost-password.routes').then(m => m.lostPasswordRoutes)
  },
  {
    path: 'me',
    canActivate: [authenticationGuard],
    loadChildren: () => import('./account/account.routes').then(m => m.accountRoutes)
  },
  {
    path: 'confidentiality',
    loadComponent: () =>
      import('./confidentiality/confidentiality.component').then(m => m.ConfidentialityComponent)
  },
  {
    path: '',
    canActivate: [authenticationGuard],
    children: [
      { path: 'admin', loadChildren: () => import('./admin/admin.routes').then(m => m.adminRoutes) }
    ],
    providers: [provideDirectMairieDatepicker()]
  }
];
