import { TestBed } from '@angular/core/testing';

import { authenticationGuard } from './authentication.guard';
import { CurrentUserService } from './current-user.service';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { provideRouter, Router, RouterStateSnapshot } from '@angular/router';
import { createMock, stubRoute } from 'ngx-speculoos';
import { AuthenticationService } from './authentication/authentication.service';
import { provideHttpClient } from '@angular/common/http';

describe('authenticationGuard', () => {
  let authenticationService: AuthenticationService;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let router: Router;

  beforeEach(() => {
    currentUserService = createMock(CurrentUserService);
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideHttpClientTesting(),
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService }
      ]
    });

    authenticationService = TestBed.inject(AuthenticationService);
    router = TestBed.inject(Router);
  });

  it('should return true if authenticated', () => {
    currentUserService.isAuthenticated.and.returnValue(true);
    expect(
      TestBed.runInInjectionContext(() =>
        authenticationGuard(stubRoute().snapshot, null as unknown as RouterStateSnapshot)
      )
    ).toBe(true);
  });

  it('should save requested URL and return UrlTree to authentication if not authenticated', () => {
    currentUserService.isAuthenticated.and.returnValue(false);
    const result = TestBed.runInInjectionContext(() =>
      authenticationGuard(stubRoute().snapshot, { url: '/foo' } as RouterStateSnapshot)
    );
    expect(result).toEqual(router.parseUrl('/authentication'));
    expect(authenticationService.requestedUrl).toBe('/foo');
  });
});
