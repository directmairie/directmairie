import { TestBed } from '@angular/core/testing';

import { RegistrationService } from './registration.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import {
  EmailVerificationCommand,
  RegistrationCommand,
  UserModel
} from './models/registration.model';
import { provideHttpClient } from '@angular/common/http';
import { AuthenticationResultModel } from '../models/authentication-result.model';

describe('RegistrationService', () => {
  let service: RegistrationService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(RegistrationService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should register', () => {
    const expectedUser: UserModel = {
      id: 1000,
      email: 'john@mail.com'
    } as UserModel;
    let actualUser: UserModel | null = null;

    const command: RegistrationCommand = {
      email: 'john@mail.com',
      password: 'passw0rd',
      firstName: 'John',
      lastName: 'Doe',
      phone: null,
      issueTransitionNotificationActivated: true
    };
    service.register(command).subscribe(user => (actualUser = user));

    const testRequest = http.expectOne({
      url: '/api/users',
      method: 'POST'
    });
    expect(testRequest.request.body).toBe(command);
    testRequest.flush(expectedUser);

    expect(actualUser!).toEqual(expectedUser);
  });

  it('should confirm registration', () => {
    const expectedResult: AuthenticationResultModel = {
      token: 'jwt'
    };
    let actualResult: AuthenticationResultModel | null = null;

    const command: EmailVerificationCommand = { token: 'abcd' };
    service.confirmRegistration(1000, command).subscribe(result => (actualResult = result));

    const testRequest = http.expectOne({
      url: '/api/users/1000/email-verifications',
      method: 'POST'
    });
    expect(testRequest.request.body).toBe(command);
    testRequest.flush(expectedResult);

    expect(actualResult!).toEqual(expectedResult);
  });
});
