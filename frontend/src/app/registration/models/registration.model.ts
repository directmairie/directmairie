export interface RegistrationCommand {
  email: string;
  password: string;
  firstName: string | null;
  lastName: string | null;
  phone: string | null;
  issueTransitionNotificationActivated: boolean;
}

export interface EmailVerificationCommand {
  token: string;
}

export interface UserModel {
  id: number;
  email: string;
  firstName: string | null;
  lastName: string | null;
}
