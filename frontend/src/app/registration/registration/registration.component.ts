import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { NonNullableFormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { RegistrationService } from '../registration.service';
import { RegistrationCommand, UserModel } from '../models/registration.model';
import * as icons from '../../icons';
import { ALREADY_REGISTERED, ErrorService } from '../../error.service';
import { passwordsMatch } from '../../custom-validators';
import { RouterLink } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ValidationErrorsComponent, ValidationErrorDirective } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';

/**
 * Component displaying the registration form, and its success message after the registration has been
 * sent.
 */
@Component({
  selector: 'dm-registration',
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.scss',
  imports: [
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    FontAwesomeModule,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegistrationComponent {
  private readonly registrationService = inject(RegistrationService);
  private readonly errorService = inject(ErrorService);

  private readonly fb = inject(NonNullableFormBuilder);
  readonly form = this.fb.group(
    {
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      passwordConfirmation: ['', [Validators.required]],
      firstName: null as string | null,
      lastName: null as string | null,
      phone: null as string | null,
      issueTransitionNotificationActivated: this.fb.control<boolean>(false)
    },
    {
      validators: passwordsMatch
    }
  );
  readonly user = signal<UserModel | null>(null);
  readonly alreadyRegistered = signal(false);
  readonly icons = icons;

  register() {
    this.alreadyRegistered.set(false);
    if (!this.form.valid) {
      return;
    }

    const formValue = this.form.value;
    const command: RegistrationCommand = {
      email: formValue.email!,
      password: formValue.password!,
      firstName: formValue.firstName!,
      lastName: formValue.lastName!,
      phone: formValue.phone!,
      issueTransitionNotificationActivated: formValue.issueTransitionNotificationActivated!
    };
    this.registrationService.register(command).subscribe({
      next: user => this.user.set(user),
      error: (error: unknown) =>
        this.alreadyRegistered.set(
          this.errorService.isFunctionalErrorWithCode(error, ALREADY_REGISTERED)
        )
    });
  }
}
