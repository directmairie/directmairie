import { TestBed } from '@angular/core/testing';

import { RegistrationComponent } from './registration.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { RegistrationService } from '../registration.service';
import { RegistrationCommand, UserModel } from '../models/registration.model';
import { of } from 'rxjs';
import { functionalErrorObservable } from '../../utils.spec';
import { ALREADY_REGISTERED } from '../../error.service';
import { provideRouter } from '@angular/router';

class RegistrationComponentTester extends ComponentTester<RegistrationComponent> {
  constructor() {
    super(RegistrationComponent);
  }

  get formSection() {
    return this.element('#formSection');
  }

  get email() {
    return this.input('#email')!;
  }

  get password() {
    return this.input('#password')!;
  }

  get passwordConfirmation() {
    return this.input('#passwordConfirmation')!;
  }

  get firstName() {
    return this.input('#firstName')!;
  }

  get lastName() {
    return this.input('#lastName')!;
  }

  get phone() {
    return this.input('#phone')!;
  }

  get issueTransitionNotificationActivated() {
    return this.input('#issue-transition-notification-activated')!;
  }

  get register() {
    return this.button('#register')!;
  }

  get successSection() {
    return this.element('#successSection');
  }

  get retry() {
    return this.button('#retry')!;
  }

  get alreadyRegistered() {
    return this.element('#alreadyRegistered');
  }
}

describe('RegistrationComponent', () => {
  let tester: RegistrationComponentTester;
  let registrationService: jasmine.SpyObj<RegistrationService>;

  beforeEach(async () => {
    registrationService = createMock(RegistrationService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: RegistrationService, useValue: registrationService }
      ]
    });

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();
    tester = new RegistrationComponentTester();
    await tester.stable();
  });

  it('should display an empty form and no success', () => {
    expect(tester.formSection).not.toBeNull();
    expect(tester.successSection).toBeNull();

    expect(tester.email).toHaveValue('');
    expect(tester.password).toHaveValue('');
    expect(tester.passwordConfirmation).toHaveValue('');
    expect(tester.firstName).toHaveValue('');
    expect(tester.lastName).toHaveValue('');
    expect(tester.phone).toHaveValue('');
    expect(tester.issueTransitionNotificationActivated).not.toBeChecked();
  });

  it('should validate the form', async () => {
    await tester.register.click();

    expect(tester.formSection).toContainText(`L'adresse courriel est obligatoire`);
    expect(tester.formSection).toContainText('Le mot de passe est obligatoire');
    expect(tester.formSection).toContainText('La confirmation du mot de passe est obligatoire');

    await tester.email.fillWith('bla');

    expect(tester.formSection).toContainText(
      `L'adresse courriel doit être une adresse courriel valide`
    );

    await tester.password.fillWith('password');

    const passwordMatchingError = 'Les deux mots de passe ne sont pas identiques';
    expect(tester.formSection).not.toContainText(passwordMatchingError);

    await tester.passwordConfirmation.fillWith('pass');

    expect(tester.formSection).toContainText(passwordMatchingError);

    await tester.passwordConfirmation.fillWith('password');

    expect(tester.formSection).not.toContainText(passwordMatchingError);

    await tester.register.click();
    expect(registrationService.register).not.toHaveBeenCalled();
  });

  it('should register, display a success message, and retry', async () => {
    const expectedCommand: RegistrationCommand = {
      email: 'john@mail.com',
      password: 'passw0rd',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: true
    };

    const user: UserModel = {
      id: 1000,
      email: expectedCommand.email,
      firstName: 'John',
      lastName: 'Doe'
    };
    registrationService.register.and.returnValue(of(user));

    tester.componentInstance.alreadyRegistered.set(true);

    await tester.email.fillWith(expectedCommand.email);
    await tester.password.fillWith(expectedCommand.password);
    await tester.passwordConfirmation.fillWith(expectedCommand.password);
    await tester.firstName.fillWith(expectedCommand.firstName!);
    await tester.lastName.fillWith(expectedCommand.lastName!);
    await tester.phone.fillWith(expectedCommand.phone!);
    await tester.issueTransitionNotificationActivated.check();
    await tester.register.click();

    expect(registrationService.register).toHaveBeenCalledWith(expectedCommand);
    expect(tester.alreadyRegistered).toBeNull();
    expect(tester.formSection).toBeNull();
    expect(tester.successSection).not.toBeNull();
    expect(tester.successSection).toContainText(
      `Un courriel vous a été envoyé à l'adresse ${expectedCommand.email}`
    );

    await tester.retry.click();

    expect(tester.formSection).not.toBeNull();
    expect(tester.successSection).toBeNull();
  });

  it('should display already registered error message if the service says so', async () => {
    registrationService.register.and.returnValue(functionalErrorObservable(ALREADY_REGISTERED));

    await tester.email.fillWith('john@mail.com');
    await tester.password.fillWith('passw0rd');
    await tester.passwordConfirmation.fillWith('passw0rd');
    await tester.register.click();

    expect(tester.alreadyRegistered).not.toBeNull();
  });
});
