import { Routes } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

export const registrationRoutes: Routes = [
  { path: '', component: RegistrationComponent },
  { path: ':userId', component: ConfirmationComponent }
];
