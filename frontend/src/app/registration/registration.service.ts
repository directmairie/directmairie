import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  EmailVerificationCommand,
  RegistrationCommand,
  UserModel
} from './models/registration.model';
import { Observable } from 'rxjs';
import { AuthenticationResultModel } from '../models/authentication-result.model';

@Injectable({
  providedIn: 'root'
})
export class RegistrationService {
  private http = inject(HttpClient);

  register(command: RegistrationCommand): Observable<UserModel> {
    return this.http.post<UserModel>('/api/users', command);
  }

  confirmRegistration(
    userId: number,
    command: EmailVerificationCommand
  ): Observable<AuthenticationResultModel> {
    return this.http.post<AuthenticationResultModel>(
      `/api/users/${userId}/email-verifications`,
      command
    );
  }
}
