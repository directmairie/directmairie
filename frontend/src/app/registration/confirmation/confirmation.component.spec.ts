import { TestBed } from '@angular/core/testing';

import { ConfirmationComponent } from './confirmation.component';
import { ComponentTester, createMock, stubRoute } from 'ngx-speculoos';
import { ActivatedRoute, provideRouter, Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
import { of, Subject } from 'rxjs';
import { functionalErrorObservable } from '../../utils.spec';
import { INVALID_REGISTRATION_TOKEN } from '../../error.service';
import { CurrentUserService } from '../../current-user.service';
import { CurrentUserModel } from '../../models/current-user.model';
import { DeanonimizedIssues, IssueService } from '../../issue.service';
import { MessageService } from '../../message.service';
import { SuccessService } from '../../success.service';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';
import { AuthenticationResultModel } from '../../models/authentication-result.model';
import { SubscriptionService } from '../../account/subscriptions/subscription.service';

class ConfirmationComponentTester extends ComponentTester<ConfirmationComponent> {
  constructor() {
    super(ConfirmationComponent);
  }

  get spinner() {
    return this.element('#spinner');
  }

  get success() {
    return this.element('#registered');
  }

  get subscriptionComponent() {
    return this.component(SubscriptionAfterSubmissionComponent);
  }

  get invalidToken() {
    return this.element('#invalidToken');
  }
}

describe('ConfirmationComponent', () => {
  let tester: ConfirmationComponentTester;
  let route: ActivatedRoute;
  let registrationService: jasmine.SpyObj<RegistrationService>;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let issueService: jasmine.SpyObj<IssueService>;
  let messageService: jasmine.SpyObj<MessageService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;

  beforeEach(() => {
    route = stubRoute({
      params: {
        userId: '1000'
      },
      queryParams: {
        token: 'abcdef'
      }
    });

    registrationService = createMock(RegistrationService);
    currentUserService = createMock(CurrentUserService);
    issueService = createMock(IssueService);
    messageService = createMock(MessageService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: ActivatedRoute, useValue: route },
        { provide: RegistrationService, useValue: registrationService },
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: IssueService, useValue: issueService },
        { provide: MessageService, useValue: messageService },
        { provide: SuccessService, useValue: successService },
        { provide: SubscriptionService, useValue: createMock(SubscriptionService) }
      ]
    });

    router = TestBed.inject(Router);
  });

  it('should display a spinner while the service has not confirmed', async () => {
    const subject = new Subject<AuthenticationResultModel>();
    registrationService.confirmRegistration.and.returnValue(subject);

    tester = new ConfirmationComponentTester();
    await tester.stable();
    expect(tester.spinner).not.toBeNull();
    expect(tester.success).toBeNull();
    expect(tester.invalidToken).toBeNull();
  });

  it('should display a success once the service has confirmed', async () => {
    const authenticatedUser: AuthenticationResultModel = { token: 'jwt' };
    registrationService.confirmRegistration.and.returnValue(of(authenticatedUser));
    const currentUser = { id: 1000 } as CurrentUserModel;
    currentUserService.storeAndRefresh.and.returnValue(of(currentUser));
    issueService.deanonymizeIssues.and.returnValue(
      of({
        count: 2,
        suggestedSubscriptionGovernment: { id: 42, name: 'Loire' }
      } as DeanonimizedIssues)
    );
    messageService.getDeanonymizedSuccessMessage.and.returnValue('2 remontées.');

    tester = new ConfirmationComponentTester();
    await tester.stable();

    expect(tester.spinner).toBeNull();
    expect(tester.success).not.toBeNull();
    expect(tester.subscriptionComponent).not.toBeNull();
    expect(tester.invalidToken).toBeNull();
    expect(currentUserService.storeAndRefresh).toHaveBeenCalledWith(authenticatedUser);
    expect(issueService.deanonymizeIssues).toHaveBeenCalled();
    expect(messageService.getDeanonymizedSuccessMessage).toHaveBeenCalledWith(2);
    expect(successService.success).toHaveBeenCalledWith('Bienvenue\u00a0! 2 remontées.');

    spyOn(router, 'navigateByUrl');
    tester.subscriptionComponent.done.emit();
    await tester.stable();
    expect(router.navigateByUrl).toHaveBeenCalledWith('/');
  });

  it('should display an invalid token error if the service says so', async () => {
    registrationService.confirmRegistration.and.returnValue(
      functionalErrorObservable(INVALID_REGISTRATION_TOKEN)
    );
    tester = new ConfirmationComponentTester();
    await tester.stable();

    expect(tester.spinner).toBeNull();
    expect(tester.success).toBeNull();
    expect(tester.invalidToken).not.toBeNull();
    expect(currentUserService.storeAndRefresh).not.toHaveBeenCalled();
    expect(issueService.deanonymizeIssues).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
  });
});
