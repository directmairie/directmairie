import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';

import { RegistrationService } from '../registration.service';
import * as icons from '../../icons';
import { ErrorService, INVALID_REGISTRATION_TOKEN } from '../../error.service';
import { CurrentUserService } from '../../current-user.service';
import { switchMap } from 'rxjs';
import { IssueService } from '../../issue.service';
import { SuccessService } from '../../success.service';
import { MessageService } from '../../message.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { GovernmentModel } from '../../admin/models/government.model';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

/**
 * Component displayed when a user clicks the link to confirm a registration in the email sent after a registration
 */
@Component({
  selector: 'dm-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrl: './confirmation.component.scss',
  imports: [RouterLink, FontAwesomeModule, SubscriptionAfterSubmissionComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationComponent {
  private readonly router = inject(Router);

  readonly status = signal<'UNKNOWN' | 'REGISTERED' | 'INVALID_TOKEN'>('UNKNOWN');
  readonly icons = icons;
  readonly suggestedSubscriptionGovernment = signal<GovernmentModel | null>(null);

  constructor() {
    const route = inject(ActivatedRoute);
    const userId = +route.snapshot.paramMap.get('userId')!;
    const token = route.snapshot.queryParamMap.get('token')!;
    const registrationService = inject(RegistrationService);
    const issueService = inject(IssueService);
    const messageService = inject(MessageService);
    const successService = inject(SuccessService);
    const errorService = inject(ErrorService);
    const currentUserService = inject(CurrentUserService);
    registrationService
      .confirmRegistration(userId, { token })
      .pipe(
        switchMap(authenticatedUser => currentUserService.storeAndRefresh(authenticatedUser)),
        // try to attach previously created issue to the user
        switchMap(() => issueService.deanonymizeIssues()),
        takeUntilDestroyed()
      )
      .subscribe({
        next: result => {
          const message = messageService.getDeanonymizedSuccessMessage(result.count);
          successService.success(`Bienvenue\u00a0! ${message}`);
          this.suggestedSubscriptionGovernment.set(result.suggestedSubscriptionGovernment);
          this.status.set('REGISTERED');
        },
        error: (error: unknown) => {
          if (errorService.isFunctionalErrorWithCode(error, INVALID_REGISTRATION_TOKEN)) {
            this.status.set('INVALID_TOKEN');
          }
        }
      });
  }

  subscriptionDone() {
    this.router.navigateByUrl('/');
  }
}
