import { TestBed } from '@angular/core/testing';

import { IssueCategoryService } from './issue-category.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import {
  IssueGroupModel,
  OTHER_CATEGORY_ID,
  OTHER_ISSUE_GROUP_ID
} from './models/issue-category.model';
import { provideHttpClient } from '@angular/common/http';

describe('IssueCategoryService', () => {
  let service: IssueCategoryService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(IssueCategoryService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should list groups of categories', () => {
    const expectedGroups: Array<IssueGroupModel> = [
      {
        id: OTHER_ISSUE_GROUP_ID,
        label: 'Other',
        categories: [
          {
            id: OTHER_CATEGORY_ID,
            label: 'Other',
            descriptionRequired: true
          }
        ]
      }
    ];
    let actualGroups: Array<IssueGroupModel> | null = null;

    service
      .getCategories({ latitude: 0, longitude: 1 })
      .subscribe(groups => (actualGroups = groups));

    http
      .expectOne(
        request =>
          request.method === 'GET' &&
          request.url === '/api/issue-groups' &&
          request.params.get('latitude') === '0' &&
          request.params.get('longitude') === '1'
      )
      .flush(expectedGroups);

    expect(actualGroups!).toEqual(expectedGroups);
  });

  it('should list all groups of categories', () => {
    const expectedGroups: Array<IssueGroupModel> = [
      {
        id: OTHER_ISSUE_GROUP_ID,
        label: 'Other',
        categories: [
          {
            id: OTHER_CATEGORY_ID,
            label: 'Other',
            descriptionRequired: true
          }
        ]
      }
    ];
    let actualGroups: Array<IssueGroupModel> | null = null;

    service.getAllCategories().subscribe(groups => (actualGroups = groups));

    http.expectOne('/api/issue-groups').flush(expectedGroups);

    expect(actualGroups!).toEqual(expectedGroups);
  });
});
