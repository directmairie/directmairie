import { ChangeDetectionStrategy, Component, input } from '@angular/core';
import { FontAwesomeModule, IconDefinition } from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'dm-account-card',
  templateUrl: './account-card.component.html',
  styleUrl: './account-card.component.scss',
  imports: [FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountCardComponent {
  readonly icon = input.required<IconDefinition>();
}
