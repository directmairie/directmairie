import { TestBed } from '@angular/core/testing';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { CurrentUserService } from '../../current-user.service';
import { of } from 'rxjs';
import { AccountService } from '../account.service';
import { SuccessService } from '../../success.service';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { CurrentUserDeletionCommand } from '../account.model';
import { provideRouter, Router } from '@angular/router';
import { functionalErrorObservable } from '../../utils.spec';
import { INCORRECT_PASSWORD } from '../../error.service';
import { AccountDeletionComponent } from './account-deletion.component';

class AccountDeletionComponentTester extends ComponentTester<AccountDeletionComponent> {
  constructor() {
    super(AccountDeletionComponent);
  }

  get password() {
    return this.input('#password')!;
  }

  get delete() {
    return this.button('#delete')!;
  }

  get errors() {
    return this.elements('val-errors div');
  }
}

describe('AccountDeletionComponent', () => {
  let tester: AccountDeletionComponentTester;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let accountService: jasmine.SpyObj<AccountService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;

  beforeEach(async () => {
    currentUserService = createMock(CurrentUserService);
    accountService = createMock(AccountService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: AccountService, useValue: accountService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    router = TestBed.inject(Router);
    spyOn(router, 'navigate');

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new AccountDeletionComponentTester();
    await tester.stable();
  });

  it('should display a form', () => {
    expect(tester.password).toHaveValue('');
  });

  it('should validate', async () => {
    await tester.delete.click();

    expect(tester.errors.length).toBe(1);

    expect(accountService.delete).not.toHaveBeenCalled();
  });

  it('should delete', async () => {
    await tester.password.fillWith('password');

    accountService.delete.and.returnValue(of(undefined));

    await tester.delete.click();
    const expectedCommand: CurrentUserDeletionCommand = {
      password: 'password'
    };
    expect(accountService.delete).toHaveBeenCalledWith(expectedCommand);
    expect(currentUserService.remove).toHaveBeenCalled();
    expect(successService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/']);
  });

  it('should display error if password is incorrect', async () => {
    await tester.password.fillWith('password');

    accountService.delete.and.returnValue(functionalErrorObservable(INCORRECT_PASSWORD));

    await tester.delete.click();

    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();

    expect(tester.testElement).toContainText('Le mot de passe est incorrect');
  });
});
