import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import * as icons from '../../icons';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ErrorService, INCORRECT_PASSWORD } from '../../error.service';
import { AccountService } from '../account.service';
import { CurrentUserService } from '../../current-user.service';
import { SuccessService } from '../../success.service';
import { Router, RouterLink } from '@angular/router';

import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AccountCardComponent } from '../account-card/account-card.component';

@Component({
  selector: 'dm-account-deletion',
  templateUrl: './account-deletion.component.html',
  styleUrl: './account-deletion.component.scss',
  imports: [
    AccountCardComponent,
    ReactiveFormsModule,
    FontAwesomeModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountDeletionComponent {
  private readonly accountService = inject(AccountService);
  private readonly currentUserService = inject(CurrentUserService);
  private readonly successService = inject(SuccessService);
  private readonly errorService = inject(ErrorService);
  private readonly router = inject(Router);

  readonly icons = icons;
  readonly form = inject(NonNullableFormBuilder).group({
    password: ['', Validators.required]
  });
  readonly error = signal<typeof INCORRECT_PASSWORD | null>(null);

  delete() {
    this.error.set(null);
    if (!this.form.valid) {
      return;
    }

    // refresh to make sure we don't overwrite a change done in a different tab by the obsolete user information
    this.accountService.delete({ password: this.form.value.password! }).subscribe({
      next: () => {
        this.successService.success('Votre compte a été supprimé');
        this.currentUserService.remove();
        this.router.navigate(['/']);
      },
      error: err => {
        if (this.errorService.isFunctionalErrorWithCode(err, INCORRECT_PASSWORD)) {
          this.error.set(INCORRECT_PASSWORD);
        }
      }
    });
  }
}
