import { TestBed } from '@angular/core/testing';

import { AccountComponent } from './account.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { CurrentUserService } from '../../current-user.service';
import { CurrentUserModel } from '../../models/current-user.model';
import { BehaviorSubject, of } from 'rxjs';
import { provideRouter } from '@angular/router';

class AccountComponentTester extends ComponentTester<AccountComponent> {
  constructor() {
    super(AccountComponent);
  }

  get email() {
    return this.element('#email');
  }

  get firstName() {
    return this.element('#first-name');
  }

  get lastName() {
    return this.element('#last-name');
  }

  get phone() {
    return this.element('#phone');
  }

  get issueTransitionNotificationActivated() {
    return this.element('#issue-transition-notification-activated');
  }

  get deleteAccountLink() {
    return this.element('#delete-account');
  }
}

describe('AccountComponent', () => {
  let tester: AccountComponentTester;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let user: CurrentUserModel;

  beforeEach(() => {
    user = {
      id: 456,
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: false,
      governmentAdmin: false,
      poolingOrganizationAdmin: false,
      superAdmin: false
    };
    currentUserService = createMock(CurrentUserService);
    currentUserService.getUserChanges.and.returnValue(of(user));

    TestBed.configureTestingModule({
      providers: [provideRouter([]), { provide: CurrentUserService, useValue: currentUserService }]
    });
  });

  it('should display current user', async () => {
    tester = new AccountComponentTester();
    await tester.stable();
    expect(tester.email).toHaveTrimmedText('john@mail.com');
    expect(tester.firstName).toHaveTrimmedText('John');
    expect(tester.lastName).toHaveTrimmedText('Doe');
    expect(tester.phone).toHaveTrimmedText('0612345678');
    expect(tester.issueTransitionNotificationActivated).toContainText(
      `Vous n'êtes pas notifié·e par courriel des changements d'état de vos remontées d'information`
    );
    expect(tester.deleteAccountLink).not.toBeNull();
  });

  it('should display other message if notification activated', async () => {
    const otherUser = { ...user, issueTransitionNotificationActivated: true };
    currentUserService.getUserChanges.and.returnValue(of(otherUser));

    tester = new AccountComponentTester();
    await tester.stable();
    expect(tester.issueTransitionNotificationActivated).toContainText(
      `Vous êtes notifié·e par courriel des changements d'état de vos remontées d'information`
    );
  });

  it('should not display link to delete account if the user is admin', async () => {
    let admin = { ...user, governmentAdmin: true };
    const userSubject = new BehaviorSubject(admin);
    currentUserService.getUserChanges.and.returnValue(userSubject);
    tester = new AccountComponentTester();
    await tester.stable();

    expect(tester.deleteAccountLink).toBeNull();

    admin = { ...user, poolingOrganizationAdmin: true };
    userSubject.next(admin);
    await tester.stable();

    expect(tester.deleteAccountLink).toBeNull();

    admin = { ...user, superAdmin: true };
    userSubject.next(admin);
    await tester.stable();

    expect(tester.deleteAccountLink).toBeNull();
  });
});
