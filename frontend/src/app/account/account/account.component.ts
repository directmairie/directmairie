import { ChangeDetectionStrategy, Component, computed, inject } from '@angular/core';
import { CurrentUserService } from '../../current-user.service';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RouterLink } from '@angular/router';
import { AccountCardComponent } from '../account-card/account-card.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'dm-account',
  templateUrl: './account.component.html',
  styleUrl: './account.component.scss',
  imports: [AccountCardComponent, RouterLink, FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountComponent {
  readonly currentUser = toSignal(inject(CurrentUserService).getUserChanges());
  readonly icons = icons;

  readonly accountDeletionAllowed = computed(() => {
    const user = this.currentUser();
    return !!user && !user.governmentAdmin && !user.poolingOrganizationAdmin && !user.superAdmin;
  });
}
