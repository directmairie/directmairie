export interface EmailModificationCommand {
  password: string;
  email: string;
}

export interface CurrentUserCommand {
  email: string;
  firstName: string | null;
  lastName: string | null;
  phone: string | null;
  issueTransitionNotificationActivated: boolean;
  currentPassword: string;
  newPassword?: string | null;
  emailModificationToken?: string | null;
}

export interface CurrentUserDeletionCommand {
  password: string;
}
