import { Routes } from '@angular/router';
import { AccountComponent } from './account/account.component';
import { AccountEditionComponent } from './account-edition/account-edition.component';
import { PasswordEditionComponent } from './password-edition/password-edition.component';
import { AccountDeletionComponent } from './account-deletion/account-deletion.component';
import { AccountNavigationComponent } from './account-navigation/account-navigation.component';
import { SubscriptionsComponent } from './subscriptions/subscriptions.component';

export const accountRoutes: Routes = [
  {
    path: '',
    component: AccountNavigationComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'account'
      },
      {
        path: 'account',
        component: AccountComponent
      },
      {
        path: 'subscriptions',
        component: SubscriptionsComponent
      }
    ]
  },
  {
    path: 'account/edit',
    component: AccountEditionComponent
  },
  {
    path: 'account/password',
    component: PasswordEditionComponent
  },
  {
    path: 'account/delete',
    component: AccountDeletionComponent
  }
];
