import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  CurrentUserCommand,
  CurrentUserDeletionCommand,
  EmailModificationCommand
} from './account.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private http = inject(HttpClient);

  requestEmailModificationToken(command: EmailModificationCommand): Observable<void> {
    return this.http.post<void>('/api/users/me/email-modification', command);
  }

  update(command: CurrentUserCommand): Observable<void> {
    return this.http.put<void>('/api/users/me', command);
  }

  delete(command: CurrentUserDeletionCommand): Observable<void> {
    return this.http.put<void>('/api/users/me/deletion', command);
  }
}
