import { TestBed } from '@angular/core/testing';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { CurrentUserService } from '../../current-user.service';
import { CurrentUserModel } from '../../models/current-user.model';
import { of } from 'rxjs';
import { AccountEditionComponent } from './account-edition.component';
import { AccountService } from '../account.service';
import { SuccessService } from '../../success.service';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { CurrentUserCommand, EmailModificationCommand } from '../account.model';
import { provideRouter, Router } from '@angular/router';
import { functionalErrorObservable } from '../../utils.spec';
import {
  EMAIL_ALREADY_USED,
  INCORRECT_PASSWORD,
  INVALID_EMAIL_MODIFICATION_TOKEN
} from '../../error.service';

class AccountEditionComponentTester extends ComponentTester<AccountEditionComponent> {
  constructor() {
    super(AccountEditionComponent);
  }

  get email() {
    return this.input('#email')!;
  }

  get firstName() {
    return this.input('#first-name')!;
  }

  get lastName() {
    return this.input('#last-name')!;
  }

  get phone() {
    return this.input('#phone')!;
  }

  get issueTransitionNotificationActivated() {
    return this.input('#issue-transition-notification-activated')!;
  }

  get currentPassword() {
    return this.input('#current-password')!;
  }

  get tokenInput() {
    return this.input('#token')!;
  }

  get save() {
    return this.button('#save')!;
  }

  get errors() {
    return this.elements('val-errors div');
  }
}

describe('AccountEditionComponent', () => {
  let tester: AccountEditionComponentTester;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let accountService: jasmine.SpyObj<AccountService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;

  let user: CurrentUserModel;

  beforeEach(async () => {
    user = {
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: true
    } as CurrentUserModel;

    currentUserService = createMock(CurrentUserService);
    currentUserService.getUserChanges.and.returnValue(of(null, user));

    accountService = createMock(AccountService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: AccountService, useValue: accountService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    router = TestBed.inject(Router);
    spyOn(router, 'navigate');

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new AccountEditionComponentTester();
    await tester.stable();
  });

  it('should display a populated form', () => {
    expect(tester.email).toHaveValue('john@mail.com');
    expect(tester.firstName).toHaveValue('John');
    expect(tester.lastName).toHaveValue('Doe');
    expect(tester.phone).toHaveValue('0612345678');
    expect(tester.issueTransitionNotificationActivated).toBeChecked();
    expect(tester.currentPassword).toHaveValue('');
    expect(tester.tokenInput).toBeNull();
  });

  it('should validate', async () => {
    await tester.email.fillWith('');
    await tester.save.click();

    expect(tester.errors.length).toBe(2);

    await tester.email.fillWith('not-an-email');

    expect(tester.errors.length).toBe(2);

    expect(accountService.requestEmailModificationToken).not.toHaveBeenCalled();
    expect(accountService.update).not.toHaveBeenCalled();
  });

  it('should save after token verification', async () => {
    await tester.email.fillWith('jane@mail.com');
    await tester.firstName.fillWith('Jane');
    await tester.lastName.fillWith('Dean');
    await tester.phone.fillWith('0687654321');
    await tester.issueTransitionNotificationActivated.uncheck();
    await tester.currentPassword.fillWith('password');

    accountService.requestEmailModificationToken.and.returnValue(of(undefined));
    await tester.save.click();

    const expectedEmailModificationCommand: EmailModificationCommand = {
      email: 'jane@mail.com',
      password: 'password'
    };
    expect(accountService.requestEmailModificationToken).toHaveBeenCalledWith(
      expectedEmailModificationCommand
    );

    expect(tester.tokenInput).toHaveValue('');
    await tester.tokenInput.fillWith('the-token');

    accountService.update.and.returnValue(of(undefined));
    currentUserService.refresh.and.returnValue(of({} as CurrentUserModel));

    await tester.save.click();

    const expectedCommand: CurrentUserCommand = {
      email: 'jane@mail.com',
      firstName: 'Jane',
      lastName: 'Dean',
      phone: '0687654321',
      issueTransitionNotificationActivated: false,
      currentPassword: 'password',
      emailModificationToken: 'the-token'
    };
    expect(accountService.update).toHaveBeenCalledWith(expectedCommand);
    expect(currentUserService.refresh).toHaveBeenCalled();
    expect(successService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/me']);
  });

  it('should save without token verification if email does not change', async () => {
    await tester.firstName.fillWith('Jane');
    await tester.lastName.fillWith('Dean');
    await tester.phone.fillWith('0687654321');
    await tester.issueTransitionNotificationActivated.uncheck();
    await tester.currentPassword.fillWith('password');

    accountService.update.and.returnValue(of(undefined));
    currentUserService.refresh.and.returnValue(of({} as CurrentUserModel));

    await tester.save.click();

    const expectedCommand: CurrentUserCommand = {
      email: 'john@mail.com',
      firstName: 'Jane',
      lastName: 'Dean',
      phone: '0687654321',
      issueTransitionNotificationActivated: false,
      currentPassword: 'password',
      emailModificationToken: null
    };
    expect(accountService.update).toHaveBeenCalledWith(expectedCommand);
    expect(currentUserService.refresh).toHaveBeenCalled();
    expect(successService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/me']);
  });

  it('should validate token form', async () => {
    await tester.email.fillWith('jane@mail.com');
    await tester.currentPassword.fillWith('password');

    accountService.requestEmailModificationToken.and.returnValue(of(undefined));
    await tester.save.click();

    expect(tester.tokenInput).toHaveValue('');

    await tester.save.click();

    expect(tester.errors.length).toBe(1);

    expect(accountService.update).not.toHaveBeenCalled();
  });

  it('should display error if current password is incorrect', async () => {
    await tester.currentPassword.fillWith('password');

    accountService.update.and.returnValue(functionalErrorObservable(INCORRECT_PASSWORD));

    await tester.save.click();

    expect(currentUserService.refresh).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();

    expect(tester.testElement).toContainText('Le mot de passe est incorrect');
  });

  it('should display error if email is already used', async () => {
    await tester.email.fillWith('jane@mail.com');
    await tester.currentPassword.fillWith('password');

    accountService.requestEmailModificationToken.and.returnValue(
      functionalErrorObservable(EMAIL_ALREADY_USED)
    );

    await tester.save.click();

    expect(currentUserService.refresh).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();

    expect(tester.testElement).toContainText(
      "L'adresse courriel est déjà utilisée par un autre utilisateur"
    );
  });

  it('should display error if current password is incorrect when requesting token', async () => {
    await tester.email.fillWith('jane@mail.com');
    await tester.currentPassword.fillWith('password');

    accountService.requestEmailModificationToken.and.returnValue(
      functionalErrorObservable(INCORRECT_PASSWORD)
    );

    await tester.save.click();

    expect(tester.testElement).toContainText('Le mot de passe est incorrect');
  });

  it('should display error if token is invalid', async () => {
    await tester.email.fillWith('jane@mail.com');
    await tester.currentPassword.fillWith('password');

    accountService.requestEmailModificationToken.and.returnValue(of(undefined));
    await tester.save.click();

    await tester.tokenInput.fillWith('the-token');

    accountService.update.and.returnValue(
      functionalErrorObservable(INVALID_EMAIL_MODIFICATION_TOKEN)
    );

    await tester.save.click();

    expect(tester.testElement).toContainText('Le code de vérification est incorrect');
  });
});
