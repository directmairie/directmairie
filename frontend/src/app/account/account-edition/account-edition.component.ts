import { ChangeDetectionStrategy, Component, effect, inject, Signal, signal } from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { CurrentUserModel } from '../../models/current-user.model';
import { CurrentUserService } from '../../current-user.service';
import { filter, first, switchMap, tap } from 'rxjs';
import * as icons from '../../icons';
import { AccountService } from '../account.service';
import { Router, RouterLink } from '@angular/router';
import { SuccessService } from '../../success.service';
import {
  EMAIL_ALREADY_USED,
  ErrorService,
  INCORRECT_PASSWORD,
  INVALID_EMAIL_MODIFICATION_TOKEN
} from '../../error.service';
import { CurrentUserCommand } from '../account.model';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';

import { AccountCardComponent } from '../account-card/account-card.component';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'dm-account-edition',
  templateUrl: './account-edition.component.html',
  styleUrl: './account-edition.component.scss',
  imports: [
    AccountCardComponent,
    RequiredFieldsMessageComponent,
    ReactiveFormsModule,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    RouterLink,
    FontAwesomeModule
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountEditionComponent {
  private readonly accountService = inject(AccountService);
  private readonly currentUserService = inject(CurrentUserService);
  private readonly router = inject(Router);
  private readonly successService = inject(SuccessService);
  private readonly errorService = inject(ErrorService);

  private readonly fb = inject(NonNullableFormBuilder);
  readonly form = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    firstName: this.fb.control<string | null>(null),
    lastName: this.fb.control<string | null>(null),
    phone: this.fb.control<string | null>(null),
    currentPassword: ['', Validators.required],
    issueTransitionNotificationActivated: this.fb.control<boolean>(false)
  });

  readonly tokenForm = this.fb.group({
    token: ['', Validators.required]
  });

  readonly editedUser: Signal<CurrentUserModel | undefined>;
  readonly icons = icons;

  readonly error = signal<
    | typeof EMAIL_ALREADY_USED
    | typeof INCORRECT_PASSWORD
    | typeof INVALID_EMAIL_MODIFICATION_TOKEN
    | null
  >(null);

  readonly status = signal<'account' | 'token'>('account');

  constructor() {
    this.editedUser = toSignal(
      this.currentUserService.getUserChanges().pipe(
        filter((currentUser): currentUser is CurrentUserModel => !!currentUser),
        first()
      )
    );
    effect(() => {
      const user = this.editedUser();
      if (user) {
        this.form.setValue({
          email: user.email,
          firstName: user.firstName,
          lastName: user.lastName,
          phone: user.phone,
          currentPassword: '',
          issueTransitionNotificationActivated: user.issueTransitionNotificationActivated
        });
      }
    });
  }

  saveOrRequestToken() {
    this.error.set(null);
    if (!this.form.valid) {
      return;
    }

    const formValue = this.form.getRawValue();
    if (formValue.email !== this.editedUser()?.email) {
      this.requestEmailModificationToken();
    } else {
      this.save(null);
    }
  }

  saveWithToken() {
    this.error.set(null);
    if (!this.tokenForm.valid) {
      return;
    }

    this.save(this.tokenForm.getRawValue().token);
  }

  private requestEmailModificationToken() {
    const formValue = this.form.getRawValue();
    this.accountService
      .requestEmailModificationToken({
        password: formValue.currentPassword,
        email: formValue.email
      })
      .subscribe({
        next: () => {
          this.status.set('token');
        },
        error: err => this.handleError(err)
      });
  }

  private save(emailModificationToken: string | null) {
    const command: CurrentUserCommand = {
      ...this.form.getRawValue(),
      emailModificationToken
    };
    this.accountService
      .update(command)
      .pipe(
        tap(() => this.successService.success('Vos informations ont été enregistrées')),
        switchMap(() => this.currentUserService.refresh())
      )
      .subscribe({
        next: () => {
          this.router.navigate(['/me']);
        },
        error: err => this.handleError(err)
      });
  }

  private handleError(error: unknown): void {
    if (this.errorService.isFunctionalErrorWithCode(error, EMAIL_ALREADY_USED)) {
      this.error.set(EMAIL_ALREADY_USED);
    }
    if (this.errorService.isFunctionalErrorWithCode(error, INCORRECT_PASSWORD)) {
      this.error.set(INCORRECT_PASSWORD);
    }
    if (this.errorService.isFunctionalErrorWithCode(error, INVALID_EMAIL_MODIFICATION_TOKEN)) {
      this.error.set(INVALID_EMAIL_MODIFICATION_TOKEN);
    }
  }
}
