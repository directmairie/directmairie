import { TestBed } from '@angular/core/testing';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { CurrentUserService } from '../../current-user.service';
import { CurrentUserModel } from '../../models/current-user.model';
import { of } from 'rxjs';
import { AccountService } from '../account.service';
import { SuccessService } from '../../success.service';
import { DefaultErrorMessagesComponent } from '../../default-error-messages/default-error-messages.component';
import { CurrentUserCommand } from '../account.model';
import { provideRouter, Router } from '@angular/router';
import { functionalErrorObservable } from '../../utils.spec';
import { INCORRECT_PASSWORD } from '../../error.service';
import { PasswordEditionComponent } from './password-edition.component';

class PasswordEditionComponentTester extends ComponentTester<PasswordEditionComponent> {
  constructor() {
    super(PasswordEditionComponent);
  }

  get newPassword() {
    return this.input('#new-password')!;
  }

  get passwordConfirmation() {
    return this.input('#password-confirmation')!;
  }

  get currentPassword() {
    return this.input('#current-password')!;
  }

  get save() {
    return this.button('#save')!;
  }

  get errors() {
    return this.elements('val-errors div');
  }
}

describe('PasswordEditionComponent', () => {
  let tester: PasswordEditionComponentTester;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let accountService: jasmine.SpyObj<AccountService>;
  let successService: jasmine.SpyObj<SuccessService>;
  let router: Router;

  let user: CurrentUserModel;

  beforeEach(async () => {
    user = {
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: true
    } as CurrentUserModel;

    currentUserService = createMock(CurrentUserService);
    currentUserService.refresh.and.returnValue(of(user));

    accountService = createMock(AccountService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: AccountService, useValue: accountService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    router = TestBed.inject(Router);
    spyOn(router, 'navigate');

    TestBed.createComponent(DefaultErrorMessagesComponent).detectChanges();

    tester = new PasswordEditionComponentTester();
    await tester.stable();
  });

  it('should display a form', () => {
    expect(tester.currentPassword).toHaveValue('');
    expect(tester.newPassword).toHaveValue('');
    expect(tester.passwordConfirmation).toHaveValue('');
  });

  it('should validate', async () => {
    await tester.save.click();

    expect(tester.errors.length).toBe(3);

    await tester.newPassword.fillWith('new-password');
    await tester.passwordConfirmation.fillWith('other-password');

    expect(tester.errors.length).toBe(2);

    expect(accountService.update).not.toHaveBeenCalled();
  });

  it('should save', async () => {
    await tester.currentPassword.fillWith('password');
    await tester.newPassword.fillWith('new-password');
    await tester.passwordConfirmation.fillWith('new-password');

    accountService.update.and.returnValue(of(undefined));

    await tester.save.click();
    const expectedCommand: CurrentUserCommand = {
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: true,
      currentPassword: 'password',
      newPassword: 'new-password'
    };
    expect(currentUserService.refresh).toHaveBeenCalled();
    expect(accountService.update).toHaveBeenCalledWith(expectedCommand);
    expect(successService.success).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/me']);
  });

  it('should display error if current password is incorrect', async () => {
    await tester.currentPassword.fillWith('password');
    await tester.newPassword.fillWith('new-password');
    await tester.passwordConfirmation.fillWith('new-password');

    accountService.update.and.returnValue(functionalErrorObservable(INCORRECT_PASSWORD));

    await tester.save.click();

    expect(successService.success).not.toHaveBeenCalled();
    expect(router.navigate).not.toHaveBeenCalled();

    expect(tester.testElement).toContainText('Le mot de passe est incorrect');
  });
});
