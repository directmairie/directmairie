import { ChangeDetectionStrategy, Component, inject, signal } from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import { CurrentUserService } from '../../current-user.service';
import { SuccessService } from '../../success.service';
import { ErrorService, INCORRECT_PASSWORD } from '../../error.service';
import { passwordsMatch } from '../../custom-validators';
import { switchMap } from 'rxjs';
import { Router, RouterLink } from '@angular/router';
import * as icons from '../../icons';

import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { RequiredComponent } from '../../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../../shared/required-fields-message/required-fields-message.component';
import { AccountCardComponent } from '../account-card/account-card.component';

@Component({
  selector: 'dm-password-edition',
  templateUrl: './password-edition.component.html',
  styleUrl: './password-edition.component.scss',
  imports: [
    AccountCardComponent,
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    ValidationErrorDirective,
    RouterLink
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordEditionComponent {
  private readonly accountService = inject(AccountService);
  private readonly currentUserService = inject(CurrentUserService);
  private readonly successService = inject(SuccessService);
  private readonly errorService = inject(ErrorService);
  private readonly router = inject(Router);

  readonly form = inject(NonNullableFormBuilder).group(
    {
      currentPassword: ['', Validators.required],
      password: ['', Validators.required],
      passwordConfirmation: ['', Validators.required]
    },
    {
      validators: passwordsMatch
    }
  );
  readonly error = signal<typeof INCORRECT_PASSWORD | null>(null);
  readonly icons = icons;

  save() {
    this.error.set(null);
    if (!this.form.valid) {
      return;
    }

    // refresh to make sure we don't overwrite a change done in a different tab by the obsolete user information
    this.currentUserService
      .refresh()
      .pipe(
        switchMap(user =>
          this.accountService.update({
            email: user.email,
            firstName: user.firstName,
            lastName: user.lastName,
            phone: user.phone,
            issueTransitionNotificationActivated: user.issueTransitionNotificationActivated,
            currentPassword: this.form.value.currentPassword!,
            newPassword: this.form.value.password
          })
        )
      )
      .subscribe({
        next: () => {
          this.successService.success('Votre mot de passe a été modifié');
          this.router.navigate(['/me']);
        },
        error: err => {
          if (this.errorService.isFunctionalErrorWithCode(err, INCORRECT_PASSWORD)) {
            this.error.set(INCORRECT_PASSWORD);
          }
        }
      });
  }
}
