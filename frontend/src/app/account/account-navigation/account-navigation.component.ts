import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  RouterNavDirective,
  RouterNavLinkDirective,
  RouterNavPanelDirective
} from '../../shared/router-nav.directive';
import { RouterLink, RouterLinkActive, RouterOutlet } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as icons from '../../icons';

@Component({
  selector: 'dm-account-navigation',
  imports: [
    RouterLink,
    RouterLinkActive,
    RouterOutlet,
    RouterNavDirective,
    RouterNavLinkDirective,
    RouterNavPanelDirective,
    FontAwesomeModule
  ],
  templateUrl: './account-navigation.component.html',
  styleUrl: './account-navigation.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AccountNavigationComponent {
  readonly icons = icons;
}
