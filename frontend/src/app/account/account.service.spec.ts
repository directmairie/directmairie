import { TestBed } from '@angular/core/testing';

import { AccountService } from './account.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { CurrentUserCommand, CurrentUserDeletionCommand } from './account.model';
import { provideHttpClient } from '@angular/common/http';

describe('AccountService', () => {
  let http: HttpTestingController;
  let service: AccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AccountService);
  });

  it('should update the current user', () => {
    const command: CurrentUserCommand = {
      email: 'john@mail.com',
      firstName: 'John',
      lastName: 'Doe',
      phone: '0612345678',
      issueTransitionNotificationActivated: true,
      currentPassword: 'password',
      newPassword: 'new-password'
    };

    let done = false;
    service.update(command).subscribe(() => (done = true));

    const request = http.expectOne({
      method: 'PUT',
      url: '/api/users/me'
    });
    expect(request.request.body).toBe(command);
    request.flush(null);

    expect(done).toBeTrue();
  });

  it('should delete the current user', () => {
    const command: CurrentUserDeletionCommand = {
      password: 'password'
    };

    let done = false;
    service.delete(command).subscribe(() => (done = true));

    const request = http.expectOne({
      method: 'PUT',
      url: '/api/users/me/deletion'
    });
    expect(request.request.body).toBe(command);
    request.flush(null);

    expect(done).toBeTrue();
  });
});
