import { TestBed } from '@angular/core/testing';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';
import { SubscriptionService } from './subscription.service';
import { SubscriptionModel } from './subscription.model';
import { GovernmentModel } from '../../admin/models/government.model';

describe('SubscriptionService', () => {
  let http: HttpTestingController;
  let service: SubscriptionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(SubscriptionService);
  });

  it('should list subscriptions', () => {
    const expectedSubscriptions = [{}] as Array<SubscriptionModel>;
    let actualSubscriptions: Array<SubscriptionModel> | undefined;

    service.list().subscribe(subscriptions => (actualSubscriptions = subscriptions));

    http.expectOne({ method: 'GET', url: '/api/subscriptions/mine' }).flush(expectedSubscriptions);

    expect(actualSubscriptions).toEqual(expectedSubscriptions);
  });

  it('should create a subscription', () => {
    let done = false;
    service.create(42).subscribe(() => (done = true));

    const request = http.expectOne({
      method: 'POST',
      url: '/api/subscriptions/mine'
    });
    expect(request.request.body).toEqual({ governmentId: 42 });
    request.flush(null);

    expect(done).toBeTrue();
  });

  it('should delete a subscription', () => {
    let done = false;
    service.delete(42).subscribe(() => (done = true));

    http.expectOne({ method: 'DELETE', url: '/api/subscriptions/mine/42' }).flush(null);

    expect(done).toBeTrue();
  });

  it('should suggest subscriptions', () => {
    const expectedGovernmentss = [{}] as Array<GovernmentModel>;
    let actualGovernments: Array<GovernmentModel> | undefined;

    service.suggest('foo').subscribe(governments => (actualGovernments = governments));

    http
      .expectOne({ method: 'GET', url: '/api/subscriptions/mine/suggestions?query=foo' })
      .flush(expectedGovernmentss);

    expect(actualGovernments).toEqual(expectedGovernmentss);
  });
});
