import { ChangeDetectionStrategy, Component, inject } from '@angular/core';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  of,
  OperatorFunction,
  startWith,
  Subject,
  switchMap
} from 'rxjs';
import { GovernmentModel } from '../../admin/models/government.model';
import { NgbTypeahead, NgbTypeaheadSelectItemEvent } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { SuccessService } from '../../success.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import * as icons from '../../icons';
import { AccountCardComponent } from '../account-card/account-card.component';
import { SubscriptionService } from './subscription.service';
import { SubscriptionModel } from './subscription.model';
import { toSignal } from '@angular/core/rxjs-interop';

@Component({
  selector: 'dm-subscriptions',
  imports: [AccountCardComponent, NgbTypeahead, ReactiveFormsModule, FontAwesomeModule],
  templateUrl: './subscriptions.component.html',
  styleUrl: './subscriptions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscriptionsComponent {
  private readonly subscriptionService = inject(SubscriptionService);
  private readonly successService = inject(SuccessService);

  private readonly refreshSubject = new Subject<void>();
  readonly subscriptions = toSignal(
    this.refreshSubject.pipe(
      startWith(undefined),
      switchMap(() => this.subscriptionService.list())
    )
  );

  readonly suggestionCtrl = new FormControl<string>('');
  readonly icons = icons;

  readonly subscriptionTypeahead: OperatorFunction<string, Array<GovernmentModel>> = text$ =>
    text$.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(query => this.subscriptionService.suggest(query).pipe(catchError(() => of([]))))
    );
  readonly governmentFormatter = (government: GovernmentModel) => government.name;

  addSubscription(event: NgbTypeaheadSelectItemEvent<GovernmentModel>) {
    event.preventDefault();
    this.suggestionCtrl.setValue('');
    this.subscriptionService.create(event.item.id).subscribe(() => {
      this.refreshSubject.next();
      this.successService.success('Abonnement ajouté');
    });
  }

  deleteSubscription(subscription: SubscriptionModel) {
    this.subscriptionService.delete(subscription.id).subscribe(() => {
      this.refreshSubject.next();
      this.successService.success('Abonnement supprimé');
    });
  }
}
