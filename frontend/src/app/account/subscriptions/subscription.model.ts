import { GovernmentModel } from '../../admin/models/government.model';

export interface SubscriptionModel {
  id: number;
  government: GovernmentModel;
}
