import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GovernmentModel } from '../../admin/models/government.model';
import { SubscriptionModel } from './subscription.model';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  private http = inject(HttpClient);

  list(): Observable<Array<SubscriptionModel>> {
    return this.http.get<Array<SubscriptionModel>>('/api/subscriptions/mine');
  }

  suggest(query: string): Observable<Array<GovernmentModel>> {
    return this.http.get<Array<GovernmentModel>>('/api/subscriptions/mine/suggestions', {
      params: { query }
    });
  }

  create(governmentId: number): Observable<SubscriptionModel> {
    return this.http.post<SubscriptionModel>('/api/subscriptions/mine', { governmentId });
  }

  delete(subscriptionId: number): Observable<void> {
    return this.http.delete<void>(`/api/subscriptions/mine/${subscriptionId}`);
  }
}
