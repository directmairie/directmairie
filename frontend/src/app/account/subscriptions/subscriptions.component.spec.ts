import { TestBed } from '@angular/core/testing';

import { SubscriptionsComponent } from './subscriptions.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { SuccessService } from '../../success.service';
import { of } from 'rxjs';
import { GovernmentModel } from '../../admin/models/government.model';
import { SubscriptionModel } from './subscription.model';
import { SubscriptionService } from './subscription.service';

class SubscriptionsComponentTester extends ComponentTester<SubscriptionsComponent> {
  constructor() {
    super(SubscriptionsComponent);
  }

  get noSubscription() {
    return this.element('#no-subscription');
  }

  get list() {
    return this.element('ul');
  }

  get subscriptions() {
    return this.elements('ul li');
  }

  get unsubscribeButtons() {
    return this.elements<HTMLButtonElement>('.unsubscribe');
  }

  get suggestionTypeahead() {
    return this.input('#subscription-suggestion')!;
  }

  get suggestionResults(): NodeListOf<HTMLButtonElement> {
    // Based on the typeahead test itself
    // see https://github.com/ng-bootstrap/ng-bootstrap/blob/master/src/typeahead/typeahead.spec.ts
    // The dropdown is appended to the body, not to this element, so we can't unfortunately return an array of
    // TestButton, but only DOM elements
    return document.querySelectorAll('ngb-typeahead-window.dropdown-menu button.dropdown-item');
  }
}

describe('SubscriptionsComponent', () => {
  let tester: SubscriptionsComponentTester;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let successService: jasmine.SpyObj<SuccessService>;

  beforeEach(() => {
    subscriptionService = createMock(SubscriptionService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        { provide: SubscriptionService, useValue: subscriptionService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    jasmine.clock().install();
    jasmine.clock().mockDate();
  });

  afterEach(() => jasmine.clock().uninstall());

  it('should display no subscription message if no subscription', async () => {
    subscriptionService.list.and.returnValue(of([]));
    tester = new SubscriptionsComponentTester();
    await tester.stable();
    expect(tester.noSubscription).not.toBeNull();
    expect(tester.list).toBeNull();
    expect(tester.suggestionTypeahead).not.toBeNull();
  });

  it('should display subscriptions if subscriptions', async () => {
    subscriptionService.list.and.returnValue(
      of([
        {
          id: 1,
          government: {
            id: 11,
            name: 'Lyon'
          }
        } as SubscriptionModel
      ])
    );
    tester = new SubscriptionsComponentTester();
    await tester.stable();
    expect(tester.noSubscription).toBeNull();
    expect(tester.list).not.toBeNull();
    expect(tester.subscriptions.length).toBe(1);
    expect(tester.subscriptions[0]).toContainText('Lyon');
    expect(tester.suggestionTypeahead).not.toBeNull();
  });

  it('should add subscription and refresh', async () => {
    const government = {
      id: 11,
      name: 'Lyon'
    } as GovernmentModel;

    subscriptionService.list.and.returnValues(
      of([]),
      of([
        {
          id: 1,
          government: { ...government }
        } as SubscriptionModel
      ])
    );
    tester = new SubscriptionsComponentTester();
    await tester.stable();

    expect(tester.list).toBeNull();

    subscriptionService.suggest.and.returnValue(of([{ ...government }]));

    await tester.suggestionTypeahead.fillWith('y');
    jasmine.clock().tick(400);
    expect(subscriptionService.suggest).toHaveBeenCalledWith('y');

    // results should appear
    expect(tester.suggestionResults.length).toBe(1);
    expect(tester.suggestionResults[0].textContent).toBe('Lyon');

    subscriptionService.create.and.returnValue(of({} as SubscriptionModel));
    // when the result is selected
    await tester.suggestionResults[0].click();
    await tester.stable();

    // then the creation and the refresh should be done
    expect(subscriptionService.create).toHaveBeenCalledWith(government.id);
    expect(tester.list).not.toBeNull();
    expect(tester.subscriptions.length).toBe(1);
    expect(tester.suggestionTypeahead).toHaveValue('');
    expect(successService.success).toHaveBeenCalled();
  });

  it('should delete subscription and refresh', async () => {
    subscriptionService.list.and.returnValues(
      of([
        {
          id: 1,
          government: {
            id: 11,
            name: 'Lyon'
          }
        } as SubscriptionModel
      ]),
      of([])
    );
    tester = new SubscriptionsComponentTester();
    await tester.stable();

    subscriptionService.delete.and.returnValue(of(undefined));

    await tester.unsubscribeButtons[0].click();
    await tester.stable();

    expect(subscriptionService.delete).toHaveBeenCalledWith(1);
    expect(successService.success).toHaveBeenCalled();
    expect(tester.list).toBeNull();
  });
});
