import { TestBed } from '@angular/core/testing';

import { ConfirmationService } from './confirmation.service';
import { Subject } from 'rxjs';
import { createMock } from 'ngx-speculoos';
import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationModalComponent } from './confirmation-modal.component';

describe('ConfirmationService', () => {
  let service: ConfirmationService;
  let closed: Subject<string>;
  let dismissed: Subject<string>;
  let componentInstance: ConfirmationModalComponent;

  beforeEach(() => {
    closed = new Subject<string>();
    dismissed = new Subject<string>();
    const ngbModal = createMock(NgbModal);
    const ngbActiveModal = createMock(NgbActiveModal);
    TestBed.configureTestingModule({
      providers: [
        { provide: NgbModal, useValue: ngbModal },
        { provide: NgbActiveModal, useValue: ngbActiveModal }
      ]
    });
    componentInstance = TestBed.runInInjectionContext(() => new ConfirmationModalComponent());
    const ngbModalRef = {
      closed,
      dismissed,
      componentInstance
    } as unknown as NgbModalRef;
    ngbModal.open.and.returnValue(ngbModalRef);
    service = TestBed.inject(ConfirmationService);
  });

  it('should configure modal with default title if none passed', () => {
    service.confirm({
      message: 'Test'
    });
    expect(componentInstance.message()).toBe('Test');
    expect(componentInstance.title()).toBe('Confirmation');
  });

  it('should configure modal with specified title', () => {
    service.confirm({
      message: 'Test',
      title: 'Title'
    });
    expect(componentInstance.message()).toBe('Test');
    expect(componentInstance.title()).toBe('Title');
  });

  it('should emit and complete if closed', () => {
    let emissionCount = 0;
    let completed = false;
    service
      .confirm({
        message: 'Test'
      })
      .subscribe({
        next: () => emissionCount++,
        complete: () => (completed = true)
      });

    closed.next('bla');
    expect(emissionCount).toBe(1);
    expect(completed).toBeTrue();
  });

  it('should complete without emission if dismissed', () => {
    let emissionCount = 0;
    let completed = false;
    service
      .confirm({
        message: 'Test'
      })
      .subscribe({
        next: () => emissionCount++,
        complete: () => (completed = true)
      });

    dismissed.next('bla');
    expect(emissionCount).toBe(0);
    expect(completed).toBeTrue();
  });
});
