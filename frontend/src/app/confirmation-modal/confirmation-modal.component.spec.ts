import { TestBed } from '@angular/core/testing';

import { ConfirmationModalComponent } from './confirmation-modal.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeDetectionStrategy, Component, TemplateRef, viewChild } from '@angular/core';

@Component({
  template: `<ng-template #template><p>Hello</p></ng-template>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly template = viewChild.required<TemplateRef<unknown>>('template');
}

class ConfirmationModalComponentTester extends ComponentTester<ConfirmationModalComponent> {
  constructor() {
    super(ConfirmationModalComponent);
  }

  get title() {
    return this.element('h1');
  }

  get message() {
    return this.element('p');
  }

  get closeButton() {
    return this.button('.btn-close')!;
  }

  get yesButton() {
    return this.button('.btn-danger')!;
  }

  get noButton() {
    return this.button('.btn-secondary')!;
  }
}

describe('ConfirmationModalComponent', () => {
  let tester: ConfirmationModalComponentTester;
  let activeModal: jasmine.SpyObj<NgbActiveModal>;
  let template: TemplateRef<unknown>;

  beforeEach(() => {
    activeModal = createMock(NgbActiveModal);

    TestBed.configureTestingModule({
      providers: [{ provide: NgbActiveModal, useValue: activeModal }]
    });

    const testComponentComponentFixture = TestBed.createComponent(TestComponent);
    testComponentComponentFixture.detectChanges();
    template = testComponentComponentFixture.componentInstance.template();

    tester = new ConfirmationModalComponentTester();
    tester.componentInstance.title.set('Confirmation');
    tester.componentInstance.message.set('Do you agree?');
  });

  it('should display specified title and message', async () => {
    await tester.stable();
    expect(tester.title).toHaveTrimmedText('Confirmation');
    expect(tester.message).toHaveTrimmedText('Do you agree?');
  });

  it('should display specified title and message when message is a template', async () => {
    tester.componentInstance.message.set(template);
    await tester.stable();

    expect(tester.title).toHaveTrimmedText('Confirmation');
    expect(tester.message).toHaveTrimmedText('Hello');
  });

  it('should dismiss on close button', async () => {
    await tester.stable();
    await tester.closeButton.click();
    expect(activeModal.dismiss).toHaveBeenCalled();
  });

  it('should close on yes button', async () => {
    await tester.stable();
    await tester.yesButton.click();
    expect(activeModal.close).toHaveBeenCalled();
  });

  it('should dismiss on no button', async () => {
    await tester.stable();
    await tester.noButton.click();
    expect(activeModal.dismiss).toHaveBeenCalled();
  });
});
