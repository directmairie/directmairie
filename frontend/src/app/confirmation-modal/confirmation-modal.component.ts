import {
  ChangeDetectionStrategy,
  Component,
  TemplateRef,
  inject,
  signal,
  computed
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgTemplateOutlet } from '@angular/common';

@Component({
  selector: 'dm-confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrl: './confirmation-modal.component.scss',
  imports: [NgTemplateOutlet],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfirmationModalComponent {
  readonly activeModal = inject(NgbActiveModal);

  readonly message = signal<string | TemplateRef<unknown> | undefined>(undefined);
  readonly title = signal<string | undefined>(undefined);

  readonly template = computed(() => {
    const message = this.message();
    return message instanceof TemplateRef ? message : undefined;
  });
}
