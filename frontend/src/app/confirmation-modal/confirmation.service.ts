import { Injectable, TemplateRef, inject } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { ConfirmationModalComponent } from './confirmation-modal.component';
import { fromModal } from '../shared/modals';

export interface ConfirmOptions {
  message: string | TemplateRef<unknown>;
  title?: string;
}

@Injectable({ providedIn: 'root' })
export class ConfirmationService {
  private modalService = inject(NgbModal);

  confirm(options: ConfirmOptions): Observable<void> {
    const modalRef = this.modalService.open(ConfirmationModalComponent, {
      ariaLabelledBy: 'confirmation-modal-title'
    });
    const component: ConfirmationModalComponent = modalRef.componentInstance;
    component.title.set(options.title ?? 'Confirmation');
    component.message.set(options.message);

    return fromModal<void>(modalRef);
  }
}
