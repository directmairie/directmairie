import { Injectable, inject } from '@angular/core';
import { CurrentUserService } from './current-user.service';
import { map, Observable, of, shareReplay, switchMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { GovernmentModel } from './admin/models/government.model';

interface BrandingModel {
  government: GovernmentModel | null;
}

@Injectable({
  providedIn: 'root'
})
export class BrandingService {
  private http = inject(HttpClient);

  private government$: Observable<GovernmentModel | null> = inject(CurrentUserService)
    .getUserChanges()
    .pipe(
      switchMap(user => {
        return user && user.governmentAdmin ? this.loadBranding() : of({ government: null });
      }),
      map(branding => branding.government),
      shareReplay()
    );

  /**
   * Returns an observable that returns the logo based on the currently authenticated user (and which reemits every time the authenticated
   * user changes)
   * The returned observable is replayed, so there is no problem subscribing many times to it.
   */
  getBrandingGoverment(): Observable<GovernmentModel | null> {
    return this.government$;
  }

  private loadBranding(): Observable<BrandingModel> {
    return this.http.get<BrandingModel>('/api/branding');
  }
}
