import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { ErrorInterceptorService, HttpError } from '../error-interceptor.service';
import { delay, filter, map, merge, Subject } from 'rxjs';
import { ErrorService } from '../error.service';

import * as icons from '../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { toSignal } from '@angular/core/rxjs-interop';

interface DisplayedError {
  /**
   * The message to display, either coming from the server if it's a technical error, or translated from
   * the errorCode if it's a functional one
   */
  message: string;

  /**
   * Flag indicating if it's a functional error. In that case, only the message should be displayed.
   */
  functional: boolean;

  /**
   * The status of the error, only displayed if it's a technical error
   */
  status: number;
}

/**
 * Component which displays the errors emitted by the error-interceptor service, until the user navigates elsewhere.
 *
 * This component displays all the emitted errors unless the error is functional (i.e. has an errorCode), and
 * its error details (obtained from `error-codes.ts`) exists and is marked as `handledByComponent`.
 */
@Component({
  selector: 'dm-error',
  templateUrl: './error.component.html',
  styleUrl: './error.component.scss',
  imports: [FontAwesomeModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorComponent {
  private readonly router = inject(Router);
  private readonly errorService = inject(ErrorService);

  readonly icons = icons;
  readonly error: Signal<DisplayedError | undefined>;
  private readonly close$ = new Subject<undefined>();

  constructor() {
    const errorInterceptor = inject(ErrorInterceptorService);
    this.error = toSignal(
      merge(
        errorInterceptor.getErrors().pipe(
          filter(error => !this.errorService.hasCustomHandling(error)),
          map(error => this.toDisplayedError(error))
        ),
        this.router.events.pipe(
          filter(event => event instanceof NavigationEnd),
          map(() => undefined)
        ),
        this.close$
        // add a delay before emitting the new value, because otherwise the error is not displayed
        // when the error observable is transformed to a signal and this error signal is read by
        // the template.
      ).pipe(delay<DisplayedError | undefined>(1))
    );
  }

  private toDisplayedError(error: HttpError): DisplayedError {
    return {
      functional: !!error.functionalError,
      message: this.errorService.getMessageToDisplay(error),
      status: error.status!
    };
  }

  public closeError() {
    this.close$.next(undefined);
  }
}
