import { TestBed } from '@angular/core/testing';
import { NavigationEnd, NavigationStart, Router, RouterEvent } from '@angular/router';
import { Subject } from 'rxjs';
import { ComponentTester, createMock } from 'ngx-speculoos';

import { ErrorComponent } from './error.component';
import { ErrorInterceptorService, HttpError } from '../error-interceptor.service';
import { ALREADY_REGISTERED } from '../error.service';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';

class ErrorComponentTester extends ComponentTester<ErrorComponent> {
  constructor() {
    super(ErrorComponent);
  }

  get error() {
    return this.element('.toast');
  }

  get status() {
    return this.element('#technical-error-status');
  }

  get closeButton() {
    return this.button('.btn-close')!;
  }

  get technicalMessage() {
    return this.element('#technical-error-message');
  }

  get functionalMessage() {
    return this.element('#functional-error-message');
  }
}

describe('ErrorComponent', () => {
  let tester: ErrorComponentTester;

  let routerEvents: Subject<RouterEvent>;
  let httpErrors: Subject<HttpError>;

  beforeEach(async () => {
    jasmine.clock().install();

    routerEvents = new Subject<RouterEvent>();
    httpErrors = new Subject<HttpError>();

    const errorInterceptorService = createMock(ErrorInterceptorService);
    errorInterceptorService.getErrors.and.returnValue(httpErrors);

    TestBed.configureTestingModule({
      providers: [
        { provide: ErrorInterceptorService, useValue: errorInterceptorService },
        { provide: Router, useValue: { events: routerEvents } },
        { provide: NgbConfig, useValue: { animation: false } as NgbConfig }
      ]
    });

    tester = new ErrorComponentTester();
    await tester.stable();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should not display any error initially', () => {
    expect(tester.error).toBeNull();
  });

  it('should display a technical error when it is emitted, and hide it when navigation succeeds', async () => {
    httpErrors.next({
      status: 500,
      message: 'Oulala',
      functionalError: null
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).toContainText('Une erreur inattendue');
    expect(tester.status).toContainText('Statut\u00a0: 500');
    expect(tester.technicalMessage).toContainText('Message\u00a0: Oulala');
    expect(tester.functionalMessage).toBeNull();

    httpErrors.next({
      status: null,
      message: null as unknown as string,
      functionalError: null
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).toContainText('Une erreur inattendue');
    expect(tester.status).toBeNull();
    expect(tester.technicalMessage).toBeNull();
    expect(tester.functionalMessage).toBeNull();

    routerEvents.next(new NavigationStart(1, 'foo', undefined));
    jasmine.clock().tick(1);
    await tester.stable();
    expect(tester.error).not.toBeNull();

    routerEvents.next(new NavigationEnd(1, 'foo', ''));
    jasmine.clock().tick(1);
    await tester.stable();
    expect(tester.error).toBeNull();
  });

  it('should display a specific message on 401 errors', async () => {
    httpErrors.next({
      status: 401,
      message: 'Oulala',
      functionalError: null
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).toContainText('Vous avez été déconnecté·e');
    expect(tester.status).toBeNull();
    expect(tester.technicalMessage).toBeNull();
    expect(tester.functionalMessage).toBeNull();
  });

  it('should display a functional error when it is emitted, and hide it when navigation succeeds', async () => {
    httpErrors.next({
      status: 400,
      message: 'Oulala',
      functionalError: '__TEST__'
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).not.toContainText('Une erreur inattendue');
    expect(tester.status).toBeNull();
    expect(tester.technicalMessage).toBeNull();
    expect(tester.functionalMessage).toContainText('test translation');

    routerEvents.next(new NavigationEnd(1, 'foo', ''));
    jasmine.clock().tick(1);
    await tester.stable();
    expect(tester.error).toBeNull();
  });

  it('should ignore errors if custom handling', async () => {
    httpErrors.next({
      status: 400,
      message: 'Oulala',
      functionalError: ALREADY_REGISTERED
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).toBeNull();
  });

  it('should close the error', async () => {
    httpErrors.next({
      status: 500,
      message: 'Oulala',
      functionalError: null
    });
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).not.toBeNull();

    await tester.closeButton.click();
    jasmine.clock().tick(1);
    await tester.stable();

    expect(tester.error).toBeNull();
  });
});
