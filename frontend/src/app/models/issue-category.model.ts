/**
 * Special issue category group containing the "Other" category
 */
export const OTHER_ISSUE_GROUP_ID = 1;
/**
 * Special "Other" issue category
 */
export const OTHER_CATEGORY_ID = 1;

export interface IssueGroupModel {
  id: number;
  label: string;
  categories: Array<IssueCategoryModel>;
}

export interface IssueCategoryModel {
  id: number;
  label: string;
  descriptionRequired: boolean;
}
