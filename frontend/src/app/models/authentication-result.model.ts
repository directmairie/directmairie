export interface AuthenticationResultModel {
  token: string;
}
