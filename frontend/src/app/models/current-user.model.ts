export interface CurrentUserModel {
  readonly id: number;
  readonly email: string;
  readonly firstName: string | null;
  readonly lastName: string | null;
  readonly phone: string | null;
  readonly issueTransitionNotificationActivated: boolean;
  readonly superAdmin: boolean;
  readonly poolingOrganizationAdmin: boolean;
  readonly governmentAdmin: boolean;
}

export function welcomeName(user: CurrentUserModel) {
  if (user.firstName?.trim()) {
    return user.firstName.trim();
  } else {
    return user.email;
  }
}
