import { IssueCategoryModel } from './issue-category.model';
import { GovernmentDetailedModel } from '../admin/models/government.model';

export interface CoordinatesModel {
  readonly latitude: number;
  readonly longitude: number;
}

export interface BoundingBox {
  readonly minLatitude: number;
  readonly maxLatitude: number;
  readonly minLongitude: number;
  readonly maxLongitude: number;
}

export type IssueStatus = 'DRAFT' | 'CREATED' | 'RESOLVED' | 'REJECTED';
export const LISTABLE_STATUSES: Array<IssueStatus> = ['CREATED', 'RESOLVED', 'REJECTED'];

export interface IssueModel {
  id: number;
  creationInstant: string;
  status: IssueStatus;
  coordinates: CoordinatesModel;
  category: IssueCategoryModel | null;
  description: string | null;
  pictures: Array<PictureModel>;
  assignedGovernment: GovernmentDetailedModel | null;
  transitions: Array<IssueTransitionModel>;
}

export interface IssueTransitionModel {
  transitionInstant: string;
  beforeStatus: IssueStatus;
  afterStatus: IssueStatus;
  message: string | null;
}

export interface IssueWithAccessTokenModel extends IssueModel {
  accessToken?: string;
}

export interface IssueCommand {
  coordinates: CoordinatesModel;
  categoryId: number | null;
  description: string | null;
  status: IssueStatus;
}

export interface IssueStatusCommand {
  status: IssueStatus;
  message: string | null;
}

export interface PictureModel {
  id: number;
  creationInstant: string;
}
