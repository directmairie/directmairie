import { PageModel } from './page.model';

export function createPage<T>(
  content: Array<T>,
  size: number,
  totalElements: number,
  number: number
): PageModel<T> {
  return {
    content,
    size,
    totalElements,
    number,
    totalPages: totalElements % size === 0 ? totalElements / size : totalElements / size + 1
  };
}
