import { Injectable } from '@angular/core';

/**
 * Service used to help with pluralized messages displayed in the application
 */
@Injectable({
  providedIn: 'root'
})
export class MessageService {
  /**
   * Builds a success message with the result of a deanonymization of issues
   * @param deanonymizations: the number of successful deanonymization
   */
  getDeanonymizedSuccessMessage(deanonymizations: number) {
    if (deanonymizations) {
      if (deanonymizations === 1) {
        return "Une remontée d'information a été associée à votre compte.";
      } else {
        return `${deanonymizations} remontées d'information ont été associées à votre compte.`;
      }
    }
    return '';
  }
}
