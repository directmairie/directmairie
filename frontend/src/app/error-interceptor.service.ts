import { inject, Injectable } from '@angular/core';
import { Observable, Subject, tap } from 'rxjs';
import { HttpErrorResponse, HttpInterceptorFn, HttpStatusCode } from '@angular/common/http';

/**
 * An error intercepted and emitted by this interceptor.
 */
export interface HttpError {
  /**
   * The status code, if the error comes back from the server. It can be null if it's a connectivity error for example.
   */
  status: number | null;
  /**
   * The message of the error, coming from the server
   */
  message: string;
  /**
   * The functional error code of the error, only present if the error is a 400 error containing an error code
   */
  functionalError: string | null;
}

@Injectable({
  providedIn: 'root'
})
export class ErrorInterceptorService {
  private errorSubject = new Subject<HttpError>();

  getErrors(): Observable<HttpError> {
    return this.errorSubject.asObservable();
  }

  emitError(httpError: HttpError) {
    this.errorSubject.next(httpError);
  }
}

function isExpectedUnauthorizedError(errorResponse: HttpErrorResponse) {
  return (
    errorResponse.status === HttpStatusCode.Unauthorized &&
    errorResponse.url!.endsWith('/api/authentications')
  );
}

function getMessage(errorResponse: HttpErrorResponse): string {
  const body = errorResponse.error;
  if (body && body.message) {
    // the error is a spring boot error
    return body.message;
  } else {
    // the error is an HTTP response, but which doesn't contain a spring boot payload
    return errorResponse.message;
  }
}

function getFunctionalError(errorResponse: HttpErrorResponse): string | null {
  const body = errorResponse.error;
  return errorResponse.status === 400 && body && body.functionalError ? body.functionalError : null;
}

function toError(errorResponse: HttpErrorResponse): HttpError | null {
  if (isExpectedUnauthorizedError(errorResponse)) {
    return null;
  } else if (errorResponse.error instanceof ProgressEvent) {
    // A client-side or network error occurred.
    return {
      status: null,
      message: getMessage(errorResponse),
      functionalError: null
    };
  } else {
    // The backend returned an unsuccessful response code.
    return {
      status: errorResponse.status,
      message: getMessage(errorResponse),
      functionalError: getFunctionalError(errorResponse)
    };
  }
}

/**
 * An HTTP interceptor that detects error responses and emits them, so that the errors component can display
 * a toast to signal the error.
 */
export const errorInterceptor: HttpInterceptorFn = (req, next) => {
  const errorInterceptorService = inject(ErrorInterceptorService);
  return next(req).pipe(
    tap({
      error: err => {
        const httpError = toError(err);
        if (httpError) {
          errorInterceptorService.emitError(httpError);
        }
      }
    })
  );
};
