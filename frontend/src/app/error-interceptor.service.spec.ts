import { TestBed } from '@angular/core/testing';
import { HttpClient, provideHttpClient, withInterceptors } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';

import { errorInterceptor, ErrorInterceptorService, HttpError } from './error-interceptor.service';
import { ALREADY_REGISTERED } from './error.service';

describe('ErrorInterceptorService', () => {
  let service: ErrorInterceptorService;
  let http: HttpTestingController;
  let httpClient: HttpClient;
  const noop = () => {
    // do nothing
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(withInterceptors([errorInterceptor])),
        provideHttpClientTesting()
      ]
    });

    service = TestBed.inject(ErrorInterceptorService);
    http = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should emit error when error is not an HTTP response', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.get('/test').subscribe({ error: noop });
    http.expectOne('/test').error(new ProgressEvent('error'));

    expect(actualError!.message).toContain('Http failure response for /test: 0');
    expect(actualError!.functionalError).toBeNull();
    expect(actualError!.status).toBeNull();
  });

  it('should emit technical error when error is an HTTP response', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.get('/test').subscribe({ error: noop });
    http.expectOne('/test').flush(null, { status: 500, statusText: 'Server Error' });

    const expectedError: HttpError = {
      functionalError: null,
      message: 'Http failure response for /test: 500 Server Error',
      status: 500
    };
    expect(actualError!).toEqual(expectedError);
  });

  it('should emit technical error when error is an HTTP response with status 400 but no functionalError and no message', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.get('/test').subscribe({ error: noop });
    http.expectOne('/test').flush(null, { status: 400, statusText: 'Bad Request' });

    const expectedError: HttpError = {
      functionalError: null,
      message: 'Http failure response for /test: 400 Bad Request',
      status: 400
    };
    expect(actualError!).toEqual(expectedError);
  });

  it('should emit technical error when error is an HTTP response with a message', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.get('/test').subscribe({ error: noop });
    http
      .expectOne('/test')
      .flush({ message: 'not good' }, { status: 400, statusText: 'Bad Request' });

    const expectedError: HttpError = {
      functionalError: null,
      message: 'not good',
      status: 400
    };
    expect(actualError!).toEqual(expectedError);
  });

  it('should emit functional error when error is a 400 HTTP response with a functional error', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.get('/test').subscribe({ error: noop });
    http
      .expectOne('/test')
      .flush(
        { message: 'not good', functionalError: ALREADY_REGISTERED },
        { status: 400, statusText: 'Bad Request' }
      );

    const expectedError: HttpError = {
      functionalError: ALREADY_REGISTERED,
      message: 'not good',
      status: 400
    };
    expect(actualError!).toEqual(expectedError);
  });

  it('should not emit when error is a 401 from api/authentications', () => {
    let actualError: HttpError | null = null;
    service.getErrors().subscribe(err => {
      actualError = err;
    });

    httpClient.post('/api/authentications', {}).subscribe({ error: noop });
    http.expectOne('/api/authentications').flush(null, { status: 401, statusText: 'Unauthorized' });

    expect(actualError).toBeNull();
  });
});
