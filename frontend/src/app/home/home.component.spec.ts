import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { BrandingService } from '../branding.service';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { GovernmentModel } from '../admin/models/government.model';
import { Subject } from 'rxjs';
import { CurrentUserService } from '../current-user.service';
import { CurrentUserModel } from '../models/current-user.model';
import { MyIssuesComponent } from '../my-issues/my-issues.component';
import { provideRouter } from '@angular/router';

@Component({
  selector: 'dm-my-issues',
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush
})
class MyIssuesMockComponent {}

class HomeComponentTester extends ComponentTester<HomeComponent> {
  constructor() {
    super(HomeComponent);
  }

  get title() {
    return this.element('h1');
  }

  get logo() {
    return this.element('img')!;
  }
}

describe('HomeComponent', () => {
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  const currentUserSubject = new Subject<CurrentUserModel | null>();
  let brandingService: jasmine.SpyObj<BrandingService>;
  const governmentSubject = new Subject<GovernmentModel | null>();
  let tester: HomeComponentTester;

  beforeEach(async () => {
    currentUserService = createMock(CurrentUserService);
    brandingService = createMock(BrandingService);
    TestBed.overrideComponent(HomeComponent, {
      remove: {
        imports: [MyIssuesComponent]
      },
      add: {
        imports: [MyIssuesMockComponent]
      }
    });
    TestBed.configureTestingModule({
      providers: [
        provideRouter([]),
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: BrandingService, useValue: brandingService }
      ]
    });

    currentUserService.getUserChanges.and.returnValue(currentUserSubject);
    brandingService.getBrandingGoverment.and.returnValue(governmentSubject);

    tester = new HomeComponentTester();
    await tester.stable();
  });

  it('should display a branding logo or a default logo', async () => {
    currentUserSubject.next(null);
    governmentSubject.next(null);
    await tester.stable();

    expect(tester.title).toHaveText('Bienvenue dans DirectMairie\xa0!');
    expect(tester.logo.attr('src')).toBe('/assets/Logo-DirectMairie.svg');

    governmentSubject.next({ logo: null } as GovernmentModel);
    await tester.stable();

    expect(tester.logo.attr('src')).toBe('/assets/Logo-DirectMairie.svg');

    governmentSubject.next({ id: 42, logo: { id: 354 } } as GovernmentModel);
    await tester.stable();

    expect(tester.logo.attr('src')).toBe('/api/governments/42/logo/354/bytes');
  });

  it('should display my issues if the user is logged in', async () => {
    const tester = new ComponentTester(HomeComponent);
    await tester.stable();

    expect(tester.element(MyIssuesMockComponent)).toBeNull();

    governmentSubject.next(null);
    currentUserSubject.next({ id: 1 } as CurrentUserModel);
    await tester.stable();
    expect(tester.component(MyIssuesMockComponent)).not.toBeNull();

    currentUserSubject.next(null);
    await tester.stable();
    expect(tester.component(MyIssuesMockComponent)).toBeNull();
  });
});
