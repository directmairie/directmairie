import { ChangeDetectionStrategy, Component, inject, Signal } from '@angular/core';
import * as icons from '../icons';
import { combineLatest, map } from 'rxjs';
import { BrandingService } from '../branding.service';
import { CurrentUserService } from '../current-user.service';
import { CurrentUserModel } from '../models/current-user.model';
import { MyIssuesComponent } from '../my-issues/my-issues.component';
import { RouterLink } from '@angular/router';
import { toSignal } from '@angular/core/rxjs-interop';

interface ViewModel {
  brandingLogoUrl: string | null;
  currentUser: CurrentUserModel | null;
}

/**
 * The component displaying the home page of the application
 */
@Component({
  selector: 'dm-home',
  templateUrl: './home.component.html',
  styleUrl: './home.component.scss',
  imports: [RouterLink, MyIssuesComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent {
  readonly icons = icons;
  readonly vm: Signal<ViewModel | undefined>;

  constructor() {
    const brandingService = inject(BrandingService);
    const currentUserService = inject(CurrentUserService);
    this.vm = toSignal(
      combineLatest([
        currentUserService.getUserChanges(),
        brandingService.getBrandingGoverment()
      ]).pipe(
        map(([currentUser, government]) => ({
          currentUser,
          brandingLogoUrl:
            government && government.logo
              ? `/api/governments/${government.id}/logo/${government.logo.id}/bytes`
              : null
        }))
      )
    );
  }
}
