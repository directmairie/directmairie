import { TestBed } from '@angular/core/testing';

import { SubscriptionAfterSubmissionComponent } from './subscription-after-submission.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { GovernmentModel } from '../admin/models/government.model';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { SubscriptionService } from '../account/subscriptions/subscription.service';
import { SuccessService } from '../success.service';
import { SubscriptionModel } from '../account/subscriptions/subscription.model';
import { of } from 'rxjs';

@Component({
  imports: [SubscriptionAfterSubmissionComponent],
  template: `<dm-subscription-after-submission [government]="loire()" (done)="done.set(true)" />`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly loire = signal({
    id: 42,
    name: 'Loire'
  } as GovernmentModel);

  readonly done = signal(false);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get subscribe() {
    return this.button('#add-subscription')!;
  }

  get doNotSubscribe() {
    return this.button('#do-not-subscribe')!;
  }
}

describe('SubscriptionAfterSubmissionComponent', () => {
  let tester: TestComponentTester;

  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let successService: jasmine.SpyObj<SuccessService>;

  beforeEach(async () => {
    subscriptionService = createMock(SubscriptionService);
    successService = createMock(SuccessService);

    TestBed.configureTestingModule({
      providers: [
        { provide: SubscriptionService, useValue: subscriptionService },
        { provide: SuccessService, useValue: successService }
      ]
    });

    tester = new TestComponentTester();
    await tester.stable();
  });

  it('should display message and two buttons', () => {
    expect(tester.testElement).toContainText('Loire');
    expect(tester.subscribe).not.toBeNull();
    expect(tester.doNotSubscribe).not.toBeNull();
  });

  it('should subscribe', async () => {
    subscriptionService.create.and.returnValue(of({} as SubscriptionModel));

    await tester.subscribe.click();

    expect(tester.componentInstance.done()).toBeTrue();
    expect(subscriptionService.create).toHaveBeenCalledWith(42);
    expect(successService.success).toHaveBeenCalledWith(
      'Vous êtes abonné aux notifications de Loire'
    );
  });

  it('should decline', async () => {
    await tester.doNotSubscribe.click();

    expect(tester.componentInstance.done()).toBeTrue();
    expect(subscriptionService.create).not.toHaveBeenCalled();
    expect(successService.success).not.toHaveBeenCalled();
  });
});
