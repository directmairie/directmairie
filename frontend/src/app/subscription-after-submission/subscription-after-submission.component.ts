import { ChangeDetectionStrategy, Component, inject, output, input } from '@angular/core';
import { GovernmentModel } from '../admin/models/government.model';
import { SubscriptionService } from '../account/subscriptions/subscription.service';
import { SuccessService } from '../success.service';

@Component({
  selector: 'dm-subscription-after-submission',
  imports: [],
  templateUrl: './subscription-after-submission.component.html',
  styleUrl: './subscription-after-submission.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SubscriptionAfterSubmissionComponent {
  private readonly subscriptionService = inject(SubscriptionService);
  private readonly successService = inject(SuccessService);

  readonly government = input.required<GovernmentModel>();
  readonly done = output<void>();

  subscribe() {
    this.subscriptionService.create(this.government().id).subscribe(() => {
      this.successService.success(
        `Vous êtes abonné aux notifications de ${this.government().name}`
      );
      this.done.emit();
    });
  }

  doNotSubscribe() {
    this.done.emit();
  }
}
