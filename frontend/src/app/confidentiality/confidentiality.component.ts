import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'dm-confidentiality',
  templateUrl: './confidentiality.component.html',
  styleUrl: './confidentiality.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfidentialityComponent {}
