import { TestBed } from '@angular/core/testing';
import { HttpEventType, HttpProgressEvent, provideHttpClient } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { DateTime } from 'luxon';

import {
  ANONYMOUS_ISSUES_KEY,
  DeanonimizedIssues,
  ISSUE_ACCESS_TOKEN_HEADER,
  IssueService
} from './issue.service';
import { IssueCommand, IssueModel, IssueStatusCommand, PictureModel } from './models/issue.model';
import { landscapeImage, portraitImage, smallImage } from './images.spec';
import { PageModel } from './models/page.model';
import { createPage } from './models/page.model.spec';
import { SubscriptionService } from './account/subscriptions/subscription.service';
import { createMock } from 'ngx-speculoos';
import { of } from 'rxjs';
import { SubscriptionModel } from './account/subscriptions/subscription.model';
import { GovernmentModel } from './admin/models/government.model';

function checkImage(blob: Blob, sizeProperty: 'width' | 'height', done: DoneFn) {
  expect(blob.size).toBeLessThan(100000);
  const img: HTMLImageElement = document.createElement('img');
  const fileReader = new FileReader();
  fileReader.onload = () => {
    img.onload = () => {
      expect(img[sizeProperty] as number).toBe(1024);
      done();
    };
    img.src = fileReader.result as string;
    expect(img.src).toContain('image/jpeg');
  };
  fileReader.readAsDataURL(blob);
}

describe('IssueService', () => {
  let http: HttpTestingController;
  let service: IssueService;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;

  beforeEach(() => {
    subscriptionService = createMock(SubscriptionService);

    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideHttpClientTesting(),
        { provide: SubscriptionService, useValue: subscriptionService }
      ]
    });

    http = TestBed.inject(HttpTestingController);
    service = TestBed.inject(IssueService);
    spyOn(Storage.prototype, 'getItem');
    spyOn(Storage.prototype, 'setItem');
    spyOn(Storage.prototype, 'removeItem');
  });

  const issue = {
    id: 42
  } as IssueModel;

  it('should list paginated issues with listable status if none is specified', () => {
    const expectedIssues: PageModel<IssueModel> = createPage([issue], 20, 1, 0);
    let actualIssues: PageModel<IssueModel> | null = null;

    service
      .list({ page: 1, statusFilter: [], poolingOrganizationIds: [], governmentIds: [] })
      .subscribe(response => (actualIssues = response));

    http
      .expectOne('/api/issues?page=0&status=CREATED&status=RESOLVED&status=REJECTED')
      .flush(expectedIssues);

    expect(actualIssues!).toEqual(expectedIssues);
  });

  it('should list paginated and filtered issues', () => {
    const expectedIssues: PageModel<IssueModel> = createPage([issue], 20, 1, 0);
    let actualIssues: PageModel<IssueModel> | null = null;

    service
      .list({
        page: 1,
        statusFilter: ['CREATED', 'RESOLVED'],
        poolingOrganizationIds: [1000, 1001],
        governmentIds: [1002, 1003]
      })
      .subscribe(response => (actualIssues = response));

    http
      .expectOne(
        '/api/issues?page=0&status=CREATED&status=RESOLVED&org=1000&org=1001&gov=1002&gov=1003'
      )
      .flush(expectedIssues);

    expect(actualIssues!).toEqual(expectedIssues);
  });

  it('should list my issues', () => {
    const expectedIssues: PageModel<IssueModel> = createPage([issue], 20, 1, 0);
    let actualIssues: PageModel<IssueModel> | null = null;

    service.listMine().subscribe(response => (actualIssues = response));

    http.expectOne('/api/issues/mine').flush(expectedIssues);

    expect(actualIssues!).toEqual(expectedIssues);
  });

  it('should get an issue', () => {
    let actualIssue: IssueModel | null = null;
    service.get(42).subscribe(response => (actualIssue = response));

    http.expectOne('/api/issues/42').flush(issue);

    expect(actualIssue!).toEqual(issue);
  });

  it('should create an issue', () => {
    const command: IssueCommand = {
      coordinates: {
        latitude: 48.858624,
        longitude: 2.294537
      },
      categoryId: null,
      description: null,
      status: 'DRAFT'
    };

    let issueReceived: IssueModel | null = null;
    service.create(command).subscribe(response => (issueReceived = response));

    const request = http.expectOne({
      method: 'POST',
      url: '/api/issues'
    });
    expect(request.request.body).toBe(command);
    request.flush(issue);

    expect(issueReceived!).toBe(issue);
  });

  it('should update an issue', () => {
    const command: IssueCommand = {
      coordinates: {
        latitude: 48.858624,
        longitude: 2.294537
      },
      categoryId: 1,
      description: 'desc',
      status: 'CREATED'
    };

    let updated = false;
    service.update(42, 'access-token', command).subscribe(() => (updated = true));

    const testRequest = http.expectOne({
      method: 'PUT',
      url: '/api/issues/42'
    });
    expect(testRequest.request.body).toBe(command);
    expect(testRequest.request.headers.get(ISSUE_ACCESS_TOKEN_HEADER)).toBe('access-token');

    testRequest.flush(issue);

    expect(updated).toBe(true);
  });

  it('should not send access token header if falsy', () => {
    const command = {} as IssueCommand;

    let updated = false;
    service.update(42, '', command).subscribe(() => (updated = true));

    const testRequest = http.expectOne({
      method: 'PUT',
      url: '/api/issues/42'
    });
    expect(testRequest.request.headers.has(ISSUE_ACCESS_TOKEN_HEADER)).toBe(false);

    testRequest.flush(issue);

    expect(updated).toBe(true);
  });

  it('should update the status of an issue', () => {
    const command: IssueStatusCommand = {
      status: 'REJECTED',
      message: 'nothing found'
    };

    let updated = false;
    service.updateStatus(42, command).subscribe(() => (updated = true));

    const request = http.expectOne({
      method: 'PUT',
      url: '/api/issues/42/status'
    });
    expect(request.request.body).toBe(command);
    request.flush(issue);

    expect(updated).toBe(true);
  });

  it('should delete a picture', () => {
    let deleted = false;
    service.deletePicture(1, 'access-token', 42).subscribe(() => (deleted = true));

    const testRequest = http.expectOne({ method: 'DELETE', url: '/api/issues/1/pictures/42' });
    testRequest.flush(null, { status: 204, statusText: 'No Content' });

    expect(testRequest.request.headers.get(ISSUE_ACCESS_TOKEN_HEADER)).toBe('access-token');
    expect(deleted).toBe(true);
  });

  it('should upload a picture', () => {
    const expectedPicture: PictureModel = {
      id: 42,
      creationInstant: '2019-01-10T00:00:00Z'
    };

    const progressListener = jasmine.createSpy('progressListener');
    const file: Blob = new Blob();

    let actualPicture: PictureModel | null = null;
    service
      .uploadPicture(1, 'access-token', file, progressListener)
      .subscribe(picture => (actualPicture = picture));

    const testRequest = http.expectOne({ method: 'POST', url: '/api/issues/1/pictures' });

    const formData = new FormData();
    formData.set('file', file);
    expect(testRequest.request.headers.get(ISSUE_ACCESS_TOKEN_HEADER)).toBe('access-token');
    expect(testRequest.request.body).toEqual(formData);

    testRequest.event({
      type: HttpEventType.UploadProgress,
      loaded: 0,
      total: 200
    } as HttpProgressEvent);
    expect(progressListener).toHaveBeenCalledWith(0);

    testRequest.event({
      type: HttpEventType.UploadProgress,
      loaded: 100,
      total: 200
    } as HttpProgressEvent);
    expect(progressListener).toHaveBeenCalledWith(50);

    testRequest.event({
      type: HttpEventType.UploadProgress,
      loaded: 200,
      total: 200
    } as HttpProgressEvent);
    expect(progressListener).toHaveBeenCalledWith(100);

    testRequest.flush(expectedPicture);

    expect(actualPicture!).toEqual(expectedPicture);
  });

  it('should resize a landscape picture', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(landscapeImage);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    service.resizePicture({} as File).subscribe(blob => {
      checkImage(blob, 'width', done);
    });
  });

  it('should resize a portrait picture', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(portraitImage);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    service.resizePicture({} as File).subscribe(blob => {
      checkImage(blob, 'height', done);
    });
  });

  it('should not resize a small-enough picture', (done: DoneFn) => {
    const fakeRead = (_file: File, callback: (image: string) => void) => {
      callback(smallImage);
    };
    spyOn(service, '_readFile').and.callFake(fakeRead);

    const originalFile = {} as File;
    service.resizePicture(originalFile).subscribe(blob => {
      expect(blob).toBe(originalFile);
      done();
    });
  });

  it('should export issues', () => {
    const expectedBlob = new Blob();
    let actualBlob: Blob | null = null;

    service.export('2019-01-01', '2019-02-01').subscribe(response => (actualBlob = response));

    http
      .expectOne(
        request =>
          request.method === 'GET' &&
          request.url === '/api/issues' &&
          request.params.get('from') === '2019-01-01' &&
          request.params.get('to') === '2019-02-01' &&
          request.params.get('timezone') === DateTime.local().zoneName &&
          request.params.get('export') === 'CSV'
      )
      .flush(expectedBlob);

    expect(actualBlob!).toEqual(expectedBlob);
  });

  it('should store issue locally when created by anonymous user', () => {
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(JSON.stringify([]));
    jasmine.clock().mockDate(new Date());
    const now = Date.now();

    // add issue with none previously stored
    service.storeAnonymousIssue(1, 'token');

    const savedIssues = JSON.stringify([{ issueId: 1, accessToken: 'token', creationDate: now }]);
    expect(Storage.prototype.setItem).toHaveBeenCalledWith(ANONYMOUS_ISSUES_KEY, savedIssues);

    // add issue with some previously stored
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(savedIssues);

    service.storeAnonymousIssue(3, 'token3');

    expect(Storage.prototype.setItem).toHaveBeenCalledWith(
      ANONYMOUS_ISSUES_KEY,
      JSON.stringify([
        { issueId: 1, accessToken: 'token', creationDate: now },
        { issueId: 3, accessToken: 'token3', creationDate: now }
      ])
    );

    jasmine.clock().uninstall();
  });

  it('should return stored issues locally', () => {
    jasmine.clock().mockDate(new Date());
    const now = Date.now();
    const values = [{ issueId: 1, accessToken: 'token', creationDate: now }];
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(JSON.stringify(values));

    const issues = service.getAnonymousIssuesStored();

    expect(Storage.prototype.getItem).toHaveBeenCalled();
    expect(issues).toEqual(values);

    jasmine.clock().uninstall();
  });

  it('should return empty array if no stored issue locally', () => {
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(null);

    const issues = service.getAnonymousIssuesStored();

    expect(Storage.prototype.getItem).toHaveBeenCalled();
    expect(issues).toEqual([]);
  });

  it('should deanonymize issues and suggest government if not subscribed yet', () => {
    const now = Date.now();
    // 3 issues but only 2 non expired
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(
      JSON.stringify([
        // created more than one hour ago
        { issueId: 1, accessToken: 'token', creationDate: now - 61 * 60 * 1000 },
        { issueId: 2, accessToken: 'token2', creationDate: now - 59 * 60 * 1000 },
        { issueId: 3, accessToken: 'token3', creationDate: now }
      ])
    );

    subscriptionService.list.and.returnValue(
      of([{ id: 56, government: { id: 34567 } }] as Array<SubscriptionModel>)
    );

    let actualResult: DeanonimizedIssues;
    service.deanonymizeIssues().subscribe(result => (actualResult = result));

    // one request in success
    const requestForIssueId2 = http.expectOne({ method: 'PUT', url: '/api/issues/2/creator' });
    expect(requestForIssueId2.request.body).toEqual({ accessToken: 'token2' });
    requestForIssueId2.flush(null);

    // one request fails
    const requestForIssueId3 = http.expectOne({ method: 'PUT', url: '/api/issues/3/creator' });
    expect(requestForIssueId3.request.body).toEqual({ accessToken: 'token3' });
    requestForIssueId3.error(new ProgressEvent('error'));

    // one request to get the details of the latest successfully deanonimized issue
    const issue2: IssueModel = {
      id: 2,
      assignedGovernment: {
        id: 42,
        name: 'Loire'
      }
    } as IssueModel;
    http.expectOne({ method: 'GET', url: '/api/issues/2' }).flush(issue2);
    expect(subscriptionService.list).toHaveBeenCalled();

    // we should have clean issues
    expect(Storage.prototype.removeItem).toHaveBeenCalled();
    // and return the number of deanonymizations
    expect(actualResult!).toEqual({
      count: 1,
      suggestedSubscriptionGovernment: issue2.assignedGovernment
    });
  });

  it('should deanonymize issues and suggest no government if already subscribed', () => {
    const now = Date.now();
    // 3 issues but only 2 non expired
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(
      JSON.stringify([
        { issueId: 2, accessToken: 'token2', creationDate: now - 59 * 60 * 1000 },
        { issueId: 3, accessToken: 'token3', creationDate: now }
      ])
    );

    const loire: GovernmentModel = {
      id: 42,
      name: 'Loire'
    } as GovernmentModel;
    subscriptionService.list.and.returnValue(of([{ id: 56, government: { ...loire } }]));

    let actualResult: DeanonimizedIssues;
    service.deanonymizeIssues().subscribe(result => (actualResult = result));

    // one request in success
    const requestForIssueId2 = http.expectOne({ method: 'PUT', url: '/api/issues/2/creator' });
    expect(requestForIssueId2.request.body).toEqual({ accessToken: 'token2' });
    requestForIssueId2.flush(null);

    // one request in success as well
    const requestForIssueId3 = http.expectOne({ method: 'PUT', url: '/api/issues/3/creator' });
    expect(requestForIssueId3.request.body).toEqual({ accessToken: 'token3' });
    requestForIssueId3.flush(null);

    // one request to get the details of the latest successfully deanonimized issue
    const issue3: IssueModel = {
      id: 3,
      assignedGovernment: { ...loire }
    } as IssueModel;
    http.expectOne({ method: 'GET', url: '/api/issues/3' }).flush(issue3);
    expect(subscriptionService.list).toHaveBeenCalled();

    // we should have clean issues
    expect(Storage.prototype.removeItem).toHaveBeenCalled();
    // and return the number of deanonymizations, but without suggested subscription government
    expect(actualResult!).toEqual({
      count: 2,
      suggestedSubscriptionGovernment: null
    });
  });

  it('should return 0 if no issue to deanonymize', () => {
    // 0 issue
    (Storage.prototype.getItem as jasmine.Spy).and.returnValue(JSON.stringify([]));

    let actualResult: DeanonimizedIssues;
    service.deanonymizeIssues().subscribe(result => (actualResult = result));
    // we should have clean issues
    expect(Storage.prototype.removeItem).toHaveBeenCalled();
    // and return the number of deanonymizations
    expect(actualResult!).toEqual({ count: 0, suggestedSubscriptionGovernment: null });
  });

  it('should delete an issue', () => {
    let deleted = false;
    service.delete(1).subscribe(() => (deleted = true));

    const testRequest = http.expectOne({ method: 'DELETE', url: '/api/issues/1' });
    testRequest.flush(null, { status: 204, statusText: 'No Content' });

    expect(deleted).toBe(true);
  });
});
