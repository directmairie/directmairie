import { TestBed } from '@angular/core/testing';

import { IssueTransitionsComponent } from './issue-transitions.component';
import { ChangeDetectionStrategy, Component, DebugElement, LOCALE_ID, signal } from '@angular/core';
import { IssueModel } from '../models/issue.model';
import { ComponentTester, TestHtmlElement } from 'ngx-speculoos';
import { IssueStatusComponent } from '../issue-status/issue-status.component';

class TestStep extends TestHtmlElement<HTMLElement> {
  constructor(tester: ComponentTester<unknown>, debugElement: DebugElement) {
    super(tester, debugElement);
  }

  get status() {
    return this.component(IssueStatusComponent);
  }

  get instant() {
    return this.element('.instant');
  }

  get message() {
    return this.element('.message');
  }
}

@Component({
  template: '<dm-issue-transitions [issue]="issue()" />',
  imports: [IssueTransitionsComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  // transitions are in the wrong order on purpose, to check that they're correctly sorted
  readonly issue = signal<IssueModel>({
    creationInstant: '2023-03-09T12:00:00.000Z',
    transitions: [
      {
        transitionInstant: '2023-03-10T12:00:00.000Z',
        beforeStatus: 'CREATED',
        afterStatus: 'REJECTED',
        message: 'not found'
      },
      {
        transitionInstant: '2023-03-12T12:00:00.000Z',
        beforeStatus: 'CREATED',
        afterStatus: 'RESOLVED',
        message: 'done'
      },
      {
        transitionInstant: '2023-03-11T12:00:00.000Z',
        beforeStatus: 'REJECTED',
        afterStatus: 'CREATED',
        message: 'reopened'
      }
    ]
  } as IssueModel);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get steps() {
    return this.customs('.step', TestStep);
  }
}

describe('IssueTransitionsComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [{ provide: LOCALE_ID, useValue: 'fr' }]
    });

    tester = new TestComponentTester();
    await tester.stable();
  });

  it('should display transitions', () => {
    expect(tester.steps.length).toBe(4);

    expect(tester.steps[0].status.status()).toBe('CREATED');
    expect(tester.steps[0].instant).toHaveTrimmedText('09/03/2023');
    expect(tester.steps[0].message).toBeNull();

    expect(tester.steps[1].status.status()).toBe('REJECTED');
    expect(tester.steps[1].instant).toHaveTrimmedText('10/03/2023');
    expect(tester.steps[1].message).toHaveTrimmedText('not found');

    expect(tester.steps[2].status.status()).toBe('CREATED');
    expect(tester.steps[2].instant).toHaveTrimmedText('11/03/2023');
    expect(tester.steps[2].message).toHaveTrimmedText('reopened');

    expect(tester.steps[3].status.status()).toBe('RESOLVED');
    expect(tester.steps[3].instant).toHaveTrimmedText('12/03/2023');
    expect(tester.steps[3].message).toHaveTrimmedText('done');
  });
});
