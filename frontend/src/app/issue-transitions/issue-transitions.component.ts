import { ChangeDetectionStrategy, Component, computed, input } from '@angular/core';
import { IssueModel } from '../models/issue.model';
import { IssueStatusComponent } from '../issue-status/issue-status.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'dm-issue-transitions',
  imports: [IssueStatusComponent, DatePipe],
  templateUrl: './issue-transitions.component.html',
  styleUrl: './issue-transitions.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueTransitionsComponent {
  readonly issue = input.required<IssueModel>();
  readonly sortedTransitions = computed(() =>
    [...this.issue().transitions].sort((a, b) =>
      a.transitionInstant.localeCompare(b.transitionInstant)
    )
  );
}
