import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IssueGroupModel } from './models/issue-category.model';
import { CoordinatesModel } from './models/issue.model';

/**
 * Service used to search and get issue categories
 */
@Injectable({
  providedIn: 'root'
})
export class IssueCategoryService {
  private http = inject(HttpClient);

  getCategories(coordinates: CoordinatesModel): Observable<Array<IssueGroupModel>> {
    const params = {
      latitude: coordinates.latitude.toString(),
      longitude: coordinates.longitude.toString()
    };
    return this.http.get<Array<IssueGroupModel>>(`/api/issue-groups`, { params });
  }

  getAllCategories(): Observable<Array<IssueGroupModel>> {
    return this.http.get<Array<IssueGroupModel>>(`/api/issue-groups`);
  }
}
