import { Injectable } from '@angular/core';
import { AuthenticationResultModel } from './models/authentication-result.model';

const LOCAL_STORAGE_KEY = 'dm-token';

/**
 * Service storing the current user token in local storage
 */
@Injectable({
  providedIn: 'root'
})
export class CurrentUserStorageService {
  loadAuthenticatedUser(): AuthenticationResultModel | null {
    const value = window.localStorage.getItem(LOCAL_STORAGE_KEY);
    if (value) {
      return JSON.parse(value);
    }
    return null;
  }

  storeAuthenticatedUser(user: AuthenticationResultModel | null) {
    if (user == null) {
      window.localStorage.removeItem(LOCAL_STORAGE_KEY);
    } else {
      window.localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(user));
    }
  }
}
