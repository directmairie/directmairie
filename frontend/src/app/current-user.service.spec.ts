import { TestBed } from '@angular/core/testing';

import { CurrentUserService } from './current-user.service';
import { CurrentUserStorageService } from './current-user-storage.service';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { CurrentUserModel } from './models/current-user.model';
import { UserModel } from './registration/models/registration.model';
import { ErrorInterceptorService, HttpError } from './error-interceptor.service';
import { createMock } from 'ngx-speculoos';
import { Subject } from 'rxjs';
import { HttpStatusCode, provideHttpClient } from '@angular/common/http';

describe('CurrentUserService', () => {
  let currentUserService: CurrentUserService;
  let currentUserStorageService: jasmine.SpyObj<CurrentUserStorageService>;
  let errorInterceptorService: jasmine.SpyObj<ErrorInterceptorService>;
  let http: HttpTestingController;
  let errorSubject: Subject<HttpError>;

  beforeEach(() => {
    currentUserStorageService = createMock(CurrentUserStorageService);

    errorSubject = new Subject<HttpError>();
    errorInterceptorService = createMock(ErrorInterceptorService);
    errorInterceptorService.getErrors.and.returnValue(errorSubject);
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideHttpClientTesting(),
        { provide: ErrorInterceptorService, useValue: errorInterceptorService },
        { provide: CurrentUserStorageService, useValue: currentUserStorageService }
      ]
    });

    currentUserService = TestBed.inject(CurrentUserService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should have no user initially', () => {
    let currentUser: UserModel | null = null;
    currentUserService.getUserChanges().subscribe(user => (currentUser = user));
    expect(currentUser).toBeNull();
  });

  it('should not load the user when initialized if there is none stored', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue(null);
    currentUserService.init();
    http.expectNone('/api/users/me');
    expect().nothing();
  });

  it('should load the user when initialized if there is one stored', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue({ token: 'abc' });
    currentUserService.init();

    const expectedCurrentUser = { id: 1000 } as CurrentUserModel;
    http
      .expectOne({
        method: 'GET',
        url: '/api/users/me'
      })
      .flush(expectedCurrentUser);

    let actualCurrentUser: CurrentUserModel | null = null;
    currentUserService.getUserChanges().subscribe(user => (actualCurrentUser = user));
    expect(actualCurrentUser!).toEqual(expectedCurrentUser);
  });

  it('should remove the token from local storage if an unexpected 401 error is emitted', () => {
    currentUserService.init();

    errorSubject.next({
      status: HttpStatusCode.BadRequest,
      message: '',
      functionalError: null
    });

    expect(currentUserStorageService.storeAuthenticatedUser).not.toHaveBeenCalled();

    errorSubject.next({
      status: HttpStatusCode.Unauthorized,
      message: '',
      functionalError: null
    });

    expect(currentUserStorageService.storeAuthenticatedUser).toHaveBeenCalledWith(null);

    let currentUser: UserModel | null = { id: 1000 } as UserModel;
    currentUserService.getUserChanges().subscribe(user => (currentUser = user));
    expect(currentUser).toBeNull();
  });

  it('should store and refresh the current user and emit it', () => {
    const expectedCurrentUser = { id: 1000 } as CurrentUserModel;
    let actualCurrentUser: CurrentUserModel | null = null;
    const authenticatedUser = { token: 'abc' };
    currentUserService
      .storeAndRefresh(authenticatedUser)
      .subscribe(user => (actualCurrentUser = user));

    expect(currentUserStorageService.storeAuthenticatedUser).toHaveBeenCalledWith(
      authenticatedUser
    );

    http
      .expectOne({
        method: 'GET',
        url: '/api/users/me'
      })
      .flush(expectedCurrentUser);

    expect(actualCurrentUser!).toEqual(expectedCurrentUser);
    let emittedCurrentUser: UserModel | null = null;
    currentUserService.getUserChanges().subscribe(user => (emittedCurrentUser = user));
    expect(emittedCurrentUser!).toEqual(expectedCurrentUser);
  });

  it('should refresh the current user and emit it', () => {
    const expectedCurrentUser = { id: 1000 } as CurrentUserModel;
    let actualCurrentUser: CurrentUserModel | null = null;
    currentUserService.refresh().subscribe(user => (actualCurrentUser = user));

    http
      .expectOne({
        method: 'GET',
        url: '/api/users/me'
      })
      .flush(expectedCurrentUser);

    expect(actualCurrentUser!).toEqual(expectedCurrentUser);
    let emittedCurrentUser: UserModel | null = null;
    currentUserService.getUserChanges().subscribe(user => (emittedCurrentUser = user));
    expect(emittedCurrentUser!).toEqual(expectedCurrentUser);
  });

  it('should remove the current user, and emit null', () => {
    currentUserService.remove();
    expect(currentUserStorageService.storeAuthenticatedUser).toHaveBeenCalledWith(null);

    let currentUser: UserModel | null = { id: 1000 } as UserModel;
    currentUserService.getUserChanges().subscribe(user => (currentUser = user));
    expect(currentUser).toBeNull();
  });

  it('should be authenticated if storage has authenticated user', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue({ token: 'abc' });
    expect(currentUserService.isAuthenticated()).toBe(true);
  });

  it('should not be authenticated if storage has no authenticated user', () => {
    currentUserStorageService.loadAuthenticatedUser.and.returnValue(null);
    expect(currentUserService.isAuthenticated()).toBe(false);
  });
});
