import { TestBed } from '@angular/core/testing';

import {
  GeolocationService,
  LocationSearchResult,
  NominatimAddress,
  NominatimSearchResult
} from './geolocation.service';
import { CoordinatesModel } from './models/issue.model';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideHttpClient } from '@angular/common/http';

describe('GeolocationService', () => {
  let service: GeolocationService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideHttpClient(), provideHttpClientTesting()]
    });

    service = TestBed.inject(GeolocationService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should get current coordinates', () => {
    spyOn(navigator.geolocation, 'getCurrentPosition');

    const expectedCoordinates: CoordinatesModel = { latitude: 1, longitude: 2 };
    let actualCoordinates: CoordinatesModel | null = null;
    let completed = false;
    service.currentCoordinates().subscribe({
      next: coordinates => (actualCoordinates = coordinates),
      complete: () => (completed = true)
    });

    expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalled();

    // call with fake GPS coords
    const getCurrentPositionCallback: (pos: GeolocationPosition) => void = (
      navigator.geolocation.getCurrentPosition as jasmine.Spy
    ).calls.mostRecent().args[0];
    getCurrentPositionCallback({
      coords: {
        latitude: expectedCoordinates.latitude,
        longitude: expectedCoordinates.longitude
      } as GeolocationCoordinates,
      timestamp: Date.now()
    } as GeolocationPosition);

    expect(actualCoordinates!).toEqual(expectedCoordinates);
    expect(completed).toBe(true);
  });

  it('should should get current coordinates as error observable in case of error', () => {
    spyOn(navigator.geolocation, 'getCurrentPosition');

    let error: unknown | null = null;
    service.currentCoordinates().subscribe({ error: e => (error = e) });

    expect(navigator.geolocation.getCurrentPosition).toHaveBeenCalled();

    // call with fake GPS coords
    const getCurrentPositionErrorCallback: (error: unknown) => void = (
      navigator.geolocation.getCurrentPosition as jasmine.Spy
    ).calls.mostRecent().args[1];
    getCurrentPositionErrorCallback('error');

    expect(error).toBe('error');
  });

  it('should search locations and strip country suffix from display name if no address', () => {
    const apiResults: Array<NominatimSearchResult> = [
      {
        lat: '1',
        lon: '2',
        display_name: 'Lyon, 69000, France',
        address: {},
        boundingbox: ['1', '2', '3', '4']
      },
      {
        lat: '2',
        lon: '3',
        display_name: 'Lyon, 69003',
        address: {},
        boundingbox: ['1', '2', '3', '4']
      }
    ];
    const expectedSearchResults: Array<LocationSearchResult> = [
      {
        coordinates: {
          latitude: 1,
          longitude: 2
        },
        label: 'Lyon, 69000',
        boundingBox: {
          minLatitude: 1,
          maxLatitude: 2,
          minLongitude: 3,
          maxLongitude: 4
        }
      },
      {
        coordinates: {
          latitude: 2,
          longitude: 3
        },
        label: 'Lyon, 69003',
        boundingBox: {
          minLatitude: 1,
          maxLatitude: 2,
          minLongitude: 3,
          maxLongitude: 4
        }
      }
    ];
    let actualSearchResults: Array<LocationSearchResult> | null = null;

    service.search('lyon france').subscribe(results => (actualSearchResults = results));

    const testRequest = http.expectOne(
      req => req.method === 'GET' && req.url === 'https://nominatim.openstreetmap.org/search'
    );

    testRequest.flush(apiResults);

    expect(actualSearchResults!).toEqual(expectedSearchResults);
    expect(testRequest.request.params.get('q')).toEqual('lyon france');
    expect(testRequest.request.params.get('countrycodes')).toEqual('FR');
    expect(testRequest.request.params.get('format')).toEqual('json');
    expect(testRequest.request.params.get('addressdetails')).toEqual('1');
  });

  it('should create label from address if possible', () => {
    function searchResult(address: NominatimAddress): NominatimSearchResult {
      return {
        lat: '1',
        lon: '2',
        display_name: '',
        address,
        boundingbox: ['0', '5', '1', '4']
      };
    }

    function expectedResult(label: string): LocationSearchResult {
      return {
        coordinates: {
          latitude: 1,
          longitude: 2
        },
        boundingBox: {
          minLatitude: 0,
          maxLatitude: 5,
          minLongitude: 1,
          maxLongitude: 4
        },
        label
      };
    }

    const apiResults: Array<NominatimSearchResult> = [
      searchResult({
        road: 'rue du moulin1',
        postcode: '75000',
        city: 'Paris'
      }),
      searchResult({
        pedestrian: 'rue du moulin2',
        postcode: '75000',
        town: 'Paris'
      }),
      searchResult({
        footway: 'rue du moulin3',
        postcode: '75000',
        village: 'Paris'
      }),
      searchResult({
        hamlet: 'rue du moulin4',
        postcode: '75000',
        county: 'Paris'
      }),
      searchResult({
        road: 'rue du moulin5',
        postcode: '75000',
        city: 'Paris',
        state: 'Ile de France'
      }),
      searchResult({
        road: 'rue du moulin6',
        city: 'Paris'
      }),
      searchResult({
        path: 'rue du moulin7',
        city: 'Paris'
      })
    ];
    const expectedSearchResults: Array<LocationSearchResult> = [
      expectedResult('rue du moulin1, 75000, Paris'),
      expectedResult('rue du moulin2, 75000, Paris'),
      expectedResult('rue du moulin3, 75000, Paris'),
      expectedResult('rue du moulin4, 75000, Paris'),
      expectedResult('rue du moulin5, 75000, Paris, Ile de France'),
      expectedResult('rue du moulin6, Paris'),
      expectedResult('rue du moulin7, Paris')
    ];
    let actualSearchResults: Array<LocationSearchResult> | null = null;

    service.search('moulin').subscribe(results => (actualSearchResults = results));

    const testRequest = http.expectOne(
      req => req.method === 'GET' && req.url === 'https://nominatim.openstreetmap.org/search'
    );

    testRequest.flush(apiResults);

    expect(actualSearchResults!).toEqual(expectedSearchResults);
  });

  it('should get location label from coordinates', () => {
    const apiResult: NominatimSearchResult = {
      lat: '1',
      lon: '2',
      display_name: 'Lyon, 69000, France',
      address: {
        road: 'rue du moulin',
        postcode: '75000',
        city: 'Paris'
      },
      boundingbox: ['0', '5', '0', '5']
    };

    let actualResult: string | null = null;

    service
      .findLocationLabel({ latitude: 1, longitude: 2 })
      .subscribe(result => (actualResult = result));

    const testRequest = http.expectOne(
      req => req.method === 'GET' && req.url === 'https://nominatim.openstreetmap.org/reverse'
    );

    testRequest.flush(apiResult);

    expect(actualResult!).toBe('rue du moulin, 75000, Paris');
    expect(testRequest.request.params.get('lat')).toBe('1');
    expect(testRequest.request.params.get('lon')).toBe('2');
    expect(testRequest.request.params.get('format')).toBe('json');
    expect(testRequest.request.params.get('addressdetails')).toBe('1');
  });

  it('should find information for osm id', () => {
    const apiResult: NominatimSearchResult = {
      lat: '1',
      lon: '2',
      display_name: 'Lyon, France',
      address: {},
      boundingbox: ['0', '5', '0', '5']
    };

    let actualResult: string | null = null;

    service.findGovernmentInfo(1).subscribe(result => (actualResult = result));

    http
      .expectOne(
        'https://nominatim.openstreetmap.org/lookup?osm_ids=R1&format=json&addressdetails=1&accept-language=fr'
      )
      .flush([apiResult]);

    expect(actualResult!).toBe('Lyon');
  });

  it('should throw if information not found for osm id', () => {
    let actualError: Error | null = null;

    service.findGovernmentInfo(1).subscribe({
      next: () => fail(),
      error: e => (actualError = e)
    });

    http
      .expectOne(
        'https://nominatim.openstreetmap.org/lookup?osm_ids=R1&format=json&addressdetails=1&accept-language=fr'
      )
      .flush([]);

    expect(actualError!.message).toBe('Nominatim reverse lookup failed');
  });
});
