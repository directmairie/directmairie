import { Injectable, inject } from '@angular/core';
import { map, Observable, Subscriber } from 'rxjs';
import { BoundingBox, CoordinatesModel } from './models/issue.model';
import { HttpClient } from '@angular/common/http';

export interface LocationSearchResult {
  coordinates: CoordinatesModel;
  label: string;
  boundingBox: BoundingBox;
}

/**
 * The shape of a nominatim address.
 * There are more fields, but these are those that we are interested about
 */
export interface NominatimAddress {
  road?: string;
  path?: string;
  pedestrian?: string;
  footway?: string;
  hamlet?: string;
  postcode?: string;
  city?: string;
  town?: string;
  village?: string;
  county?: string;
  state?: string;
}

/**
 * The shape of a nominatim search result.
 * There are more fields, but these are those that we are interested about
 */
export interface NominatimSearchResult {
  lat: string;
  lon: string;
  display_name: string;
  address: NominatimAddress;
  boundingbox: [string, string, string, string];
}

function createLabel(r: NominatimSearchResult): string {
  const address = r.address;

  const streetOrSimilar =
    address.road || address.path || address.pedestrian || address.footway || address.hamlet;
  const cityOrSimilar = address.city || address.town || address.village || address.county;

  if (streetOrSimilar && cityOrSimilar) {
    const parts = [];
    parts.push(streetOrSimilar);

    if (address.postcode) {
      parts.push(address.postcode);
    }

    parts.push(cityOrSimilar);

    if (r.address.state) {
      parts.push(r.address.state);
    }

    return parts.join(', ');
  }

  const label = r.display_name;
  const suffix = ', France';
  if (label.endsWith(suffix)) {
    return label.substring(0, label.length - suffix.length);
  }
  return label;
}

/**
 * Service allowing to interact with the Nominatim OpenStreetMap API directly from the browser
 * See http://nominatim.org/release-docs/latest/api/Overview/
 *
 * The idea of this service is to expose an API and result objects thar are not coupled with Nominatim, and instead
 * expose just the information that the application needs.
 */
@Injectable({
  providedIn: 'root'
})
export class GeolocationService {
  private http = inject(HttpClient);

  currentCoordinates(): Observable<CoordinatesModel> {
    return new Observable((subscriber: Subscriber<CoordinatesModel>) => {
      navigator.geolocation.getCurrentPosition(
        position => {
          subscriber.next({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          });
          subscriber.complete();
        },
        error => subscriber.error(error)
      );
    });
  }

  search(text: string): Observable<Array<LocationSearchResult>> {
    const params = {
      q: text,
      format: 'json',
      countrycodes: 'FR',
      'accept-language': 'fr',
      addressdetails: '1'
    };
    return this.http
      .get<Array<NominatimSearchResult>>('https://nominatim.openstreetmap.org/search', { params })
      .pipe(
        map(array =>
          array.map(
            elem =>
              ({
                label: createLabel(elem),
                coordinates: {
                  latitude: +elem.lat,
                  longitude: +elem.lon
                },
                boundingBox: {
                  minLatitude: +elem.boundingbox[0],
                  maxLatitude: +elem.boundingbox[1],
                  minLongitude: +elem.boundingbox[2],
                  maxLongitude: +elem.boundingbox[3]
                }
              }) as LocationSearchResult
          )
        )
      );
  }

  findLocationLabel(coordinates: CoordinatesModel): Observable<string> {
    const params = {
      lat: coordinates.latitude,
      lon: coordinates.longitude,
      format: 'json',
      addressdetails: '1',
      'accept-language': 'fr'
    };
    return this.http
      .get<NominatimSearchResult>('https://nominatim.openstreetmap.org/reverse', { params })
      .pipe(map(elem => createLabel(elem)));
  }

  findGovernmentInfo(osmId: number): Observable<string> {
    const params = {
      osm_ids: [`R${osmId}`],
      format: 'json',
      addressdetails: '1',
      'accept-language': 'fr'
    };
    return this.http
      .get<Array<NominatimSearchResult>>('https://nominatim.openstreetmap.org/lookup', { params })
      .pipe(
        map(results => {
          // if the osmId doesn't exist then Nominatim returns a success
          // but with a body containing an empty array
          // we transform such responses in an error
          // otherwise we build the label with the first result
          if (!results || results.length === 0) {
            throw new Error('Nominatim reverse lookup failed');
          } else {
            return createLabel(results[0]);
          }
        })
      );
  }
}
