import { AbstractControl } from '@angular/forms';

export function passwordsMatch(group: AbstractControl) {
  const value = group.value;
  if (
    value.password &&
    value.passwordConfirmation &&
    value.password !== value.passwordConfirmation
  ) {
    return { passwordsMatch: true };
  } else {
    return null;
  }
}
