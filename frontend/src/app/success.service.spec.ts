import { TestBed } from '@angular/core/testing';

import { SuccessService } from './success.service';

describe('SuccessService', () => {
  it('should emit every time a success is called', () => {
    const service = TestBed.inject(SuccessService);

    let message: string | null = null;
    service.getMessage().subscribe(m => (message = m));

    service.success('hello');
    expect(message!).toBe('hello');

    service.success('world');
    expect(message!).toBe('world');
  });
});
