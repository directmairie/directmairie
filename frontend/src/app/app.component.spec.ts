import { TestBed } from '@angular/core/testing';
import { provideRouter, RouterOutlet } from '@angular/router';
import { VersionEvent } from '@angular/service-worker';
import { Subject } from 'rxjs';

import { AppComponent } from './app.component';
import { ErrorComponent } from './error/error.component';
import { SwUpdateService } from './sw-update.service';
import { NavbarComponent } from './navbar/navbar.component';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import { SuccessComponent } from './success/success.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { FooterComponent } from './footer/footer.component';
import { provideHttpClient } from '@angular/common/http';

describe('AppComponent', () => {
  const updatesAvailable = new Subject<VersionEvent>();
  const swUpdate = createMock(SwUpdateService);
  swUpdate.updatesAvailable.and.returnValue(updatesAvailable);
  let tester: ComponentTester<AppComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [
        provideHttpClient(),
        provideHttpClientTesting(),
        provideRouter([]),
        { provide: SwUpdateService, useValue: swUpdate }
      ]
    });
    tester = new ComponentTester(AppComponent);
    await tester.stable();
  });

  it('should display a navbar component', () => {
    const navbar = tester.element(NavbarComponent);
    expect(navbar).not.toBeNull();
  });

  it('should display a router outlet', () => {
    const routerOutlet = tester.element(RouterOutlet);
    expect(routerOutlet).not.toBeNull();
  });

  it('should display an error component', () => {
    const errorComponent = tester.element(ErrorComponent);
    expect(errorComponent).not.toBeNull();
  });

  it('should display a success component', () => {
    const successComponent = tester.element(SuccessComponent);
    expect(successComponent).not.toBeNull();
  });

  it('should display a footer component', () => {
    const footerComponent = tester.element(FooterComponent);
    expect(footerComponent).not.toBeNull();
  });

  it('should display a notification if a new version is available', async () => {
    // a new version is available
    updatesAvailable.next({} as VersionEvent);
    await tester.stable();

    const updateAvailableNotification = tester.element('.alert-warning');
    expect(updateAvailableNotification).not.toBeNull();
  });
});
