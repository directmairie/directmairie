import { Injectable, NgZone, inject } from '@angular/core';
import {
  HttpClient,
  HttpEventType,
  HttpHeaders,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { catchError, forkJoin, last, map, Observable, of, Subscriber, switchMap, tap } from 'rxjs';

import {
  IssueCommand,
  IssueModel,
  IssueStatus,
  IssueStatusCommand,
  IssueWithAccessTokenModel,
  LISTABLE_STATUSES,
  PictureModel
} from './models/issue.model';
import { PageModel } from './models/page.model';
import { DateTime } from 'luxon';
import { GovernmentModel } from './admin/models/government.model';
import { SubscriptionService } from './account/subscriptions/subscription.service';

/**
 * The max size of the width and height of a picture
 */
const MAX_PICTURE_SIZE = 1024;

/**
 * The key used in the LocalStorage to store created issues by anonymous users
 */
export const ANONYMOUS_ISSUES_KEY = 'dm-anonymous-issues';

export const ISSUE_ACCESS_TOKEN_HEADER = 'X-Issue-Access';

interface AnonymousIssueModel {
  issueId: number;
  accessToken: string;
  creationDate: number;
}

interface IssueCriteria {
  page: number;
  statusFilter: Array<IssueStatus>;
  poolingOrganizationIds: Array<number>;
  governmentIds: Array<number>;
}

export interface DeanonimizedIssues {
  count: number;
  suggestedSubscriptionGovernment: GovernmentModel | null;
}

/**
 * Service for the CRUD of issues and their pictures
 */
@Injectable({
  providedIn: 'root'
})
export class IssueService {
  private http = inject(HttpClient);
  private subscriptionService = inject(SubscriptionService);
  private ngZone = inject(NgZone);

  create(command: IssueCommand): Observable<IssueWithAccessTokenModel> {
    return this.http.post<IssueModel>('/api/issues', command);
  }

  update(issueId: number, accessToken: string, command: IssueCommand): Observable<IssueModel> {
    return this.http.put<IssueModel>(`/api/issues/${issueId}`, command, {
      headers: this.accessTokenHeaders(accessToken)
    });
  }

  delete(issueId: number): Observable<void> {
    return this.http.delete<void>(`/api/issues/${issueId}`);
  }

  updateStatus(issueId: number, command: IssueStatusCommand): Observable<IssueModel> {
    return this.http.put<IssueModel>(`/api/issues/${issueId}/status`, command);
  }

  resizePicture(file: File): Observable<Blob> {
    return new Observable((observer: Subscriber<Blob>) => {
      const img = document.createElement('img');
      this._readFile(file, image => {
        img.src = image;
        img.onload = () => {
          let width = img.width;
          let height = img.height;

          if (width <= MAX_PICTURE_SIZE && height <= MAX_PICTURE_SIZE) {
            observer.next(file);
            observer.complete();
            return;
          }

          if (width > height) {
            if (width > MAX_PICTURE_SIZE) {
              height *= MAX_PICTURE_SIZE / width;
              width = MAX_PICTURE_SIZE;
            }
          } else {
            if (height > MAX_PICTURE_SIZE) {
              width *= MAX_PICTURE_SIZE / height;
              height = MAX_PICTURE_SIZE;
            }
          }

          const canvas = document.createElement('canvas');
          canvas.width = width;
          canvas.height = height;
          const ctx = canvas.getContext('2d')!;
          ctx.drawImage(img, 0, 0, width, height);

          const blobCallback = (blob: Blob | null) => {
            // this is necessary because Zone.js doesn't patch canvas anymore: see https://github.com/angular/angular/issues/30939
            this.ngZone.run(() => {
              observer.next(blob!);
              observer.complete();
            });
          };

          canvas.toBlob(blobCallback, 'image/jpeg', 0.8);
        };
      });
    });
  }

  uploadPicture(
    issueId: number,
    accessToken: string,
    file: Blob,
    progressListener: (progress: number) => void
  ): Observable<PictureModel> {
    const formData = new FormData();
    formData.set('file', file);
    const request = new HttpRequest('POST', `/api/issues/${issueId}/pictures`, formData, {
      reportProgress: true,
      headers: this.accessTokenHeaders(accessToken)
    });
    return this.http.request(request).pipe(
      tap(event => {
        if (event.type === HttpEventType.UploadProgress) {
          const percentDone = (100 * event.loaded) / event.total!;
          progressListener(percentDone);
        }
      }),
      last(),
      map(event => (event as HttpResponse<PictureModel>).body!)
    );
  }

  deletePicture(issueId: number, accessToken: string, pictureId: number): Observable<void> {
    return this.http.delete<void>(`/api/issues/${issueId}/pictures/${pictureId}`, {
      headers: this.accessTokenHeaders(accessToken)
    });
  }

  /**
   * For the sake of unit testing
   */
  _readFile(file: File, callback: (image: string) => void) {
    const reader = new FileReader();
    reader.onload = () => callback(reader.result as string);
    reader.readAsDataURL(file);
  }

  list(criteria: IssueCriteria): Observable<PageModel<IssueModel>> {
    // we decrease the page as the frontend is 1 based, and the backend 0 based.
    const page = criteria.page - 1;
    // we built the search parameters
    const params: Record<string, string | Array<string> | number | Array<number>> = {
      page,
      status: criteria.statusFilter.length ? criteria.statusFilter : LISTABLE_STATUSES,
      org: criteria.poolingOrganizationIds,
      gov: criteria.governmentIds
    };
    return this.http.get<PageModel<IssueModel>>('/api/issues', { params });
  }

  listMine(): Observable<PageModel<IssueModel>> {
    // the backend returns a paginated result, so we could ask for a specific page
    // but we only need the last issues, so we ask for page 0 (by default)
    return this.http.get<PageModel<IssueModel>>('/api/issues/mine');
  }

  get(issueId: number): Observable<IssueModel> {
    return this.http.get<IssueModel>(`/api/issues/${issueId}`);
  }

  export(from: string, to: string): Observable<Blob> {
    const timezone = DateTime.local().zoneName!;
    const params = {
      from,
      to,
      timezone,
      export: 'CSV'
    };
    const headers = {
      Accept: 'text/csv'
    };
    return this.http.get('/api/issues', { params, headers, responseType: 'blob' });
  }

  private accessTokenHeaders(accessToken: string): HttpHeaders {
    if (accessToken) {
      return new HttpHeaders({ [ISSUE_ACCESS_TOKEN_HEADER]: accessToken });
    }
    return new HttpHeaders();
  }

  /**
   * Adds an issue anonymously created to LocalStorage,
   * with its access token, and a creation timestamp.
   * @param issueId - the issue ID
   * @param accessToken - the access token
   */
  storeAnonymousIssue(issueId: number, accessToken: string) {
    const issues: Array<AnonymousIssueModel> = this.getAnonymousIssuesStored();
    issues.push({ issueId, accessToken, creationDate: Date.now() });
    window.localStorage.setItem(ANONYMOUS_ISSUES_KEY, JSON.stringify(issues));
  }

  /**
   * Get the issues anonymously created previously,
   * with their access token, and their creation timestamp.
   * Returns an empty Array if none.
   */
  getAnonymousIssuesStored(): Array<AnonymousIssueModel> {
    const issuesAsString = window.localStorage.getItem(ANONYMOUS_ISSUES_KEY);
    if (issuesAsString) {
      return JSON.parse(issuesAsString);
    }
    return [];
  }

  /**
   * Retrieves the issues anonymously created previously
   * and tries to attach them to the current user
   * whe he/she is logged in/registered.
   * Returns an observable emitting the number of successful deanonymizations, as well as a
   * the government of the latest issue if the user is not usbscribed to it yet
   */
  deanonymizeIssues(): Observable<DeanonimizedIssues> {
    // we only try to deanonymize the issues created in the last hour
    const expiration = Date.now() - 60 * 60 * 1000;

    // create a request for each issue
    const requests = this.getAnonymousIssuesStored()
      // not expired
      .filter(issue => issue.creationDate > expiration)
      .map(issue =>
        this.http
          .put<void>(`/api/issues/${issue.issueId}/creator`, { accessToken: issue.accessToken })
          .pipe(
            // return true if the deanonymization is a success
            map(() => ({ issue, deanonimized: true })),
            // we don't really care if a deanonymization fails
            // so we return false
            catchError(() => of({ issue, deanonimized: false }))
          )
      );

    // reset the local storage
    window.localStorage.removeItem(ANONYMOUS_ISSUES_KEY);

    if (requests.length) {
      // send the requests
      return forkJoin(requests).pipe(
        map(results => results.filter(r => r.deanonimized)),
        switchMap(results => {
          if (results.length > 0) {
            const latestDeanonimizedIssue: AnonymousIssueModel = results[results.length - 1].issue;
            return this.getSuggestedSubscriptionGovernment(latestDeanonimizedIssue).pipe(
              map(suggestedSubscriptionGovernment => ({
                count: results.length,
                suggestedSubscriptionGovernment
              }))
            );
          } else {
            return of({ count: 0, suggestedSubscriptionGovernment: null });
          }
        })
      );
    }
    return of({ count: 0, suggestedSubscriptionGovernment: null });
  }

  private getSuggestedSubscriptionGovernment(
    issue: AnonymousIssueModel
  ): Observable<GovernmentModel | null> {
    return forkJoin({
      issue: this.get(issue.issueId),
      subscriptions: this.subscriptionService.list()
    }).pipe(
      map(({ issue, subscriptions }) =>
        subscriptions.some(
          subscription => subscription.government.id === issue.assignedGovernment!.id
        )
          ? null
          : issue.assignedGovernment
      )
    );
  }
}
