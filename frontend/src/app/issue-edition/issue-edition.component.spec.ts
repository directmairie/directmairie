import { TestBed } from '@angular/core/testing';
import { provideHttpClientTesting } from '@angular/common/http/testing';
import {
  ActivatedRouteStub,
  ComponentTester,
  createMock,
  provideAutomaticChangeDetection,
  stubRoute
} from 'ngx-speculoos';
import { of, Subject } from 'rxjs';

import { IssueEditionComponent } from './issue-edition.component';
import { IssueService } from '../issue.service';
import {
  CoordinatesModel,
  IssueCommand,
  IssueModel,
  IssueWithAccessTokenModel
} from '../models/issue.model';
import { CoordinatesSelectionComponent } from './coordinates-selection/coordinates-selection.component';
import { PicturesComponent } from './pictures/pictures.component';
import { IssueCategoryService } from '../issue-category.service';
import { IssueCategoryModel, IssueGroupModel } from '../models/issue-category.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DefaultErrorMessagesComponent } from '../default-error-messages/default-error-messages.component';
import { GovernmentModel } from '../admin/models/government.model';
import { provideHttpClient } from '@angular/common/http';
import { NgbConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThankYouComponent } from './thank-you/thank-you.component';

class IssueEditionComponentTester extends ComponentTester<IssueEditionComponent> {
  constructor() {
    super(IssueEditionComponent);
  }

  get title() {
    return this.element('h1');
  }

  get coordinateSelectionComponent() {
    return this.component(CoordinatesSelectionComponent);
  }

  get loadingCategoriesSpinner() {
    return this.element('#loadingCategoriesSpinner');
  }

  get issueForm() {
    return this.element('form');
  }

  get noCategoryError() {
    return this.element('#noCategoryError');
  }

  get category() {
    return this.select('#category')!;
  }

  get descriptionAcknowledgementButton() {
    return this.button('#description-acnowledgment-button')!;
  }

  get description() {
    return this.textarea('#description')!;
  }

  get descriptionRequiredMarker() {
    return this.element('#descriptionRequiredMarker');
  }

  get picturesComponent() {
    return this.component(PicturesComponent);
  }

  get submit() {
    return this.button('#submit')!;
  }

  get thankYou() {
    return this.component(ThankYouComponent);
  }
}

describe('IssueEditionComponent', () => {
  let tester: IssueEditionComponentTester;
  let issueService: jasmine.SpyObj<IssueService>;
  let issueCategoryService: jasmine.SpyObj<IssueCategoryService>;
  let router: Router;
  let route: ActivatedRouteStub;

  beforeEach(async () => {
    TestBed.overrideTemplate(CoordinatesSelectionComponent, '');
    TestBed.overrideTemplate(PicturesComponent, '');

    issueService = createMock(IssueService);
    issueCategoryService = createMock(IssueCategoryService);
    issueCategoryService.getCategories.and.returnValue(of([]));

    route = stubRoute();

    TestBed.configureTestingModule({
      providers: [
        provideAutomaticChangeDetection(),
        provideHttpClient(),
        provideHttpClientTesting(),
        { provide: IssueService, useValue: issueService },
        { provide: IssueCategoryService, useValue: issueCategoryService },
        { provide: ActivatedRoute, useValue: route },
        { provide: NgbConfig, useValue: { animation: false } as NgbConfig }
      ]
    });

    router = TestBed.inject(Router);
    spyOn(router, 'navigate');
    spyOn(router, 'navigateByUrl');

    // to enable the default error messages
    await TestBed.createComponent(DefaultErrorMessagesComponent).whenStable();
  });

  describe('creation of a new issue', () => {
    beforeEach(async () => {
      tester = new IssueEditionComponentTester();
      await tester.change();
    });

    describe('before coordinate selection', () => {
      it('should have a title', () => {
        expect(tester.title).toContainText('Remonter une information');
      });

      it('should not display form until coordinates are chosen', () => {
        expect(tester.coordinateSelectionComponent).not.toBeNull();
        expect(tester.issueForm).toBeNull();
        expect(tester.picturesComponent).toBeNull();
        expect(tester.loadingCategoriesSpinner).toBeNull();
        expect(tester.noCategoryError).toBeNull();
      });
    });

    describe('after coordinate selection', () => {
      let coordinates: CoordinatesModel;
      let issue: IssueWithAccessTokenModel;

      beforeEach(() => {
        coordinates = { latitude: 0, longitude: 1 };
        issue = {
          id: 42,
          coordinates,
          creationInstant: '2019-01-11T00:00:00Z',
          category: null,
          description: null,
          status: 'DRAFT',
          pictures: [],
          accessToken: 'access-token',
          assignedGovernment: null,
          transitions: []
        };
        issueService.create.and.returnValue(of(issue));
      });

      it('should create a draft issue when coordinates are chosen the first time only', async () => {
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        const expectedCommand: IssueCommand = {
          coordinates,
          categoryId: null,
          description: null,
          status: 'DRAFT'
        };
        expect(issueService.create).toHaveBeenCalledWith(expectedCommand);
        expect(tester.componentInstance.issue()).toEqual(issue);
        expect(tester.componentInstance.accessToken()).toBe(issue.accessToken!);
        expect(router.navigate).toHaveBeenCalledWith(
          [{ id: issue.id, 'access-token': issue.accessToken }],
          { relativeTo: route, replaceUrl: true }
        );

        issueService.create.calls.reset();
        (router.navigate as jasmine.Spy).calls.reset();
        const otherCoordinates: CoordinatesModel = { latitude: 1, longitude: 2 };
        tester.coordinateSelectionComponent.coordinatesSelected.emit(otherCoordinates);
        await tester.change();

        expect(issueService.create).not.toHaveBeenCalled();
        expect(router.navigate).not.toHaveBeenCalled();
        expect(tester.componentInstance.issue()!.coordinates).toEqual(otherCoordinates);
      });

      it('should display a spinner the first time categories are loaded', async () => {
        const categoriesSubject1 = new Subject<Array<IssueGroupModel>>();
        const categoriesSubject2 = new Subject<Array<IssueGroupModel>>();
        issueCategoryService.getCategories.and.returnValues(categoriesSubject1, categoriesSubject2);

        expect(tester.loadingCategoriesSpinner).toBeNull();
        expect(tester.noCategoryError).toBeNull();

        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        expect(tester.loadingCategoriesSpinner).not.toBeNull();
        expect(tester.noCategoryError).toBeNull();

        const groups: Array<IssueGroupModel> = [
          {
            id: 2,
            label: 'Lighting',
            categories: [
              {
                id: 2,
                label: 'Light broken',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        categoriesSubject1.next(groups);
        categoriesSubject1.complete();
        await tester.change();

        expect(tester.loadingCategoriesSpinner).toBeNull();
        expect(tester.noCategoryError).toBeNull();

        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        expect(tester.loadingCategoriesSpinner).toBeNull();
        expect(tester.noCategoryError).toBeNull();
      });

      it('should refresh the categories when the coordinates change and display an error if no category', async () => {
        const coordinates2: CoordinatesModel = { latitude: 1, longitude: 2 };

        const groups1: Array<IssueGroupModel> = [
          {
            id: 2,
            label: 'Lighting',
            categories: [
              {
                id: 2,
                label: 'Light broken',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        const groups2: Array<IssueGroupModel> = [
          {
            id: 3,
            label: 'Roads',
            categories: [
              {
                id: 3,
                label: 'Damaged street',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        const groups3: Array<IssueGroupModel> = [];

        issueCategoryService.getCategories.and.returnValues(of(groups1), of(groups2), of(groups3));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        expect(tester.category.optionLabels).toEqual(['', 'Light broken', 'Other']);
        expect(tester.category).toHaveSelectedLabel('');

        await tester.category.selectLabel('Light broken');

        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates2);
        await tester.change();

        expect(tester.category.optionLabels).toEqual(['', 'Damaged street', 'Other']);
        expect(tester.category).toHaveSelectedLabel('');

        await tester.category.selectLabel('Other');
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        expect(tester.category).toBeNull();
        expect(tester.noCategoryError).not.toBeNull();
      });

      it('should change the validator and revalidate the description acknowledgement when the category changes', async () => {
        expect(tester.descriptionRequiredMarker).toBeNull();
        const groups: Array<IssueGroupModel> = [
          {
            id: 2,
            label: 'Lighting',
            categories: [
              {
                id: 2,
                label: 'Light broken',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        issueCategoryService.getCategories.and.returnValue(of(groups));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        const descriptionAcknowledgementCtrl =
          tester.componentInstance.issueForm.controls.descriptionAcknowledgement;
        expect(descriptionAcknowledgementCtrl.valid).toBeTrue();
        expect(tester.descriptionRequiredMarker).toBeNull();

        await tester.category.selectLabel('Other');

        expect(descriptionAcknowledgementCtrl.valid).toBeFalse();
        expect(tester.descriptionRequiredMarker).not.toBeNull();

        await tester.category.selectLabel('Light broken');

        expect(descriptionAcknowledgementCtrl.valid).toBeTrue();
        expect(tester.descriptionRequiredMarker).toBeNull();

        await tester.category.selectLabel('');

        expect(descriptionAcknowledgementCtrl.valid).toBe(true);
        expect(tester.descriptionRequiredMarker).toBeNull();
      });

      it('should change the validator and revalidate the description when the category changes', async () => {
        expect(tester.descriptionRequiredMarker).toBeNull();
        const groups: Array<IssueGroupModel> = [
          {
            id: 2,
            label: 'Lighting',
            categories: [
              {
                id: 2,
                label: 'Light broken',
                descriptionRequired: false
              }
            ]
          },
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        issueCategoryService.getCategories.and.returnValue(of(groups));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        await tester.descriptionAcknowledgementButton.click();
        const descriptionCtrl = tester.componentInstance.issueForm.controls.description;
        expect(descriptionCtrl.valid).toBeTrue();
        expect(tester.descriptionRequiredMarker).toBeNull();

        await tester.category.selectLabel('Other');

        expect(descriptionCtrl.valid).toBeFalse();
        expect(tester.descriptionRequiredMarker).not.toBeNull();

        await tester.category.selectLabel('Light broken');

        expect(descriptionCtrl.valid).toBeTrue();
        expect(tester.descriptionRequiredMarker).toBeNull();

        await tester.category.selectLabel('');

        expect(descriptionCtrl.valid).toBe(true);
        expect(tester.descriptionRequiredMarker).toBeNull();
      });

      it('should not submit the issue if invalid', async () => {
        const groups: Array<IssueGroupModel> = [
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        issueCategoryService.getCategories.and.returnValue(of(groups));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        await tester.submit.click();
        expect(issueService.update).not.toHaveBeenCalled();
        expect(tester.testElement).toContainText('La catégorie du problème est obligatoire');

        await tester.category.selectLabel('Other');
        expect(tester.testElement).not.toContainText('La catégorie du problème est obligatoire');
        expect(tester.testElement).toContainText('Votre accord est indispensable pour continuer');

        await tester.submit.click();
        expect(issueService.update).not.toHaveBeenCalled();

        await tester.descriptionAcknowledgementButton.click();
        expect(tester.description.disabled).toBeFalse();
        expect(tester.testElement).toContainText('La description du problème est obligatoire');

        await tester.submit.click();
        expect(issueService.update).not.toHaveBeenCalled();
      });

      it('should submit the issue if valid', async () => {
        const groups: Array<IssueGroupModel> = [
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        issueCategoryService.getCategories.and.returnValue(of(groups));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        issueService.update.and.returnValue(of(undefined as unknown as IssueModel));
        const assignedGovernment = { id: 1, logo: { id: 12 } };
        issueService.get.and.returnValue(of({ ...issue, assignedGovernment } as IssueModel));
        await tester.category.selectLabel('Other');

        expect(tester.descriptionAcknowledgementButton).toBeVisible();
        expect(tester.description.disabled).toBeTrue();

        await tester.descriptionAcknowledgementButton.click();
        expect(tester.descriptionAcknowledgementButton).not.toBeVisible();
        expect(tester.description.disabled).toBeFalse();

        await tester.description.fillWith('blabla');

        expect(tester.picturesComponent.accessToken()).toBe(issue.accessToken!);

        await tester.submit.click();
        const expectedCommand: IssueCommand = {
          coordinates,
          status: 'CREATED',
          categoryId: 1,
          description: 'blabla'
        };
        expect(issueService.update).toHaveBeenCalledWith(
          issue.id,
          issue.accessToken!,
          expectedCommand
        );
        expect(issueService.get).toHaveBeenCalledWith(issue.id);
        expect(tester.thankYou).not.toBeNull();
      });

      it('should submit anonymously the issue if valid', async () => {
        const groups: Array<IssueGroupModel> = [
          {
            id: 1,
            label: 'Other',
            categories: [
              {
                id: 1,
                label: 'Other',
                descriptionRequired: true
              }
            ]
          }
        ];

        issueCategoryService.getCategories.and.returnValue(of(groups));
        tester.coordinateSelectionComponent.coordinatesSelected.emit(coordinates);
        await tester.change();

        issueService.update.and.returnValue(of(undefined as unknown as IssueModel));
        const assignedGovernment = {
          id: 1,
          name: 'Rhône',
          logo: { id: 12 },
          url: 'https://rhone.fr'
        } as GovernmentModel;

        const createdIssue = { ...issue, assignedGovernment } as IssueModel;
        issueService.get.and.returnValue(of(createdIssue));

        await tester.category.selectLabel('Other');
        await tester.descriptionAcknowledgementButton.click();
        await tester.description.fillWith('blabla');

        expect(tester.picturesComponent.accessToken()).toBe(issue.accessToken!);

        await tester.submit.click();
        const expectedCommand: IssueCommand = {
          coordinates,
          status: 'CREATED',
          categoryId: 1,
          description: 'blabla'
        };
        expect(issueService.update).toHaveBeenCalledWith(
          issue.id,
          issue.accessToken!,
          expectedCommand
        );
        expect(issueService.get).toHaveBeenCalledWith(issue.id);

        // should display the thank-you screen
        expect(tester.thankYou).not.toBeNull();
        expect(tester.thankYou.issue()).toBe(createdIssue);
        expect(tester.thankYou.accessToken()).toBe(issue.accessToken!);
      });
    });
  });

  describe('edition of an existing issue', () => {
    beforeEach(() => {
      route.setParams({
        id: '42',
        'access-token': 'access-token'
      });

      const groups: Array<IssueGroupModel> = [
        {
          id: 1,
          label: 'Other',
          categories: [
            {
              id: 1,
              label: 'Other',
              descriptionRequired: true
            }
          ]
        }
      ];

      issueCategoryService.getCategories.and.returnValue(of(groups));
    });

    it('should load a draft issue without category or description', async () => {
      const issueSubject = new Subject<IssueModel>();
      issueService.get.and.returnValue(issueSubject);
      const issue: IssueModel = {
        id: 42,
        creationInstant: '2019-01-11T00:00:00Z',
        status: 'DRAFT',
        coordinates: {
          latitude: 1,
          longitude: 2
        },
        category: null,
        description: null,
        pictures: [],
        assignedGovernment: null,
        transitions: []
      };

      tester = new IssueEditionComponentTester();
      await tester.change();

      spyOn(tester.coordinateSelectionComponent, 'selectCoordinates').and.callFake(() =>
        tester.coordinateSelectionComponent.coordinatesSelected.emit(issue.coordinates)
      );

      issueSubject.next(issue);
      await tester.change();

      expect(tester.componentInstance.issue()).toEqual(issue);
      expect(tester.componentInstance.accessToken()).toBe('access-token');
      expect(tester.picturesComponent.accessToken()).toBe('access-token');
      expect(tester.category.optionLabels).toEqual(['', 'Other']);
      expect(tester.category).toHaveSelectedLabel('');
      expect(tester.descriptionAcknowledgementButton).toBeVisible();
      expect(tester.description.disabled).toBeTrue();
      expect(tester.description).toHaveValue('');
      expect(tester.coordinateSelectionComponent.selectCoordinates).toHaveBeenCalledWith(
        issue.coordinates
      );
      expect(issueCategoryService.getCategories).toHaveBeenCalledWith(issue.coordinates);
    });

    it('should load a draft issue with category or description', async () => {
      const issueSubject = new Subject<IssueModel>();
      issueService.get.and.returnValue(issueSubject);
      const issue: IssueModel = {
        id: 42,
        creationInstant: '2019-01-11T00:00:00Z',
        status: 'DRAFT',
        coordinates: {
          latitude: 1,
          longitude: 2
        },
        category: { id: 1 } as IssueCategoryModel,
        description: 'Oulala',
        pictures: [],
        assignedGovernment: null,
        transitions: []
      };

      tester = new IssueEditionComponentTester();
      await tester.change();

      spyOn(tester.coordinateSelectionComponent, 'selectCoordinates').and.callFake(() =>
        tester.coordinateSelectionComponent.coordinatesSelected.emit(issue.coordinates)
      );

      issueSubject.next(issue);
      await tester.change();

      expect(tester.category).toHaveSelectedLabel('Other');
      expect(tester.descriptionAcknowledgementButton).not.toBeVisible();
      expect(tester.description.disabled).toBeFalse();
      expect(tester.description).toHaveValue(issue.description!);
    });

    it('should navigate back to creation page when loading a non-draft issue', async () => {
      const issue: IssueModel = {
        id: 42,
        creationInstant: '2019-01-11T00:00:00Z',
        status: 'CREATED',
        coordinates: {
          latitude: 1,
          longitude: 2
        },
        category: { id: 1 } as IssueCategoryModel,
        description: 'Oulala',
        pictures: [],
        assignedGovernment: null,
        transitions: []
      };

      issueService.get.and.returnValue(of(issue));

      tester = new IssueEditionComponentTester();
      await tester.change();

      spyOn(tester.coordinateSelectionComponent, 'selectCoordinates');

      expect(tester.issueForm).toBeNull();
      expect(tester.coordinateSelectionComponent.selectCoordinates).not.toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['.', {}], {
        relativeTo: route,
        replaceUrl: true
      });
    });
  });
});
