import { ChangeDetectionStrategy, Component, inject, input, Signal, computed } from '@angular/core';
import { IssueModel } from '../../models/issue.model';
import { CurrentUserService } from '../../current-user.service';
import { SubscriptionService } from '../../account/subscriptions/subscription.service';
import { map, of, switchMap } from 'rxjs';
import * as icons from '../../icons';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgTemplateOutlet } from '@angular/common';
import { Router, RouterLink } from '@angular/router';
import { IssueService } from '../../issue.service';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';
import { toObservable, toSignal } from '@angular/core/rxjs-interop';

interface ViewModel {
  canSubscribe: boolean;
}

@Component({
  selector: 'dm-thank-you',
  imports: [FontAwesomeModule, RouterLink, NgTemplateOutlet, SubscriptionAfterSubmissionComponent],
  templateUrl: './thank-you.component.html',
  styleUrl: './thank-you.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ThankYouComponent {
  private readonly issueService = inject(IssueService);
  private readonly router = inject(Router);

  readonly issue = input.required<IssueModel>();
  readonly accessToken = input.required<string>();

  readonly isAuthenticated: boolean;
  readonly governmentLogoUrl = computed(() => {
    const issue = this.issue();
    return issue.assignedGovernment && issue.assignedGovernment.logo
      ? `/api/governments/${issue.assignedGovernment.id}/logo/${
          issue.assignedGovernment.logo.id
        }/bytes`
      : null;
  });

  readonly vm: Signal<ViewModel | undefined>;
  readonly icons = icons;

  constructor() {
    const currentUserService = inject(CurrentUserService);
    const subscriptionService = inject(SubscriptionService);
    this.isAuthenticated = currentUserService.isAuthenticated();
    this.vm = toSignal(
      this.isAuthenticated
        ? toObservable(this.issue).pipe(
            switchMap(issue =>
              subscriptionService.list().pipe(
                map(subscriptions => ({
                  canSubscribe: !subscriptions.some(
                    s => s.government.id === issue.assignedGovernment!.id
                  )
                }))
              )
            )
          )
        : of({ canSubscribe: false })
    );
  }

  /**
   * Store the anonymously created issue and navigate to authentication
   */
  authenticate() {
    this.issueService.storeAnonymousIssue(this.issue().id, this.accessToken());
    this.router.navigateByUrl('/authentication');
  }

  /**
   * Store the anonymously created issue and navigate to register
   */
  register() {
    this.issueService.storeAnonymousIssue(this.issue().id, this.accessToken());
    this.router.navigateByUrl('/registration');
  }

  subscriptionDone() {
    this.router.navigateByUrl('/');
  }
}
