import { TestBed } from '@angular/core/testing';

import { ThankYouComponent } from './thank-you.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { IssueModel } from '../../models/issue.model';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { CurrentUserService } from '../../current-user.service';
import { SubscriptionService } from '../../account/subscriptions/subscription.service';
import { IssueService } from '../../issue.service';
import { provideRouter, Router } from '@angular/router';
import { of } from 'rxjs';
import { SubscriptionModel } from '../../account/subscriptions/subscription.model';
import { SubscriptionAfterSubmissionComponent } from '../../subscription-after-submission/subscription-after-submission.component';
import { GovernmentDetailedModel } from '../../admin/models/government.model';

const loireGovernment: GovernmentDetailedModel = {
  id: 42,
  name: 'Loire',
  logo: {
    id: 76
  },
  url: 'https://loire.fr'
} as GovernmentDetailedModel;

@Component({
  imports: [ThankYouComponent],
  template: `<dm-thank-you [issue]="issue()" [accessToken]="accessToken" />`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly issue = signal<IssueModel>({
    id: 1000,
    assignedGovernment: loireGovernment
  } as IssueModel);
  accessToken = 'the-token';
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get title() {
    return this.element('h1');
  }

  get thankYou() {
    return this.element('#thank-you');
  }

  get logo() {
    return this.element('#logo')!;
  }

  get governmentLink() {
    return this.element('#government-link')!;
  }

  get governmentLogoLink() {
    return this.element('#government-logo-link')!;
  }

  get subscriptionComponent() {
    return this.component(SubscriptionAfterSubmissionComponent);
  }

  get authenticate() {
    return this.button('#authenticate')!;
  }

  get register() {
    return this.button('#register')!;
  }
}

describe('ThankYouComponent', () => {
  let tester: TestComponentTester;
  let currentUserService: jasmine.SpyObj<CurrentUserService>;
  let subscriptionService: jasmine.SpyObj<SubscriptionService>;
  let issueService: jasmine.SpyObj<IssueService>;
  let router: Router;

  beforeEach(() => {
    currentUserService = createMock(CurrentUserService);
    subscriptionService = createMock(SubscriptionService);
    issueService = createMock(IssueService);

    TestBed.configureTestingModule({
      providers: [
        { provide: CurrentUserService, useValue: currentUserService },
        { provide: SubscriptionService, useValue: subscriptionService },
        { provide: IssueService, useValue: issueService },
        provideRouter([])
      ]
    });

    router = TestBed.inject(Router);
    spyOn(router, 'navigateByUrl');
  });

  describe('authenticated', () => {
    beforeEach(() => {
      currentUserService.isAuthenticated.and.returnValue(true);
    });

    describe('without subscription', () => {
      beforeEach(() => {
        subscriptionService.list.and.returnValue(of([]));

        tester = new TestComponentTester();
      });

      it('should display thank you', async () => {
        await tester.stable();
        expect(tester.title).toContainText('Merci\xa0!');
        expect(tester.thankYou).toContainText(
          "La collectivité Loire vous remercie pour cette remontée d'information."
        );
        expect(tester.logo.attr('src')).toBe('/api/governments/42/logo/76/bytes');
        expect(tester.logo.attr('alt')).toBe('Loire');
        expect(tester.governmentLink.attr('href')).toBe('https://loire.fr');
        expect(tester.governmentLogoLink.attr('href')).toBe('https://loire.fr');

        expect(tester.authenticate).toBeNull();
        expect(tester.register).toBeNull();
      });

      it('should display the thank you screen without links if the government has no URL', async () => {
        tester.componentInstance.issue.update(issue => ({
          ...issue,
          assignedGovernment: {
            ...loireGovernment,
            url: null
          } as GovernmentDetailedModel
        }));
        await tester.stable();

        expect(tester.title).toContainText('Merci\xa0!');
        expect(tester.thankYou).toContainText(
          "La collectivité Loire vous remercie pour cette remontée d'information."
        );
        expect(tester.logo.attr('src')).toBe('/api/governments/42/logo/76/bytes');
        expect(tester.logo.attr('alt')).toBe('Loire');
        expect(tester.governmentLink).toBeNull();
        expect(tester.governmentLogoLink).toBeNull();
      });

      it('should propose to subscribe', async () => {
        await tester.stable();
        expect(tester.subscriptionComponent).not.toBeNull();

        tester.subscriptionComponent.done.emit();
        await tester.stable();

        expect(router.navigateByUrl).toHaveBeenCalledWith('/');
      });
    });

    describe('with subscription', () => {
      beforeEach(async () => {
        subscriptionService.list.and.returnValue(
          of([
            {
              government: {
                id: 42
              }
            }
          ] as Array<SubscriptionModel>)
        );

        tester = new TestComponentTester();
        await tester.stable();
      });

      it('should not propose to subscribe', () => {
        expect(tester.thankYou).toContainText(
          "La collectivité Loire vous remercie pour cette remontée d'information."
        );
        expect(tester.subscriptionComponent).toBeNull();
      });
    });
  });

  describe('not authenticated', () => {
    beforeEach(async () => {
      currentUserService.isAuthenticated.and.returnValue(false);
      tester = new TestComponentTester();
      await tester.stable();
    });

    it('should display thank you and suggest to register and authenticate', () => {
      expect(tester.title).toContainText('Merci\xa0!');
      expect(tester.thankYou).toContainText(
        "La collectivité Loire vous remercie pour cette remontée d'information."
      );
      expect(tester.logo.attr('src')).toBe('/api/governments/42/logo/76/bytes');
      expect(tester.logo.attr('alt')).toBe('Loire');
      expect(tester.governmentLink.attr('href')).toBe('https://loire.fr');
      expect(tester.governmentLogoLink.attr('href')).toBe('https://loire.fr');

      expect(tester.authenticate).not.toBeNull();
      expect(tester.register).not.toBeNull();
    });

    it('should authenticate', async () => {
      await tester.authenticate.click();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/authentication');
      expect(issueService.storeAnonymousIssue).toHaveBeenCalledWith(
        tester.componentInstance.issue().id,
        tester.componentInstance.accessToken
      );
    });

    it('should register', async () => {
      await tester.register.click();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/registration');
      expect(issueService.storeAnonymousIssue).toHaveBeenCalledWith(
        tester.componentInstance.issue().id,
        tester.componentInstance.accessToken
      );
    });
  });
});
