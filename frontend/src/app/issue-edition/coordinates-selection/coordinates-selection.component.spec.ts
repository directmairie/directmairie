import { TestBed } from '@angular/core/testing';

import { CoordinatesSelectionComponent } from './coordinates-selection.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { BoundingBox, CoordinatesModel } from '../../models/issue.model';
import {
  ComponentTester,
  createMock,
  provideAutomaticChangeDetection,
  TestHtmlElement
} from 'ngx-speculoos';
import { CoordinatesSelectionMapComponent } from '../coordinates-selection-map/coordinates-selection-map.component';
import { GeolocationService, LocationSearchResult } from '../../geolocation.service';
import { of, Subject } from 'rxjs';

@Component({
  template: ` <dm-coordinates-selection (coordinatesSelected)="lastEvent.set($event)" />`,
  imports: [CoordinatesSelectionComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly lastEvent = signal<CoordinatesModel | null>(null);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get mapComponent() {
    return this.component(CoordinatesSelectionMapComponent);
  }

  get myPosition() {
    return this.button('#myPosition')!;
  }

  get myPositionIcon() {
    return this.element('#myPositionIcon');
  }

  get geolocatingSpinner() {
    return this.element('#geolocatingSpinner');
  }

  get location() {
    return this.input('#location')!;
  }

  get search() {
    return this.button('#search')!;
  }

  get searching() {
    return this.element('#searching');
  }

  get searchResults() {
    return this.element('#searchResults');
  }

  get searchResultLinks() {
    return this.elements('.searchResult') as Array<TestHtmlElement<HTMLLinkElement>>;
  }
}

describe('CoordinatesSelectionComponent', () => {
  let tester: TestComponentTester;
  let geolocationService: jasmine.SpyObj<GeolocationService>;

  beforeEach(async () => {
    geolocationService = createMock(GeolocationService);
    TestBed.configureTestingModule({
      providers: [
        provideAutomaticChangeDetection(),
        { provide: GeolocationService, useValue: geolocationService }
      ]
    });

    tester = new TestComponentTester();
    await tester.change();
  });

  it('should display a map, an empty form, no marker location and no results initially', () => {
    expect(tester.mapComponent).not.toBeNull();
    expect(tester.location).toHaveValue('');
    expect(tester.search.disabled).toBe(true);
    expect(tester.searching).toBeNull();
    expect(tester.searchResults).toBeNull();
    expect(tester.mapComponent.markerLocation()).toBeNull();
  });

  it('should locate me when clicking my position', async () => {
    const coordinates = { latitude: 0, longitude: 1 };
    const currentCoordinatesSubject = new Subject<CoordinatesModel>();
    geolocationService.currentCoordinates.and.returnValue(currentCoordinatesSubject);
    geolocationService.findLocationLabel.and.returnValue(of('my location road'));
    expect(tester.myPositionIcon).not.toBeNull();
    expect(tester.geolocatingSpinner).toBeNull();

    await tester.myPosition.click();

    expect(geolocationService.currentCoordinates).toHaveBeenCalled();
    expect(tester.myPositionIcon).toBeNull();
    expect(tester.geolocatingSpinner).not.toBeNull();

    spyOn(tester.mapComponent, 'moveTo');
    spyOn(tester.mapComponent, 'setMarker').and.callThrough();

    currentCoordinatesSubject.next(coordinates);
    currentCoordinatesSubject.complete();
    await tester.change();

    expect(tester.mapComponent.moveTo).toHaveBeenCalledWith(coordinates);
    expect(tester.mapComponent.setMarker).toHaveBeenCalledWith(coordinates);
    expect(tester.mapComponent.markerLocation()).toBe('my location road');

    expect(tester.myPositionIcon).not.toBeNull();
    expect(tester.geolocatingSpinner).toBeNull();
  });

  it('should display no result if search finds no result', async () => {
    const searchSubject = new Subject<Array<LocationSearchResult>>();
    geolocationService.search.and.returnValue(searchSubject);

    await tester.location.fillWith('blabla');
    expect(tester.search.disabled).toBe(false);

    await tester.search.click();

    expect(geolocationService.search).toHaveBeenCalledWith('blabla');
    expect(tester.searching).not.toBeNull();
    expect(tester.searchResults).toBeNull();

    searchSubject.next([]);
    searchSubject.complete();

    await tester.change();

    expect(tester.searching).toBeNull();
    expect(tester.searchResults).toContainText('Aucun résultat');
    expect(tester.searchResultLinks.length).toBe(0);
  });

  it('should mark unique search result', async () => {
    const uniqueSearchResult: LocationSearchResult = {
      coordinates: { latitude: 1, longitude: 2 },
      label: 'Lyon',
      boundingBox: {
        minLatitude: 0,
        minLongitude: 1,
        maxLatitude: 2,
        maxLongitude: 3
      }
    };

    const searchSubject = new Subject<Array<LocationSearchResult>>();
    geolocationService.search.and.returnValue(searchSubject);

    await tester.location.fillWith('lyon');
    expect(tester.search.disabled).toBe(false);

    await tester.search.click();

    expect(geolocationService.search).toHaveBeenCalledWith('lyon');
    expect(tester.searching).not.toBeNull();
    expect(tester.searchResults).toBeNull();

    spyOn(tester.mapComponent, 'moveTo');
    spyOn(tester.mapComponent, 'setMarker').and.callThrough();
    geolocationService.findLocationLabel.and.returnValue(of('unique result road'));

    searchSubject.next([uniqueSearchResult]);
    searchSubject.complete();

    await tester.change();

    expect(tester.searching).toBeNull();
    expect(tester.searchResults).not.toBeNull();
    expect(tester.searchResultLinks.length).toBe(1);
    expect(tester.searchResultLinks[0]).toContainText('Lyon');
    expect(tester.mapComponent.markerLocation()).toBe('unique result road');

    expect(tester.mapComponent.moveTo).toHaveBeenCalledWith(
      uniqueSearchResult.coordinates,
      uniqueSearchResult.boundingBox
    );
    expect(tester.mapComponent.setMarker).toHaveBeenCalledWith(uniqueSearchResult.coordinates);
  });

  it('should not mark if multiple results, but mark when a search result is clicked', async () => {
    const boundingBox: BoundingBox = {
      minLatitude: 0,
      minLongitude: 1,
      maxLatitude: 2,
      maxLongitude: 3
    };

    const searchResults: Array<LocationSearchResult> = [
      {
        coordinates: { latitude: 0, longitude: 1 },
        label: 'Lyon1',
        boundingBox
      },
      {
        coordinates: { latitude: 1, longitude: 2 },
        label: 'Lyon2',
        boundingBox
      }
    ];

    const searchSubject = new Subject<Array<LocationSearchResult>>();
    geolocationService.search.and.returnValue(searchSubject);

    await tester.location.fillWith('lyon');
    expect(tester.search.disabled).toBe(false);

    await tester.search.click();

    expect(geolocationService.search).toHaveBeenCalledWith('lyon');
    expect(tester.searching).not.toBeNull();
    expect(tester.searchResults).toBeNull();

    spyOn(tester.mapComponent, 'moveTo');
    spyOn(tester.mapComponent, 'setMarker').and.callThrough();
    geolocationService.findLocationLabel.and.returnValue(of('second result road'));

    searchSubject.next(searchResults);
    searchSubject.complete();

    await tester.change();

    expect(tester.searching).toBeNull();
    expect(tester.searchResults).not.toBeNull();
    expect(tester.searchResultLinks.length).toBe(2);
    expect(tester.searchResultLinks[0]).toContainText('Lyon1');
    expect(tester.searchResultLinks[1]).toContainText('Lyon2');

    expect(tester.mapComponent.moveTo).not.toHaveBeenCalled();
    expect(tester.mapComponent.setMarker).not.toHaveBeenCalled();
    expect(tester.mapComponent.markerLocation()).toBeNull();

    await tester.searchResultLinks[1].click();
    expect(tester.mapComponent.moveTo).toHaveBeenCalledWith(
      searchResults[1].coordinates,
      searchResults[1].boundingBox
    );
    expect(tester.mapComponent.setMarker).toHaveBeenCalledWith(searchResults[1].coordinates);
    expect(tester.mapComponent.markerLocation()).toBe('second result road');
  });

  it('should display marker location and emit when map is marked', async () => {
    geolocationService.findLocationLabel.and.returnValue(of('marked road'));
    const coordinates = { latitude: 0, longitude: 1 };
    tester.mapComponent.setMarker(coordinates);
    await tester.change();

    expect(tester.componentInstance.lastEvent()).toBe(coordinates);
    expect(tester.mapComponent.markerLocation()).toBe('marked road');
  });
});
