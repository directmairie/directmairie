import {
  ChangeDetectionStrategy,
  Component,
  DestroyRef,
  ElementRef,
  inject,
  output,
  signal,
  Signal,
  viewChild
} from '@angular/core';
import { BoundingBox, CoordinatesModel } from '../../models/issue.model';
import * as icons from '../../icons';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { GeolocationService, LocationSearchResult } from '../../geolocation.service';
import { catchError, finalize, of, switchMap } from 'rxjs';
import { CoordinatesSelectionMapComponent } from '../coordinates-selection-map/coordinates-selection-map.component';
import { InvalidFormControlDirective } from '../../shared/invalid-form-control.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { outputToObservable, takeUntilDestroyed, toSignal } from '@angular/core/rxjs-interop';

/**
 * The sub-component, used inside the issue edition component, to display a search field, search results and a map
 * allowing to select the coordinates of an issue.
 */
@Component({
  selector: 'dm-coordinates-selection',
  templateUrl: './coordinates-selection.component.html',
  styleUrl: './coordinates-selection.component.scss',
  imports: [
    FontAwesomeModule,
    ReactiveFormsModule,
    InvalidFormControlDirective,
    CoordinatesSelectionMapComponent
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoordinatesSelectionComponent {
  private readonly geolocationService = inject(GeolocationService);
  private readonly destroyRef = inject(DestroyRef);

  readonly icons = icons;

  readonly coordinatesSelected = output<CoordinatesModel>();

  readonly geolocating = signal(false);
  readonly searching = signal(false);
  readonly searchForm = inject(NonNullableFormBuilder).group({
    location: ['', Validators.required]
  });
  readonly searchResults = signal<Array<LocationSearchResult> | undefined>(undefined);

  readonly markerLocation: Signal<string | null>;

  readonly map = viewChild.required(CoordinatesSelectionMapComponent);
  readonly mapElementRef = viewChild.required(CoordinatesSelectionMapComponent, {
    read: ElementRef
  });

  constructor() {
    this.markerLocation = toSignal(
      outputToObservable(this.coordinatesSelected).pipe(
        switchMap(coordinates =>
          this.geolocationService.findLocationLabel(coordinates).pipe(catchError(() => of(null)))
        )
      ),
      { initialValue: null }
    );
  }

  search() {
    this.searching.set(true);
    this.searchResults.set(undefined);
    this.geolocationService
      .search(this.searchForm.value.location!)
      .pipe(
        finalize(() => this.searching.set(false)),
        takeUntilDestroyed(this.destroyRef)
      )
      .subscribe(results => {
        this.searchResults.set(results);
        if (results.length === 1) {
          this.selectCoordinates(results[0].coordinates, results[0].boundingBox);
        }
      });
  }

  markMyPosition() {
    this.geolocating.set(true);
    this.geolocationService
      .currentCoordinates()
      .pipe(
        finalize(() => this.geolocating.set(false)),
        takeUntilDestroyed(this.destroyRef)
      )
      .subscribe(coordinates => {
        this.map().setMarker(coordinates);
        this.map().moveTo(coordinates);
        this.mapElementRef().nativeElement.scrollIntoView({ behavior: 'smooth' });
      });
  }

  selectCoordinates(
    coordinates: CoordinatesModel,
    boundingBox: BoundingBox | null = null,
    event?: Event
  ) {
    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }
    this.map().setMarker(coordinates);
    this.map().moveTo(coordinates, boundingBox);
    this.mapElementRef().nativeElement.scrollIntoView({ behavior: 'smooth' });
  }
}
