import { ChangeDetectionStrategy, Component, effect, input, output, signal } from '@angular/core';
import { BoundingBox, CoordinatesModel } from '../../models/issue.model';
import {
  latLng,
  LatLng,
  latLngBounds,
  LatLngBounds,
  LeafletMouseEvent,
  MapOptions,
  marker,
  Marker,
  Map as LMap,
  Control
} from 'leaflet';
import { defaultMapOptions, defaultMarkerOptions } from '../../map';
import { LeafletModule } from '@bluehalo/ngx-leaflet';

function toLatLngBounds(boundingBox: BoundingBox): LatLngBounds {
  return latLngBounds([
    [boundingBox.minLatitude, boundingBox.minLongitude],
    [boundingBox.maxLatitude, boundingBox.maxLongitude]
  ]);
}

class MarkerLocationControl extends Control {
  private div: HTMLDivElement;

  constructor() {
    super({ position: 'topright' });
    this.div = document.createElement('div');
    this.div.classList.add('marker-location');
    this.div.addEventListener('click', event => event.stopPropagation());
  }

  onAdd(): HTMLElement {
    return this.div;
  }

  setLocation(text: string | null) {
    this.div.innerText = text ?? '';
  }

  getLocation() {
    return this.div.innerText;
  }
}

/**
 * The sub-component, used inside the coordinates selection component, which is itself used inside the issue edition
 * component, to display a map allowing to display/select the coordinates of an issue.
 */
@Component({
  selector: 'dm-coordinates-selection-map',
  templateUrl: './coordinates-selection-map.component.html',
  styleUrl: './coordinates-selection-map.component.scss',
  exportAs: 'coordinatesSelectionMap',
  imports: [LeafletModule],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CoordinatesSelectionMapComponent {
  readonly mark = output<CoordinatesModel>();

  readonly mapCenter = signal(new LatLng(46.71109, 1.7191036));
  readonly mapZoom = signal<number | null>(5);

  readonly mapOptions: MapOptions = {
    ...defaultMapOptions(),
    zoom: this.mapZoom()!,
    center: this.mapCenter()!
  };
  readonly mapMarkers = signal<Array<Marker>>([]);
  readonly mapFitBounds = signal<LatLngBounds | null>(null);

  private readonly markerLocationControl = new MarkerLocationControl();
  private readonly map = signal<LMap | null>(null);

  readonly markerLocation = input.required<string | null>();

  constructor() {
    effect(() => {
      const location = this.markerLocation();
      this.markerLocationControl.setLocation(location);
      const map = this.map();
      if (map) {
        if (location) {
          this.markerLocationControl.addTo(map);
        } else {
          this.markerLocationControl.remove();
        }
      }
    });
  }

  setMarker(coordinates: CoordinatesModel) {
    this.mapMarkers.set([
      marker(
        [coordinates.latitude, coordinates.longitude],
        defaultMarkerOptions(`${coordinates.latitude} - ${coordinates.longitude}`)
      )
    ]);
    this.mark.emit(coordinates);
  }

  moveTo(coordinates: CoordinatesModel, boundingBox: BoundingBox | null = null) {
    this.mapCenter.set(latLng(coordinates.latitude, coordinates.longitude));
    this.mapZoom.set(boundingBox ? null : 17);
    this.mapFitBounds.set(boundingBox ? toLatLngBounds(boundingBox) : null);
  }

  mapClicked(event: LeafletMouseEvent) {
    this.setMarker({
      latitude: event.latlng.lat,
      longitude: event.latlng.lng
    });
  }

  onMapReady(map: LMap) {
    this.map.set(map);
  }
}
