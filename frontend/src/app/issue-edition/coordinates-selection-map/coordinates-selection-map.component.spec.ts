import { TestBed } from '@angular/core/testing';

import { CoordinatesSelectionMapComponent } from './coordinates-selection-map.component';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { BoundingBox, CoordinatesModel } from '../../models/issue.model';
import { ComponentTester, provideAutomaticChangeDetection } from 'ngx-speculoos';
import { latLng, LeafletMouseEvent } from 'leaflet';

@Component({
  template: ` <dm-coordinates-selection-map
    (mark)="lastEvent.set($event)"
    [markerLocation]="markerLocation()"
  />`,
  imports: [CoordinatesSelectionMapComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly lastEvent = signal<CoordinatesModel | null>(null);
  readonly markerLocation = signal<string | null>(null);
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get mapComponent() {
    return this.component(CoordinatesSelectionMapComponent);
  }

  get markers(): HTMLCollectionOf<Element> {
    return document.getElementsByClassName('leaflet-marker-icon');
  }

  get markerLocation(): HTMLElement {
    return (document.getElementsByClassName('marker-location')[0] as HTMLElement) || null;
  }
}

describe('CoordinatesSelectionMapComponent', () => {
  let tester: TestComponentTester;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      providers: [provideAutomaticChangeDetection()]
    });

    tester = new TestComponentTester();
    await tester.change();
  });

  it('should display France and have no marker initially', () => {
    expect(tester.mapComponent.mapZoom()).toBe(5);
    expect(tester.mapComponent.mapCenter()).not.toBeNull();
    expect(tester.mapComponent.mapOptions.center).toBe(tester.mapComponent.mapCenter());
    expect(tester.mapComponent.mapOptions.zoom).toBe(tester.mapComponent.mapZoom()!);
    expect(tester.mapComponent.mapMarkers().length).toBe(0);
    expect(tester.markers.length).toBe(0);
  });

  it('should move and display marker and emit when methods called from outside without bounding box', async () => {
    const coordinates: CoordinatesModel = { latitude: 0, longitude: 1 };

    tester.mapComponent.moveTo(coordinates);
    await tester.change();

    expect(tester.componentInstance.lastEvent()).toBeNull();
    expect(tester.mapComponent.mapZoom()).toBe(17);
    expect(tester.mapComponent.mapFitBounds()).toBeNull();
    expect(tester.mapComponent.mapCenter().lat).toBe(coordinates.latitude);
    expect(tester.mapComponent.mapCenter().lng).toBe(coordinates.longitude);
    expect(tester.markers.length).toBe(0);
    expect(tester.mapComponent.mapMarkers().length).toBe(0);

    tester.mapComponent.setMarker(coordinates);
    await tester.change();

    // I have no idea why this doesn't pass, even with a timeout, async, whenStable etc.
    expect(tester.markers.length).toBe(1);
    expect(tester.mapComponent.mapMarkers().length).toBe(1);
    expect(tester.mapComponent.mapMarkers()[0].getLatLng().lat).toBe(coordinates.latitude);
    expect(tester.mapComponent.mapMarkers()[0].getLatLng().lng).toBe(coordinates.longitude);
    expect(tester.componentInstance.lastEvent()).toBe(coordinates);
  });

  it('should move with bounding box', async () => {
    const coordinates: CoordinatesModel = { latitude: 1, longitude: 2 };
    const boundingBox: BoundingBox = {
      minLatitude: 0,
      minLongitude: 1,
      maxLatitude: 2,
      maxLongitude: 3
    };
    tester.mapComponent.moveTo(coordinates, boundingBox);
    await tester.change();

    expect(tester.componentInstance.lastEvent()).toBeNull();
    expect(tester.mapComponent.mapZoom()).toBeNull();
    expect(tester.mapComponent.mapCenter().lat).toBe(coordinates.latitude);
    expect(tester.mapComponent.mapCenter().lng).toBe(coordinates.longitude);
    expect(tester.mapComponent.mapFitBounds()!.getSouthWest().lat).toBe(boundingBox.minLatitude);
    expect(tester.mapComponent.mapFitBounds()!.getSouthWest().lng).toBe(boundingBox.minLongitude);
    expect(tester.mapComponent.mapFitBounds()!.getNorthEast().lat).toBe(boundingBox.maxLatitude);
    expect(tester.mapComponent.mapFitBounds()!.getNorthEast().lng).toBe(boundingBox.maxLongitude);
  });

  it('should move marker and emit on click', async () => {
    tester.mapComponent.mapClicked({
      latlng: latLng(0, 1)
    } as LeafletMouseEvent);
    await tester.change();

    expect(tester.mapComponent.mapMarkers().length).toBe(1);
    expect(tester.mapComponent.mapMarkers()[0].getLatLng().lat).toBe(0);
    expect(tester.mapComponent.mapMarkers()[0].getLatLng().lng).toBe(1);
    expect(tester.componentInstance.lastEvent()).toEqual({ latitude: 0, longitude: 1 });
  });

  it('should add and remove the marker location', async () => {
    await tester.change();

    expect(tester.markerLocation).toBeNull();

    tester.componentInstance.markerLocation.set('hello world');
    await tester.change();

    expect(tester.markerLocation.innerText).toBe('hello world');

    tester.componentInstance.markerLocation.set(null);
    await tester.change();

    expect(tester.markerLocation).toBeNull();
  });
});
