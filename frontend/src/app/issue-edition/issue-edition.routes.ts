import { IssueEditionComponent } from './issue-edition.component';
import { Routes } from '@angular/router';

export const issueEditionRoutes: Routes = [
  {
    path: '',
    component: IssueEditionComponent
  }
];
