import { ChangeDetectionStrategy, Component, inject, signal, viewChild } from '@angular/core';
import { NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as icons from '../icons';

import { IssueService } from '../issue.service';
import { CoordinatesModel, IssueModel, IssueWithAccessTokenModel } from '../models/issue.model';
import { IssueCategoryService } from '../issue-category.service';
import { catchError, EMPTY, Observable, of, Subject, switchMap, tap } from 'rxjs';
import { IssueCategoryModel, IssueGroupModel } from '../models/issue-category.model';
import { CoordinatesSelectionComponent } from './coordinates-selection/coordinates-selection.component';
import { PicturesComponent } from './pictures/pictures.component';
import { ValidationErrorDirective, ValidationErrorsComponent } from 'ngx-valdemort';
import { InvalidFormControlDirective } from '../shared/invalid-form-control.directive';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { RequiredComponent } from '../shared/required/required.component';
import { RequiredFieldsMessageComponent } from '../shared/required-fields-message/required-fields-message.component';

import { NgbCollapse } from '@ng-bootstrap/ng-bootstrap';
import { ThankYouComponent } from './thank-you/thank-you.component';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';

const accessTokenParam = 'access-token';

/**
 * The component allowing to enter and submit an issue into the application. It's a complex component delegating its
 * task to several other sub-components.
 */
@Component({
  selector: 'dm-issue',
  templateUrl: './issue-edition.component.html',
  styleUrl: './issue-edition.component.scss',
  imports: [
    CoordinatesSelectionComponent,
    ReactiveFormsModule,
    RequiredFieldsMessageComponent,
    RequiredComponent,
    FontAwesomeModule,
    InvalidFormControlDirective,
    ValidationErrorsComponent,
    PicturesComponent,
    ValidationErrorDirective,
    NgbCollapse,
    ThankYouComponent
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class IssueEditionComponent {
  private readonly issueService = inject(IssueService);
  private readonly router = inject(Router);
  private readonly route = inject(ActivatedRoute);

  readonly mode = signal<'creation' | 'success'>('creation');

  readonly icons = icons;

  readonly issueForm = inject(NonNullableFormBuilder).group({
    categoryId: [null as number | null, Validators.required],
    descriptionAcknowledgement: [false as boolean],
    description: { value: '' as string | null, disabled: true }
  });

  readonly issue = signal<IssueModel | null>(null);
  private readonly coordinates$ = new Subject<CoordinatesModel>();

  readonly issueGroups = signal<Array<IssueGroupModel> | null>(null);
  readonly noCategory = signal(false);
  readonly loadingCategories = signal(false);

  readonly descriptionRequired = signal(false);

  readonly coordinatesSelectionComponent = viewChild(CoordinatesSelectionComponent);

  readonly accessToken = signal<string | undefined>(undefined);

  constructor() {
    this.issueForm.controls.descriptionAcknowledgement.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe(acknowledged => {
        if (acknowledged) {
          this.issueForm.controls.description.enable();
        } else {
          this.issueForm.controls.description.disable();
        }
      });

    const issueCategoryService = inject(IssueCategoryService);
    this.coordinates$
      .pipe(
        tap(() => this.loadingCategories.set(true)),
        switchMap(coordinates =>
          this.createDraftOrUpdateExistingIssue(coordinates).pipe(catchError(() => EMPTY))
        ),
        switchMap(issue =>
          issueCategoryService.getCategories(issue.coordinates).pipe(catchError(() => EMPTY))
        ),
        tap(() => this.loadingCategories.set(false)),
        takeUntilDestroyed()
      )
      .subscribe(issueGroups => {
        this.issueGroups.set(issueGroups);
        this.noCategory.set(issueGroups.length === 0);
        const formValue = this.issueForm.value;
        if (
          !issueGroups.some(group =>
            group.categories.some(category => category.id === formValue.categoryId)
          )
        ) {
          this.issueForm.controls.categoryId.setValue(null);
        }
      });

    this.issueForm.controls.categoryId.valueChanges
      .pipe(takeUntilDestroyed())
      .subscribe((categoryId: number | null) => {
        const selectedCategory = this.findCategory(categoryId!);
        const descriptionCtrl = this.issueForm.controls.description;
        const descriptionAcknowledgementCtrl = this.issueForm.controls.descriptionAcknowledgement;
        this.descriptionRequired.set(
          selectedCategory !== null && selectedCategory.descriptionRequired
        );
        descriptionCtrl.setValidators(this.descriptionRequired() ? Validators.required : null);
        descriptionCtrl.updateValueAndValidity();
        descriptionAcknowledgementCtrl.setValidators(
          this.descriptionRequired() ? Validators.requiredTrue : null
        );
        descriptionAcknowledgementCtrl.updateValueAndValidity();
      });

    const issueIdAsString = this.route.snapshot.paramMap.get('id');
    if (issueIdAsString) {
      const issueId = +issueIdAsString;
      this.accessToken.set(this.route.snapshot.paramMap.get(accessTokenParam) ?? undefined);
      this.issueService
        .get(issueId)
        .pipe(takeUntilDestroyed())
        .subscribe(issue => {
          if (issue.status === 'DRAFT') {
            this.issue.set(issue);
            if (issue.status === 'DRAFT') {
              this.coordinatesSelectionComponent()?.selectCoordinates(issue.coordinates);
              this.issueForm.setValue({
                categoryId: issue.category ? issue.category.id : null,
                descriptionAcknowledgement:
                  issue.description !== null && issue.description.length > 0,
                description: issue.description
              });
            }
          } else {
            this.router.navigate(['.', {}], { relativeTo: this.route, replaceUrl: true });
          }
        });
    }
  }

  private createDraftOrUpdateExistingIssue(coordinates: CoordinatesModel): Observable<IssueModel> {
    const existingIssue = this.issue();
    if (existingIssue) {
      return of({ ...existingIssue, coordinates }).pipe(tap(issue => this.issue.set(issue)));
    } else {
      return this.issueService
        .create({
          coordinates,
          categoryId: null,
          description: null,
          status: 'DRAFT'
        })
        .pipe(
          tap((issue: IssueWithAccessTokenModel) => {
            this.issue.set(issue);
            const routeParams: Record<string, string | number> = { id: issue.id };
            if (issue.accessToken) {
              routeParams[accessTokenParam] = issue.accessToken;
            }
            this.accessToken.set(issue.accessToken);
            this.router.navigate([routeParams], { replaceUrl: true, relativeTo: this.route });
          })
        );
    }
  }

  submitIssue() {
    if (this.issueForm.invalid) {
      return;
    }

    const issueFormValue = this.issueForm.value;
    const issue = this.issue()!;
    const accessToken = this.accessToken()!;

    this.issueService
      .update(issue.id, accessToken, {
        coordinates: issue.coordinates,
        categoryId: issueFormValue.categoryId!,
        description: issueFormValue.description!,
        status: 'CREATED'
      })
      .pipe(
        // we fetch the updated issue right away to know the assigned government
        switchMap(() => this.issueService.get(issue.id))
      )
      .subscribe(issue => {
        this.mode.set('success');
        this.issue.set(issue);
      });
  }

  coordinatesSelected(coordinates: CoordinatesModel) {
    this.coordinates$.next(coordinates);
  }

  private findCategory(categoryId: number): IssueCategoryModel | null {
    for (const group of this.issueGroups() || []) {
      for (const category of group.categories) {
        if (category.id === categoryId) {
          return category;
        }
      }
    }
    return null;
  }

  acknowledgeDescription() {
    this.issueForm.controls.descriptionAcknowledgement.setValue(true);
  }

  get descriptionAcknowledged() {
    return this.issueForm.controls.descriptionAcknowledgement.value;
  }
}
