import { TestBed } from '@angular/core/testing';

import { Picture, PicturesComponent } from './pictures.component';
import { ComponentTester, createMock } from 'ngx-speculoos';
import { NgbProgressbar } from '@ng-bootstrap/ng-bootstrap';
import { IssueService } from '../../issue.service';
import { of, Subject } from 'rxjs';
import { IssueModel, PictureModel } from '../../models/issue.model';
import { ChangeDetectionStrategy, Component, signal } from '@angular/core';

@Component({
  template: `<dm-pictures [accessToken]="accessToken()" [issue]="issue()" />`,
  imports: [PicturesComponent],
  changeDetection: ChangeDetectionStrategy.OnPush
})
class TestComponent {
  readonly accessToken = signal('access-token');
  readonly issue = signal<IssueModel>({
    id: 1,
    creationInstant: '2024-10-01T00:00:00.000Z',
    status: 'DRAFT',
    assignedGovernment: null,
    category: null,
    coordinates: { latitude: 10, longitude: 10 },
    description: null,
    transitions: [],
    pictures: []
  });
}

class TestComponentTester extends ComponentTester<TestComponent> {
  constructor() {
    super(TestComponent);
  }

  get pictures() {
    return this.elements('.picture');
  }

  pictureImage(index: number) {
    if (index > this.pictures.length) {
      throw new Error('Index out of bound');
    }
    return this.pictures[index].element('img')!;
  }

  pictureDeleteButton(index: number) {
    if (index > this.pictures.length) {
      throw new Error('Index out of bound');
    }
    return this.pictures[index].button('.deleteButton')!;
  }

  get uploadButton() {
    return this.button('#uploadButton')!;
  }

  get progressBar() {
    return this.component(NgbProgressbar);
  }

  get picturesComponent() {
    return this.component(PicturesComponent);
  }

  get lastPicture(): Picture {
    return this.picturesComponent.pictures()[this.picturesComponent.pictures().length - 1];
  }
}

describe('PicturesComponent', () => {
  let tester: TestComponentTester;
  let issueService: jasmine.SpyObj<IssueService>;

  beforeEach(() => {
    issueService = createMock(IssueService);
    TestBed.configureTestingModule({
      providers: [{ provide: IssueService, useValue: issueService }]
    });

    tester = new TestComponentTester();
  });

  it('should only display a placeholder picture if none is present', async () => {
    await tester.stable();

    expect(tester.pictures.length).toBe(1);
    expect(tester.lastPicture.status()).toBe('PLACEHOLDER');
    expect(tester.pictureDeleteButton(0)).toBeNull();
  });

  it('should display one picture per picture found plus one placeholder picture', async () => {
    const pictureModels: Array<PictureModel> = [
      { id: 42, creationInstant: '2019-01-09T12:00:00Z' },
      { id: 43, creationInstant: '2019-01-10T12:00:00Z' }
    ];
    tester.componentInstance.issue.update(issue => ({ ...issue, pictures: pictureModels }));
    await tester.stable();

    expect(tester.pictures.length).toBe(3);
    expect(tester.pictureImage(0).attr('src')).toBe('/api/issues/1/pictures/42/bytes');
    expect(tester.lastPicture.status()).toBe('PLACEHOLDER');
  });

  it('should delete picture', async () => {
    const beforeDeletionPictureModels: Array<PictureModel> = [
      { id: 42, creationInstant: '2019-01-09T12:00:00Z' },
      { id: 43, creationInstant: '2019-01-10T12:00:00Z' }
    ];
    const afterDeletionPictureModels: Array<PictureModel> = [
      { id: 43, creationInstant: '2019-01-10T12:00:00Z' }
    ];

    tester.componentInstance.issue.update(issue => ({
      ...issue,
      pictures: beforeDeletionPictureModels
    }));
    await tester.stable();

    const deletionSubject = new Subject<void>();
    issueService.deletePicture.and.returnValue(deletionSubject);

    expect(tester.pictureDeleteButton(0).element('fa-icon')).not.toBeNull();
    expect(tester.pictureDeleteButton(0).element('.spinner-border')).toBeNull();

    await tester.pictureDeleteButton(0).click();

    expect(issueService.deletePicture).toHaveBeenCalledWith(1, 'access-token', 42);
    expect(tester.pictureDeleteButton(0).disabled).toBe(true);
    expect(tester.pictureDeleteButton(1).disabled).toBe(true);
    expect(tester.uploadButton.disabled).toBe(true);
    expect(tester.pictureDeleteButton(0).element('fa-icon')).toBeNull();
    expect(tester.pictureDeleteButton(0).element('.spinner-border')).not.toBeNull();

    issueService.get.and.returnValue(
      of({
        id: 1,
        pictures: afterDeletionPictureModels
      } as IssueModel)
    );

    deletionSubject.next(undefined);
    deletionSubject.complete();
    await tester.stable();

    expect(tester.pictures.length).toBe(2);
    expect(tester.pictureDeleteButton(0)!.disabled).toBe(false);
    expect(tester.uploadButton.disabled).toBe(false);
  });

  it('should upload picture', async () => {
    const beforeUploadPictureModels: Array<PictureModel> = [
      { id: 42, creationInstant: '2019-01-09T12:00:00Z' }
    ];
    const afterUploadPictureModels: Array<PictureModel> = [
      { id: 43, creationInstant: '2019-01-10T12:00:00Z' },
      { id: 43, creationInstant: '2019-01-10T12:00:00Z' }
    ];
    const afterUploadSubject = new Subject<IssueModel>();
    issueService.get.and.returnValue(afterUploadSubject);

    tester.componentInstance.issue.update(issue => ({
      ...issue,
      pictures: beforeUploadPictureModels
    }));
    await tester.stable();

    const resizeSubject = new Subject<Blob>();
    const uploadSubject = new Subject<PictureModel>();
    issueService.resizePicture.and.returnValue(resizeSubject);
    issueService.uploadPicture.and.returnValue(uploadSubject);

    const originalFile = {} as File;
    tester.picturesComponent.upload({
      target: {
        files: [originalFile]
      }
    } as unknown as Event);
    await tester.stable();

    expect(issueService.resizePicture).toHaveBeenCalledWith(originalFile);
    expect(tester.pictures.length).toBe(2);
    expect(tester.pictureDeleteButton(0)!.disabled).toBe(true);
    expect(tester.pictureDeleteButton(1)!).toBeNull();
    expect(tester.uploadButton).toBeNull();

    expect(tester.lastPicture.status()).toBe('RESIZING');
    expect(tester.progressBar.value).toBe(0);

    const resizedBlob = {} as Blob;
    resizeSubject.next(resizedBlob);
    resizeSubject.complete();
    await tester.stable();

    expect(issueService.uploadPicture).toHaveBeenCalledWith(
      1,
      'access-token',
      resizedBlob,
      jasmine.anything()
    );
    const progressListener: (progress: number) => void = (
      issueService.uploadPicture as jasmine.Spy
    ).calls.mostRecent().args[3];
    expect(tester.lastPicture.status()).toBe('UPLOADING');
    expect(tester.progressBar.value).toBe(0);

    progressListener(50);
    await tester.stable();
    expect(tester.progressBar.value).toBe(50);

    progressListener(100);
    await tester.stable();
    expect(tester.progressBar.value).toBe(100);

    uploadSubject.next(afterUploadPictureModels[1]);
    uploadSubject.complete();
    await tester.stable();

    expect(tester.progressBar.animated).toBe(true);
    expect(tester.progressBar.striped).toBe(true);
    expect(tester.lastPicture.status()).toBe('SAVING');

    afterUploadSubject.next({
      id: 1,
      pictures: afterUploadPictureModels
    } as IssueModel);
    afterUploadSubject.complete();
    await tester.stable();

    expect(tester.progressBar).toBeNull();
    expect(tester.lastPicture.status()).toBe('PLACEHOLDER');
    expect(tester.pictureImage(1).attr('src')).toBe('/api/issues/1/pictures/43/bytes');
  });
});
