import {
  Component,
  inject,
  input,
  linkedSignal,
  signal,
  WritableSignal,
  computed,
  Signal,
  ChangeDetectionStrategy
} from '@angular/core';
import { IssueModel, PictureModel } from '../../models/issue.model';
import { IssueService } from '../../issue.service';
import * as icons from '../../icons';
import { finalize, switchMap } from 'rxjs';
import { NgbProgressbar } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DatePipe } from '@angular/common';

type Status = 'RESIZING' | 'UPLOADING' | 'SAVING' | 'DONE' | 'DELETING' | 'PLACEHOLDER';

export class Picture {
  readonly progress = signal(0);
  readonly status: WritableSignal<Status>;
  readonly display: Signal<{
    readonly displayPicture: boolean;
    readonly displayPlaceholder: boolean;
    readonly displayProgress: boolean;
    readonly progressIndeterminate: boolean;
    readonly displayDeleteButton: boolean;
    readonly displayDeleteSpinner: boolean;
  }>;

  constructor(
    status: Status,
    public readonly picture: PictureModel | null = null
  ) {
    this.status = signal(status);
    this.display = computed(() => ({
      displayPicture: this.status() === 'DONE' || this.status() === 'DELETING',
      displayPlaceholder: this.status() === 'PLACEHOLDER',
      displayProgress:
        this.status() === 'RESIZING' || this.status() === 'UPLOADING' || this.status() === 'SAVING',
      progressIndeterminate: this.status() === 'SAVING',
      displayDeleteButton: this.status() === 'DONE' || this.status() === 'DELETING',
      displayDeleteSpinner: this.status() === 'DELETING'
    }));
  }
}

/**
 * The sub-component, used inside the issue edition component, to display and upload the pictures of the issue
 */
@Component({
  selector: 'dm-pictures',
  templateUrl: './pictures.component.html',
  styleUrl: './pictures.component.scss',
  imports: [FontAwesomeModule, NgbProgressbar, DatePipe],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PicturesComponent {
  private readonly issueService = inject(IssueService);

  readonly icons = icons;

  readonly accessToken = input.required<string>();
  readonly issue = input.required<IssueModel>();
  private readonly originalPictures = computed(() => this.issue().pictures);
  readonly pictures = linkedSignal(() => this.createPictures(this.originalPictures()));
  readonly editing = linkedSignal({ source: this.originalPictures, computation: () => false });

  private createPictures(pictures: Array<PictureModel>): Array<Picture> {
    return pictures.map(p => new Picture('DONE', p)).concat(new Picture('PLACEHOLDER'));
  }

  url(picture: Picture) {
    return `/api/issues/${this.issue().id}/pictures/${picture.picture!.id}/bytes`;
  }

  upload(event: Event) {
    this.editing.set(true);
    const picture = this.pictures()[this.pictures().length - 1];
    picture.status.set('RESIZING');
    picture.progress.set(0);

    const fileInput = event.target as HTMLInputElement;
    const file = fileInput.files![0];

    this.issueService
      .resizePicture(file)
      .pipe(
        switchMap(blob => {
          picture.status.set('UPLOADING');
          return this.issueService.uploadPicture(
            this.issue().id,
            this.accessToken(),
            blob,
            progress => picture.progress.set(progress)
          );
        }),
        switchMap(() => {
          picture.status.set('SAVING');
          picture.progress.set(100);
          return this.issueService.get(this.issue().id);
        }),
        finalize(() => this.editing.set(false))
      )
      .subscribe(issue => this.pictures.set(this.createPictures(issue.pictures)));

    fileInput.value = '';
  }

  delete(picture: Picture) {
    this.editing.set(true);
    picture.status.set('DELETING');
    this.issueService
      .deletePicture(this.issue().id, this.accessToken(), picture.picture!.id)
      .pipe(
        switchMap(() => this.issueService.get(this.issue().id)),
        finalize(() => this.editing.set(false))
      )
      .subscribe(issue => this.pictures.set(this.createPictures(issue.pictures)));
  }
}
