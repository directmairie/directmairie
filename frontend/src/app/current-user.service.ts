import { Injectable, inject } from '@angular/core';
import { HttpClient, HttpStatusCode } from '@angular/common/http';
import { BehaviorSubject, Observable, tap } from 'rxjs';

import { CurrentUserModel } from './models/current-user.model';
import { AuthenticationResultModel } from './models/authentication-result.model';
import { CurrentUserStorageService } from './current-user-storage.service';
import { ErrorInterceptorService } from './error-interceptor.service';

/**
 * Service allowing to initialize, get and remove the information about the current user, and to tell if the
 * user is authenticated.
 */
@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {
  private currentUserStorageService = inject(CurrentUserStorageService);
  private http = inject(HttpClient);
  private errorInterceptor = inject(ErrorInterceptorService);

  private userChanges = new BehaviorSubject<CurrentUserModel | null>(null);

  init() {
    const authenticatedUser = this.currentUserStorageService.loadAuthenticatedUser();
    if (authenticatedUser) {
      this.http
        .get<CurrentUserModel>('/api/users/me')
        .subscribe(user => this.userChanges.next(user));
    }
    this.errorInterceptor.getErrors().subscribe(error => {
      if (error.status === HttpStatusCode.Unauthorized) {
        this.remove();
      }
    });
  }

  getUserChanges(): Observable<CurrentUserModel | null> {
    return this.userChanges.asObservable();
  }

  refresh(): Observable<CurrentUserModel> {
    return this.http
      .get<CurrentUserModel>('/api/users/me')
      .pipe(tap(user => this.userChanges.next(user)));
  }

  storeAndRefresh(authenticationResult: AuthenticationResultModel): Observable<CurrentUserModel> {
    this.currentUserStorageService.storeAuthenticatedUser(authenticationResult);
    return this.refresh();
  }

  remove(): void {
    this.currentUserStorageService.storeAuthenticatedUser(null);
    this.userChanges.next(null);
  }

  isAuthenticated(): boolean {
    return this.currentUserStorageService.loadAuthenticatedUser() != null;
  }
}
