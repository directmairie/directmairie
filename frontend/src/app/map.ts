import { MapOptions, MarkerOptions, tileLayer, divIcon, Browser } from 'leaflet';

export function defaultMapOptions(): MapOptions {
  return {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution:
          'Cartographie &copy; <a href="https://www.openstreetmap.org/copyright">Contributeurs OpenStreetMap</a>, ' +
          '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
      })
    ],
    dragging: !Browser.mobile,
    tapHold: !Browser.mobile
  };
}

export function defaultMarkerOptions(a11yTitle: string, className = 'created'): MarkerOptions {
  return {
    icon: divIcon({
      // this is the SVG of the map-marker-alt font-awesome icon (https://fontawesome.com/v5.0/icons/map-marker-alt?style=solid),
      // where the class, data-prefix and data-icon attributes have been removed
      html: `
        <svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512">
          <path
            fill="currentColor"
            d="M172.268 501.67C26.97 291.031 0 269.413 0 192 0 85.961 85.961 0 192 0s192 85.961 192 192c0 77.413-26.97 99.031-172.268 309.67-9.535 13.774-29.93 13.773-39.464 0zM192 272c44.183 0 80-35.817 80-80s-35.817-80-80-80-80 35.817-80 80 35.817 80 80 80z">
          </path>
       </svg>
       `,
      className,
      iconSize: [30, 40],
      iconAnchor: [15, 40],
      tooltipAnchor: [0, -25] // display the tooltip at the center of the icon
    }),
    title: a11yTitle // improves the accessibility by adding a title to the marker div
  };
}
