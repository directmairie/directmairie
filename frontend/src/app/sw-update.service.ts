import { Injectable, inject } from '@angular/core';
import { SwUpdate, VersionEvent } from '@angular/service-worker';
import { filter, Observable } from 'rxjs';

/**
 * Service emitting an event to signal that a new version of the installed PWA is available.
 */
@Injectable({
  providedIn: 'root'
})
export class SwUpdateService {
  private updates = inject(SwUpdate);

  updatesAvailable(): Observable<VersionEvent> {
    return this.updates.versionUpdates.pipe(filter(event => event.type === 'VERSION_READY'));
  }
}
