# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 3.0.2 - 2023-09-20

### Fixed

* utiliser l'api nominatim non dépréciée pour la recherche inversée

## 3.0.1 - 2023-05-16

### Changed

* Modification du terme `Notification` en `Alerte` pour plus de clarté pour le canal de communication collectivité vers usager
* Modification textuelle en fin de remontée concernant le choix d'abonnement à une collectivité
* Modification en gras du terme `irréversible` en cas de suppression de remontée

## 3.0.0 - 2023-04-20

### Added

* Notification par mail du traitement d'une remontée au citoyen (si inscrit) [#92](https://gitlab.adullact.net/directmairie/directmairie/-/issues/92)
  * L'utilisateur peut, à l'inscription ou en éditant les informations de son compte, choisir d'être notifié·e par courriel des
  changements d'état de ses remontées d'information 
  * L'administrateur, lorsqu'il change l'état d'une remontée d'information, est invité à saisir un message
  * Les changements d'état, accompagnés de leur message, sont affichés dans les remontées d'information (pour les administrateurs
  ainsi que dans la section "Mes remontées d'information" de la page d'accueil 
* L'utilisateur, à la soumission d'une remontée, doit cliquer sur un bouton "J'ai compris" avant de pouvoir saisir la description
  de la remontée, après avoir lu un avertissement lui indiquant de ne pas y saisir de données personnelles [#142](https://gitlab.adullact.net/directmairie/directmairie/-/issues/142)
* Les administrateurs peuvent définitivement supprimer des remontées d'information [#114](https://gitlab.adullact.net/directmairie/directmairie/-/issues/114)
* Ouverture d'un canal de communication de la collectivité vers le citoyen [#139](https://gitlab.adullact.net/directmairie/directmairie/-/issues/139)
  * Les administrateurs ont accès à un nouveau menu "Alertes" leur permettant d'envoyer aux abonnés d'une collectivité une 
  alerte par courriel
  * L'utilisateur peut, via sa page "Mes abonnements", s'abonner ou se désabonner des alertes de sa/ses collectivités (message descendants)
  envoyées par les administrateurs
  * Après la soumission d'une remontée d'information, l'utilisateur est invité, s'il ne l'est pas déjà, à s'abonner aux alertes 
  envoyées par la collectivité. Cette invitation est aussi affichée après une identification ou un enregistrement, pour la dernière
  remontée qu'il a réalisée et qui est attachée à son compte
* Le clic sur un lien de la barre de navigation fait revenir la barre de navigation à son état initial (réduit plutôt qu'étendu)

### Changed

* Montée de version des diverses dépendances backend et frontend (Spring Boot 3.0.5, Angular 15). Le passage à Spring Boot 3.x 
  nécessite l'utilisation de Java 17 plutôt que Java 11. 
* Utilisation de composants Angular _standalone_ dans le frontend

## 2.2.1 - 2022-10-28

### Changed

* Modification du nom du fichier Manifest

## 2.2.0 - 2022-10-20

### Added

* Amélioration de l'ergonomie du scroll sur la carte OSM sur mobile [#131](https://gitlab.adullact.net/directmairie/directmairie/-/issues/131)
* Scroll vers la carte OSM après sélection d'une adresse dans le menu ou via "Ma position" [#133](https://gitlab.adullact.net/directmairie/directmairie/-/issues/133)
* Ajout d'un pied de page avec un lien vers la politique de confidentialité / RGPD [#110](https://gitlab.adullact.net/directmairie/directmairie/-/issues/110)
* Permettre au citoyen de supprimer son compte et de modifier ses informations personnelles et son mot de passe [#112](https://gitlab.adullact.net/directmairie/directmairie/-/issues/112)
* L'adresse email "from" est maintenant configurable via une seule propriété

### Changed

* Montée de version des diverses dépendances backend et frontend (Spring Boot 2.7.4, Angular 14, Bootstrap 5, etc.)
* Le bleu primaire de l'application est le même bleu que celui du logo
* la couleur de la flèche du logo principal est de la même couleur que la flèche du favicon

### Fixed

* Le support de la PWA a été corrigé (le fichier manifest était incorrect)
* Lorsque le jeton d'authentification est expiré, un message d'erreur compréhensible est affiché une fois et le jeton est supprimé, au lieu d'afficher une erreur 500 jusqu'au prochain login.

## 2.1.0 - 2021-12-02

### Added

* Filtrer les demandes pour les agents d'EPCI [#96](https://gitlab.adullact.net/directmairie/directmairie/-/issues/96)
* ETQ citoyen: pouvoir visualiser uniquement mes remontées (et pas celles des autres) [#107](https://gitlab.adullact.net/directmairie/directmairie/-/issues/107)
* Ajout d'un lien retour vers le site de la collectivité [#73](https://gitlab.adullact.net/directmairie/directmairie/-/issues/73)
* ETQ citoyen: laisser mon numéro de téléphone pour être rappelé [#108](https://gitlab.adullact.net/directmairie/directmairie/-/issues/108)
* Changement de statut des informations [#99](https://gitlab.adullact.net/directmairie/directmairie/-/issues/99)
* Personnaliser la communication vers le citoyen [#93](https://gitlab.adullact.net/directmairie/directmairie/-/issues/93)
* Add copyright mention on map for OpenStreetMap contributors
  [#75](https://gitlab.adullact.net/directmairie/directmairie/issues/75),
  [#113](https://gitlab.adullact.net/directmairie/directmairie/issues/113),
  [#127](https://gitlab.adullact.net/directmairie/directmairie/issues/127),
* Ajout du logo de la collectivité [#89](https://gitlab.adullact.net/directmairie/directmairie/-/issues/89)

### Fixed

* Replace favicon.ico with correct DirectMairie logo
  [#82](https://gitlab.adullact.net/directmairie/directmairie/issues/82)
* Renaming Amies to DirectMairie
  * in documentation [#79](https://gitlab.adullact.net/directmairie/directmairie/issues/79)
  * in metadata.json + title tag [#83](https://gitlab.adullact.net/directmairie/directmairie/issues/83)
  * [#66](https://gitlab.adullact.net/directmairie/directmairie/issues/66)
  * [#80](https://gitlab.adullact.net/directmairie/directmairie/issues/80)
* CI does not run anymore : You must specify POSTGRES_PASSWORD for the superuser
  [#81](https://gitlab.adullact.net/directmairie/directmairie/issues/81)

Security

## 2.0.0 - 2019-10-08

### Changed

* REFACTOR/Renaming DirectMairie on homepage [#67](https://gitlab.adullact.net/directmairie/directmairie/issues/67)
* Add DirectMaire logo [#65](https://gitlab.adullact.net/directmairie/directmairie/issues/65)
* Mauvais sujet dans les mail de notification [#70](https://gitlab.adullact.net/directmairie/directmairie/issues/70)

## 1.0.0 - 2019-06-26

### Added

* First release :)
