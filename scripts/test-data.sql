-- useful script to insert test data in the integ database (for demos, for example).
-- to execute on the integ database, start the application to make sure the tables are created, then run
--   psql -h localhost -U directmairie -d directmairie -f ./scripts/test-data.sql

INSERT INTO pooling_organization (id, name) VALUES (1, 'Adullact');

INSERT INTO government (id, name, code, url, osm_id, pooling_organization_id, contact_email)
  VALUES (1, 'France Métropolitaine', 'FM', 'https://www.france.fr/fr', 1403916, 1, 'contact@france.fr');
INSERT INTO government (id, name, code, url, osm_id, pooling_organization_id, contact_email)
  VALUES (2, 'Auvergne-Rhône-Alpes', '20005376700014', 'https://www.auvergnerhonealpes.fr', 3792877, 1, 'contact@ara.fr');
INSERT INTO government (id, name, code, url, osm_id, pooling_organization_id, contact_email)
  VALUES (3, 'Rhône', '22690001701004', 'https://www.rhone.fr/', 7378, 1, 'contact@rhone.fr');
INSERT INTO government (id, name, code, url, osm_id, pooling_organization_id, contact_email)
  VALUES (4, 'Loire', '17420001400010', 'https://www.loire.fr', 7420, 1, 'contact@loire.fr');

INSERT INTO issue_group (id, label) VALUES (2, 'Éclairage & électricité');
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (2, 'Éclairage insuffisant', false, 2);

INSERT INTO issue_group (id, label) VALUES (3, 'Voirie & circulation');
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (3, 'Chaussée dégradée', true, 3);

INSERT INTO issue_group (id, label) VALUES (4, 'Propreté');
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (4, 'Tags & graffiti', false, 4);
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (5, 'Dépôt sauvage', true, 4);

INSERT INTO issue_group (id, label) VALUES (5, 'Autoroutes');
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (6, 'Barrière endommagée', true, 5);

-- France non métropolitaine: nothing (to test non-covered zones)

-- France Métropolitaine: Autoroutes, Autre
INSERT INTO government_issue_category (government_id, category_id) VALUES (1, 1);
INSERT INTO government_issue_category (government_id, category_id) VALUES (1, 6);

-- ARA: Voirie, Autre
INSERT INTO government_issue_category (government_id, category_id) VALUES (2, 1);
INSERT INTO government_issue_category (government_id, category_id) VALUES (2, 3);

-- Rhône: Eclairage, Voirie, Déchets, Autre
INSERT INTO government_issue_category (government_id, category_id) VALUES (3, 1);
INSERT INTO government_issue_category (government_id, category_id) VALUES (3, 2);
INSERT INTO government_issue_category (government_id, category_id) VALUES (3, 3);
INSERT INTO government_issue_category (government_id, category_id) VALUES (3, 5);

-- Loire: Eclairage, Voirie, Déchets, Graffiti, Autre
INSERT INTO government_issue_category (government_id, category_id) VALUES (4, 1);
INSERT INTO government_issue_category (government_id, category_id) VALUES (4, 2);
INSERT INTO government_issue_category (government_id, category_id) VALUES (4, 3);
INSERT INTO government_issue_category (government_id, category_id) VALUES (4, 4);
INSERT INTO government_issue_category (government_id, category_id) VALUES (4, 5);

-- Government admins
-- admin@loire.fr/loire
INSERT INTO auser (id, email, password, super_admin, email_verified, issue_transition_notification_activated)
VALUES (1, 'admin@loire.fr',
        'tMFfspotbvAfDEMaPYwbTQnoQN3PbvC8PAoxAWhhK+CRveFOuL1N2SogVt/ig8jcvh32D2bXZIrxnpTGcOy1zOb0v/2yN9EJuYLiRXypM1g=',
        false, true, false);
INSERT INTO government_administrator (government_id, user_id)
VALUES (4, 1);
-- admin@rhone.fr/rhone
INSERT INTO auser (id, email, password, super_admin, email_verified, issue_transition_notification_activated)
VALUES (2, 'admin@rhone.fr',
        'M3zdx8zM89uMNJrv8Rq0OEu1OfOUExVr6tdDirx4MrgJ/1fGRz1bsTNRIlQQqRYdK/ipdHYTTyAQIdsdrWP5P8gNxLa7epbLMLYuF38D2ls=',
        false, true, false);
INSERT INTO government_administrator (government_id, user_id) VALUES (3, 2);

-- Pooling organization admins
-- mutualisant@adullact.org/adullact
INSERT INTO auser (id, email, password, super_admin, email_verified, issue_transition_notification_activated)
VALUES (3, 'mutualisant@adullact.org',
        'YdojSlKZwB1nH5LjVK+uQBEnh5b9iUuMe67bDgRfoHVePOfnBu6iC0GxFbbKhSZp3NgPxLs/lhnfxBYf/4okSkYyg3DixVXHkdEvdCoWl7o=',
        false, true, false);
INSERT INTO pooling_organization_administrator (pooling_organization_id, user_id)
VALUES (1, 3);

-- Super admins
-- superadmin@adullact.org/adullact
INSERT INTO auser (id, email, password, super_admin, email_verified, issue_transition_notification_activated)
VALUES (4, 'superadmin@adullact.org',
        'YdojSlKZwB1nH5LjVK+uQBEnh5b9iUuMe67bDgRfoHVePOfnBu6iC0GxFbbKhSZp3NgPxLs/lhnfxBYf/4okSkYyg3DixVXHkdEvdCoWl7o=',
        true, true, false);

-- government contacts
-- Rhône - Autre
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (31, 'autre@rhone.fr', 3, 1);
-- Rhône - Éclairage
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (32, 'eclairage@rhone.fr', 3, 2);
-- Rhône - Voirie
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (33, 'voirie@rhone.fr', 3, 3);
-- Rhône - Propreté
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (34, 'proprete@rhone.fr', 3, 4);
-- Loire - Autre
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (41, 'autre@loire.fr', 4, 1);
-- Loire - Éclairage
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (42, 'eclairage@loire.fr', 4, 2);
-- Loire - Voirie
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (43, 'voirie@loire.fr', 4, 3);
-- Loire - Propreté
INSERT INTO government_contact (id, email, government_id, group_id) VALUES (44, 'proprete@loire.fr', 4, 4);

commit;
