# DirectMairie

## DOCUMENTATION

* [Documentation for developers](./Documentation/For_developers/README.md)
* [Documentation for operators](./Documentation/For_operators/README.md)
* [Documentation for users](./Documentation/For_users/README.md)
* [Documentation for API](./backend/src/main/asciidoc/index.adoc)

## LEXIQUE APPLICATION

* *Direct Mairie (DM)* : service de remontée d'information citoyenne.
* *directmairie.adullact.org* : instance Direct Mairie Adullact
* *Remontée* `issue` : envoi d'information effectué par un citoyen depuis l'application.
* *Groupe de catégorie* `issue group` et *Catégorie* `issue category` : Catégories et leurs familles qui permettent de classer les remontées par thème.

ex : dans la catégorie Affiches/Tags il sera possible de choisir de décrire une affiche, un graffiti, un message haineux...

* *Mutualisant* `pooling organization administrator` : ou *administrateur de collectivité(s)*. Administrateur d'une collectivité ou d'un groupe de collectivités regroupées sous une même autorité. C'est lui qui pourra créer/paramétrer les collectivités dans l'instance DM.
* *Gestionnaire de Collectivité* `Local government administrator` : Le gestionnaire des remontées de sa collectivité. Il n'a accès qu'aux remontées d'informations. Pas de droit de configuation.
* *Super administrateur(s)* : gestionnaire applicatif. Compte qui permet la création de mutualisants et de collectivités.
