# Documentation for developers

## Project overview

The project uses 

- [Spring Boot](https://spring.io/projects/spring-boot) (MVC) with JPA (Hibernate) for the backend, using [Kotlin](http://kotlinlang.org/)
- [Angular](https://angular.io/) for the frontend, using [TypeScript](http://typescriptlang.org/)
- [PostgreSQL](https://www.postgresql.org/) for the database
- [Gradle](https://gradle.org/) to build the application, delegating to [pnpm](https://pnpm.io/) and [Angular CLI](https://cli.angular.io/) to build the frontend

### Backend

You'll need a recent enough Java 11 SDK to develop backend code.

In order to avoid having to install a PostgreSQL database (although that's fine, too) and an SMTP server (used to send emails), you can use [Docker](https://www.docker.com/): 

    docker-compose up

starts a PostgreSQL server on `http://localhost:5432`, an SMTP server on `localhost:25` (which doesn't actually send any email), and a [simple web mail client](https://github.com/mailhog/MailHog) allowing to visualize the emails sent by the application, on `http://localhost:8025`.

Note: If you only want to start the PostgreSQL using Docker, just run

    docker-compose up postgres
    
If you only went to start the SMTP server using Docker, just run

    docker-compose up smtp

### Frontend

To develop frontend code, you'll need

- a recent enough [NodeJS](https://nodejs.org/) (10.15+)
- Pnpm as a package manager (see [here to install](https://pnpm.io/))
- Chrome, used to run the automated unit tests

Then in the `frontend` directory, run `pnpm install` to download the dependencies.
Then run `pnpm start` to start the app, using the proxy configuration to reroute calls to `/api` to the backend, expected to run on `http://localhost:8080`.

The application will be available at `http://localhost:4200/`

#### PWA

To test the PWA support (more accurately the service worker support), you need to serve the app with the service worker enabled.
As it is enabled in the `production` configuration in our application, you can run:

    ng serve -c production --no-live-reload

Then go to `http://localhost:4200`.
The app should be displayed.
Now go offline, and reload the page: the app should still be there.

To test the service worker notification, update something in the source code. Reload the page and you should see a notification.

## Building and running

### Create the database and its user

Once the database server is started, execute 
 
    psql -h localhost -U postgres -f backend/database/database.sql

### Running the build

The build uses the standard Gradle lifecycle, and uses the Spring Boot plugin. 

- `./gradlew build`: runs the whole build, including the backend and frontend tests and other verification tasks. Note that this installs a local version of NodeJS and pnpm, so you don't need them to be installed to run the build.
- `./gradlew assemble`: only assembles the application jar file, without running tests. The standalone jar is generated in `backend/build/libs/directmairie.jar`, and you can run it using `java -jar backend/build/libs/directmairie.jar`
- `./gradlew bootRun`: runs the backend Spring Boot application, without building and serving the frontend files. This is useful to have the backend available while developing the frontend using Angular CLI.
- `./gradlew asciidoc`: generates the HTTP API documentation. This documentation is generated using [Spring REST Docs](https://spring.io/projects/spring-restdocs), based on all the backend tests named `XxxDocTest` (this convention must be respected). Once this task has been executed successfully, the generated documentation is available in `backend/build/docs/asciidoc/index.html`

### Test data

You can populate your local database by running:

    psql -h localhost -U directmairie -f ./scripts/test-data.sql 


### Database schema evolutions

The project uses [FlywayDB](https://flywaydb.org/) and its integration into Spring Boot to apply changes to the database schema (adding columns, tables, constraints, etc.) without losing data.
Making a change thus consists in creating a new `VXXXX__some_description.sql` file in `backend/src/main/resources/db/migration`. 
The changes are applied to the database automatically when the application is redeployed and restarted.

During development, it's sometimes useful to recreate the schema from scratch. Two Gradle tasks are available to do that:

 - `./gradlew flywayCleanTest`: cleans the database schema used by automated tests
 - `./gradlew flywayCleanInteg`: cleans the database schema used by by the application

Having two separate schemas is useful to avoid losing data used for manual testing and demos every time automated tests are run.

## Continuous Integration (CI)

The `.gitlab-ci.yml` file describes how Gitlab is running the CI jobs.

It uses a base docker image maintained by circleci and available on DockerHub.
The image contains a JDK 11, browsers and the libraries allowing us to run the frontend tests.

We install `node` and `pnpm` in `/tmp` (this is not the case for local builds)
to avoid symbolic links issues on Docker.

## Setting up the project in IntelliJ IDEA

Use *File* - *Open*, and select the `directmairie` directory (or whatever directory you chose to clone the project). Then leave all default options checked (in particular, the option *Use default gradle wrapper*). Make sure the Gradle build uses the project JDK, which should be a Java 11 JDK.

To start the backend Spring Boot application from the IDE, create a *Run configuration* (of type *Spring Boot Application*  if you have the IntelliJ ultimate edition, or *Application* if you only have the community edition). Enter `net.adullact.directmairie.Application` as the main class.

The application needs a secret key to be able to run. But you can simply run it without any argument to get the error message indicating how to generate one and pass it as argument when starting the application).
