# Various Notes

## Other tools

* [Tell My City](http://www.spallian.com/solution/tellmycity/) proprietary software
* [PopVox](https://www.popvox.fr/) proprietary online service
* [Flui.city](https://www.flui.city/) proprietary online service
* [BeeCitiz](http://www.beecitiz.com/) proprietary online service
* [FixMyStreet](http://fixmystreet.org/) open source software, only one active developer, developped in Perl. No support in France. Difficulties to find skills on a declining language and maybe inappropriate for a modern web application.

## Examples of cities implementing similar services

* [TellMyCity Argentueil](http://go.tellmycity.com/?lon=2.2500000&lat=48.9500000&theme=greensea#)
* [TellMyCity Villepinte](http://ville-villepinte.fr/tell-my-city-une-application-pour-ameliorer-la-ville/)
* [TellMyCity Limoges](http://go.tellmycity.com/?lon=1.265&lat=45.829&theme=pumpkin#/new_report/step0/)
* [TellMyCity Oyonnax](http://go.tellmycity.com/?lon=5.655663&lat=46.257551&theme=belizehole#/home)
* [Fontenay-sous-bois Signalement sur l'espace public](https://teleservices.fontenay-sous-bois.fr/signalement-sur-l-espace-public/)

## Categories of issues (French)

* Eau & assainissement
    * Égout bouché
    * Fuite d'eau
    * inondation
    * Mauvaises odeurs
* Éclairage & électricité
    * Éclairage insuffisant
    * Feu tricolor & signalisation en panne
    * Lampadaire en panne ou cassé
* Espaces vers & jardins
    * Aires de jeux
    * Élagage
    * Arbre tombé à terre
    * Espaces verts mal entretenus
* Propreté
    * Dépôt sauvage
    * Affichage sauvage
    * Saletés
    * Tags & graffiti
* Voirie & circulation
    * Chaussée dégradée
    * Marquage au sol
    * Signalisation routière
    * Véhicule épave
    * Plaque d'égout descellée
    * Mobilier urbain dégradé
    * Potelets et barrière endommagés
