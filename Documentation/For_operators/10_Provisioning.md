# Hardware & network Provisioning

## CPU and RAM

There is no specific requirement in terms of RAM or CPU: the application is mainly doing IO, and is not manipulating any large data structure in memory.

## Disk

The application stores uploaded pictures on the disk, but these pictures are shrinked by the frontend before being uploaded (their largest side is set to 1024 pixels), so the disk space shouldn't be a concern before a while. In the long term, the necessary disk space of course depends on the popularity of the application (as does everything else: RAM, CPU, number of necessary machines, etc).

Note that if several machines are running the application backend, they all need to access the same directory containing the pictures.

## Network

Here are the various networking flux and protocols used by the application:

- static assets (html pages, JS scripts, etc.) are downloaded by the end user's browser from the application server over HTTPS.
  Note however that the application server itself doesn't deal with TLS. 
  This should be the responsibility of a reverse proxy;
- dynamic assets (mainly JSON documents and pictures) are downloaded and uploaded by the end user's browser from/to the web server over HTTPS;
- dynamic JSON documents are downloaded by the end user's browser from the Nominatim API servers (https://nominatim.openstreetmap.org) over HTTPS;
- static assets (map pictures) are downloaded by the end user's browser from the openstreetmap.org servers over HTTPS;
- the application server also communicates over HTTPS with the Nominatim API servers;
- the application server communicates with the PostgreSQL database;
- the application server communicates with the SMTP server to send emails;

| Source host    | Destination host             | Destination port  | Detail             |
|:---------------|:-----------------------------|:------------------|:-------------------|
| citizen device | DirectMairie host                   | 80 / 443          | DirectMairie application |
| citizen device | nominatim.openstreetmap.org  | 443               | OpenStreetMap search |
| DirectMairie host     | Database host                | 5432              | Postgres              |
| DirectMairie host     | SMTP host                    | 465               | SMTP               |




