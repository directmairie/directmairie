# Documentation for operators

* [Provisioning](10_Provisioning.md) 
* [Pre-requisites](20_Prerequisites.md)

## Configuring the application

Once the application is deployed, you have to create a superadministrator, and may want to create *issues categories* and *issue groups* (which are in fact groups of categories).

### Superadmin (mandatory)

To create a superadmin `superadmin@adullact.org` with `adullact` as password, type the following SQL code:

```postgresql 
INSERT INTO auser (id, email, password, super_admin, email_verified)
VALUES (1, 'superadmin@adullact.org', 'YdojSlKZwB1nH5LjVK+uQBEnh5b9iUuMe67bDgRfoHVePOfnBu6iC0GxFbbKhSZp3NgPxLs/lhnfxBYf/4okSkYyg3DixVXHkdEvdCoWl7o=', true, true);
```

### Categories (optional)

To create the following structure of categories

* Graffiti, tags and posters
    * Posters
    * Graffiti
    * Hateful message
* Wreck and junk
    * Wreck
    * Bicycle wreck

...type in the following SQL code: 

```postgresql
INSERT INTO issue_group (id, label)
VALUES 
    (2, 'Graffiti, tags and posters'),
    (3, 'Wreck and junk')
    ;

INSERT INTO issue_category (id, label, description_required, group_id)
VALUES 
(2, 'Posters', 'f', 2),
(3, 'Graffiti', 'f', 2),
(4, 'Hateful message', 'f', 2),
(5, 'Wreck', 'f', 3),
(6, 'Bicycle wreck', 'f', 3)
;
```


## Building the application

Gradle is used to build the application. See the [developer documentation](../For_developers/README.md) for more details about the tasks useful during development. To build the jar file that needs to be deployed in production (without running the tests), you need to have a Java 11 SDK installed. 
Then, in the root directory, you can run

    ./gradlew assemble

This will download Gradle itself, all the libraries and tools necessary to build the application (backend + frontend), and produce the jar file under `backend/build/libs/directmairie.jar`. This jar file contains the whole application (backend + frontend).

## Installing the application

A PostgreSQL database server must be installed and configured with a user allowed to access it over the network, and allowed to create tables in its database. See the script `backend/database/database.sql` for an example script which creates a user (directmairie) and its database (note: the directmairie_test database created by this script is not necessary in production: it's only used for automated tests during development).

### Deploying the application

Deploying the application simply consists in copying the `directmairie.jar` file to the machine(s) which will run the application backend.

### Running the application

The values of all the properties that must be configured must be known (see the *Configuration* section later).

Running the application simply consists in running the jar file.
Properties can be set using environment variables. 
In that case, running the jar file simply consists in executing

    java -jar directmairie.jar
    
Properties can also be passed on the command line. If, for example, you want to pass the secret key used to encrypt the JSON web tokens used to identify the users, you will use

    java -jar directmairie.jar --directmairie.security.secret-key=<the secret key>

`<the secret key>` must of course be replaced by the actual alphanumeric secret key.

If you want to run the application at startup automatically, please refer to the [Spring Boot guide](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment-install). 
Note that, as recommended by this documentation, since we don't know yet if the jar files need to be executable, they are not.
Making them executable can be done trivially by adding `launchScript()` inside the `bootJar` task customization of the `backend/build.gradle.kts` file.

Running the application automatically creates the necessary database tables, constraints, sequences, etc. (if necessary). Once that has been done, at the very least, a super administrator user must be inserted in the database (to be able to start using the application and the HTTP API). 
Here's an example query allowing to create such a user with the password `adullact`:

```sql
INSERT INTO user (id, email, password, super_admin, email_verified)
VALUES (1, 'superadmin@adullact.org', 'YdojSlKZwB1nH5LjVK+uQBEnh5b9iUuMe67bDgRfoHVePOfnBu6iC0GxFbbKhSZp3NgPxLs/lhnfxBYf/4okSkYyg3DixVXHkdEvdCoWl7o=', true, true);
```

You can choose your own password and hash it using the `hashPassword` gradle task:

```
./gradlew hashPassword --args <the-password>
```

Refer to the HTTP API documentation for help on creating issue groups and categories.

If the application evolves and the database schema must evolve, this will be done using FlywayDB (the initial schema is already created that way), and restarting the new version of the application will migrate the schema and the data.

## Documentation for operations

### How to start/stop services?

TBD: it depends if there are services in the first place, and if they are System V or systemd services.

See also:
 
* https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html#deployment-systemd-service
* https://stackoverflow.com/questions/21503883/spring-boot-application-as-a-service

### Where are located log files?

By default, the application logs everything to the standard output. You can also log to rolling files by setting the `logging.file` property. See [the Spring Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-logging) for details.

### How to change the verbosity of log files?

The application itself doesn't log anything (yet). The libraries however (Spring, Hibernate, etc.) generate logs. The global verbosity can be set by setting the `logging.level.root` property (for example: `logging.level.root=WARN`). 

More specific levels can be set, for example: `logging.level.org.hibernate=ERROR`.  See [the Spring Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-custom-log-levels) for details.

### How to backup? (what, where, how)

The application generates 3 kinds of data:

- data stored in the PostgreSQL database;
- uploaded pictures in the root directory configured by the `directmairie.pictures.root-directory` property;
- log files if configured.
 
The database and the pictures should be backed up. The logs could be backed up if you feel it can be useful, and should probably be purged from time to time. Please consult your PostgreSQL server documentation for backup strategies.

### How to supervise? (what, where, how)

Exceptions in the logs can indicate that something is malfunctioning.

The `/actuator/health` endpoint is also helpful to supervise the application. It's only accessible to super administrators, and allows knowing if the application is up and running of course, but also if the database and the SMTP server are up and running, and accessible from the application. Here's how you can access it using httpie

    http --auth <email>:<password> https://<application.url>/actuator/health

This produces the following JSON response:

```json
{
    "details": {
        "db": {
            "details": {
                "database": "PostgreSQL",
                "hello": 1
            },
            "status": "UP"
        },
        "diskSpace": {
            "details": {
                "free": 21241024512,
                "threshold": 10485760,
                "total": 250790436864
            },
            "status": "UP"
        },
        "mail": {
            "details": {
                "location": "localhost:25"
            },
            "status": "UP"
        }
    },
    "status": "UP"
}
```

## Configuration

The application is configured using the standard [properties-based mechanism of Spring Boot](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-external-config).

The standard Spring Boot properties are described in the [Spring Boot documentation](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#common-application-properties).

The application itself adds custom properties to the default Spring Boot properties. 
All are defined in the `backend/src/main/resources/application.yml` file. 
You might want to overload some of those.

The additional ones, all starting by the prefix `directmairie`, are described in the following section

### Security properties

* `directmairie.security.secret-key`: The secret key used to encrypt the JSON web tokens used to identify users. As its name indicates, it must stay secret (and might need to be changed in case it has been compromised), and is thus not provided with the application.

You can generate a random secret key using `./gradlew generateSecretKey`.

### Database properties

* `directmairie.database.host-and-port`: This is just a placeholder used to ease the continuous integration job configuration. It's actually just used as part of the definition of the `spring.datasource.url` property. That standard Spring property is the one you should override to change the URL of the production database.
* `spring.datasource.url` (see the `backend/src/main/resources/application.yml` file for an example, and the [PostgreSQL documentation](https://jdbc.postgresql.org/documentation/head/connect.html) for a complete format description)
* `spring.datasource.username`
* `spring.datasource.password`

The application uses the [Hikari connection pool](https://github.com/brettwooldridge/HikariCP), which allows specifying several properties under `spring.datasource.hikari.*`. The one that you might want to set if the application becomes very popular is the property `spring.datasource.hikari.maximum-pool-size` (it defaults to 10).

### Users pictures storage properties 

* `directmairie.pictures.root-directory`: This is the absolute path of the root directory where pictures uploaded by users (to describe issues, but also to provide a logo for local governments) are stored. It must be a directory that is writeable and readable by the application, and has enough disk space to hold all the uploaded pictures.

### Email properties

* `directmairie.mail.registration.from`: The email address used as the sender address of emails sent to users in order to confirm their registration. 
* `directmairie.mail.registration.subject`: The subject of the emails sent to users in order to confirm their registration. 
* `directmairie.mail.new-issue.from`: The email address used as the sender address of emails sent to local government contacts in order to inform them that a new issue has been received. 
* `directmairie.mail.new-issue.subject`: The subject of the emails sent to local government contacts in order to inform them that a new issue has been received. It's actually a pattern, where the `%s` placeholder will be replaced by the category of the received issue.
* `directmairie.mail.lost-password.from`: The email address used as the sender address of emails sent to users in order to confirm their password change after a lost password. 
* `directmairie.mail.lost-password.subject`: The subject of the emails sent to users in order to confirm their password change after a lost password.
* `directmairie.mail.issue-transition.from`: The email address used as the sender address of emails sent to citizens to inform them that one of their issues has transitions to a new status.
* `directmairie.mail.issue-transition.subject`: The subject of the emails sent to citizens to inform them that one of their issues has transitions to a new status.
* `directmairie.mail.notification.from`: The email address used as the sender address of notification emails sent to subscribed users by a government.
* `directmairie.mail.from`: The email address used as the sender address of all emails sent to users if the `directmairie.mail.xxx.from` properties are left to their default values (i.e. the default value of these properties is `${directmairie.mail.from}`).

### Hosting properties

* `directmairie.hosting.host`: The host part of the public facing URL of the deployed application. For example, if the application is deployed at the URL `https://directmairie.adullact.org`, then this property must have the value `directmairie.adullact.org`. This property is used inside the email templates to generate links to pages of the application. It's also by default used as part of the value of the `directmairie.mail.from` property.

### Email templates

Note that the content of the emails themselves are generated from [mustache](https://mustache.github.io/) templates. These templates are bundled in the application, but they can be modified if needed before assembling the application. The default ones are located under `backend/src/main/resources/mail`.

## Mandatory properties

Here are the environment-related configuration properties that must absolutely be set for the application to work fine in production.

* `directmairie.pictures.root-directory`
* `directmairie.hosting.host`
* `spring.mail.host`
* `spring.mail.username`
* `spring.mail.password`
* `spring.mail.port`
* `spring.datasource.url` 
* `spring.datasource.username`
* `spring.datasource.password`

## Upgrades

TBD
