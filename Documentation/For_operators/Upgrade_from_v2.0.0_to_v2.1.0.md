# Upgrade from v2.0.0 to v2.1.0

## Assumptions

* DirectMairie v2.0.0 is hosted on `server`
* DirectMairie v2.1.0 will be hosted on another machine called `server02`

## TL;DR

1. On `server`: Stop service
2. On `server`: Export Database
3. On `server`: Export files
4. On `server02`: Install DirectMairie
5. On `server02`: Import database
6. On `server02`: Import files
7. On `server02`: Launch DirectMairie

## 1. Stop service

On `server`:

```shell
systemctl stop directmairie.service
```

## 2. Export database

On `server`:

```shell
sudo -i
su - postgres
pg_dump --username=directmairie --dbname=directmairie --clean --file=/tmp/DirectMairie_export.psql
```

## 3. Export files

On `server`:

* Identify where the directory used to store files. Get this information with `grep pictures.root-directory /etc/systemd/system/directmairie.service`
* Create an archive of this directory (as root)

## 4. Install DirectMairie

On `server02`: let's do it (or let Puppet do the job)

**/!\ BEWARE** Do **not** launch DirectMairie. This is to prevent the upgrading of the database (as data is not imported yet)

## 5. Import database

On `server02`:

```shell
sudo -i
systemctl stop directmairie.service
su - postgres
psql directmairie < /tmp/DirectMairie_export.psql
```

## 6. Import files

On `server02`:

* Identify where the directory used to store files. Get this information with `grep pictures.root-directory /etc/systemd/system/directmairie.service`
* Extract the archive made at step 3 in the directory just identified.
* Take care of ownership of files/directories, they must match the user defined in configuration (See `grep User /etc/systemd/system/directmairie.service`)

## 7. Launch DirectMairie service

On `server02`:

```shell
systemctl start directmairie.service
```

