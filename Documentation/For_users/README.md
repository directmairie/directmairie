# Documentation for users

Besides regular citizens who can just submit issues to *DirectMairie*, and who will never read this documentation, there are 3 kinds of "special" users:

 1. local government administrators
 2. pooling organization administrators
 3. super administrators
 
When you log in to DirectMairie, the menus displayed in the navigation bar depend on what kind of user you are.
If you're not allowed to do something, then the menu won't appear in the navigation bar.
If a menu appears in the navigation bar, it means you're allowed to use it.
 
## Super administrators
 
The super administrators main role is to create pooling organizations and select their administrators.
They also configure the global aspects of the application, like the categories of issues that can be submitted in general. 
Note that some (few) super-administrator actions can only be done using the command-line. See the HTTP services documentation for more details.

But they can also do everything that pooling organization administrators and government administrators can do.

## Pooling organization administrators

Their main role is to create the local governments that are under the responsibility of the pooling organization(s) that they administer, and to configure them (select which categories of issues are enabled for the government, its contact emails, its administrators). 
The pages allowing to create and modify governments should be intuitive enough to make it sufficiently easy.

In addition, pooling organization administrators can also do everything that the administrators of their local governments can do.

## Local government administrators

Their role is to supervise issues that are submitted and assigned to the government(s) that they administer (based on the location of the issue, and on its category). 
They can visualize issues, with their description, pictures, and location on the map. 
And they can modify their status (created, resolved, rejected).
They can also export issues, from a date to another date, as a CSV file, that can then be opened in any tool supporting CSV (a simple text editor, or a spreadsheet).

## FAQ

> I know for a fact that someone has submitted an issue in my local government, and I'm the administrator of the government, but I can't find the issue. Why?

The issue is assigned to a government based on the location on the issue, but also based on the chosen category of the issue. So, depending on the chosen category, the issue can be assigned to your local government, or to a smaller local government that covers the selected location, or to a a larger local government that covers the selected location. 
