package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.Instant

/**
 * Automated tests for [EmailVerificationDao]
 * @author JB Nizet
 */
class EmailVerificationDaoTest(@Autowired val dao: EmailVerificationDao) : BaseDaoTest() {

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("auser") {
                withDefaultValue("issue_transition_notification_activated", false)
                columns("id", "email", "password", "super_admin", "email_verified")
                values(1L, "john@mail.com", "foo", false, false)
                values(2L, "jane@mail.com", "foo", false, false)
            }

            val farInTheFuture = Instant.parse("2100-01-01T00:00:00Z")
            val inThePast = Instant.parse("2018-01-01T00:00:00Z")
            insertInto("email_verification") {
                columns("id", "email", "secret_token", "validity_instant", "user_id")
                values(11L, "john@mail.com", "secret1", farInTheFuture, 1L)
                values(12L, "john@mail.com", "secret2", inThePast, 1L)
                values(21L, "jane@mail.com", "secret3", inThePast, 2L)
            }
        }
    }

    @Test
    fun `should find valid by user and token`() {
        skipNextLaunch()
        // bad user ID
        assertThat(dao.findValidByUserAndToken(123456L, "secret1")).isNull()
        // bad secret
        assertThat(dao.findValidByUserAndToken(1L, "badSecret")).isNull()
        // invalid
        assertThat(dao.findValidByUserAndToken(1L, "secret2")).isNull()
        // ok
        assertThat(dao.findValidByUserAndToken(1L, "secret1")?.id).isEqualTo(11L)
    }

    @Test
    fun `should delete by user`() {
        assertThat(dao.findAll()).hasSize(3)
        dao.deleteByUser(User().apply { id = 2L })
        assertThat(dao.findAll()).hasSize(2)
        dao.deleteByUser(User().apply { id = 1L })
        assertThat(dao.findAll()).isEmpty()
    }
}
