package net.adullact.directmairie.dao

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

/**
 * Automated tests for [IssueGroupDao]
 * @author JB Nizet
 */
class IssueGroupDaoTest(@Autowired val dao: IssueGroupDao) : BaseDaoTest() {

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("issue_group") {
                columns("id", "label")
                values(1L, "Other")
                values(2L, "Roads")
            }
        }
    }

    @Test
    fun `should find by label`() {
        skipNextLaunch()
        assertThat(dao.findByLabel("Roads")).isNotNull()
        assertThat(dao.findByLabel("NotExisting")).isNull()
    }
}
