package net.adullact.directmairie.dao

import com.ninja_squad.dbsetup.DbSetupTracker
import com.ninja_squad.dbsetup_kotlin.DbSetupBuilder
import com.ninja_squad.dbsetup_kotlin.dbSetup
import com.ninja_squad.dbsetup_kotlin.launchWith
import net.adullact.directmairie.config.PersistenceConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.annotation.Rollback
import org.springframework.test.context.TestPropertySource
import javax.sql.DataSource

/**
 * Base class for DAO tests
 * @author JB Nizet
 */
@DataJpaTest
@Import(PersistenceConfig::class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource("/test.properties")
@Rollback(false)
abstract class BaseDaoTest {

    @Autowired
    private lateinit var dataSource: DataSource

    protected fun setup(configure: DbSetupBuilder.() -> Unit) {
        dbSetup(to = dataSource) {
            deleteAllFrom(
                "subscription",
                "issue_picture",
                "picture",
                "issue_transition",
                "issue",
                "government_issue_category",
                "government_contact",
                "issue_category",
                "issue_group",
                "government_administrator",
                "government",
                "pooling_organization_administrator",
                "pooling_organization",
                "email_verification",
                "auser"
            )

            configure()
        }.launchWith(TRACKER)
    }

    protected fun skipNextLaunch() {
        TRACKER.skipNextLaunch()
    }

    companion object {
        val TRACKER = DbSetupTracker()
    }
}
