package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

/**
 * Automated tests for [PoolingOrganizationDao]
 * @author JB Nizet
 */
class PoolingOrganizationDaoTest(@Autowired val dao: PoolingOrganizationDao) : BaseDaoTest() {

    val johnAdullactId = 1L
    val janeBretagneId = 2L
    val adullactId = 1L
    val bretagneId = 2L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("auser") {
                columns("id", "email", "password")
                withDefaultValue("super_admin", false)
                withDefaultValue("email_verified", true)
                withDefaultValue("issue_transition_notification_activated", false)
                values(johnAdullactId, "john@mail.com", "foo")
                values(janeBretagneId, "jane@mail.com", "foo")
            }
            insertInto("pooling_organization") {
                columns("id", "name")
                values(adullactId, "adullact")
                values(bretagneId, "Bretagne")
            }
            insertInto("pooling_organization_administrator") {
                columns("pooling_organization_id", "user_id")
                values(adullactId, johnAdullactId)
                values(bretagneId, janeBretagneId)
            }
        }
    }

    @Test
    fun `should find by query and order by case insensitive`() {
        skipNextLaunch()
        var result = dao.findByQuery("")
        assertThat(result.map(PoolingOrganization::id)).containsExactly(adullactId, bretagneId)

        result = dao.findByQuery("A")
        assertThat(result.map(PoolingOrganization::id)).containsExactly(adullactId, bretagneId)

        result = dao.findByQuery("UlL")
        assertThat(result.map(PoolingOrganization::id)).containsExactly(adullactId)
    }

    @Test
    fun `should find by query and user and order by case insensitive`() {
        skipNextLaunch()
        val johnAdullact = User().apply {
            id = johnAdullactId
        }
        val janeBretagne = User().apply {
            id = janeBretagneId
        }
        var result = dao.findByQueryAndAdministrator("", johnAdullact)
        assertThat(result.map(PoolingOrganization::id)).containsExactly(adullactId)

        result = dao.findByQueryAndAdministrator("A", janeBretagne)
        assertThat(result.map(PoolingOrganization::id)).containsExactly(bretagneId)

        result = dao.findByQueryAndAdministrator("UlL", johnAdullact)
        assertThat(result.map(PoolingOrganization::id)).containsExactly(adullactId)

        result = dao.findByQueryAndAdministrator("UlL", janeBretagne)
        assertThat(result.map(PoolingOrganization::id)).isEmpty()
    }

    @Test
    fun `should find by name`() {
        skipNextLaunch()
        assertThat(dao.findByName("adullact")).isNotNull()
        assertThat(dao.findByName("notExisting")).isNull()
    }
}
