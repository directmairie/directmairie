package net.adullact.directmairie.dao

import com.ninja_squad.dbsetup.generator.ValueGenerators
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

/**
 * Automated tests for [IssueCategoryDao]
 * @author JB Nizet
 */
class IssueCategoryDaoTest(@Autowired val dao: IssueCategoryDao) : BaseDaoTest() {

    val franceId = 1L
    val lyonId = 2L

    val franceOsmId = 1001L
    val lyonOsmId = 1002L

    val otherCategoryId = OTHER_CATEGORY_ID
    val lightingCategoryId = 2L
    val highwaysCategoryId = 3L
    val tagsCategoryId = 4L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("issue_group") {
                columns("id", "label")
                values(1L, "Other")
            }

            insertInto("issue_category") {
                columns("id", "label", "description_required")
                withDefaultValue("group_id", 1L)
                values(otherCategoryId, "Other", true)
                values(lightingCategoryId, "Lighting", false)
                values(highwaysCategoryId, "Highways", true)
                values(tagsCategoryId, "Tags", false)
            }

            insertInto("pooling_organization") {
                columns("id", "name")
                values(1L, "Adullact")
            }

            insertInto("government") {
                columns("id", "name", "osm_id")
                withDefaultValue("pooling_organization_id", 1L)
                withDefaultValue("contact_email", "dummy@test.fr")
                withGeneratedValue("code", ValueGenerators.stringSequence("code"))
                values(franceId, "France", franceOsmId)
                values(lyonId, "Lyon", lyonOsmId)
            }

            insertInto("government_issue_category") {
                columns("government_id", "category_id")
                values(franceId, otherCategoryId)
                values(franceId, highwaysCategoryId)
                values(lyonId, otherCategoryId)
                values(lyonId, lightingCategoryId)
            }
        }
    }

    @Test
    fun `should find by government OSM ids`() {
        skipNextLaunch()
        val unexistingOsmId = 98765L
        val result = dao.findByGovernmentOsmIds(setOf(franceOsmId, lyonOsmId, unexistingOsmId))
        assertThat(result.map(IssueCategory::id)).containsOnly(otherCategoryId, lightingCategoryId, highwaysCategoryId)
    }

    @Test
    fun `should find by label`() {
        skipNextLaunch()
        assertThat(dao.findByLabel("Lighting")).isNotNull();
        assertThat(dao.findByLabel("NotExisting")).isNull();
    }
}
