package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest

/**
 * Automated tests for [GovernmentDao]
 * @author JB Nizet
 */
class GovernmentDaoTest(@Autowired val dao: GovernmentDao) : BaseDaoTest() {

    val franceId = 1L
    val lyonId = 2L
    val laTourDuPinId = 3L

    val franceOsmId = 1001L
    val lyonOsmId = 1002L
    val laTourDuPinOsmId = 1003L

    val poolingOrgId = 1L
    val poolingOrgAdminId = 4L
    val lyonAdminId = 5L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("pooling_organization") {
                columns("id", "name")
                values(poolingOrgId, "Adullact")
            }

            insertInto("auser") {
                columns("id", "email")
                withDefaultValue("email_verified", true)
                withDefaultValue("password", "password")
                withDefaultValue("super_admin", false)
                withDefaultValue("issue_transition_notification_activated", false)
                values(poolingOrgAdminId, "admin@pooling.fr")
                values(lyonAdminId, "admin@lyon.fr")
            }

            insertInto("pooling_organization_administrator") {
                columns("pooling_organization_id", "user_id")
                values(poolingOrgId, poolingOrgAdminId)
            }

            insertInto("government") {
                columns("id", "name", "code", "osm_id", "contact_email")
                withDefaultValue("pooling_organization_id", poolingOrgId)
                values(franceId, "France", "abcd", franceOsmId, "contact@france.fr")
                values(lyonId, "Lyon", "cdef", lyonOsmId, "contact@lyon.fr")
                values(laTourDuPinId, "la Tour du Pin", "efgh", laTourDuPinOsmId, "contact@latourdupin.fr")
            }

            insertInto("government_administrator") {
                columns("government_id", "user_id")
                values(lyonId, lyonAdminId)
            }
        }
    }

    @Test
    fun `should find by OSM ids`() {
        skipNextLaunch()
        val unexistingOsmId = 98765L
        val result = dao.findByOsmIds(setOf(franceOsmId, lyonOsmId, unexistingOsmId))
        assertThat(result.map(Government::id)).containsOnly(franceId, lyonId)
    }

    @Test
    fun `should find by query and order by case insensitive`() {
        skipNextLaunch()
        val page = PageRequest.of(0, 20)

        var result = dao.findByQuery("", page)
        assertThat(result.map(Government::id)).containsExactly(franceId, laTourDuPinId, lyonId)

        result = dao.findByQuery("o", page)
        assertThat(result.map(Government::id)).containsExactly(laTourDuPinId, lyonId)

        result = dao.findByQuery("A", page)
        assertThat(result.map(Government::id)).containsExactly(franceId, laTourDuPinId)

        result = dao.findByQuery("CD", page)
        assertThat(result.map(Government::id)).containsExactly(franceId, lyonId)

        // correctly derives the count query
        result = dao.findByQuery("", PageRequest.of(0, 2))
        assertThat(result).hasSize(2)
    }

    @Test
    fun `should find by query and user and order by case insensitive`() {
        skipNextLaunch()
        val poolingOrgAdmin = User().apply { id = poolingOrgAdminId }
        val lyonAdmin = User().apply { id = lyonAdminId }
        val randomUser = User().apply { id = 765L }

        val page = PageRequest.of(0, 20)

        var result = dao.findByQueryAndUser("", randomUser, page)
        assertThat(result).isEmpty()

        result = dao.findByQueryAndUser("o", poolingOrgAdmin, page)
        assertThat(result.map(Government::id)).containsExactly(laTourDuPinId, lyonId)

        result = dao.findByQueryAndUser("A", poolingOrgAdmin, page)
        assertThat(result.map(Government::id)).containsExactly(franceId, laTourDuPinId)

        result = dao.findByQueryAndUser("CD", poolingOrgAdmin, page)
        assertThat(result.map(Government::id)).containsExactly(franceId, lyonId)

        result = dao.findByQueryAndUser("CD", lyonAdmin, page)
        assertThat(result.map(Government::id)).containsExactly(lyonId)

        // correctly derives the count query
        result = dao.findByQueryAndUser("", poolingOrgAdmin, PageRequest.of(0, 2))
        assertThat(result).hasSize(2)
    }

    @Test
    fun `should find by osm id`() {
        assertThat(dao.findByOsmId(franceOsmId)).isNotNull()
        assertThat(dao.findByOsmId(987654321L)).isNull()
    }

    @Test
    fun `should find by code`() {
        assertThat(dao.findByCode("abcd")).isNotNull()
        assertThat(dao.findByCode("notExisting")).isNull()
    }
}
