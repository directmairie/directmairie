package net.adullact.directmairie.dao

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest

/**
 * Automated tests for [UserDao]
 * @author JB Nizet
 */
class UserDaoTest(@Autowired val dao: UserDao) : BaseDaoTest() {

    val johnVerifiedId = 1L
    val janeId = 2L
    val janeVerifiedId = 3L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("auser") {
                columns("id", "email", "password", "super_admin", "email_verified")
                withDefaultValue("issue_transition_notification_activated", false)
                values(johnVerifiedId, "john@mail.com", "foo", false, true)
                values(janeId, "jane@mail.com", "foo", false, false)
                values(janeVerifiedId, "jane.verified@mail.com", "foo", false, true)
            }
        }
    }

    @Test
    fun `should find by email`() {
        skipNextLaunch()
        assertThat(dao.findByEmail("john@mail.com")).isNotNull()
        assertThat(dao.findByEmail("JOHN@mail.com")).isNotNull()
        assertThat(dao.findByEmail("jane@mail.com")).isNotNull()
    }

    @Test
    fun `should find by verified email`() {
        skipNextLaunch()
        assertThat(dao.findByVerifiedEmail("john@mail.com")).isNotNull()
        assertThat(dao.findByVerifiedEmail("JOHN@mail.com")).isNotNull()
        assertThat(dao.findByVerifiedEmail("jane@mail.com")).isNull()
    }

    @Test
    fun `should search by verified email and order by email`() {
        skipNextLaunch()
        val page = PageRequest.of(0, 20)
        assertThat(dao.searchByVerifiedEmail("mail.com", page).map { it.id }).containsExactly(janeVerifiedId, johnVerifiedId)
        assertThat(dao.searchByVerifiedEmail("JOHN", page).map { it.id }).containsExactly(johnVerifiedId)
    }

    @Test
    fun `should find verified by ID`() {
        skipNextLaunch()

        val exceptionSupplier = { NotFoundException() }

        assertThat(dao.findVerifiedByIdOrThrow(johnVerifiedId, exceptionSupplier)).isNotNull()

        assertThatExceptionOfType(NotFoundException::class.java).isThrownBy {
            dao.findVerifiedByIdOrThrow(janeId, exceptionSupplier)
        }
    }
}
