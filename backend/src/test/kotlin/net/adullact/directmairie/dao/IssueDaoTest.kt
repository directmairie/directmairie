package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.GeoCoordinates
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.IssueStatus.*
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest
import org.springframework.test.annotation.Rollback
import java.time.Instant

/**
 * Automated tests for [IssueDao] (and [BaseRepository])
 * @author JB Nizet
 */
class IssueDaoTest(@Autowired val dao: IssueDao) : BaseDaoTest() {

    val loireAdminId = 1L
    val rhoneAdminId = 2L
    val allAdminId = 3L
    val poolingOrgAdminId = 4L
    val superAdminId = 5L
    val creatorId = 6L
    val loireId = 1L
    val rhoneId = 2L
    val adullactId = 1L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("auser") {
                columns("id", "email", "password", "super_admin")
                withDefaultValue("email_verified", true)
                withDefaultValue("issue_transition_notification_activated", false)
                values(loireAdminId, "admin@loire.fr", "loire", false)
                values(rhoneAdminId, "admin@rhone.fr", "rhone", false)
                values(allAdminId, "admin@all.fr", "all", false)
                values(poolingOrgAdminId, "admin@pooling.fr", "poolingOrg", false)
                values(superAdminId, "superadmin@adullact.org", "superAmin", true)
                values(creatorId, "creator@lambda.org", "creator", false)
            }

            insertInto("pooling_organization") {
                columns("id", "name")
                values(adullactId, "Adullact")
            }

            insertInto("government") {
                columns("id", "name", "code", "osm_id")
                withDefaultValue("pooling_organization_id", adullactId)
                withDefaultValue("contact_email", "dummy@test.fr")
                values(loireId, "Loire", "loire", 7420L)
                values(rhoneId, "Rhone", "rhone", 7378L)
            }

            insertInto("government_administrator") {
                columns("government_id", "user_id")
                values(loireId, loireAdminId)
                values(rhoneId, rhoneAdminId)
                values(loireId, allAdminId)
                values(rhoneId, allAdminId)
            }

            insertInto("pooling_organization_administrator") {
                columns("pooling_organization_id", "user_id")
                values(adullactId, poolingOrgAdminId)
            }

            insertInto("issue") {
                columns(
                    "id",
                    "status",
                    "creation_instant",
                    "description",
                    "latitude",
                    "longitude",
                    "assigned_government_id",
                    "creator_id"
                )
                values(
                    1L,
                    DRAFT,
                    Instant.parse("2019-01-07T13:00:00Z"),
                    "tower fell down",
                    48.858624,
                    2.294537,
                    loireId,
                    creatorId
                )
                values(
                    2L,
                    CREATED,
                    Instant.parse("2019-01-08T14:00:00Z"),
                    "some guy fell",
                    48.858624,
                    2.294537,
                    rhoneId,
                    creatorId
                )
                values(
                    3L,
                    REJECTED,
                    Instant.parse("2019-01-09T15:00:00Z"),
                    "spam",
                    48.858624,
                    2.294537,
                    rhoneId,
                    creatorId
                )
                values(
                    4L,
                    RESOLVED,
                    Instant.parse("2019-01-10T16:00:00Z"),
                    "tower fell down",
                    48.858624,
                    2.294537,
                    loireId,
                    null
                )
            }
        }
    }

    @Test
    fun `should find by id`() {
        skipNextLaunch()
        val optionalIssue = dao.findById(1L)
        assertThat(optionalIssue).isPresent()
        optionalIssue.get().apply {
            assertThat(id).isEqualTo(1L)
            assertThat(creationInstant).isEqualTo(Instant.parse("2019-01-07T13:00:00Z"))
            assertThat(description).isEqualTo("tower fell down")
            assertThat(coordinates).isEqualTo(GeoCoordinates(48.858624, 2.294537))
        }
    }

    @Test
    fun `should find by id or null`() {
        skipNextLaunch()
        assertThat(dao.findByIdOrNull(1L)).isNotNull()
        assertThat(dao.findByIdOrNull(987654L)).isNull()
    }

    @Test
    @Rollback
    fun `should find by id or throw`() {
        skipNextLaunch()
        assertThat(dao.findByIdOrThrow(1L)).isNotNull()
        assertThatExceptionOfType(NotFoundException::class.java).isThrownBy { dao.findByIdOrThrow(987654L) }
    }

    @Test
    fun `should insert`() {
        val issue = Issue().apply {
            creationInstant = Instant.now()
            coordinates = GeoCoordinates(48.858624, 2.294537)
        }
        dao.saveAndFlush(issue)

        assertThat(issue.id).isGreaterThanOrEqualTo(1000L)
    }

    @Test
    fun `should find by criteria, ordered by creation instant desc`() {
        skipNextLaunch()
        val page = PageRequest.of(0, 20)
        val loireAdmin = User().apply {
            id = loireAdminId
        }
        val rhoneAdmin = User().apply {
            id = rhoneAdminId
        }
        val allAdmin = User().apply {
            id = allAdminId
        }
        val poolingOrgAdmin = User().apply {
            id = poolingOrgAdminId
        }
        val superAdmin = User().apply {
            id = superAdminId
            superAdmin = true
        }

        // all statuses and superadmin
        var result = dao.findByCriteria(IssueSearchCriteria(user = superAdmin, statuses = IssueStatus.values().toSet()), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L, 1L)

        // non-draft statuses and all governments
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        // non-draft statuses and pooling organization
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin, poolingOrganizationIds = setOf(adullactId)), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        // non-draft statuses and unexisting pooling organization
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin, poolingOrganizationIds = setOf(345678L)), page)

        assertThat(result).isEmpty()

        // non-draft statuses and pooling organization and government
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin, poolingOrganizationIds = setOf(adullactId), governmentIds = setOf(loireId)), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        // non-draft statuses and pooling organization admin
        result = dao.findByCriteria(IssueSearchCriteria(user = poolingOrgAdmin), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        // one status and all governments
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin, statuses = setOf(CREATED)), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(2L)

        // non-draft statuses and government admin
        result = dao.findByCriteria(IssueSearchCriteria(user = loireAdmin), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L)

        // one status and government admin with an issue
        result = dao.findByCriteria(IssueSearchCriteria(user = rhoneAdmin, statuses = setOf(CREATED)), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(2L)

        // one status and government admin with no issue
        result = dao.findByCriteria(IssueSearchCriteria(user = loireAdmin, statuses = setOf(CREATED)), page)

        assertThat(result).isEmpty()

        // correctly derives the count query
        result = dao.findByCriteria(IssueSearchCriteria(user = allAdmin), PageRequest.of(0, 1))
        assertThat(result).hasSize(1)
    }

    @Test
    fun `should find by creator and statuses, ordered by creation instant desc`() {
        skipNextLaunch()
        val page = PageRequest.of(0, 20)
        val creator = User().apply {
            id = creatorId
        }

        // all statuses
        var result = dao.findByCreatorAndStatuses(creator, IssueStatus.values().toSet(), page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(3L, 2L, 1L)

        // non-draft statuses
        result = dao.findByCreatorAndStatuses(creator, IssueStatus.nonDraft, page)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(3L, 2L)

        // correctly derives the count query
        result = dao.findByCreatorAndStatuses(creator, IssueStatus.nonDraft, PageRequest.of(0, 1))
        assertThat(result).hasSize(1)
    }

    @Test
    fun `should find non draft by interval and user, ordered by creation instant desc`() {
        skipNextLaunch()
        val loireAdmin = User().apply {
            id = loireAdminId
        }
        val allAdmin = User().apply {
            id = allAdminId
        }
        val poolingOrgAdmin = User().apply {
            id = poolingOrgAdminId
        }

        var from = Instant.parse("2019-01-01T00:00:00Z")
        var to = Instant.parse("2019-02-01T00:00:00Z")

        // all governments
        var result = dao.findNonDraftByIntervalAndUser(from, to, allAdmin)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        from = Instant.parse("2019-01-09T00:00:00Z")
        // all governments
        result = dao.findNonDraftByIntervalAndUser(from, to, allAdmin)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L)

        from = Instant.parse("2019-01-01T00:00:00Z")
        to = Instant.parse("2019-01-10T00:00:00Z")
        // all governments
        result = dao.findNonDraftByIntervalAndUser(from, to, allAdmin)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(3L, 2L)

        to = Instant.parse("2019-02-01T00:00:00Z")
        // pooling organization admin
        result = dao.findNonDraftByIntervalAndUser(from, to, poolingOrgAdmin)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        // government admin
        result = dao.findNonDraftByIntervalAndUser(from, to, loireAdmin)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L)
    }

    @Test
    fun `should find non draft by interval, ordered by creation instant desc`() {
        skipNextLaunch()
        var from = Instant.parse("2019-01-01T00:00:00Z")
        var to = Instant.parse("2019-02-01T00:00:00Z")

        var result = dao.findNonDraftByInterval(from, to)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L, 2L)

        from = Instant.parse("2019-01-09T00:00:00Z")
        result = dao.findNonDraftByInterval(from, to)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(4L, 3L)

        from = Instant.parse("2019-01-01T00:00:00Z")
        to = Instant.parse("2019-01-10T00:00:00Z")
        result = dao.findNonDraftByInterval(from, to)

        assertThat(result).extracting<Long>(Issue::id).containsExactly(3L, 2L)
    }

    @Test
    fun `should anonymize by creator`() {
        val creator = User().apply { id = creatorId }
        dao.anonymizeByCreator(creator)
        assertThat(dao.findByCreatorAndStatuses(creator, IssueStatus.values().toSet(), PageRequest.of(0, 100))).isEmpty()
    }
}
