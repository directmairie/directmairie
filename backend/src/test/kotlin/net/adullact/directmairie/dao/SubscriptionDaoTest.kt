package net.adullact.directmairie.dao

import com.ninja_squad.dbsetup.generator.ValueGenerators
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageRequest

/**
 * Automated tests for [SubscriptionDao]
 * @author JB Nizet
 */
class SubscriptionDaoTest(@Autowired val dao: SubscriptionDao) : BaseDaoTest() {

    val lyonId = 1L
    val loireId = 2L

    @BeforeEach
    fun prepare() {
        setup {
            insertInto("auser") {
                columns("id", "email")
                withDefaultValue("email_verified", true)
                withDefaultValue("password", "password")
                withDefaultValue("super_admin", false)
                withDefaultValue("issue_transition_notification_activated", false)

                (1..20).forEach {
                    values(it.toLong(), "user${it}@mail.com")
                }
            }

            insertInto("pooling_organization") {
                columns("id", "name")
                values(1L, "Adullact")
            }

            insertInto("government") {
                columns("id", "name", "code", "osm_id", "contact_email")
                withDefaultValue("pooling_organization_id", 1L)
                values(lyonId, "Lyon", "cdef", 69L, "contact@lyon.fr")
                values(loireId, "Loire", "qskjd", 42L, "contact@loire.fr")
            }

            insertInto("subscription") {
                columns("user_id")
                withDefaultValue("government_id", lyonId)
                withGeneratedValue("id", ValueGenerators.sequence())
                (1..20).forEach {
                    if (it % 2 == 0) {
                        values(it)
                    }
                }
            }
        }
    }

    @Test
    fun `should find first batch of recipients`() {
        skipNextLaunch()
        val batch = dao.batchOfRecipients(lyonId, 5, null)
        assertThat(batch).containsExactly(
            Recipient(2L, "user2@mail.com"),
            Recipient(4L, "user4@mail.com"),
            Recipient(6L, "user6@mail.com"),
            Recipient(8L, "user8@mail.com"),
            Recipient(10L, "user10@mail.com"),
        )
    }

    @Test
    fun `should find next batch of recipients`() {
        skipNextLaunch()
        val batch = dao.batchOfRecipients(lyonId, 5, 10L)
        assertThat(batch).containsExactly(
            Recipient(12L, "user12@mail.com"),
            Recipient(14L, "user14@mail.com"),
            Recipient(16L, "user16@mail.com"),
            Recipient(18L, "user18@mail.com"),
            Recipient(20L, "user20@mail.com"),
        )
    }

    @Test
    fun `should return empty batch if no government`() {
        skipNextLaunch()
        val batch = dao.batchOfRecipients(345678L, 5, null)
        assertThat(batch).isEmpty()
    }

    @Test
    fun `should suggest governments`() {
        skipNextLaunch()

        // query doesn't match anything
        var result = dao.suggestGovernments("abcd", 1, 10)
        assertThat(result).isEmpty()

        // query matches loire and lyon, user 1 has no subscription
        result = dao.suggestGovernments("l", 1, 10)
        assertThat(result.map { it.name }).containsExactly("Loire", "Lyon")

        // query matches loire and lyon, user 2 is subscribed to lyon
        result = dao.suggestGovernments("l", 2, 10)
        assertThat(result.map { it.name }).containsExactly("Loire")
    }
}
