package net.adullact.directmairie.web.government

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Picture
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.issue.PictureFileService
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.image.IMAGE_SVG
import net.adullact.directmairie.web.util.image.imageExtension
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.byLessThan
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.header
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * MVC tests for [GovernmentLogoController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(GovernmentLogoController::class)
class GovernmentLogoControllerMvcTest(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockPictureFileService: PictureFileService

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @BeforeEach
    fun prepare() {
        // may update logo by default
        every { mockAuthorizations.atLeastPoolingOrganizationAdmin(any()) } returns true
    }

    @Nested
    inner class GetLogoBytes {

        lateinit var government: Government
        lateinit var picture: Picture

        @BeforeEach
        fun prepare() {
            picture = Picture().apply {
                id = 421L
                path = "0/1/2/3.jpg"
                contentType = MediaType.IMAGE_JPEG_VALUE
            }
            government = Government().apply {
                id = 42L
                logo = picture
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government
            every { mockPictureFileService.toResource(picture.path) } returns ByteArrayResource("hello".toByteArray())
        }

        @Test
        fun `should get the bytes with the content type of the logo`() {
            mockMvc.perform(
                get("/api/governments/{governmentId}/logo/{pictureId}/bytes", government.id, picture.id)
            )
                .andExpect(status().isOk)
                .andExpect(content().bytes("hello".toByteArray()))
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
                .andExpect(header().longValue("Content-Length", "hello".length.toLong()))
        }

        @Test
        fun `should not get the bytes if picture is not the logo of the government`() {
            val otherPictureId = 345678L
            mockMvc.perform(
                get("/api/governments/{governmentId}/pictures/{pictureId}/bytes", government.id, otherPictureId)
            )
                .andExpect(status().isNotFound)
        }

        @Test
        fun `should not get the bytes if government has no logo`() {
            government.logo = null
            mockMvc.perform(get("/api/governments/{governmentId}/pictures/{pictureId}/bytes", government.id, 421L))
                .andExpect(status().isNotFound)
        }

        @Test
        fun `should not get the bytes if file is not present`() {
            val absentResource = mockk<Resource>()
            every { absentResource.exists() } returns false
            every { mockPictureFileService.toResource(picture.path) } returns absentResource

            mockMvc.perform(
                get("/api/governments/{governmentId}/pictures/{pictureId}/bytes", government.id, picture.id)
            )
                .andExpect(status().isNotFound)
        }
    }

    @Nested
    inner class Delete {
        lateinit var government: Government
        lateinit var picture: Picture

        @BeforeEach
        fun prepare() {
            picture = Picture().apply {
                id = 421L
                path = "0/1/2/3.jpg"
            }
            government = Government().apply {
                id = 42L
                logo = picture
                poolingOrganization = PoolingOrganization()
            }
            every { mockGovernmentDao.findByIdOrNull(government.id!!) } returns government
        }

        @Test
        fun `should delete logo and its file`() {
            mockMvc.perform(delete("/api/governments/{governmentId}/logo/{pictureId}", government.id, picture.id))
                .andExpect(status().isNoContent)

            verify { mockPictureFileService.delete(picture.path) }
            verify { mockAuthorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization) }

            assertThat(government.logo).isNull()
        }

        @Test
        fun `should throw when user may not update government`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization) } returns false
            mockMvc.perform(delete("/api/governments/{governmentId}/logo/{pictureId}", government.id, picture.id))
                .andExpect(status().isForbidden)

            verify(inverse = true) { mockPictureFileService.delete(picture.path) }

            assertThat(government.logo).isNotNull()
        }

        @Test
        fun `should do nothing if government has no logo`() {
            government.logo = null
            mockMvc.perform(delete("/api/governments/{governmentId}/logo/{pictureId}", government.id, picture.id))
                .andExpect(status().isNoContent)

            verify(inverse = true) { mockPictureFileService.delete(picture.path) }
        }

        @Test
        fun `should do nothing if government has other logo`() {
            government.logo = Picture().apply { id = 98754L }
            mockMvc.perform(delete("/api/governments/{governmentId}/logo/{pictureId}", government.id, picture.id))
                .andExpect(status().isNoContent)

            verify(inverse = true) { mockPictureFileService.delete(picture.path) }
        }
    }

    @Nested
    inner class Upload {
        lateinit var government: Government

        @BeforeEach
        fun prepare() {
            government = Government().apply {
                id = 42L
                poolingOrganization = PoolingOrganization()
            }
            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government
        }

        @Test
        fun `should upload logo with JPEG image`() {
            testUploadWithFormat(MediaType.IMAGE_JPEG)
        }

        @Test
        fun `should upload logo with PNG image`() {
            testUploadWithFormat(MediaType.IMAGE_PNG)
        }

        @Test
        fun `should upload logo with GIF image`() {
            testUploadWithFormat(MediaType.IMAGE_GIF)
        }

        @Test
        fun `should upload logo with SVG image`() {
            testUploadWithFormat(IMAGE_SVG)
        }

        fun testUploadWithFormat(format: MediaType) {
            every { mockGovernmentDao.flush() } answers { government.logo!!.id = 12345L }
            every {
                mockPictureFileService.idToRelativePath(
                    12345L,
                    format.imageExtension
                )
            } returns "0/1/2/12345.${format.imageExtension}"

            mockMvc.perform(
                multipart("/api/governments/{governmentId}/logo", government.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            format.toString(),
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(12345L))

            verify { mockAuthorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization) }

            val picture = government.logo!!
            assertThat(picture.creationInstant).isCloseTo(Instant.now(), byLessThan(1, ChronoUnit.MINUTES))
            assertThat(picture.path).isEqualTo("0/1/2/12345.${format.imageExtension}")
            assertThat(picture.contentType).isEqualTo(format.toString())
        }

        @Test
        fun `should throw when uploading a logo to a government that the user may not update`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization) } returns false

            mockMvc.perform(
                multipart("/api/governments/{governmentId}/logo", government.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            MediaType.IMAGE_JPEG_VALUE,
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isForbidden)

            assertThat(government.logo).isNull()
        }

        @Test
        fun `should replace the old logo and delete it`() {
            government.logo = Picture().apply {
                id = 421L
                path = "0/1/2/3.jpg"
            }

            every { mockGovernmentDao.flush() } answers { government.logo!!.id = 12345L }
            every { mockPictureFileService.idToRelativePath(12345L, "jpeg") } returns "0/1/2/12345.jpeg"

            mockMvc.perform(
                multipart("/api/governments/{governmentId}/logo", government.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            MediaType.IMAGE_JPEG_VALUE,
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(12345L))


            verify { mockPictureFileService.delete("0/1/2/3.jpg") }
        }
    }
}
