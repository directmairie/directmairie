package net.adullact.directmairie.web.password

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.config.SecurityPropertiesConfig
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.domain.generateSecretToken
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.test.USER_EMAIL
import net.adullact.directmairie.test.USER_ID
import net.adullact.directmairie.test.docPost
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieDocTest
import net.adullact.directmairie.web.user.JwtService
import net.adullact.directmairie.web.user.PasswordHasher
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

/**
 * Documentation tests for [PasswordController]
 * @author JB Nizet
 */
@DirectMairieDocTest(PasswordController::class)
@Import(PasswordHasher::class, JwtService::class, EventPublisher::class, SecurityPropertiesConfig::class)
@TestPropertySource("/test.properties")
class PasswordControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockEmailVerificationDao: EmailVerificationDao

    @Test
    fun lost() {
        val command = LostPasswordCommandDTO(USER_EMAIL)
        val user = User().apply {
            id = USER_ID
        }
        every { mockUserDao.findByVerifiedEmail(command.email) } returns user
        every { mockEmailVerificationDao.saveAndFlush(any<EmailVerification>()) } returnsModifiedFirstArg {
            id = 42L
        }

        mockMvc.perform(
            docPost("/api/passwords/lost")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "passwords/lost",
                    requestFields(
                        fieldWithPath("email").description("The email of the user who lost his/her password")
                    )
                )
            )
    }

    @Test
    fun change() {
        val command = PasswordModificationCommandDTO(
            password = "p4ssword",
            token = generateSecretToken()
        )
        val user = User().apply {
            id = USER_ID
        }
        val emailVerification = EmailVerification(
            email = USER_EMAIL,
            validityInstant = Instant.now(),
            secretToken = command.token,
            user = user
        )
        every {
            mockEmailVerificationDao.findValidByUserAndToken(
                USER_ID,
                command.token
            )
        } returns emailVerification

        mockMvc.perform(
            post("/api/passwords/{userId}/modifications", USER_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "passwords/change",
                    requestFields(
                        fieldWithPath("password").description("The new password chosen by the user"),
                        fieldWithPath("token").description("The secret token found in the link sent in the email")
                    ),
                    responseFields(
                        fieldWithPath("token").description("The JWT that can be used to authenticate the subsequent requests")
                    )
                )
            )
    }
}
