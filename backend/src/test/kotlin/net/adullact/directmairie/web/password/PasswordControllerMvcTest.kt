package net.adullact.directmairie.web.password

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.PasswordLost
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.user.JwtService
import net.adullact.directmairie.web.user.PasswordHasher
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * MVC tests for [PasswordController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(PasswordController::class)
class PasswordControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper)
{

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockEmailVerificationDao: EmailVerificationDao

    @MockkBean
    lateinit var mockEventPublisher: EventPublisher

    @MockkBean
    lateinit var mockPasswordHasher: PasswordHasher

    @MockkBean
    lateinit var mockJwtService: JwtService

    @Nested
    inner class Lost {
        @Test
        fun `should send a lost password request`() {
            val command = LostPasswordCommandDTO("john@mail.com")
            val user = User().apply {
                id = 42L
            }
            every { mockUserDao.findByVerifiedEmail(command.email) } returns user
            every { mockEmailVerificationDao.saveAndFlush(any<EmailVerification>()) } returnsModifiedFirstArg {
                id = 42L
            }

            mockMvc.perform(
                post("/api/passwords/lost")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            verify {
                mockEventPublisher.publish(PasswordLost(42L))
                mockEmailVerificationDao.saveAndFlush(withArg<EmailVerification> {
                    assertThat(it.user).isEqualTo(user)
                    assertThat(it.secretToken).isNotNull()
                    assertThat(it.validityInstant).isCloseTo(
                        Instant.now().plus(24, ChronoUnit.HOURS),
                        within(1, ChronoUnit.MINUTES)
                    )
                    assertThat(it.email).isEqualTo(command.email)
                })
            }
        }

        @Test
        fun `should throw bad request if email unknown`() {
            val command = LostPasswordCommandDTO("john@mail.com")
            every { mockUserDao.findByVerifiedEmail(command.email) } returns null

            mockMvc.perform(
                post("/api/passwords/lost")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            verify(inverse = true) {
                mockEmailVerificationDao.saveAndFlush(any<EmailVerification>())
                mockEventPublisher.publish(any())
            }
        }
    }

    @Nested
    inner class Change {
        @Test
        fun `should change password`() {
            val command = PasswordModificationCommandDTO(
                password = "passw0rd",
                token = "abc"
            )
            val user = User().apply {
                id = 42L
            }
            val emailVerification = EmailVerification(
                email = "john@mail.com",
                validityInstant = Instant.now(),
                secretToken = command.token,
                user = user
            )
            every {
                mockEmailVerificationDao.findValidByUserAndToken(
                    user.id!!,
                    command.token
                )
            } returns emailVerification

            val hashedPassword = "hashed"
            every { mockPasswordHasher.hash(command.password) } returns hashedPassword
            val jwt = "jwt"
            every { mockJwtService.buildToken(user.id!!) } returns jwt

            mockMvc.perform(
                post("/api/passwords/${user.id}/modifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.token").value(jwt))

            assertThat(user.password).isEqualTo(hashedPassword)

            verify { mockEmailVerificationDao.delete(emailVerification) }
        }

        @Test
        fun `should throw bad request if invalid token`() {
            val command = PasswordModificationCommandDTO(
                password = "passw0rd",
                token = "abc"
            )
            every { mockEmailVerificationDao.findValidByUserAndToken(42, command.token) } returns null

            mockMvc.perform(
                post("/api/passwords/42/modifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }
    }
}
