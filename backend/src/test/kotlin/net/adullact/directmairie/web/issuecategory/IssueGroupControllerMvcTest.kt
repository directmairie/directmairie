package net.adullact.directmairie.web.issuecategory

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.service.nominatim.DetailsAddress
import net.adullact.directmairie.service.nominatim.DetailsResult
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.service.nominatim.OsmType
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * MVC tests for [IssueGroupController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(IssueGroupController::class)
class IssueGroupControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val controller: IssueGroupController
) {
    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockNominatimService: NominatimService

    @Nested
    inner class Search {
        @Test
        fun `should search and sort groups and categories by label with 'other' in last position`() {
            val coordinates = GeoCoordinates(1.2, 3.4)

            every { mockNominatimService.details(coordinates) } returns DetailsResult(
                listOf(
                    DetailsAddress(osmId = null, osmType = null, adminLevel = null),
                    DetailsAddress(osmId = 1002L, osmType = OsmType.RELATION, adminLevel = 2),
                    DetailsAddress(osmId = 1004L, osmType = OsmType.RELATION, adminLevel = 4)
                )
            )

            val otherGroup = IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
            }
            val xGroup = IssueGroup().apply {
                id = 2
                label = "X"
            }
            val yGroup = IssueGroup().apply {
                id = 3
                label = "Y"
            }

            every { mockIssueCategoryDao.findByGovernmentOsmIds(setOf(1002L, 1004L)) } returns listOf(
                IssueCategory().apply {
                    id = OTHER_CATEGORY_ID
                    label = "Other"
                }.also { otherGroup.addCategory(it) },
                IssueCategory().apply {
                    id = 11
                    label = "ba"
                }.also { yGroup.addCategory(it) },
                IssueCategory().apply {
                    id = 12
                    label = "Bc"
                }.also { yGroup.addCategory(it) },
                IssueCategory().apply {
                    id = 13
                    label = "A"
                    descriptionRequired = true
                }.also { xGroup.addCategory(it) },
                IssueCategory().apply {
                    id = 14
                    label = "Z"
                    descriptionRequired = true
                }.also { otherGroup.addCategory(it) }
            )

            mockMvc.perform(
                get("/api/issue-groups")
                    .param("latitude", coordinates.latitude.toString())
                    .param("longitude", coordinates.longitude.toString())
            )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].label").value("X"))
                .andExpect(jsonPath("$[0].categories[0].id").value(13))
                .andExpect(jsonPath("$[0].categories[0].label").value("A"))
                .andExpect(jsonPath("$[0].categories[0].descriptionRequired").value(true))
                .andExpect(jsonPath("$[1].label").value("Y"))
                .andExpect(jsonPath("$[1].categories[0].label").value("ba"))
                .andExpect(jsonPath("$[1].categories[1].label").value("Bc"))
                .andExpect(jsonPath("$[2].label").value("Other"))
                .andExpect(jsonPath("$[2].categories[0].label").value("Z"))
                .andExpect(jsonPath("$[2].categories[1].label").value("Other"))

        }

        @Test
        fun `should return empty list if nominatim service returns no detail`() {
            val coordinates = GeoCoordinates(1.2, 3.4)

            every { mockNominatimService.details(coordinates) } returns DetailsResult(emptyList())

            assertThat(controller.search(coordinates.latitude, coordinates.longitude)).isEmpty()
            verify(inverse = true) { mockIssueCategoryDao.findByGovernmentOsmIds(any()) }
        }

        @Test
        fun `should list all groups and sort by label with 'other' in last position`() {
            every { mockIssueGroupDao.findAll() } returns listOf(
                IssueGroup().apply {
                    id = OTHER_ISSUE_GROUP_ID
                    label = "Other"
                    addCategory(IssueCategory().apply {
                        id = OTHER_CATEGORY_ID
                        label = "Other"
                    })
                    addCategory(IssueCategory().apply {
                        id = 2
                        label = "Z"
                    })
                },
                IssueGroup().apply {
                    id = 2
                    label = "X"
                }
            )

            mockMvc.perform(get("/api/issue-groups"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$").isArray)
                .andExpect(jsonPath("$[0].label").value("X"))
                .andExpect(jsonPath("$[1].label").value("Other"))
                .andExpect(jsonPath("$[1].categories[0].label").value("Z"))
                .andExpect(jsonPath("$[1].categories[1].label").value("Other"))
        }
    }

    @Nested
    inner class Get {
        @Test
        fun `should get group by ID`() {
            every { mockIssueGroupDao.findByIdOrThrow(OTHER_ISSUE_GROUP_ID) } returns IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
                addCategory(IssueCategory().apply {
                    id = OTHER_CATEGORY_ID
                    label = "Other"
                })
                addCategory(IssueCategory().apply {
                    id = 2
                    label = "Z"
                })
            }

            mockMvc.perform(get("/api/issue-groups/${OTHER_CATEGORY_ID}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(OTHER_ISSUE_GROUP_ID))
                .andExpect(jsonPath("$.label").value("Other"))
                .andExpect(jsonPath("$.categories[0].label").value("Z"))
                .andExpect(jsonPath("$.categories[1].label").value("Other"))
        }
    }

    @Nested
    inner class Create {
        @Test
        fun `should create group`() {
            val command = IssueGroupCommandDTO(label = "test")

            every { mockIssueGroupDao.findByLabel(command.label) } returns null
            every { mockIssueGroupDao.saveAndFlush(any<IssueGroup>()) } returnsModifiedFirstArg { id = 1000 }

            mockMvc.perform(
                post("/api/issue-groups")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000))
                .andExpect(jsonPath("$.label").value(command.label))
        }

        @Test
        fun `should throw when creating group with existing label`() {
            val command = IssueGroupCommandDTO(label = "test")
            every { mockIssueGroupDao.findByLabel(command.label) } returns IssueGroup()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.create(command) }
                .matches { it.errorCode == ErrorCode.ISSUE_GROUP_WITH_SAME_LABEL_ALREADY_EXISTS }
        }
    }

    @Nested
    inner class Update {
        @Test
        fun `should update group`() {
            val group = IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
            }
            every { mockIssueGroupDao.findByIdOrThrow(OTHER_ISSUE_GROUP_ID) } returns group

            val command = IssueGroupCommandDTO(label = "test")

            every { mockIssueGroupDao.findByLabel(command.label) } returns null

            mockMvc.perform(
                put("/api/issue-groups/${group.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(group.label).isEqualTo(command.label)
        }

        @Test
        fun `should throw when updating group with existing label`() {
            val group = IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
            }
            every { mockIssueGroupDao.findByIdOrThrow(OTHER_ISSUE_GROUP_ID) } returns group

            val command = IssueGroupCommandDTO(label = "test")
            every { mockIssueGroupDao.findByLabel(command.label) } returns IssueGroup()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.update(group.id!!, command) }
                .matches { it.errorCode == ErrorCode.ISSUE_GROUP_WITH_SAME_LABEL_ALREADY_EXISTS }
        }

        @Test
        fun `should not throw when updating group with unmodified label`() {
            val group = IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
            }
            every { mockIssueGroupDao.findByIdOrThrow(OTHER_ISSUE_GROUP_ID) } returns group

            val command = IssueGroupCommandDTO(label = group.label)
            every { mockIssueGroupDao.findByLabel(command.label) } returns group

            controller.update(group.id!!, command)
        }
    }

    @Nested
    inner class Delete {
        @Test
        fun `should delete group`() {
            val group = IssueGroup().apply {
                id = 123456L
                label = "foo"
            }
            every { mockIssueGroupDao.findByIdOrNull(group.id!!) } returns group

            mockMvc.perform(delete("/api/issue-groups/${group.id}"))
                .andExpect(status().isNoContent)

            verify { mockIssueGroupDao.delete(group) }
        }

        @Test
        fun `should delete non-existing group`() {
            val id = 123456L
            every { mockIssueGroupDao.findByIdOrNull(id) } returns null

            mockMvc.perform(delete("/api/issue-groups/$id"))
                .andExpect(status().isNoContent)

            verify(inverse = true) { mockIssueGroupDao.delete(any()) }
        }

        @Test
        fun `should not delete 'Other' group`() {
            val group = IssueGroup().apply {
                id = OTHER_ISSUE_GROUP_ID
                label = "Other"
            }
            every { mockIssueGroupDao.findByIdOrNull(OTHER_ISSUE_GROUP_ID) } returns group

            mockMvc.perform(delete("/api/issue-groups/$OTHER_ISSUE_GROUP_ID"))
                .andExpect(status().isBadRequest)

            verify(inverse = true) { mockIssueGroupDao.delete(any()) }
        }
    }
}
