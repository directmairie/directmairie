package net.adullact.directmairie.web.user

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.config.SecurityPropertiesConfig
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.USER_ID
import net.adullact.directmairie.test.USER_PASSWORD
import net.adullact.directmairie.test.web.DirectMairieDocTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Documentation tests for [AuthenticationController]
 * @author JB Nizet
 */
@DirectMairieDocTest(AuthenticationController::class)
@Import(PasswordHasher::class, JwtService::class, SecurityPropertiesConfig::class)
@TestPropertySource("/test.properties")
class AuthenticationControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val passwordHasher: PasswordHasher
) {

    @MockkBean
    lateinit var mockUserDao: UserDao

    @Test
    fun authenticate() {
        val command = AuthenticationCommandDTO("john@mail.com", "passw0rd")

        val user = User().apply {
            id = USER_ID
            password = passwordHasher.hash(USER_PASSWORD)
        }

        every { mockUserDao.findByVerifiedEmail(command.email) } returns user

        mockMvc.perform(MockMvcRequestBuilders.post("/api/authentications")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsBytes(command)))
            .andExpect(status().isOk)
            .andDo(
                document(
                    "users/authenticate",
                    requestFields(
                        fieldWithPath("email").description("The email of the user"),
                        fieldWithPath("password").description("The password of the user")
                    ),
                    responseFields(
                        fieldWithPath("token").description("The JWT that can be used to authenticate the subsequent requests")
                    )
                )
            )
    }
}
