package net.adullact.directmairie.web.issue

import net.adullact.directmairie.config.PictureProperties
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.mock.web.MockMultipartFile
import java.nio.file.Files

/**
 * Unit tests for [PictureFileService]
 * @author JB Nizet
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PictureFileServiceTest {
    lateinit var service: PictureFileService
    lateinit var pictureProperties: PictureProperties

    @BeforeEach
    fun prepare() {
        pictureProperties = PictureProperties(Files.createTempDirectory(null).toFile())
        service = PictureFileService(pictureProperties)
    }

    @AfterEach
    fun cleanup() {
        pictureProperties.rootDirectory.deleteRecursively()
    }

    fun picturePathsById(): List<Arguments> = listOf(
        0L to "0/0/0/0",
        50L to "0/0/0/50",
        100L to "0/0/1/100",
        150L to "0/0/1/150",
        200L to "0/0/2/200",
        950L to "0/0/9/950",
        9950L to "0/0/99/9950",
        10000L to "0/1/0/10000",
        10050L to "0/1/0/10050",
        10100L to "0/1/1/10100",
        19950L to "0/1/99/19950",
        20000L to "0/2/0/20000",
        999999L to "0/99/99/999999",
        1000000L to "1/0/0/1000000",
        2000000L to "2/0/0/2000000",
        3015999L to "3/1/59/3015999"
    ).map { it -> Arguments.arguments(it.first, it.second) }

    @ParameterizedTest
    @MethodSource("picturePathsById")
    fun `should compute picture path`(id: Long, expectedPath: String) {
        val extension = "jpg";
        assertThat(service.idToRelativePath(id, extension)).isEqualTo("$expectedPath.jpg")
    }

    @Test
    fun `should write and create parent directories if needed`() {
        val content = "hello".toByteArray()
        val file = MockMultipartFile("file", content)

        val relativePath = "0/1/2/2000.jpg"
        service.write(file, relativePath)

        assertThat(pictureProperties.rootPath.resolve(relativePath).toFile().readBytes()).isEqualTo(content)
    }

    @Test
    fun `should delete`() {
        val content = "hello".toByteArray()
        val file = MockMultipartFile("file", content)

        val relativePath = "0/1/2/2000.jpg"
        service.write(file, relativePath)

        service.delete(relativePath)
        assertThat(pictureProperties.rootPath.resolve(relativePath)).doesNotExist()
    }

    @Test
    fun `should transform to resource`() {
        val content = "hello".toByteArray()
        val file = MockMultipartFile("file", content)

        val relativePath = "0/1/2/2000.jpg"
        service.write(file, relativePath)

        val resource = service.toResource(relativePath)
        assertThat(resource.contentLength()).isEqualTo(content.size.toLong())
        assertThat(resource.exists()).isTrue()
        assertThat(resource.inputStream.readBytes()).isEqualTo(content)
    }
}
