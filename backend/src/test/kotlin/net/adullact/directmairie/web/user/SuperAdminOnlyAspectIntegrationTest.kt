package net.adullact.directmairie.web.user

import io.mockk.mockk
import net.adullact.directmairie.web.exception.UnauthorizedException
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.stereotype.Component

/**
 * Integration test to verify that calling a method annotated with [SuperAdminOnly] while not being super-admin
 * indeed triggers the aspect and throws an exception
 * @author JB Nizet
 */
@SpringBootTest
class SuperAdminOnlyAspectIntegrationTest {

    @SuperAdminOnly
    @Component
    class A {
        fun foo() {}
    }

    @Component
    class B {
        @SuperAdminOnly
        fun foo() {}
    }

    @Configuration
    @EnableAspectJAutoProxy
    class AlternateConfiguration {
        @Bean
        fun a() = A()

        @Bean
        fun b() = B()

        @Bean
        fun currentUser() = CurrentUser()

        @Bean
        fun superAdminOnlyAspect() = SuperAdminOnlyAspect(currentUser(), mockk())
    }

    @Autowired
    lateinit var a: A

    @Autowired
    lateinit var b: B

    @Test
    fun `should throw when calling super-admin only annotated method`() {
        assertThatExceptionOfType(UnauthorizedException::class.java).isThrownBy {
            b.foo()
        }
    }
    @Test
    fun `should throw when calling method of super-admin only annotated class`() {
        assertThatExceptionOfType(UnauthorizedException::class.java).isThrownBy {
            a.foo()
        }
    }
}
