package net.adullact.directmairie.web.user

import io.mockk.every
import io.mockk.mockk
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import jakarta.servlet.http.HttpServletRequest

/**
 * Unit tests for [Authorizations]
 * @author JB Nizet
 */
class AuthorizationsTest {
    val user = User().apply {
        id = 1L
    }

    val mockCurrentUser = mockk<CurrentUser> {
        every { userIdOrThrow } returns user.id!!
    }
    val mockUserDao = mockk<UserDao> {
        every { findByIdOrThrow(user.id!!, any()) } returns user
    }
    val mockRequest= mockk<HttpServletRequest>()

    val authorizations = Authorizations(mockCurrentUser, mockUserDao, mockRequest)

    @Nested
    inner class AtLeastGovernmentAdmin {
        @Test
        fun `should return false by default`() {
            val government = Government().apply {
                poolingOrganization = PoolingOrganization()
            }
            assertThat(authorizations.atLeastGovernmentAdmin(government)).isFalse()
        }

        @Test
        fun `should return true if super admin`() {
            val government = Government()
            user.superAdmin = true
            assertThat(authorizations.atLeastGovernmentAdmin(government)).isTrue()
        }

        @Test
        fun `should return true if pooling organization admin`() {
            val government = Government().apply {
                poolingOrganization = PoolingOrganization()
            }
            user.addAdministeredPoolingOrganization(government.poolingOrganization)

            assertThat(authorizations.atLeastGovernmentAdmin(government)).isTrue()
        }

        @Test
        fun `should return true if government admin`() {
            val government = Government().apply {
                poolingOrganization = PoolingOrganization()
            }
            user.addAdministeredGovernment(government)

            assertThat(authorizations.atLeastGovernmentAdmin(government)).isTrue()
        }
    }

    @Nested
    inner class AtLeastPoolingOrganizationAdmin {
        @Test
        fun `should return false by default`() {
            val poolingOrganization = PoolingOrganization()
            assertThat(authorizations.atLeastPoolingOrganizationAdmin(poolingOrganization)).isFalse()
        }

        @Test
        fun `should return true if super admin`() {
            val poolingOrganization = PoolingOrganization()
            user.superAdmin = true
            assertThat(authorizations.atLeastPoolingOrganizationAdmin(poolingOrganization)).isTrue()
        }

        @Test
        fun `should return true if pooling organization admin`() {
            val poolingOrganization = PoolingOrganization()
            user.addAdministeredPoolingOrganization(poolingOrganization)

            assertThat(authorizations.atLeastPoolingOrganizationAdmin(poolingOrganization)).isTrue()
        }
    }

    @Nested
    inner class AtLeastAnyPoolingOrganizationAdmin {
        @Test
        fun `should return false by default`() {
            assertThat(authorizations.atLeastAnyPoolingOrganizationAdmin()).isFalse()
        }

        @Test
        fun `should return true if super admin`() {
            user.superAdmin = true
            assertThat(authorizations.atLeastAnyPoolingOrganizationAdmin()).isTrue()
        }

        @Test
        fun `should return true if pooling organization admin`() {
            val poolingOrganization = PoolingOrganization()
            user.addAdministeredPoolingOrganization(poolingOrganization)

            assertThat(authorizations.atLeastAnyPoolingOrganizationAdmin()).isTrue()
        }
    }

    @Nested
    inner class Anonymous {
        @Test
        fun `should not be anonymous when there is a user`() {
            every { mockCurrentUser.userId } returns user.id!!
            assertThat(authorizations.isAnonymous).isFalse()
        }

        @Test
        fun `should be anonymous when there is no user`() {
            every { mockCurrentUser.userId } returns null
            assertThat(authorizations.isAnonymous).isTrue()
        }
    }

    @Nested
    inner class MayUpdateIssue() {
        @Test
        fun `should not access to issue if anonymous and no access token`() {
            every { mockCurrentUser.userId } returns null
            every { mockRequest.getHeader(ISSUE_ACCESS_TOKEN_HEADER) } returns null
            val issue = Issue()

            assertThat(authorizations.mayUpdateIssue(issue)).isFalse()
        }

        @Test
        fun `should not access to issue if anonymous and no access token in the issue`() {
            every { mockCurrentUser.userId } returns null
            every { mockRequest.getHeader(ISSUE_ACCESS_TOKEN_HEADER) } returns "token"
            val issue = Issue()

            assertThat(authorizations.mayUpdateIssue(issue)).isFalse()
        }

        @Test
        fun `should not access to issue if anonymous and access token in the issue different`() {
            every { mockCurrentUser.userId } returns null
            every { mockRequest.getHeader(ISSUE_ACCESS_TOKEN_HEADER) } returns "token"
            val issue = Issue().apply { accessToken = "different" }

            assertThat(authorizations.mayUpdateIssue(issue)).isFalse()
        }

        @Test
        fun `should access to issue if anonymous and access token in the issue identical`() {
            every { mockCurrentUser.userId } returns null
            every { mockRequest.getHeader(ISSUE_ACCESS_TOKEN_HEADER) } returns "token"
            val issue = Issue().apply { accessToken = "token" }

            assertThat(authorizations.mayUpdateIssue(issue)).isTrue()
        }

        @Test
        fun `should not access to issue if authenticated and issue has no creator`() {
            every { mockCurrentUser.userId } returns user.id!!
            val issue = Issue()

            assertThat(authorizations.mayUpdateIssue(issue)).isFalse()
        }

        @Test
        fun `should not access to issue if authenticated and issue has different creator`() {
            every { mockCurrentUser.userId } returns user.id!!
            val issue = Issue().apply { creator = User() }

            assertThat(authorizations.mayUpdateIssue(issue)).isFalse()
        }

        @Test
        fun `should access to issue if authenticated and issue has same creator`() {
            every { mockCurrentUser.userId } returns user.id!!
            val issue = Issue().apply { creator = user }

            assertThat(authorizations.mayUpdateIssue(issue)).isTrue()
        }
    }
}
