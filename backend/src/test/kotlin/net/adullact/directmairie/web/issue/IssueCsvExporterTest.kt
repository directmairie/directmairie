package net.adullact.directmairie.web.issue

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.web.user.Authorizations
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.time.Instant
import java.time.ZoneId
import java.util.stream.Stream

/**
 * Unit tests for IssueCsvExporter
 * @author JB Nizet
 */
class IssueCsvExporterTest {
    lateinit var mockIssueDao: IssueDao
    lateinit var mockAuthorizations: Authorizations
    lateinit var exporter: IssueCsvExporter

    val from = Instant.parse("2019-01-01T00:00:00Z")
    val to = Instant.parse("2019-02-01T00:00:00Z")
    val timezone = ZoneId.of("Europe/Paris")


    @BeforeEach
    fun prepare() {
        mockIssueDao = mockk()
        mockAuthorizations = mockk()
        exporter = IssueCsvExporter(mockIssueDao, mockAuthorizations)
    }

    @Test
    fun `should export all issues super admin`() {
        every { mockAuthorizations.currentUser } returns User().apply { superAdmin = true }
        every { mockIssueDao.findNonDraftByInterval(from, to) } returns Stream.empty()

        exporter.export(ByteArrayOutputStream(), from, to, timezone)

        verify { mockIssueDao.findNonDraftByInterval(from, to) }
    }

    @Test
    fun `should export issues visible to current user if not super admin`() {
        val user = User().apply { superAdmin = false }
        every { mockAuthorizations.currentUser } returns user
        every { mockIssueDao.findNonDraftByIntervalAndUser(from, to, user) } returns Stream.empty()

        exporter.export(ByteArrayOutputStream(), from, to, timezone)

        verify { mockIssueDao.findNonDraftByIntervalAndUser(from, to, user) }
    }

    @Test
    fun `should always export headers`() {
        every { mockAuthorizations.currentUser } returns User().apply { superAdmin = true }
        every { mockIssueDao.findNonDraftByInterval(from, to) } returns Stream.empty()

        val outputStream = ByteArrayOutputStream()
        exporter.export(outputStream, from, to, timezone)

        val records = outputStream.parse()

        assertThat(records).hasSize(1)
        with(records[0]) {
            assertThat(get(0)).isEqualTo("ID")
            assertThat(get(1)).isEqualTo("Status")
        }
    }

    @Test
    fun `should export issues`() {
        every { mockAuthorizations.currentUser } returns User().apply { superAdmin = true }
        every { mockIssueDao.findNonDraftByInterval(from, to) } returns Stream.of(
            createIssue(1000L),
            createIssue(1001L)
        )

        val outputStream = ByteArrayOutputStream()
        exporter.export(outputStream, from, to, timezone)

        val records = outputStream.parse()

        // all the same size
        assertThat(records).hasSize(3)
        assertThat(records.map { it.size() }.toSet()).hasSize(1)

        // IDs are correct
        assertThat(records[1][0]).isEqualTo("1000")
        assertThat(records[2][0]).isEqualTo("1001")

        // other than the first column, the two records should be identical
        (1 until records[0].size()).forEach { column ->
            assertThat(records[2][column]).isEqualTo(records[1][column])
        }

        // The creation instant should be formatted in the given time zone (which is one hour later than UTC)
        assertThat(records[1][2]).isEqualTo("2019-02-01T01:01:02")

        assertThat(records[1]).contains(
            "CREATED",
            "1.0",
            "2.0",
            "description\non several lines",
            "30",
            "Foo",
            "40",
            "Bar",
            "1024",
            "Loire",
            "6543",
            "codeLoire",
            "108",
            "ARA"
        )
    }

    fun createIssue(issueId: Long): Issue {
        return Issue().apply {
            id = issueId
            status = IssueStatus.CREATED
            creationInstant = Instant.parse("2019-02-01T00:01:02Z")
            coordinates = GeoCoordinates(1.0, 2.0)
            description = "description\non several lines"
            category = IssueCategory().apply {
                id = 30
                label = "Foo"
                group = IssueGroup().apply {
                    id = 40
                    label = "Bar"
                }
            }
            assignedGovernment = Government().apply {
                id = 1024L
                name = "Loire"
                osmId = 6543L
                code = "codeLoire"
                poolingOrganization = PoolingOrganization().apply {
                    id = 108L
                    name = "ARA"
                }
            }
        }
    }

    fun ByteArrayOutputStream.parse(): List<CSVRecord> {
        val bytes = toByteArray()
        val parser = CSVFormat.DEFAULT.parse(InputStreamReader(ByteArrayInputStream(bytes), IssueCsvExporter.CHARSET))
        return parser.records
    }
}
