package net.adullact.directmairie.web.user

import io.mockk.every
import io.mockk.mockk
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.exception.UnauthorizedException
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test

/**
 * Unit tests for [SuperAdminOnlyAspect]
 * @author JB Nizet
 */
class SuperAdminOnlyAspectTest {
    @Test
    fun `should do nothing if user is authenticated and super-admin`() {
        val currentUser = CurrentUser(1000L)
        val userDao = mockk<UserDao> {
            every { findByIdOrNull(currentUser.userId!!) } returns User().apply { superAdmin = true }
        }
        SuperAdminOnlyAspect(currentUser, userDao).checkUserIsSuperAdmin(null)
    }

    @Test
    fun `should throw if user is not authenticated`() {
        val currentUser = CurrentUser(null)
        assertThatExceptionOfType(UnauthorizedException::class.java).isThrownBy {
            SuperAdminOnlyAspect(currentUser, mockk()).checkUserIsSuperAdmin(null)
        }
    }

    @Test
    fun `should throw if user doesn't exist`() {
        val currentUser = CurrentUser(1000L)
        val userDao = mockk<UserDao> {
            every { findByIdOrNull(currentUser.userId!!) } returns null
        }
        assertThatExceptionOfType(UnauthorizedException::class.java).isThrownBy {
            SuperAdminOnlyAspect(currentUser, userDao).checkUserIsSuperAdmin(null)
        }
    }

    @Test
    fun `should throw if user is not super-admin`() {
        val currentUser = CurrentUser(1000L)
        val userDao = mockk<UserDao> {
            every { findByIdOrNull(currentUser.userId!!) } returns User()
        }
        assertThatExceptionOfType(ForbiddenException::class.java).isThrownBy {
            SuperAdminOnlyAspect(currentUser, userDao).checkUserIsSuperAdmin(null)
        }
    }
}
