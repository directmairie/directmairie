package net.adullact.directmairie.web.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.SubscriptionDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.Subscription
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.NotificationRequested
import net.adullact.directmairie.test.GIVORS_ADMIN_EMAIL
import net.adullact.directmairie.test.GIVORS_GOVERNMENT_ID
import net.adullact.directmairie.test.basicAuth
import net.adullact.directmairie.test.docPost
import net.adullact.directmairie.test.web.DirectMairieDocTest
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.issue.ReplaceAccessTokenPreprocessor
import net.adullact.directmairie.web.issue.generatedAccessToken
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.user.ISSUE_ACCESS_TOKEN_HEADER
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation
import org.springframework.restdocs.payload.PayloadDocumentation
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Documentation tests for [NotificationController]
 * @author JB Nizet
 */
@DirectMairieDocTest(NotificationController::class)
class NotificationControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {
    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockEventPublisher: EventPublisher

    val givors = Government().apply {
        id = GIVORS_GOVERNMENT_ID
        name = "Givors"
    }

    @BeforeEach
    fun prepare() {
        every { mockGovernmentDao.findByIdOrThrow(GIVORS_GOVERNMENT_ID, any()) } returns givors
        every { mockAuthorizations.atLeastGovernmentAdmin(givors) } returns true
    }

    @Nested
    inner class Send {
        @Test
        fun `should send notification`() {
            val command = NotificationCommandDTO(
                governmentId = GIVORS_GOVERNMENT_ID,
                subject = "New initiative to fill pot holes",
                message = "We decided to launch a project to fill all the pot holes in the city",
                url = "https://givors.fr/potholes"
            )
            mockMvc.perform(
                docPost("/api/notifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
                    .basicAuth(GIVORS_ADMIN_EMAIL)
            )
                .andExpect(status().isNoContent())
                .andDo(
                    MockMvcRestDocumentation.document(
                        "notifications/send",
                        requestFields(
                            fieldWithPath("governmentId").description(
                                "The ID of the government sending the email notification"
                            ),
                            fieldWithPath("subject").description(
                                "The subject of the email being sent. Must be less than ${MAX_SUBJECT_LENGTH} characters long"
                            ),
                            fieldWithPath("message").description(
                                "The message of the email being sent. Must be less than ${MAX_MESSAGE_LENGTH} characters long"
                            ),
                            fieldWithPath("url").description(
                                "The URL linking to a page allowing to have more details. Optional. if present, must be a valid URL"
                            )
                        )
                    )
                )

            verify { mockEventPublisher.publish(NotificationRequested(command)) }
        }
    }
}
