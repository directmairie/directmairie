package net.adullact.directmairie.web.issuecategory

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.domain.GeoCoordinates
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import net.adullact.directmairie.service.nominatim.DetailsAddress
import net.adullact.directmairie.service.nominatim.DetailsResult
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.service.nominatim.OsmType
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Documentation tests for [IssueGroupController]
 * @author JB Nizet
 */
@DirectMairieDocTest(IssueGroupController::class)
class IssueGroupControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockNominatimService: NominatimService

    val creationResponseFields = listOf(
        fieldWithPath("id").description("The ID of the issue group"),
        fieldWithPath("label").description("The label of the issue group"),
        fieldWithPath("categories").description("The categories of the issue group")
    )

    val singleResponseFields = creationResponseFields + listOf(
        fieldWithPath("categories[].id").description("The ID of the issue category"),
        fieldWithPath("categories[].label").description("The label of the issue category"),
        fieldWithPath("categories[].descriptionRequired").description("true if a description must be entered by the user when creating an issue with that category, false if the description can be left blank")
    )

    val listResponseFields = listOf(
        fieldWithPath("[]").description("The array of issue groups")
    ) + applyPathPrefix("[].", singleResponseFields)

    val newGroupId = 1000L

    @Test
    fun list() {
        every { mockIssueGroupDao.findAll() } returns allIssueGroups
        mockMvc.perform(docGet("/api/issue-groups"))
            .andExpect(status().isOk)
            .andDo(document("issue-groups/list", responseFields(listResponseFields)))
    }

    @Test
    fun search() {
        val coordinates = GeoCoordinates(48.8582602, 2.29449905431968)

        every { mockNominatimService.details(coordinates) } returns DetailsResult(
            listOf(
                DetailsAddress(osmId = 1000L, osmType = OsmType.RELATION, adminLevel = 4)
            )
        )

        every { mockIssueCategoryDao.findByGovernmentOsmIds(setOf(1000L)) } returns
            allIssueGroups
                .flatMap { it.categories }
                .filter { it.id in setOf(OTHER_CATEGORY_ID, POTHOLE_CATEGORY_ID, GARBAGE_CATEGORY_ID, GRAFFITI_CATEGORY_ID) }

        mockMvc.perform(
            docGet("/api/issue-groups")
                .param("latitude", coordinates.latitude.toString())
                .param("longitude", coordinates.longitude.toString())
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issue-groups/search",
                    queryParameters(
                        parameterWithName("latitude").description("The latitude of the point for which the issue groups are requested"),
                        parameterWithName("longitude").description("The longitude of the point for which the issue groups are requested")
                    ),
                    responseFields(listResponseFields)
                )
            )
    }

    @Test
    fun get() {
        every { mockIssueGroupDao.findByIdOrThrow(LIGHTING_GROUP_ID) } returns issueGroupWithId(LIGHTING_GROUP_ID)

        mockMvc.perform(
            docGet("/api/issue-groups/{issueGroupId}", LIGHTING_GROUP_ID)
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issue-groups/get",
                    pathParameters(
                        parameterWithName("issueGroupId").description("The ID of the issue group to get")
                    ),
                    responseFields(singleResponseFields)
                )
            )
    }

    @Test
    fun create() {
        val command = IssueGroupCommandDTO(label = "Gardens")

        every { mockIssueGroupDao.findByLabel(command.label) } returns null
        every { mockIssueGroupDao.saveAndFlush(any<IssueGroup>()) } returnsModifiedFirstArg { id = newGroupId }

        mockMvc.perform(
            docPost("/api/issue-groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "issue-groups/create",
                    requestFields(
                        fieldWithPath("label").description("The label of the issue group to create")
                    ),
                    responseFields(creationResponseFields)
                )
            )
    }

    @Test
    fun update() {
        val id = LIGHTING_GROUP_ID
        val command = IssueGroupCommandDTO(label = "Public lighting")

        every { mockIssueGroupDao.findByIdOrThrow(id) } returns issueGroupWithId(id)
        every { mockIssueGroupDao.findByLabel(command.label) } returns null

        mockMvc.perform(
            docPut("/api/issue-groups/{issueGroupId}", id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issue-groups/update",
                    pathParameters(
                        parameterWithName("issueGroupId").description("The ID of the issue group to update")
                    ),
                    requestFields(
                        fieldWithPath("label").description("The new label of the issue group to update")
                    )
                )
            )
    }

    @Test
    fun delete() {
        every { mockIssueGroupDao.findByIdOrNull(newGroupId) } returns null

        mockMvc.perform(docDelete("/api/issue-groups/{issueGroupId}", newGroupId).basicAuthSuperAdmin())
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issue-groups/delete",
                    pathParameters(
                        parameterWithName("issueGroupId").description("The ID of the issue group to delete")
                    )
                )
            )
    }
}
