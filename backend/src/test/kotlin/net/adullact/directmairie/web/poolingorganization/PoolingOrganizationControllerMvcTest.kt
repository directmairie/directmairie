package net.adullact.directmairie.web.poolingorganization

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.PoolingOrganizationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.user.Authorizations
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * MVC tests for [PoolingOrganizationController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(PoolingOrganizationController::class)
class PoolingOrganizationControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val controller: PoolingOrganizationController
) {

    @MockkBean
    lateinit var mockOrganizationDao: PoolingOrganizationDao

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    val john = User().apply {
        id = 1
        email = "John@mail.com"
    }
    val jane = User().apply {
        id = 2
        email = "jane@mail.com"
    }
    val superAdmin = User().apply {
        id = 3
        email = "super@mail.com"
        superAdmin = true
    }

    @BeforeEach
    fun prepare() {
        for (user in listOf(john, jane)) {
            every { mockUserDao.findVerifiedByIdOrThrow(user.id!!, any()) } returns user
        }
    }

    @Nested
    inner class Search {
        @Test
        fun `should list all organizations for the user`() {
            val organizations = listOf(
                PoolingOrganization().apply {
                    id = 1L
                    name = "Bretagne"
                    addAdministrator(john)
                }
            )

            every { mockOrganizationDao.findByQueryAndAdministrator("", john) } returns organizations
            every { mockAuthorizations.currentUser } returns john

            mockMvc.perform(get("/api/pooling-organizations"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("Bretagne"))
        }

        @Test
        fun `should find organizations by query for super admin`() {
            val organizations = listOf(
                PoolingOrganization().apply {
                    id = 1L
                    name = "Bretagne"
                }
            )

            every { mockOrganizationDao.findByQuery("bre") } returns organizations
            every { mockAuthorizations.currentUser } returns superAdmin

            mockMvc.perform(get("/api/pooling-organizations").param("query", "bre"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("Bretagne"))
        }

        @Test
        fun `should find organizations by query for user`() {
            val organizations = listOf(
                PoolingOrganization().apply {
                    id = 1L
                    name = "Bretagne"
                    addAdministrator(john)
                }
            )

            every { mockOrganizationDao.findByQueryAndAdministrator("bre", john) } returns organizations
            every { mockAuthorizations.currentUser } returns john

            mockMvc.perform(get("/api/pooling-organizations").param("query", "bre"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].name").value("Bretagne"))
        }
    }

    @Nested
    inner class Get {
        @Test
        fun `should get detailed organization`() {
            val organization = PoolingOrganization().apply {
                id = 42L
                name = "Bretagne"
                addAdministrator(john)
                addAdministrator(jane)
            }

            every { mockOrganizationDao.findByIdOrThrow(organization.id!!) } returns organization
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(organization) } returns true

            mockMvc.perform(get("/api/pooling-organizations/${organization.id}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(organization.id!!))
                .andExpect(jsonPath("$.name").value(organization.name))
                .andExpect(jsonPath("$.administrators").isArray)
                // administrators must be sorted by email, case-insensitive
                .andExpect(jsonPath("$.administrators[0].id").value(jane.id!!))
                .andExpect(jsonPath("$.administrators[0].email").value(jane.email))
                .andExpect(jsonPath("$.administrators[1].id").value(john.id!!))
                .andExpect(jsonPath("$.administrators[1].email").value(john.email))
        }
    }

    @Nested
    inner class Create {
        @Test
        fun `should create`() {
            every { mockOrganizationDao.saveAndFlush(any<PoolingOrganization>()) } returnsModifiedFirstArg { id = 1000 }

            val command = PoolingOrganizationCommandDTO(
                name = "Loire-Forez",
                administratorIds = setOf(john.id!!, jane.id!!)
            )

            every { mockOrganizationDao.findByName(command.name) } returns null

            mockMvc.perform(
                post("/api/pooling-organizations")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000L))
                .andExpect(jsonPath("$.name").value(command.name))
                .andExpect(jsonPath("$.administrators").isArray)

            verify {
                mockOrganizationDao.saveAndFlush(withArg<PoolingOrganization> { organization ->
                    Assertions.assertThat(organization.administrators.map { it.id }).containsOnly(john.id, jane.id)
                })
            }
        }

        @Test
        fun `should throw when creating with already existing name`() {
            val command = PoolingOrganizationCommandDTO(name = "exising")
            every { mockOrganizationDao.findByName(command.name) } returns PoolingOrganization()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.create(command) }
                .matches { it.errorCode == ErrorCode.POOLING_ORGANIZATION_WITH_SAME_NAME_ALREADY_EXISTS }
        }
    }

    @Nested
    inner class Update {
        @Test
        fun `should update`() {
            val organization = PoolingOrganization().apply {
                id = 42L
                name = "Plaine du Forez"
                addAdministrator(john)
            }

            every { mockOrganizationDao.findByIdOrThrow(organization.id!!) } returns organization
            val command = PoolingOrganizationCommandDTO(
                name = "Loire Forez",
                administratorIds = setOf(jane.id!!)
            )

            every { mockOrganizationDao.findByName(command.name) } returns null

            mockMvc.perform(
                put("/api/pooling-organizations/${organization.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            Assertions.assertThat(organization.name).isEqualTo(command.name)
            Assertions.assertThat(organization.administrators).containsOnly(jane)
        }

        @Test
        fun `should throw when updating with already existing name`() {
            val id = 1L
            val command = PoolingOrganizationCommandDTO(name = "exising")
            every { mockOrganizationDao.findByIdOrThrow(id) } returns PoolingOrganization()
            every { mockOrganizationDao.findByName(command.name) } returns PoolingOrganization()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.update(id, command) }
                .matches { it.errorCode == ErrorCode.POOLING_ORGANIZATION_WITH_SAME_NAME_ALREADY_EXISTS }
        }

        @Test
        fun `should not throw when updating with unmodified name`() {
            val id = 1L
            val command = PoolingOrganizationCommandDTO(name = "exising")
            val existingOrganization = PoolingOrganization()
            every { mockOrganizationDao.findByIdOrThrow(id) } returns existingOrganization
            every { mockOrganizationDao.findByName(command.name) } returns existingOrganization

            controller.update(id, command)
        }
    }

    @Nested
    inner class Delete {
        @Test
        fun `should delete`() {
            val organization = PoolingOrganization().apply {
                id = 42L
                name = "Loire Forez"
            }

            every { mockOrganizationDao.findByIdOrNull(organization.id!!) } returns organization

            mockMvc.perform(delete("/api/pooling-organizations/${organization.id}"))
                .andExpect(status().isNoContent)

            verify { mockOrganizationDao.delete(organization) }
        }

        @Test
        fun `should delete unexisting organization`() {
            every { mockOrganizationDao.findByIdOrNull(42L) } returns null

            mockMvc.perform(delete("/api/pooling-organizations/42"))
                .andExpect(status().isNoContent)

            verify(inverse = true) { mockOrganizationDao.delete(any()) }
        }
    }
}
