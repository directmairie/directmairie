package net.adullact.directmairie.web.government

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.*
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.user.Authorizations
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * MVC tests for [GovernmentController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(GovernmentController::class)
class GovernmentControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val controller: GovernmentController
) {

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockPoolingOrganizationDao: PoolingOrganizationDao

    val otherCategory = IssueCategory().apply {
        id = OTHER_CATEGORY_ID
        label = "Other"
        descriptionRequired = true
    }
    val brokenLampCategory = IssueCategory().apply {
        id = 2
        label = "Broken lamp"
    }
    val holeInRoadCategory = IssueCategory().apply {
        id = 3
        label = "Hole in road"
    }

    val otherGroup = IssueGroup().apply {
        id = OTHER_ISSUE_GROUP_ID
        label = "Other"
        addCategory(otherCategory)
    }
    val lightingGroup = IssueGroup().apply {
        id = 2L
        label = "Lighting"
        addCategory(brokenLampCategory)
    }
    val roadsGroup = IssueGroup().apply {
        id = 3L
        label = "Roads"
        addCategory(holeInRoadCategory)
    }

    val adullact = PoolingOrganization().apply {
        id = 1
        name = "Adullact"
    }
    val otherPoolingOrganization = PoolingOrganization().apply {
        id = 2
        name = "Other"
    }

    val john = User().apply {
        id = 1
        email = "john@mail.com"
        firstName = "John"
        lastName = "Doe"
        addAdministeredPoolingOrganization(adullact)
    }
    val jane = User().apply {
        id = 2
        email = "jane@mail.com"
        firstName = "Jane"
    }
    val superAdmin = User().apply {
        id = 3
        email = "superAdmin@mail.com"
        superAdmin = true
    }

    @BeforeEach
    fun prepare() {
        for (group in listOf(otherGroup, lightingGroup, roadsGroup)) {
            every { mockIssueGroupDao.findByIdOrThrow(group.id!!, any()) } returns group
        }
        for (issueCategory in listOf(otherCategory, brokenLampCategory, holeInRoadCategory)) {
            every { mockIssueCategoryDao.findByIdOrThrow(issueCategory.id!!, any()) } returns issueCategory
        }
        for (user in listOf(john, jane, superAdmin)) {
            every { mockUserDao.findVerifiedByIdOrThrow(user.id!!, any()) } returns user
        }
        for (poolingOrganization in listOf(adullact, otherPoolingOrganization)) {
            every { mockPoolingOrganizationDao.findByIdOrThrow(poolingOrganization.id!!, any()) } returns poolingOrganization
        }
    }

    @Nested
    inner class Search {
        @Test
        fun `should list all governments if super admin and use first page by default`() {
            val pageRequest = PageRequest.of(0, PAGE_SIZE)
            val governments = listOf(
                Government().apply {
                    id = 1L
                    name = "France"
                    code = "12345"
                    url = "https://france.fr"
                    osmId = 1001L
                }
            )

            every { mockAuthorizations.currentUser } returns superAdmin
            every { mockGovernmentDao.findByQuery("", pageRequest) } returns PageImpl(governments, pageRequest, 1L)

            mockMvc.perform(get("/api/governments"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.content").isArray)
                .andExpect(jsonPath("$.content[0].id").value(1L))
                .andExpect(jsonPath("$.content[0].name").value("France"))
                .andExpect(jsonPath("$.content[0].code").value("12345"))
                .andExpect(jsonPath("$.content[0].url").value("https://france.fr"))
                .andExpect(jsonPath("$.content[0].osmId").value(1001L))
        }

        @Test
        fun `should find governments by query and user if not super admin and honor page`() {
            val pageRequest = PageRequest.of(1, PAGE_SIZE)
            val governments = listOf(
                Government().apply {
                    id = 1L
                    name = "France"
                    code = "12345"
                    url = "https://france.fr"
                    osmId = 1001L
                }
            )

            every { mockAuthorizations.currentUser } returns john
            every { mockGovernmentDao.findByQueryAndUser("AN", john, pageRequest) } returns PageImpl(
                governments,
                pageRequest,
                1L
            )

            mockMvc.perform(get("/api/governments").param("query", "AN").param("page", "1"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.content").isArray)
                .andExpect(jsonPath("$.number").value(1))
        }
    }

    @Nested
    inner class Get {
        @Test
        fun `should get detailed government`() {
            val contact1 = GovernmentContact().apply {
                group = otherGroup
                email = "john@mail.com"
            }
            val contact2 = GovernmentContact().apply {
                group = lightingGroup
                email = "jane@mail.com"
            }

            val government = Government().apply {
                id = 42L
                name = "France"
                code = "12345"
                url = "https://france.fr"
                osmId = 1001L
                poolingOrganization = adullact
                contactEmail = "contact@france.fr"
                addIssueCategory(otherCategory)
                addIssueCategory(brokenLampCategory)
                addContact(contact1)
                addContact(contact2)
            }

            every { mockAuthorizations.atLeastGovernmentAdmin(government) } returns true

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            mockMvc.perform(get("/api/governments/${government.id}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(government.id!!))
                .andExpect(jsonPath("$.name").value(government.name))
                .andExpect(jsonPath("$.code").value(government.code))
                .andExpect(jsonPath("$.url").value(government.url))
                .andExpect(jsonPath("$.osmId").value(government.osmId))
                .andExpect(jsonPath("$.contactEmail").value(government.contactEmail))
                .andExpect(jsonPath("$.poolingOrganizationId").value(government.poolingOrganization.id!!))
                .andExpect(jsonPath("$.issueGroups").isArray)
                .andExpect(jsonPath("$.issueGroups[0].id").value(lightingGroup.id!!))
                .andExpect(jsonPath("$.issueGroups[0].label").value(lightingGroup.label))
                .andExpect(jsonPath("$.issueGroups[0].categories[0].id").value(brokenLampCategory.id!!))
                .andExpect(jsonPath("$.issueGroups[0].categories[0].label").value(brokenLampCategory.label))
                .andExpect(jsonPath("$.issueGroups[1].label").value(otherGroup.label))
                .andExpect(jsonPath("$.issueGroups[1].categories[0].label").value(otherCategory.label))
                .andExpect(jsonPath("$.contacts").isArray)
                .andExpect(jsonPath("$.contacts[0].issueGroup.id").value(contact1.group.id!!))
                .andExpect(jsonPath("$.contacts[0].issueGroup.label").value(contact1.group.label))
                .andExpect(jsonPath("$.contacts[0].email").value(contact1.email))
                .andExpect(jsonPath("$.contacts[1].issueGroup.id").value(contact2.group.id!!))
                .andExpect(jsonPath("$.contacts[1].issueGroup.label").value(contact2.group.label))
                .andExpect(jsonPath("$.contacts[1].email").value(contact2.email))
        }

        @Test
        fun `should throw when getting detailed government and not authorized`() {
            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockAuthorizations.atLeastGovernmentAdmin(government) } returns false
            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            mockMvc.perform(get("/api/governments/${government.id}"))
                .andExpect(status().isForbidden)
        }
    }

    @Nested
    inner class Create {
        @Test
        fun `should create`() {
            every { mockGovernmentDao.saveAndFlush(any<Government>()) } returnsModifiedFirstArg { id = 1000 }
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val command = GovernmentCommandDTO(
                name = "Lyon",
                code = "54321",
                url = "https://lyon.fr",
                osmId = 1234L,
                poolingOrganizationId = adullact.id!!,
                issueCategoryIds = setOf(brokenLampCategory.id!!),
                administratorIds = setOf(john.id!!, jane.id!!),
                contactEmail = "contact@lyon.fr",
                contacts = setOf(
                    GovernmentContactCommandDTO(
                        issueGroupId = otherGroup.id!!,
                        email = "john@mail.com"
                    ),
                    GovernmentContactCommandDTO(
                        issueGroupId = lightingGroup.id!!,
                        email = "jane@mail.com"
                    )
                )
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
            every { mockGovernmentDao.findByCode(command.code) } returns null

            mockMvc.perform(
                post("/api/governments")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000L))
                .andExpect(jsonPath("$.name").value(command.name))
                .andExpect(jsonPath("$.code").value(command.code))
                .andExpect(jsonPath("$.url").value(command.url))
                .andExpect(jsonPath("$.osmId").value(command.osmId))
                .andExpect(jsonPath("$.contactEmail").value(command.contactEmail))
                .andExpect(jsonPath("$.issueGroups").isArray)
                .andExpect(jsonPath("$.administrators").isArray)
                .andExpect(jsonPath("$.contacts").isArray)

            verify {
                mockGovernmentDao.saveAndFlush(withArg<Government> { government ->
                    assertThat(government.issueCategories).containsOnly(otherCategory, brokenLampCategory)
                    assertThat(government.administrators).containsOnly(john, jane)
                    assertThat(government.contacts).hasSize(2)
                    assertThat(government.contacts.map { it.group }).containsOnly(otherGroup, lightingGroup)
                    assertThat(government.contacts.map { it.email }).containsOnly("john@mail.com", "jane@mail.com")
                })
            }
        }

        @Test
        fun `should throw when creating with existing osm id`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val command = GovernmentCommandDTO(
                name = "Lyon",
                code = "54321",
                url = "https://lyon.fr",
                osmId = 1234L,
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns Government()
            every { mockGovernmentDao.findByCode(command.code) } returns null

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.create(command) }
                .matches { it.errorCode == ErrorCode.GOVERNMENT_WITH_SAME_OSM_ID_ALREADY_EXISTS }
        }

        @Test
        fun `should throw when creating with existing code`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val command = GovernmentCommandDTO(
                name = "Lyon",
                code = "54321",
                url = "https://lyon.fr",
                osmId = 1234L,
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
            every { mockGovernmentDao.findByCode(command.code) } returns Government()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.create(command) }
                .matches { it.errorCode == ErrorCode.GOVERNMENT_WITH_SAME_CODE_ALREADY_EXISTS }
        }

        @Test
        fun `should throw when creating with invalid URL`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val command = GovernmentCommandDTO(
                name = "Lyon",
                code = "54321",
                url = " lyon.fr", // does not start with http or https
                osmId = 1234L,
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            mockMvc.perform(
                post("/api/governments")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw when creating with unauthorized pooling organization`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns false

            val command = GovernmentCommandDTO(
                name = "Lyon",
                code = "54321",
                url = "https://lyon.fr",
                osmId = 1234L,
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            mockMvc.perform(
                post("/api/governments")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }
    }

    @Nested
    inner class Update {
        @Test
        fun `should update`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(otherPoolingOrganization) } returns true
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val oldContact = GovernmentContact().apply {
                group = lightingGroup
                email = "john@mail.com"
            }

            val government = Government().apply {
                id = 42L
                name = "France"
                code = "12345"
                url = "https://france.fr"
                osmId = 1001L
                poolingOrganization = otherPoolingOrganization
                contactEmail = "old@lyon.fr"
                addIssueCategory(otherCategory)
                addIssueCategory(brokenLampCategory)
                addAdministrator(john)
                addContact(oldContact)
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                osmId = 1234L,
                code = "54321",
                url = "https://lyon.fr",
                poolingOrganizationId = adullact.id!!,
                issueCategoryIds = setOf(otherCategory.id!!, holeInRoadCategory.id!!),
                administratorIds = setOf(jane.id!!),
                contactEmail = "contact@lyon.fr",
                contacts = setOf(
                    GovernmentContactCommandDTO(
                        issueGroupId = roadsGroup.id!!,
                        email = "jane@mail.com"
                    )
                )
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
            every { mockGovernmentDao.findByCode(command.code) } returns null

            mockMvc.perform(
                put("/api/governments/${government.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(government.name).isEqualTo(command.name)
            assertThat(government.code).isEqualTo(command.code)
            assertThat(government.url).isEqualTo(command.url)
            assertThat(government.osmId).isEqualTo(command.osmId)
            assertThat(government.poolingOrganization).isEqualTo(adullact)
            assertThat(government.contactEmail).isEqualTo(command.contactEmail)
            assertThat(government.issueCategories).containsOnly(otherCategory, holeInRoadCategory)
            assertThat(government.administrators).containsOnly(jane)
            assertThat(government.contacts).hasSize(1)
            assertThat(government.contacts.map { it.group }).containsOnly(roadsGroup)
            assertThat(government.contacts.map { it.email }).containsOnly("jane@mail.com")
        }

        @Test
        fun `should throw when updating government having unauthorized pooling organization`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(otherPoolingOrganization) } returns false

            val government = Government().apply {
                id = 42L
                poolingOrganization = otherPoolingOrganization
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                url = "https://lyon.fr",
                osmId = 1234L,
                code = "54321",
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            mockMvc.perform(
                put("/api/governments/${government.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isForbidden)
        }

        @Test
        fun `should throw when updating with an unauthorized new pooling organization`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(otherPoolingOrganization) } returns true
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns false

            val government = Government().apply {
                id = 42L
                poolingOrganization = otherPoolingOrganization
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                osmId = 1234L,
                code = "54321",
                url = "https://lyon.fr",
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            mockMvc.perform(
                put("/api/governments/${government.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw when updating with existing osm id`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                osmId = 1234L,
                code = "54321",
                url = "https://lyon.fr",
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns Government()
            every { mockGovernmentDao.findByCode(command.code) } returns null

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.update(government.id!!, command) }
                .matches { it.errorCode == ErrorCode.GOVERNMENT_WITH_SAME_OSM_ID_ALREADY_EXISTS }
        }

        @Test
        fun `should throw when updating with existing code`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                osmId = 1234L,
                code = "54321",
                url = "https://lyon.fr",
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
            every { mockGovernmentDao.findByCode(command.code) } returns Government()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.update(government.id!!, command) }
                .matches { it.errorCode == ErrorCode.GOVERNMENT_WITH_SAME_CODE_ALREADY_EXISTS }
        }

        @Test
        fun `should not throw when updating with unmodified osm id and unmodified code`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true

            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government

            val command = GovernmentCommandDTO(
                name = "Lyon",
                osmId = 1234L,
                code = "54321",
                url = "https://lyon.fr",
                poolingOrganizationId = adullact.id!!,
                contactEmail = "contact@lyon.fr"
            )

            every { mockGovernmentDao.findByOsmId(command.osmId) } returns government
            every { mockGovernmentDao.findByCode(command.code) } returns government

            controller.update(government.id!!, command)
        }
    }

    @Nested
    inner class Delete {
        @Test
        fun `should delete`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns true
            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockGovernmentDao.findByIdOrNull(government.id!!) } returns government

            mockMvc.perform(delete("/api/governments/${government.id}"))
                .andExpect(status().isNoContent)

            verify { mockGovernmentDao.delete(government) }
        }

        @Test
        fun `should delete unexisting government`() {
            every { mockGovernmentDao.findByIdOrNull(42L) } returns null

            mockMvc.perform(delete("/api/governments/42"))
                .andExpect(status().isNoContent)

            verify(inverse = true) { mockGovernmentDao.delete(any()) }
        }

        @Test
        fun `should throw when deleting unauthorized government`() {
            every { mockAuthorizations.atLeastPoolingOrganizationAdmin(adullact) } returns false
            val government = Government().apply {
                id = 42L
                poolingOrganization = adullact
            }

            every { mockGovernmentDao.findByIdOrNull(government.id!!) } returns government

            mockMvc.perform(delete("/api/governments/${government.id}"))
                .andExpect(status().isForbidden)
        }
    }
}
