package net.adullact.directmairie.web.subscription

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.SubscriptionDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Subscription
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.user.Authorizations
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post

/**
 * MVC tests for [SubscriptionController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(SubscriptionController::class)
class SubscriptionControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {
    @MockkBean(relaxUnitFun = true)
    lateinit var mockSubscriptionDao: SubscriptionDao

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    lateinit var currentUser: User

    val lyon = Government().apply {
        id = 1L
        name = "Lyon"
        code = "12345"
        url = "https://lyon.fr"
        osmId = 1001L
    }
    val loire = Government().apply {
        id = 2L
        name = "Loire"
        code = "12346"
        url = "https://loire.fr"
        osmId = 1002L
    }

    lateinit var lyonSubscription: Subscription

    @BeforeEach
    fun prepare() {
        listOf(lyon, loire).forEach {
            every { mockGovernmentDao.findByIdOrThrow(it.id!!, any()) } returns it
        }

        lyonSubscription = Subscription().apply {
            id = 69L
            government = lyon
        }
        currentUser = User().apply {
            id = 99L
            addSubscription(lyonSubscription)
        }
        every { mockAuthorizations.currentUser } returns currentUser
    }

    @Nested
    inner class List {
        @Test
        fun `should list`() {
            currentUser.addSubscription(
                Subscription().apply {
                    id = 42L
                    government = loire
                }
            )
            mockMvc.get("/api/subscriptions/mine").andExpect {
                content {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                    jsonPath("$[0].id") { value(42L) }
                    jsonPath("$[0].government.id") { value(loire.id) }
                    jsonPath("$[0].government.name") { value(loire.name) }
                    jsonPath("$[1].government.name") { value(lyon.name) }
                }
            }
        }
    }

    @Nested
    inner class Create {
        @Test
        fun `should create`() {
            val command = SubscriptionCommandDTO(loire.id!!)

            every { mockSubscriptionDao.flush() } answers {
                currentUser.subscriptions.find { it.government == loire }?.id = 42L
            }

            mockMvc.post("/api/subscriptions/mine") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsBytes(command)
            }.andExpect {
                status { isCreated() }
                content {
                    contentType(MediaType.APPLICATION_JSON)
                    jsonPath("$.id") { value(42L) }
                    jsonPath("$.government.id") { value(loire.id) }
                    jsonPath("$.government.name") { value(loire.name) }
                }
            }
        }

        @Test
        fun `should throw if subscription already exists`() {
            val command = SubscriptionCommandDTO(lyon.id!!)

            mockMvc.post("/api/subscriptions/mine") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsBytes(command)
            }.andExpect {
                status { isBadRequest() }
            }
        }
    }

    @Nested
    inner class Delete {
        @Test
        fun `should delete`() {
            mockMvc.delete("/api/subscriptions/mine/{id}", lyonSubscription.id).andExpect {
                status { isNoContent() }
            }

            assertThat(currentUser.subscriptions).isEmpty()
        }

        @Test
        fun `should throw if no subscription found in current user`() {
            mockMvc.delete("/api/subscriptions/mine/{id}", 345678L).andExpect {
                status { isNotFound() }
            }
        }
    }

    @Nested
    inner class Suggest {
        @Test
        fun `should suggest`() {
            every { mockSubscriptionDao.suggestGovernments("foo", currentUser.id!!, 10) } returns listOf(
                loire,
                lyon
            )

            mockMvc.get("/api/subscriptions/mine/suggestions") {
                param("query", "foo")
            }.andExpect {
                content {
                    status { isOk() }
                    jsonPath("$.length()") { value(2) }
                    jsonPath("$[0].id") { value(loire.id) }
                    jsonPath("$[0].name") { value(loire.name) }
                    jsonPath("$[1].name") { value(lyon.name) }
                }
            }
        }
    }
}
