package net.adullact.directmairie.web

import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.MockkBeans
import io.mockk.every
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.web.issuecategory.IssueGroupController
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl

/**
 * Unit tests for [IndexFilter]
 * @author JB Nizet
 */
@WebMvcTest(controllers = [IssueGroupController::class])
@MockkBeans(MockkBean(IssueCategoryDao::class), MockkBean(NominatimService::class))
internal class IndexFilterTest(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @BeforeEach
    fun prepare() {
        every { mockIssueGroupDao.findAll() } returns emptyList()
    }

    @Test
    fun `should forward to index for angular url`() {
        mockMvc.perform(get("/issues"))
            .andExpect(forwardedUrl("/index.html"))
    }

    @Test
    fun `should not forward to index when not get`() {
        mockMvc.perform(post("/issue-categories")).andExpect {
            assertThat(it.getResponse().getForwardedUrl()).isNull()
        }
    }

    @ParameterizedTest
    @ValueSource(strings = [
        "/api/issue-groups",
        "/index.html",
        "/script.js",
        "/style.css",
        "/image.gif",
        "/icon.ico",
        "/image.png",
        "/image.jpg",
        "/manifest.json",
        "/robots.txt",
        "/actuator/info",
        "/manifest.webmanifest"
    ])
    fun `should not forward to index when static resource`(url: String) {
        mockMvc.perform(get(url)).andExpect {
            assertThat(it.getResponse().getForwardedUrl()).isNull()
        }
    }
}
