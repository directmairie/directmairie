package net.adullact.directmairie.web.issue

import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.runs
import io.mockk.verify
import net.adullact.directmairie.event.IssueDeleted
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.io.IOException

/**
 * Unit tests for [PictureDeletionListener]
 * @author JB Nizet
 */
class PictureDeletionListenerTest {
    @Test
    fun `should delete all pictures and ignore exceptions`() {
        val mockPictureFileService = mockk<PictureFileService>()

        every { mockPictureFileService.delete("p1") } throws IOException()
        every { mockPictureFileService.delete("p2") } throws RuntimeException()
        every { mockPictureFileService.delete("p3") } just runs

        val listener = PictureDeletionListener(mockPictureFileService)
        listener.deletePicturesOfDeletedIssue(IssueDeleted(42L, setOf("p1", "p2", "p3")));

        verify {
            mockPictureFileService.delete("p1")
            mockPictureFileService.delete("p2")
            mockPictureFileService.delete("p3")
        }
    }
}
