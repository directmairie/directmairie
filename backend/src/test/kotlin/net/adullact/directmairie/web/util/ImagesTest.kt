package net.adullact.directmairie.web.util

import net.adullact.directmairie.web.util.image.imageExtension
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.mock.web.MockMultipartFile

/**
 * Unit tests for the images functions
 * @author JB Nizet
 */
class ImagesTest {
    @Test
    fun `should return correct extension`() {
        var multipartFile = MockMultipartFile("foo", "foo", "image/png", byteArrayOf())
        assertThat(multipartFile.imageExtension).isEqualTo("png")

        multipartFile = MockMultipartFile("foo", "foo", "image/jpeg", byteArrayOf())
        assertThat(multipartFile.imageExtension).isEqualTo("jpeg")

        multipartFile = MockMultipartFile("foo", "foo", "image/gif", byteArrayOf())
        assertThat(multipartFile.imageExtension).isEqualTo("gif")

        multipartFile = MockMultipartFile("foo", "foo", "image/svg+xml", byteArrayOf())
        assertThat(multipartFile.imageExtension).isEqualTo("svg")
    }
}
