package net.adullact.directmairie.web.poolingorganization

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.PoolingOrganizationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import net.adullact.directmairie.web.user.Authorizations
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Documentation tests for [PoolingOrganizationController]
 * @author JB Nizet
 */
@DirectMairieDocTest(PoolingOrganizationController::class)
class PoolingOrganizationControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockOrganizationDao: PoolingOrganizationDao

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    val superAdmin = User().apply {
        id = 1
        email = "super@mail.com"
        superAdmin = true
    }
    val poolingOrganizationAdmin1 = User().apply {
        id = 1001
        email = "admin1@grand-lyon.fr"
        firstName = "Jane"
        lastName = "Doe"
    }
    val poolingOrganizationAdmin2 = User().apply {
        id = 1002
        email = "admin2@grand-lyon.fr"
    }

    val newPoolingOrganizationId = 1042L
    val newPoolingOrganizationAdmin1 = User().apply {
        id = 1022
        email = "admin1@loire-forez.fr"
        firstName = "Marcel"
        lastName = "Dupont"
    }
    val newPoolingOrganizationAdmin2 = User().apply {
        id = 1023
        email = "admin2@loire-forez.fr"
    }

    val organization
        get() = PoolingOrganization().apply {
            id = GRAND_LYON_POOLING_ORGANIZATION_ID
            name = "Grand Lyon Pooling"
            addAdministrator(poolingOrganizationAdmin1)
        }

    val basicFields = listOf(
        fieldWithPath("id").description("the ID of the pooling organization"),
        fieldWithPath("name").description("the name of the pooling organization")
    )

    val detailedFields = basicFields + listOf(
        fieldWithPath("administrators").description("the administrators of the pooling organization, sorted by email"),
        fieldWithPath("administrators[].id").description("the ID of the administrator"),
        fieldWithPath("administrators[].email").description("the email of the administrator"),
        fieldWithPath("administrators[].firstName").optional().description("the first name of the administrator"),
        fieldWithPath("administrators[].lastName").optional().description("the last name of the administrator")
    )

    @BeforeEach
    fun prepare() {
        for (user in listOf(
            superAdmin,
            poolingOrganizationAdmin1,
            poolingOrganizationAdmin2,
            newPoolingOrganizationAdmin1,
            newPoolingOrganizationAdmin2
        )) {
            every { mockUserDao.findVerifiedByIdOrThrow(user.id!!, any()) } returns user
        }

        every { mockOrganizationDao.findByIdOrThrow(organization.id!!) } returns organization
        every { mockAuthorizations.atLeastPoolingOrganizationAdmin(any()) } returns true
    }

    @Test
    fun search() {
        every { mockAuthorizations.currentUser } returns poolingOrganizationAdmin1
        every { mockOrganizationDao.findByQueryAndAdministrator("lyon", poolingOrganizationAdmin1) } returns
            listOf(organization)

        mockMvc.perform(
            docGet("/api/pooling-organizations").param("query", "lyon")
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "pooling-organizations/search",
                    queryParameters(
                        parameterWithName("query").description("the query (part of the name of the searched pooling organization)")
                    ),
                    responseFields(
                        listOf(fieldWithPath("[]").description("the pooling organizations matching with the query")) +
                            applyPathPrefix("[].", basicFields)
                    )
                )
            )
    }

    @Test
    fun get() {
        mockMvc.perform(
            docGet("/api/pooling-organizations/{organizationId}", organization.id!!)
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "pooling-organizations/get",
                    pathParameters(
                        parameterWithName("organizationId").description("The ID of the pooling organization to get")
                    ),
                    responseFields(detailedFields)
                )
            )
    }

    @Test
    fun create() {
        every { mockOrganizationDao.saveAndFlush(any<PoolingOrganization>()) } returnsModifiedFirstArg { id = newPoolingOrganizationId }

        val command = PoolingOrganizationCommandDTO(
            name = "Loire-Forez",
            administratorIds = setOf(newPoolingOrganizationAdmin1.id!!, newPoolingOrganizationAdmin2.id!!)
        )

        every { mockOrganizationDao.findByName(command.name) } returns null

        mockMvc.perform(
            docPost("/api/pooling-organizations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "pooling-organizations/create",
                    requestFields(
                        fieldWithPath("name").description("The name of the pooling organization to create"),
                        fieldWithPath("administratorIds").description("The IDs of the administrators of the pooling organization to create")
                    ),
                    responseFields(detailedFields)
                )
            )
    }

    @Test
    fun update() {
        val command = PoolingOrganizationCommandDTO(
            name = "Grand Lyon",
            administratorIds = setOf(poolingOrganizationAdmin1.id!!, poolingOrganizationAdmin2.id!!)
        )

        every { mockOrganizationDao.findByName(command.name) } returns null

        mockMvc.perform(
            docPut("/api/pooling-organizations/{organizationId}", organization.id!!)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "pooling-organizations/update",
                    pathParameters(
                        parameterWithName("organizationId").description("The ID of the pooling organization to update")
                    ),
                    requestFields(
                        fieldWithPath("name").description("The new name of the pooling organization"),
                        fieldWithPath("administratorIds").description("The new set of IDs of the administrators of the pooling organization"))
                )
            )
    }

    @Test
    fun delete() {
        every { mockOrganizationDao.findByIdOrNull(newPoolingOrganizationId) } returns null

        mockMvc.perform(
            docDelete("/api/pooling-organizations/{organizationId}", newPoolingOrganizationId)
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "pooling-organizations/delete",
                    pathParameters(
                        parameterWithName("organizationId").description("The ID of the pooling organization to delete")
                    )
                )
            )
    }
}
