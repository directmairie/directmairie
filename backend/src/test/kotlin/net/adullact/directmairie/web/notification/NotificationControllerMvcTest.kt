package net.adullact.directmairie.web.notification

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.SubscriptionDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Subscription
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.NotificationRequested
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.user.Authorizations
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.post

/**
 * MVC tests for [NotificationController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(NotificationController::class)
class NotificationControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {
    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockEventPublisher: EventPublisher

    val lyon = Government().apply {
        id = 1L
        name = "Lyon"
    }

    @BeforeEach
    fun prepare() {
        every { mockGovernmentDao.findByIdOrThrow(lyon.id!!, any()) } returns lyon

        every { mockAuthorizations.atLeastGovernmentAdmin(lyon) } returns true
    }

    @Nested
    inner class Send {
        @Test
        fun `should send notification`() {
            val command = NotificationCommandDTO(
                governmentId = lyon.id!!,
                subject = "Subject",
                message = "Message",
                url = "https://lyon.fr"
            )
            mockMvc.post("/api/notifications") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsBytes(command)
            }.andExpect {
                status { isNoContent() }
            }

            verify { mockEventPublisher.publish(NotificationRequested(command)) }
        }

        @Test
        fun `should throw if current user not at least government admin for the given government`() {
            every { mockAuthorizations.atLeastGovernmentAdmin(lyon) } returns false
            val command = NotificationCommandDTO(lyon.id!!, "Subject", "Message", "https://lyon.fr")

            mockMvc.post("/api/notifications") {
                contentType = MediaType.APPLICATION_JSON
                content = objectMapper.writeValueAsBytes(command)
            }.andExpect {
                status { isForbidden() }
            }

            verify(inverse = true) { mockEventPublisher.publish(any()) }
        }
    }
}
