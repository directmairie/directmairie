package net.adullact.directmairie.web.user

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.config.SecurityPropertiesConfig
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.domain.generateSecretToken
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.*
import org.springframework.test.context.TestPropertySource
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

/**
 * Docuentation tests for [UserController]
 * @author JB Nizet
 */
@DirectMairieDocTest(UserController::class)
@Import(SecurityPropertiesConfig::class, PasswordHasher::class, JwtService::class, EventPublisher::class)
@TestPropertySource("/test.properties")
@MockkBean(IssueDao::class)
class UserControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockEmailVerificationDao: EmailVerificationDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    val newUserId = USER_ID

    @Test
    fun register() {
        val command = UserRegistrationCommandDTO(
            email = USER_EMAIL,
            password = USER_PASSWORD,
            firstName = "John",
            lastName = "Doe",
            phone = "0612345678",
            issueTransitionNotificationActivated = true
        )

        every { mockUserDao.findByEmail(command.email) } returns null
        every { mockUserDao.saveAndFlush(any<User>()) } returnsModifiedFirstArg { id = newUserId }
        every { mockEmailVerificationDao.saveAndFlush(any<EmailVerification>()) } returnsModifiedFirstArg {
            id = 10984L
        }

        mockMvc.perform(
            docPost("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
        )
            .andDo(
                document(
                    "users/register",
                    requestFields(
                        fieldWithPath("email").description("The email of the person who registers him/herself"),
                        fieldWithPath("password").description("The password chosen by the person who registers him/herself"),
                        fieldWithPath("firstName").optional().description("The first name of the person who registers him/herself"),
                        fieldWithPath("lastName").optional().description("The last name of the person who registers him/herself"),
                        fieldWithPath("phone").optional().description("The phone number of the person who registers him/herself"),
                        fieldWithPath("issueTransitionNotificationActivated").optional().description("Whether the person who registers him/herself wants to receive notifications when his/her issues transition to a new status")
                    ),
                    responseFields(
                        fieldWithPath("id").description("The ID of the created user"),
                        fieldWithPath("email").description("The email of the created user"),
                        fieldWithPath("firstName").optional().description("The first name of the created user"),
                        fieldWithPath("lastName").optional().description("The last name of the created user")
                    )
                )
            )
    }

    @Test
    fun `email verification`() {
        val user = User().apply {
            id = newUserId
        }

        val token = generateSecretToken()
        val emailVerification = EmailVerification(
            user = user,
            secretToken = token,
            email = USER_EMAIL,
            validityInstant = Instant.now().plusMillis(12345L)
        )

        every { mockEmailVerificationDao.findValidByUserAndToken(newUserId, token) } returns emailVerification

        val command = EmailVerificationCommandDTO(token)

        mockMvc.perform(
            docPost("/api/users/{userId}/email-verifications", newUserId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "users/email-verification",
                    pathParameters(
                        parameterWithName("userId").description("The ID of the user to verify (sent by email in the link)")
                    ),
                    requestFields(
                        fieldWithPath("token").description("The secret token (sent by email in the link)")
                    ),
                    responseFields(
                        fieldWithPath("token").description("The JWT that can be used to authenticate the subsequent requests")
                    )
                )
            )
    }

    @Test
    fun me() {
        val user = User().apply {
            id = USER_ID
            email = USER_EMAIL
            superAdmin = false
            firstName = "Amandine"
            lastName = "Blasquier"
            phone = "0687654321"
        }

        every { mockAuthorizations.currentUser } returns user

        mockMvc.perform(docGet("/api/users/me").basicAuth(USER_EMAIL))
            .andExpect(status().isOk)
            .andDo(
                document(
                    "users/me",
                    responseFields(
                        fieldWithPath("id").description("The ID of the current user"),
                        fieldWithPath("email").description("The email of the current user"),
                        fieldWithPath("firstName").optional().description("The first name of the current user"),
                        fieldWithPath("lastName").optional().description("The last name of the current user"),
                        fieldWithPath("phone").optional().description("The phone number of the current user"),
                        fieldWithPath("issueTransitionNotificationActivated").description("true if the current user opted for notifications when issues that he/she created transition to a new status"),
                        fieldWithPath("superAdmin").description("true if the current user is a super-admin"),
                        fieldWithPath("poolingOrganizationAdmin").description("true if the current user is an administrator of at least one pooling organization"),
                        fieldWithPath("governmentAdmin").description("true if the current user is an administrator of at least one government")
                    )
                )
            )
    }

    @Test
    fun search() {
        val pageRequest = PageRequest.of(0, PAGE_SIZE)
        val user = User().apply {
            id = USER_ID
            email = "Jane.DOE@mail.com"
        }
        val otherUser = User().apply {
            id = USER_ID
            email = "john.doe@givors.fr"
            firstName = "John"
            lastName = "Doe"
        }

        val query = "doe"
        every { mockAuthorizations.atLeastAnyPoolingOrganizationAdmin() } returns true
        every { mockUserDao.searchByVerifiedEmail(query, pageRequest) } returns PageImpl(
            listOf(user, otherUser),
            pageRequest,
            2L
        )

        mockMvc.perform(
            docGet("/api/users").param("query", query).param("page", "0")
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "users/search",
                    queryParameters(
                        pageParameter,
                        parameterWithName("query").description("The query used to find users")
                    ),
                    paginatedResponseFields(
                        fieldWithPath("content").description("The found users for the page"),
                        fieldWithPath("content[].id").description("The ID of the user"),
                        fieldWithPath("content[].email").description("The email of the user"),
                        fieldWithPath("content[].firstName").optional().description("The first name of the user"),
                        fieldWithPath("content[].lastName").optional().description("The last name of the user")
                    )
                )
            )
    }
}
