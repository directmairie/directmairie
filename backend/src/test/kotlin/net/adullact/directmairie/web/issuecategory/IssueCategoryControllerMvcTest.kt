package net.adullact.directmairie.web.issuecategory

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * MVC tests for [IssueCategoryController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(IssueCategoryController::class)
class IssueCategoryControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val controller: IssueCategoryController
) {
    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    lateinit var group: IssueGroup
    lateinit var category: IssueCategory

    @BeforeEach
    fun prepare() {
        category = IssueCategory().apply {
            id = 23L
            label = "existing category"
        }

        group = IssueGroup().apply {
            id = 42L
            label = "test"
            addCategory(category)
        }
        every { mockIssueGroupDao.findByIdOrThrow(group.id!!) } returns group
    }

    @Nested
    inner class Create {
        @Test
        fun `should create category`() {
            val command = IssueCategoryCommandDTO(label = "test", descriptionRequired = true)
            every { mockIssueCategoryDao.findByLabel(command.label) } returns null

            every { mockIssueGroupDao.flush() } answers {
                group.categories.find { it.label == command.label }?.id = 1000
            }

            mockMvc.perform(
                post("/api/issue-groups/${group.id}/categories")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000))
                .andExpect(jsonPath("$.label").value(command.label))
                .andExpect(jsonPath("$.descriptionRequired").value(command.descriptionRequired))

            assertThat(group.categories).hasSize(2)
        }

        @Test
        fun `should throw when creating category with existing label`() {
            val command = IssueCategoryCommandDTO(label = "test", descriptionRequired = true)
            every { mockIssueCategoryDao.findByLabel(command.label) } returns IssueCategory()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.create(group.id!!, command) }
                .matches { it.errorCode == ErrorCode.ISSUE_CATEGORY_WITH_SAME_LABEL_ALREADY_EXISTS }
        }
    }

    @Nested
    inner class Update {
        @Test
        fun `should update category`() {
            val command = IssueCategoryCommandDTO(label = "test", descriptionRequired = false)
            every { mockIssueCategoryDao.findByLabel(command.label) } returns null

            mockMvc.perform(
                put("/api/issue-groups/${group.id}/categories/${category.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(category.label).isEqualTo(command.label)
            assertThat(category.descriptionRequired).isEqualTo(command.descriptionRequired)
        }

        @Test
        fun `should throw when updating category with existing label`() {
            val command = IssueCategoryCommandDTO(label = "test", descriptionRequired = false)
            every { mockIssueCategoryDao.findByLabel(command.label) } returns IssueCategory()

            assertThatExceptionOfType(BadRequestException::class.java)
                .isThrownBy { controller.update(group.id!!, category.id!!, command) }
                .matches { it.errorCode == ErrorCode.ISSUE_CATEGORY_WITH_SAME_LABEL_ALREADY_EXISTS }
        }

        @Test
        fun `should not throw when updating category with unmodified label`() {
            val command = IssueCategoryCommandDTO(label = category.label, descriptionRequired = false)
            every { mockIssueCategoryDao.findByLabel(command.label) } returns category

            controller.update(group.id!!, category.id!!, command)
        }
    }

    @Nested
    inner class Delete {
        @Test
        fun `should delete category`() {
            mockMvc.perform(delete("/api/issue-groups/${group.id}/categories/${category.id}"))
                .andExpect(status().isNoContent)

            assertThat(group.categories).doesNotContain(category)
        }

        @Test
        fun `should delete non-existing category`() {
            val id = 123456L

            mockMvc.perform(delete("/api/issue-groups/${group.id}/categories/${id}"))
                .andExpect(status().isNoContent)

            assertThat(group.categories).contains(category)
        }

        @Test
        fun `should not delete 'Other' category`() {
            val otherCategory = IssueCategory().apply {
                id = OTHER_CATEGORY_ID
                label = "Other"
            }
            group.addCategory(otherCategory)

            mockMvc.perform(delete("/api/issue-groups/${group.id}/categories/$OTHER_CATEGORY_ID"))
                .andExpect(status().isBadRequest)

            assertThat(group.categories).contains(otherCategory)
        }
    }
}
