package net.adullact.directmairie.web.actuator

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.web.user.CurrentUser
import org.junit.jupiter.api.Test
import org.springframework.http.HttpStatus
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse

/**
 * Unit tests for [ActuatorFilter]
 * @author JB Nizet
 */
class ActuatorFilterTest {
    @Test
    fun `should return 401 is not authenticated`() {
        val currentUser = CurrentUser(null)
        val filter = ActuatorFilter(mockk(), currentUser)

        filter.checkErrorSent(HttpStatus.UNAUTHORIZED)
    }

    @Test
    fun `should return 401 is no user found`() {
        val currentUser = CurrentUser(1000)
        val userDao = mockk<UserDao>() {
            every { findByIdOrNull(currentUser.userId!!) } returns null
        }
        val filter = ActuatorFilter(userDao, currentUser)

        filter.checkErrorSent(HttpStatus.UNAUTHORIZED)
    }

    @Test
    fun `should return 403 is not super-admin`() {
        val currentUser = CurrentUser(1000)
        val userDao = mockk<UserDao>() {
            every { findByIdOrNull(currentUser.userId!!) } returns User()
        }
        val filter = ActuatorFilter(userDao, currentUser)

        filter.checkErrorSent(HttpStatus.FORBIDDEN)
    }

    @Test
    fun `should pass to the chain if super-admin`() {
        val currentUser = CurrentUser(1000)
        val userDao = mockk<UserDao>() {
            every { findByIdOrNull(currentUser.userId!!) } returns User().apply { superAdmin = true }
        }
        val filter = ActuatorFilter(userDao, currentUser)

        val request = mockk<HttpServletRequest>()
        val response = mockk<HttpServletResponse>()
        val chain = mockk<FilterChain>()

        filter.doFilter(request, response, chain)

        verify(inverse = true) { response.sendError(any()) }
        verify { chain.doFilter(request, response) }
    }

    fun ActuatorFilter.checkErrorSent(expectedError: HttpStatus) {
        val request = mockk<HttpServletRequest>()
        val response = mockk<HttpServletResponse>()
        val chain = mockk<FilterChain>()

        doFilter(request, response, chain)

        verify {response.sendError(expectedError.value()) }
        verify(inverse = true) { chain.doFilter(request, response) }
    }
}
