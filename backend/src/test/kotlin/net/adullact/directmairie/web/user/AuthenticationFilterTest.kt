package net.adullact.directmairie.web.user

import io.jsonwebtoken.JwtException
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.http.HttpHeaders
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.mock.web.MockHttpServletResponse
import java.util.*
import jakarta.servlet.FilterChain

/**
 * Unit tests for [AuthenticationFilter]
 * @author JB Nizet
 */
class AuthenticationFilterTest {

    lateinit var currentUser: CurrentUser
    lateinit var mockJwtService: JwtService
    lateinit var mockUserDao: UserDao
    lateinit var mockPasswordHasher: PasswordHasher

    lateinit var filter: AuthenticationFilter

    @BeforeEach
    fun prepare() {
        currentUser = CurrentUser()
        mockJwtService = mockk()
        mockUserDao = mockk()
        mockPasswordHasher = mockk()
        filter = AuthenticationFilter(mockJwtService, currentUser, mockUserDao, mockPasswordHasher)
    }

    @Test
    fun `should pass to the chain if no authorization header`() {
        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should extract the token from the Bearer authorization header`() {
        val token = "someToken"
        every { mockJwtService.extractUserId(token) } returns 1000L

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer  $token")

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isEqualTo(1000L)
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if Bearer token is wrong`() {
        val token = "someToken"
        every { mockJwtService.extractUserId(token) } throws JwtException("test")

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Bearer  $token")

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if the authorization header is neither Bearer nor Basic`() {
        val token = "someToken"

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, token)

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should authenticate the credentials from the Basic authorization header`() {
        val token = Base64.getEncoder().encodeToString("john@mail.com:passw0rd".toByteArray())

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Basic  $token")

        val user = User().apply {
            id = 1000L
            password = "hashed"
        }
        every { mockUserDao.findByVerifiedEmail("john@mail.com") } returns user
        every { mockPasswordHasher.verify("passw0rd", user.password) } returns true

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isEqualTo(1000L)
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if Basic token can't be decoded`() {
        val token = "è¨¨£"

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Basic  $token")

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if Basic token doesn't contain user and password`() {
        val token = Base64.getEncoder().encodeToString("john@mail.com".toByteArray())

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Basic  $token")

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if Basic token contains unknown user`() {
        val token = Base64.getEncoder().encodeToString("john@mail.com;passw0rd".toByteArray())

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Basic  $token")

        every { mockUserDao.findByVerifiedEmail("john@mail.com") } returns null

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }

    @Test
    fun `should set null in the current user if Basic token contains user with bad password`() {
        val token = Base64.getEncoder().encodeToString("john@mail.com;passw0rd".toByteArray())

        val request = MockHttpServletRequest()
        val response = MockHttpServletResponse()
        val chain = mockk<FilterChain>()

        request.addHeader(HttpHeaders.AUTHORIZATION, "Basic  $token")

        val user = User().apply {
            id = 1000L
            password = "hashed"
        }
        every { mockUserDao.findByVerifiedEmail("john@mail.com") } returns user
        every { mockPasswordHasher.verify("passw0rd", user.password) } returns false

        filter.doFilter(request, response, chain)

        assertThat(currentUser.userId).isNull()
        verify { chain.doFilter(request, response) }
    }
}
