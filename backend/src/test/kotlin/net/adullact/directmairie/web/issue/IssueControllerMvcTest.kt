package net.adullact.directmairie.web.issue

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.Ordering
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.IssueSearchCriteria
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.IssueCreated
import net.adullact.directmairie.event.IssueDeleted
import net.adullact.directmairie.event.IssueTransitioned
import net.adullact.directmairie.service.nominatim.DetailsAddress
import net.adullact.directmairie.service.nominatim.DetailsResult
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.service.nominatim.OsmType
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.user.Authorizations
import org.assertj.core.api.Assertions.*
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.nullValue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.io.OutputStream
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit

/**
 * MVC tests for [IssueController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(IssueController::class)
class IssueControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val controller: IssueController
) {

    @MockkBean
    lateinit var mockIssueDao: IssueDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockNominatimService: NominatimService

    @MockkBean
    lateinit var mockEventPublisher: EventPublisher

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockIssueCsvExporter: IssueCsvExporter

    lateinit var lyon: Government
    lateinit var ara: Government
    lateinit var france: Government

    lateinit var otherCategory: IssueCategory
    lateinit var highwaysCategory: IssueCategory
    lateinit var lightingCategory: IssueCategory

    @BeforeEach
    fun prepare() {
        otherCategory = IssueCategory().apply {
            id = 1L
            label = "Other"
            descriptionRequired = true
        }
        highwaysCategory = IssueCategory().apply {
            id = 2L
            label = "Highways"
        }
        lightingCategory = IssueCategory().apply {
            id = 3L
            label = "Lighting"
        }

        every { mockIssueCategoryDao.findByIdOrThrow(otherCategory.id!!, any()) } returns otherCategory
        every { mockIssueCategoryDao.findByIdOrThrow(highwaysCategory.id!!, any()) } returns highwaysCategory
        every { mockIssueCategoryDao.findByIdOrThrow(lightingCategory.id!!, any()) } returns lightingCategory

        france = Government().apply {
            id = 1L
            name = "France"
            code = "1234"
            url = "https://france.fr"
            osmId = 1001
            addIssueCategory(otherCategory)
            addIssueCategory(highwaysCategory)
        }
        lyon = Government().apply {
            id = 2L
            name = "Lyon"
            code = "5678"
            url = "https://lyon.fr"
            osmId = 1002
            addIssueCategory(otherCategory)
            addIssueCategory(lightingCategory)
            logo = Picture().apply {
                id = 10021
                creationInstant = Instant.now()
            }
        }
        ara = Government().apply {
            id = 3L
            name = "Auvergne Rhone Alpes"
            code = "9012"
            url = "https://auvergne-rhone-alpes.fr"
            osmId = 1003
            addIssueCategory(otherCategory)
            addIssueCategory(lightingCategory)
        }

        every { mockNominatimService.details(any()) } returns
                DetailsResult(
                    listOf(
                        DetailsAddress(osmId = null, osmType = null, adminLevel = null),
                        DetailsAddress(osmId = france.osmId, osmType = OsmType.RELATION, adminLevel = 2),
                        DetailsAddress(osmId = ara.osmId, osmType = OsmType.RELATION, adminLevel = 3),
                        DetailsAddress(osmId = lyon.osmId, osmType = OsmType.RELATION, adminLevel = 4)
                    )
                )

        every { mockGovernmentDao.findByOsmIds(setOf(france.osmId, ara.osmId, lyon.osmId)) } returns listOf(
            france,
            lyon
        )
    }

    @Nested
    inner class Search {
        lateinit var towerFellDown: Issue
        lateinit var bridgeCollapsed: Issue

        lateinit var lyonAdmin: User
        lateinit var superAdmin: User

        @BeforeEach
        fun prepare() {
            lyonAdmin = User().apply {
                id = 11L
            }
            superAdmin = User().apply {
                id = 42L
                superAdmin = true
            }

            towerFellDown = Issue().apply {
                id = 1001
                creationInstant = Instant.now()
                status = IssueStatus.CREATED
                description = "Tower fell down"
                coordinates = GeoCoordinates(48.858624, 2.294537)
            }
            bridgeCollapsed = Issue().apply {
                id = 1002
                creationInstant = Instant.now()
                status = IssueStatus.CREATED
                description = "Bridge collapsed"
                coordinates = GeoCoordinates(48.861978, 2.3104929)
                assignedGovernment = lyon
            }
        }

        @Test
        fun `should list all non-draft administered issues if no status`() {
            every { mockAuthorizations.currentUser } returns superAdmin
            val page = PageRequest.of(0, PAGE_SIZE)

            every {
                mockIssueDao.findByCriteria(
                    IssueSearchCriteria(
                        user = superAdmin,
                        statuses = IssueStatus.nonDraft
                    ),
                    page
                )
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                2
            )
            mockMvc.perform(get("/api/issues"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.totalPages").value(1))
        }

        @Test
        fun `should list first page of all issues with given statuses if user is super admin`() {
            every { mockAuthorizations.currentUser } returns superAdmin
            val page = PageRequest.of(0, PAGE_SIZE)
            every {
                mockIssueDao.findByCriteria(
                    IssueSearchCriteria(
                        user = superAdmin,
                        statuses = setOf(IssueStatus.DRAFT, IssueStatus.CREATED)
                    ),
                    page
                )
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                2
            )

            mockMvc.perform(
                get("/api/issues")
                    .param("status", IssueStatus.DRAFT.name, IssueStatus.CREATED.name)
            )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.content[0].pictures").isArray)
                .andExpect(jsonPath("$.content[0].accessToken").doesNotExist())
                .andExpect(jsonPath("$.content[0].assignedGovernment").value(nullValue()))
                .andExpect(jsonPath("$.content[0].transitions").isArray)
                .andExpect(jsonPath("$.content[1].assignedGovernment.id").value(lyon.id!!))
                .andExpect(jsonPath("$.content[1].assignedGovernment.logo.id").value(lyon.logo!!.id!!))
        }

        @Test
        fun `should list given page of given statuses for not super admin user`() {
            every { mockAuthorizations.currentUser } returns lyonAdmin

            val page = PageRequest.of(6, PAGE_SIZE)
            every {
                mockIssueDao.findByCriteria(
                    IssueSearchCriteria(
                        user = lyonAdmin,
                        statuses = setOf(IssueStatus.DRAFT, IssueStatus.CREATED)
                    ),
                    page
                )
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                62
            )

            mockMvc.perform(
                get("/api/issues")
                    .param("page", "6")
                    .param("status", IssueStatus.DRAFT.name, IssueStatus.CREATED.name)
            )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(6))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(62))
                .andExpect(jsonPath("$.totalPages").value(7))
        }
    }

    @Nested
    inner class Mine {
        lateinit var towerFellDown: Issue
        lateinit var bridgeCollapsed: Issue

        lateinit var me: User

        @BeforeEach
        fun prepare() {
            me = User().apply {
                id = 11L
            }

            towerFellDown = Issue().apply {
                id = 1001
                creationInstant = Instant.now()
                status = IssueStatus.CREATED
                description = "Tower fell down"
                coordinates = GeoCoordinates(48.858624, 2.294537)
            }
            bridgeCollapsed = Issue().apply {
                id = 1002
                creationInstant = Instant.now()
                status = IssueStatus.RESOLVED
                description = "Bridge collapsed"
                coordinates = GeoCoordinates(48.861978, 2.3104929)
                assignedGovernment = lyon
            }
        }

        @Test
        fun `should list all non-draft issues created by me if no status`() {
            every { mockAuthorizations.currentUser } returns me
            val page = PageRequest.of(0, PAGE_SIZE)

            every {
                mockIssueDao.findByCreatorAndStatuses(me, IssueStatus.nonDraft, page)
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                2
            )
            mockMvc.perform(get("/api/issues/mine"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.totalPages").value(1))
        }

        @Test
        fun `should list first page of my issues with given statuses`() {
            every { mockAuthorizations.currentUser } returns me
            val page = PageRequest.of(0, PAGE_SIZE)
            every {
                mockIssueDao.findByCreatorAndStatuses(me, setOf(IssueStatus.CREATED, IssueStatus.RESOLVED), page)
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                2
            )

            mockMvc.perform(
                get("/api/issues/mine")
                    .param("status", IssueStatus.CREATED.name, IssueStatus.RESOLVED.name)
            )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(0))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(2))
                .andExpect(jsonPath("$.totalPages").value(1))
                .andExpect(jsonPath("$.content[0].pictures").isArray)
                .andExpect(jsonPath("$.content[0].accessToken").doesNotExist())
                .andExpect(jsonPath("$.content[0].assignedGovernment").value(nullValue()))
                .andExpect(jsonPath("$.content[1].assignedGovernment.id").value(lyon.id!!))
                .andExpect(jsonPath("$.content[1].assignedGovernment.logo.id").value(lyon.logo!!.id!!))
        }

        @Test
        fun `should list given page of given statuses`() {
            every { mockAuthorizations.currentUser } returns me

            val page = PageRequest.of(6, PAGE_SIZE)
            every {
                mockIssueDao.findByCreatorAndStatuses(me, setOf(IssueStatus.CREATED, IssueStatus.RESOLVED), page)
            } returns PageImpl(
                listOf(towerFellDown, bridgeCollapsed),
                page,
                62
            )

            mockMvc.perform(
                get("/api/issues/mine")
                    .param("page", "6")
                    .param("status", IssueStatus.CREATED.name, IssueStatus.RESOLVED.name)
            )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.number").value(6))
                .andExpect(jsonPath("$.size").value(PAGE_SIZE))
                .andExpect(jsonPath("$.totalElements").value(62))
                .andExpect(jsonPath("$.totalPages").value(7))
        }
    }

    @Nested
    inner class Get {
        @Test
        fun `should get an issue`() {
            val issue = Issue().apply {
                id = 1001
                creationInstant = Instant.now()
                coordinates = GeoCoordinates(48.858624, 2.294537)
                status = IssueStatus.RESOLVED
                addPicture(Picture().apply {
                    id = 10012
                    creationInstant = Instant.now().minusMillis(1000L)
                })
                addPicture(Picture().apply {
                    id = 10011
                    creationInstant = Instant.now().minusMillis(2000L)
                })
                assignedGovernment = lyon
                addTransition(IssueTransition().apply {
                    afterStatus = IssueStatus.RESOLVED
                    beforeStatus = IssueStatus.CREATED
                    message = "well done"
                    transitionInstant = Instant.now()
                })
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue

            mockMvc.perform(get("/api/issues/${issue.id}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(issue.id!!))
                .andExpect(jsonPath("$.pictures[0].id").value(10011)) // pictures should be present and sorted by creation instant
                .andExpect(jsonPath("$.pictures[0].creationInstant").exists())
                .andExpect(jsonPath("$.accessToken").doesNotExist())
                .andExpect(jsonPath("$.assignedGovernment.id").value(lyon.id!!))
                .andExpect(jsonPath("$.assignedGovernment.name").value(lyon.name))
                .andExpect(jsonPath("$.assignedGovernment.logo.id").value(lyon.logo!!.id!!))
                .andExpect(jsonPath("$.transitions.length()").value(1))
                .andExpect(jsonPath("$.transitions[0].message").value("well done"))
                .andExpect(jsonPath("$.transitions[0].transitionInstant").isString)
                .andExpect(jsonPath("$.transitions[0].beforeStatus").value(IssueStatus.CREATED.name))
                .andExpect(jsonPath("$.transitions[0].afterStatus").value(IssueStatus.RESOLVED.name))
        }
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class Create {

        @BeforeEach
        fun prepare() {
            // anonymous by default
            every { mockAuthorizations.isAnonymous } returns true
        }

        @Test
        fun `should create DRAFT issue with minimum required properties, anonymously`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537)
            )

            every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 42L }

            mockMvc.perform(
                post("/api/issues")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(42L))
                .andExpect(jsonPath("$.status").value(IssueStatus.DRAFT.name))
                .andExpect(jsonPath("$.coordinates.latitude").value(command.coordinates.latitude))
                .andExpect(jsonPath("$.coordinates.longitude").value(command.coordinates.longitude))
                .andExpect(jsonPath("$.description").value(nullValue()))
                .andExpect(jsonPath("$.category").value(nullValue()))
                .andExpect(jsonPath("$.accessToken").value(CoreMatchers.notNullValue()))

            verify(inverse = true) { mockEventPublisher.publish(any()) }
            verify {
                mockIssueDao.saveAndFlush(
                    withArg<Issue> {
                        assertThat(it.accessToken).isNotBlank()
                        assertThat(it.creator).isNull()
                    }
                )
            }
        }

        @Test
        fun `should create DRAFT issue with minimum required properties, as an authenticated user`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537)
            )

            every { mockAuthorizations.isAnonymous } returns false
            val user = User()
            every { mockAuthorizations.currentUser } returns user
            every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 42L }

            mockMvc.perform(
                post("/api/issues")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(42L))
                .andExpect(jsonPath("$.status").value(IssueStatus.DRAFT.name))
                .andExpect(jsonPath("$.coordinates.latitude").value(command.coordinates.latitude))
                .andExpect(jsonPath("$.coordinates.longitude").value(command.coordinates.longitude))
                .andExpect(jsonPath("$.description").value(nullValue()))
                .andExpect(jsonPath("$.category").value(nullValue()))
                .andExpect(jsonPath("$.accessToken").doesNotExist())

            verify(inverse = true) { mockEventPublisher.publish(any()) }
            verify {
                mockIssueDao.saveAndFlush(
                    withArg<Issue> {
                        assertThat(it.accessToken).isNull()
                        assertThat(it.creator).isEqualTo(user)
                    }
                )
            }
        }

        @Test
        fun `should create CREATED issue with all values`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = otherCategory.id,
                status = IssueStatus.CREATED
            )

            every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 42L }

            mockMvc.perform(
                post("/api/issues")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(42L))
                .andExpect(jsonPath("$.status").value(IssueStatus.CREATED.name))
                .andExpect(jsonPath("$.coordinates.latitude").value(command.coordinates.latitude))
                .andExpect(jsonPath("$.coordinates.longitude").value(command.coordinates.longitude))
                .andExpect(jsonPath("$.description").value(command.description!!))
                .andExpect(jsonPath("$.category.id").value(otherCategory.id!!))
                .andExpect(jsonPath("$.category.label").value(otherCategory.label))
                .andExpect(jsonPath("$.accessToken").doesNotExist())

            verify { mockEventPublisher.publish(IssueCreated(42L)) }
            verify {
                mockIssueDao.saveAndFlush(
                    withArg<Issue> {
                        assertThat(it.accessToken).isNull()
                    }
                )
            }
        }

        @Test
        fun `should not allow other statuses than CREATED and DRAFT`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = otherCategory.id,
                status = IssueStatus.REJECTED
            )

            mockMvc.perform(
                post("/api/issues")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            verify(inverse = true) { mockIssueDao.saveAndFlush(any<Issue>()) }
        }

        @Test
        fun `should set leaf and assigned governments when creating with CREATED status`() {
            val command1 = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = null,
                categoryId = lightingCategory.id,
                status = IssueStatus.CREATED
            )
            val command2 = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = null,
                categoryId = highwaysCategory.id,
                status = IssueStatus.CREATED
            )

            every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 42L }

            val createdIssues = mutableListOf<Issue>()

            controller.create(command1)
            controller.create(command2)

            verify(exactly = 2) { mockIssueDao.saveAndFlush(capture(createdIssues)) }

            with(createdIssues[0]) {
                assertThat(leafGovernment).isEqualTo(lyon)
                assertThat(assignedGovernment).isEqualTo(lyon)
            }
            with(createdIssues[1]) {
                assertThat(leafGovernment).isEqualTo(lyon)
                assertThat(assignedGovernment).isEqualTo(france)
            }
        }

        @Test
        fun `should throw if government can't be found`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = lightingCategory.id,
                status = IssueStatus.CREATED
            )
            every { mockNominatimService.details(command.coordinates) } returns DetailsResult(emptyList())

            assertThatExceptionOfType(BadRequestException::class.java).isThrownBy {
                controller.create(command)
            }.withMessageContaining("No government for coordinates")
        }

        @ParameterizedTest
        @MethodSource("blankDescriptions")
        fun `should throw if no description and description required`(blankDescription: String?) {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = blankDescription,
                categoryId = otherCategory.id,
                status = IssueStatus.CREATED
            )

            every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 42L }

            assertThatExceptionOfType(BadRequestException::class.java).isThrownBy {
                controller.create(command)
            }.withMessageContaining("A description is required")
        }

        fun blankDescriptions() = listOf(null, "", "  ", "\n")
    }

    @Nested
    @TestInstance(TestInstance.Lifecycle.PER_CLASS)
    inner class Update {
        lateinit var issue: Issue

        @BeforeEach
        fun prepare() {
            issue = Issue().apply {
                id = 42L
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue

            // may update by default
            every { mockAuthorizations.mayUpdateIssue(any()) } returns true
        }

        @Test
        fun `should update DRAFT issue with minimum required properties anonymously`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537)
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            with(issue) {
                assertThat(status).isEqualTo(IssueStatus.DRAFT)
                assertThat(coordinates).isEqualTo(command.coordinates)
                assertThat(description).isNull()
                assertThat(category).isNull()
            }

            verify(inverse = true) { mockEventPublisher.publish(any()) }
            verify { mockAuthorizations.mayUpdateIssue(issue) }
        }

        @Test
        fun `should update CREATED issue with all values`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = 1L,
                status = IssueStatus.CREATED
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            with(issue) {
                assertThat(status).isEqualTo(command.status)
                assertThat(coordinates).isEqualTo(command.coordinates)
                assertThat(description).isEqualTo(command.description)
                assertThat(category).isEqualTo(category)

                assertThat(issue.leafGovernment).isEqualTo(lyon)
                assertThat(issue.assignedGovernment).isEqualTo(lyon)
            }

            verify { mockEventPublisher.publish(IssueCreated(issue.id!!)) }
            verify { mockAuthorizations.mayUpdateIssue(issue) }
        }

        @Test
        fun `should not allow other statuses than CREATED and DRAFT`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = 1L,
                status = IssueStatus.RESOLVED
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            with(issue) {
                assertThat(status).isEqualTo(issue.status)
            }
        }

        @Test
        fun `should not allow an issue to go from CREATED to DRAFT`() {
            issue.status = IssueStatus.CREATED
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = 1L,
                status = IssueStatus.DRAFT
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            with(issue) {
                assertThat(status).isEqualTo(issue.status)
            }
        }

        @Test
        fun `should throw if user may not update issue`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = "hello",
                categoryId = 1L,
                status = IssueStatus.DRAFT
            )

            every { mockAuthorizations.mayUpdateIssue(issue) } returns false

            mockMvc.perform(
                put("/api/issues/${issue.id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isForbidden)
        }

        @ParameterizedTest
        @MethodSource("blankDescriptions")
        fun `should throw if no description and description required`() {
            val command = IssueCommandDTO(
                coordinates = GeoCoordinates(48.858624, 2.294537),
                description = null,
                categoryId = otherCategory.id,
                status = IssueStatus.CREATED
            )

            assertThatExceptionOfType(BadRequestException::class.java).isThrownBy {
                controller.update(issue.id!!, command)
            }.withMessageContaining("A description is required")
        }

        fun blankDescriptions() = listOf(null, "", "  ", "\n")
    }

    @Nested
    inner class UpdateStatus {
        lateinit var issue: Issue

        @BeforeEach
        fun prepare() {
            issue = Issue().apply {
                id = 42L
                status = IssueStatus.CREATED
                assignedGovernment = Government()
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
            every { mockAuthorizations.atLeastGovernmentAdmin(issue.assignedGovernment!!) } returns true
        }

        @Test
        fun `should update the status of an issue`() {
            val command = IssueStatusCommandDTO(
                status = IssueStatus.RESOLVED,
                message = "well done"
            )

            every { mockIssueDao.flush() } answers {
                issue.transitions[0].id = 4321
            }

            mockMvc.perform(
                put("/api/issues/${issue.id}/status")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(issue.status).isEqualTo(command.status)
            assertThat(issue.transitions).hasSize(1)
            issue.transitions.first().let {
                assertThat(it.issue).isEqualTo(issue)
                assertThat(it.beforeStatus).isEqualTo(IssueStatus.CREATED)
                assertThat(it.afterStatus).isEqualTo(IssueStatus.RESOLVED)
                assertThat(it.message).isEqualTo(command.message)
                assertThat(it.transitionInstant).isCloseTo(Instant.now(), byLessThan(10, ChronoUnit.SECONDS))
            }

            verify { mockEventPublisher.publish(IssueTransitioned(4321)) }
        }

        @Test
        fun `should throw if the existing status of an issue is DRAFT`() {
            issue.status = IssueStatus.DRAFT

            val command = IssueStatusCommandDTO(
                status = IssueStatus.RESOLVED
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}/status")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            assertThat(issue.status).isEqualTo(IssueStatus.DRAFT)
        }

        @Test
        fun `should throw if the new status is DRAFT`() {
            val command = IssueStatusCommandDTO(
                status = IssueStatus.DRAFT
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}/status")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            assertThat(issue.status).isEqualTo(IssueStatus.CREATED)
        }

        @Test
        fun `should throw if the issue status may not be modified by the user`() {
            every { mockAuthorizations.atLeastGovernmentAdmin(issue.assignedGovernment!!) } returns false

            val command = IssueStatusCommandDTO(
                status = IssueStatus.RESOLVED
            )

            mockMvc.perform(
                put("/api/issues/${issue.id}/status")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isForbidden)

            assertThat(issue.status).isEqualTo(IssueStatus.CREATED)
        }
    }

    @Nested
    inner class Export {

        @Test
        fun `should export with provided time zone`() {
            val from = LocalDate.of(2019, 1, 1)
            val to = LocalDate.of(2019, 1, 31)
            val timezone = ZoneId.of("Asia/Tokyo")

            val fakeCsvOutput = "csv content".toByteArray()

            every { mockIssueCsvExporter.export(any(), any(), any(), any()) } answers {
                val outputStream = firstArg() as OutputStream
                outputStream.write(fakeCsvOutput)
            }

            mockMvc.perform(
                get("/api/issues")
                    .param("export", "CSV")
                    .param("from", from.toString())
                    .param("to", to.toString())
                    .param("timezone", timezone.toString())
            )
                .andExpect(request().asyncStarted())
                .andDo { it.getAsyncResult() }
                .andExpect(status().isOk)
                .andExpect(content().contentType("text/csv"))
                .andExpect(content().bytes(fakeCsvOutput))

            verify {
                mockIssueCsvExporter.export(
                    any(),
                    Instant.parse("2018-12-31T15:00:00Z"),
                    Instant.parse("2019-01-31T15:00:00Z"),
                    timezone
                )
            }
        }

        @Test
        fun `should export with system time zone when none is provided`() {
            val from = LocalDate.of(2019, 1, 1)
            val to = LocalDate.of(2019, 1, 31)

            val fakeCsvOutput = "csv content".toByteArray()

            every { mockIssueCsvExporter.export(any(), any(), any(), any()) } answers {
                val outputStream = firstArg() as OutputStream
                outputStream.write(fakeCsvOutput)
            }

            mockMvc.perform(
                get("/api/issues")
                    .param("export", "CSV")
                    .param("from", from.toString())
                    .param("to", to.toString())
            )
                .andExpect(request().asyncStarted())
                .andDo { it.getAsyncResult() }
                .andExpect(status().isOk)
                .andExpect(content().contentType("text/csv"))
                .andExpect(content().bytes(fakeCsvOutput))

            verify {
                mockIssueCsvExporter.export(
                    any(),
                    from.atStartOfDay(ZoneId.systemDefault()).toInstant(),
                    to.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant(),
                    ZoneId.systemDefault()
                )
            }
        }
    }

    @Nested
    inner class Deanonymize {

        lateinit var issue: Issue
        lateinit var user: User

        @BeforeEach
        fun prepare() {
            issue = Issue().apply {
                id = 1001
                accessToken = "access-token"
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue

            user = User()
            every { mockAuthorizations.currentUser } returns user
        }

        @Test
        fun `should deanonymize anonymous issue`() {
            mockMvc.perform(
                put("/api/issues/${issue.id}/creator")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(DeanonymizationCommandDTO(issue.accessToken!!)))
            )
                .andExpect(status().isNoContent)

            assertThat(issue.creator).isEqualTo(user)
        }

        @Test
        fun `should throw if issue is anonymous and incorrect access token`() {
            mockMvc.perform(
                put("/api/issues/${issue.id}/creator")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(DeanonymizationCommandDTO("other-token")))
            )
                .andExpect(status().isForbidden)

            assertThat(issue.creator).isNull()
        }

        @Test
        fun `should do nothing if issue already has current user as creator`() {
            issue.creator = user

            mockMvc.perform(
                put("/api/issues/${issue.id}/creator")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(DeanonymizationCommandDTO("other-token")))
            )
                .andExpect(status().isNoContent)

            assertThat(issue.creator).isEqualTo(user)
        }

        @Test
        fun `should throw if issue already has other creator`() {
            val otherUser = User()
            issue.creator = otherUser

            mockMvc.perform(
                put("/api/issues/${issue.id}/creator")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(DeanonymizationCommandDTO("other-token")))
            )
                .andExpect(status().isForbidden)

            assertThat(issue.creator).isEqualTo(otherUser)
        }
    }

    @Nested
    inner class Delete {
        lateinit var issue: Issue

        @BeforeEach
        fun prepare() {
            issue = Issue().apply {
                id = 42L
                assignedGovernment = Government()
                addPicture(Picture().apply { path = "p1" })
                addPicture(Picture().apply { path = "p2" })
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
            every { mockAuthorizations.atLeastGovernmentAdmin(issue.assignedGovernment!!) } returns true
        }

        @Test
        fun `should delete an issue`() {
            mockMvc.perform(
                delete("/api/issues/${issue.id}")
            )
                .andExpect(status().isNoContent)
            verify(ordering = Ordering.ORDERED) {
                mockEventPublisher.publish(IssueDeleted(issue.id!!, setOf("p1", "p2") ))
                mockIssueDao.delete(issue)
            }
        }

        @Test
        fun `should throw if the user is not at least admin of government of issue`() {
            every { mockAuthorizations.atLeastGovernmentAdmin(issue.assignedGovernment!!) } returns false

            mockMvc.perform(
                delete("/api/issues/${issue.id}")
            )
                .andExpect(status().isForbidden)
            verify(inverse = true) { mockIssueDao.delete(issue) }
        }

        @Test
        fun `should throw if the user is not a superadmin and the issue doesn't have an assigned government`() {
            issue.assignedGovernment = null
            val notSuperAdmin = User().apply { superAdmin = false }
            every { mockAuthorizations.currentUser } returns notSuperAdmin

            mockMvc.perform(
                delete("/api/issues/${issue.id}")
            )
                .andExpect(status().isForbidden)
            verify(inverse = true) { mockIssueDao.delete(issue) }
        }

        @Test
        fun `should delete an issue if superAdmin and no assigned government`() {
            issue.assignedGovernment = null
            val superAdmin = User().apply { superAdmin = true }
            every { mockAuthorizations.currentUser } returns superAdmin

            mockMvc.perform(
                delete("/api/issues/${issue.id}")
            )
                .andExpect(status().isNoContent)
            verify { mockIssueDao.delete(issue) }
        }
    }
}
