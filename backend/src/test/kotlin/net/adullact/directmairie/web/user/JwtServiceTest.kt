package net.adullact.directmairie.web.user

import io.jsonwebtoken.JwtException
import net.adullact.directmairie.config.SecurityProperties
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test

/**
 * Unit test for [JwtService]
 * @author JB Nizet
 */
internal class JwtServiceTest {

    val jwtService = JwtService(SecurityProperties(JwtService.generateSecretKey()))

    @Test
    fun `should generate token`() {
        val token = jwtService.buildToken(1000L)
        assertThat(token).matches(""".*\..*\..*""")
        assertThat(jwtService.extractUserId(token)).isEqualTo(1000L)
    }

    @Test
    fun `should fail parsing token if wrong key`() {
        val token = jwtService.buildToken(1000L)
        val otherKey = JwtService.generateSecretKey()
        val otherJwtService = JwtService(SecurityProperties(otherKey))
        assertThatExceptionOfType(JwtException::class.java).isThrownBy { otherJwtService.extractUserId(token) }
    }
}
