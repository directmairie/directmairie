package net.adullact.directmairie.web.user

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verify
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.EmailModificationRequested
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.UserRegistered
import net.adullact.directmairie.test.USER_EMAIL
import net.adullact.directmairie.test.USER_ID
import net.adullact.directmairie.test.returnsModifiedFirstArg
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.within
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * MVC tests for [UserController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(UserController::class)
class UserControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockPasswordHasher: PasswordHasher

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockEmailVerificationDao: EmailVerificationDao

    @MockkBean
    lateinit var mockEventPublisher: EventPublisher

    @MockkBean
    lateinit var mockJwtService: JwtService

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockIssueDao: IssueDao

    @Nested
    inner class Register {
        @Test
        fun `should register user`() {
            val command = UserRegistrationCommandDTO(
                email = "john@mail.com",
                password = "passw0rd",
                firstName = "Joe",
                lastName = "Doe",
                phone = "0612345678",
                issueTransitionNotificationActivated = true
            )

            every { mockPasswordHasher.hash(command.password) } returns "hashed"
            every { mockUserDao.findByEmail(command.email) } returns null
            every { mockUserDao.saveAndFlush(any<User>()) } returnsModifiedFirstArg { id = 1000L }
            every { mockEmailVerificationDao.saveAndFlush(any<EmailVerification>()) } returnsModifiedFirstArg {
                id = 1042L
            }

            mockMvc.perform(
                post("/api/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000))
                .andExpect(jsonPath("$.email").value(command.email))

            verify {
                mockEmailVerificationDao.saveAndFlush<EmailVerification>(withArg { verification ->
                    assertThat(verification.email).isEqualTo(command.email)
                    assertThat(verification.user.id).isEqualTo(1000)
                    assertThat(verification.secretToken.length).isGreaterThan(8)
                    assertThat(verification.validityInstant).isCloseTo(
                        Instant.now().plus(1, ChronoUnit.DAYS),
                        within(1, ChronoUnit.MINUTES)
                    )
                })
                mockEventPublisher.publish(UserRegistered(1042L))
            }
        }

        @Test
        fun `should register again existing user whose email is not verified yet so that a retry is possible`() {
            val command = UserRegistrationCommandDTO(
                email = "john@mail.com",
                password = "passw0rd",
                firstName = "John",
                lastName = "Doe",
                phone = "0612345678",
                issueTransitionNotificationActivated = true
            )

            val existingUser = User().apply {
                id = 1000L
                email = "john@mail.com"
                emailVerified = false
            }

            every { mockUserDao.findByEmail(command.email) } returns existingUser
            every { mockPasswordHasher.hash(command.password) } returns "hashed"
            every { mockEmailVerificationDao.saveAndFlush(any<EmailVerification>()) } returnsModifiedFirstArg {
                id = 1042L
            }

            mockMvc.perform(
                post("/api/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(1000))
                .andExpect(jsonPath("$.email").value(command.email))

            verify {
                mockEmailVerificationDao.saveAndFlush<EmailVerification>(withArg { verification ->
                    assertThat(verification.email).isEqualTo(command.email)
                    assertThat(verification.user).isEqualTo(existingUser)
                })
                mockEventPublisher.publish(UserRegistered(1042L))
            }

            assertThat(existingUser.email).isEqualTo(command.email)
            assertThat(existingUser.firstName).isEqualTo(command.firstName)
            assertThat(existingUser.lastName).isEqualTo(command.lastName)
            assertThat(existingUser.phone).isEqualTo(command.phone)
            assertThat(existingUser.issueTransitionNotificationActivated).isEqualTo(command.issueTransitionNotificationActivated)
            assertThat(existingUser.password).isEqualTo("hashed")
        }

        @Test
        fun `should fail registration if existing user has verified email()`() {
            val command = UserRegistrationCommandDTO(
                email = "john@mail.com",
                password = "passw0rd",
                firstName = null,
                lastName = null,
                phone = null
            )

            val existingUser = User().apply {
                id = 1000L
                email = "john@mail.com"
                emailVerified = true
            }

            every { mockUserDao.findByEmail(command.email) } returns existingUser

            mockMvc.perform(
                post("/api/users")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)

            verify(inverse = true) {
                mockEmailVerificationDao.saveAndFlush(any<EmailVerification>())
                mockEventPublisher.publish(any())
            }
        }
    }

    @Nested
    inner class VerifyEmail {
        @Test
        fun `should verify email`() {
            val user = User().apply {
                id = 1000L
                emailVerified = false
            }

            val token = "secret"
            val emailVerification = EmailVerification(
                user = user,
                secretToken = token,
                email = "john@mail.com",
                validityInstant = Instant.now().plusMillis(12345L)
            )

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, token) } returns emailVerification

            val jwtToken = "jwtToken"
            every { mockJwtService.buildToken(user.id!!) } returns jwtToken

            val command = EmailVerificationCommandDTO(token)

            mockMvc.perform(
                post("/api/users/${user.id}/email-verifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.token").value(jwtToken))

            assertThat(user.email).isEqualTo(emailVerification.email)
            assertThat(user.emailVerified).isTrue()

            verify { mockEmailVerificationDao.delete(emailVerification) }
        }

        @Test
        fun `should fail email verification if email verification is invalid`() {
            val user = User().apply {
                id = 1000L
                emailVerified = false
            }

            val token = "secret"

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, token) } returns null

            val command = EmailVerificationCommandDTO(token)

            mockMvc.perform(
                post("/api/users/${user.id}/email-verifications")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }
    }

    @Nested
    inner class Me {
        @Test
        fun `should get the current user details`() {
            val user = User().apply {
                id = 1000L
                email = "john@mail.com"
                firstName = "John"
                lastName = "Doe"
                phone = "0612345678"
                superAdmin = true
                addAdministeredGovernment(Government())
                addAdministeredPoolingOrganization(PoolingOrganization())
            }

            every { mockAuthorizations.currentUser } returns user

            mockMvc.perform(get("/api/users/me"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(user.id!!))
                .andExpect(jsonPath("$.email").value(user.email))
                .andExpect(jsonPath("$.firstName").value(user.firstName))
                .andExpect(jsonPath("$.lastName").value(user.lastName))
                .andExpect(jsonPath("$.phone").value(user.phone))
                .andExpect(jsonPath("$.issueTransitionNotificationActivated").value(user.issueTransitionNotificationActivated))
                .andExpect(jsonPath("$.superAdmin").value(user.superAdmin))
                .andExpect(jsonPath("$.poolingOrganizationAdmin").value(true))
                .andExpect(jsonPath("$.governmentAdmin").value(true))
        }
    }

    @Nested
    inner class Search {
        @Test
        fun `should search by email`() {
            val pageRequest = PageRequest.of(0, PAGE_SIZE)
            val user = User().apply {
                id = 1000L
                email = "john@mail.com"
            }

            every { mockAuthorizations.atLeastAnyPoolingOrganizationAdmin() } returns true
            every { mockUserDao.searchByVerifiedEmail("john", pageRequest) } returns PageImpl(
                listOf(user),
                pageRequest,
                1L
            )

            mockMvc.perform(get("/api/users?query=john"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.content").isArray)
                .andExpect(jsonPath("$.content[0].id").value(user.id!!))
                .andExpect(jsonPath("$.content[0].email").value(user.email))
        }

        @Test
        fun `should throw when searching by email if not at least pooling organization admin`() {
            every { mockAuthorizations.atLeastAnyPoolingOrganizationAdmin() } returns false

            mockMvc.perform(get("/api/users?query=john"))
                .andExpect(status().isForbidden)
        }
    }

    @Nested
    inner class RequestEmailVerificationToken {
        val user = User().apply {
            id = USER_ID
            email = USER_EMAIL
            firstName = "John"
            lastName = "Doe"
            phone = "0612345678"
            password = "hashed-password"
            emailVerified = true
        }

        val command = EmailModificationCommandDTO(
            password = "password",
            email = "jane@mail.com"
        )

        @BeforeEach
        fun prepare() {
            every { mockAuthorizations.currentUser } returns user
            every { mockPasswordHasher.verify(any(), user.password) } returns true
            every { mockUserDao.findByVerifiedEmail(any()) } returns null
            every { mockEmailVerificationDao.saveAndFlush(any()) } returnsModifiedFirstArg { id = 54L }
        }

        @Test
        fun `should send an email by publishing an event`() {
            mockMvc.perform(
                post("/api/users/me/email-modification")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            verify {
                mockEmailVerificationDao.saveAndFlush(withArg {
                    assertThat(it.email).isEqualTo(command.email)
                    assertThat(it.secretToken).isNotNull()
                    assertThat(it.user).isEqualTo(user)
                    assertThat(it.validityInstant).isAfter(Instant.now())
                })
                mockEventPublisher.publish(EmailModificationRequested(54L))
            }
        }

        @Test
        fun `should throw if password is incorrect`() {
            every { mockPasswordHasher.verify(command.password, user.password) } returns false

            mockMvc.perform(
                post("/api/users/me/email-modification")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw if email is already used by other user`() {
            every { mockUserDao.findByVerifiedEmail(command.email) } returns User()

            mockMvc.perform(
                post("/api/users/me/email-modification")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }
    }

    @Nested
    inner class UpdateCurrentUser {
        val user = User().apply {
            id = USER_ID
            email = USER_EMAIL
            firstName = "John"
            lastName = "Doe"
            phone = "0612345678"
            password = "hashed-password"
            emailVerified = true
        }

        @BeforeEach
        fun prepare() {
            every { mockAuthorizations.currentUser } returns user
            every { mockPasswordHasher.verify(any(), user.password) } returns true
            every { mockUserDao.findByVerifiedEmail(any()) } returns null
        }

        @Test
        fun `should change everything except the password if the new password is null`() {
            val emailVerification = EmailVerification(
                email = "jane@mail.com",
                user = user,
                secretToken = "theToken",
                validityInstant = Instant.now()
            )

            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = "jane@mail.com",
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                issueTransitionNotificationActivated = true,
                newPassword = null,
                emailModificationToken = emailVerification.secretToken
            )

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, command.emailModificationToken!!) } returns emailVerification

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(user.email).isEqualTo(command.email)
            assertThat(user.firstName).isEqualTo(command.firstName)
            assertThat(user.lastName).isEqualTo(command.lastName)
            assertThat(user.phone).isEqualTo(command.phone)
            assertThat(user.issueTransitionNotificationActivated).isEqualTo(command.issueTransitionNotificationActivated)
            assertThat(user.password).isEqualTo("hashed-password")
        }

        @Test
        fun `should change the password if not null and set fields to null if blank`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                newPassword = "new-password",
                email = user.email,
                firstName = "",
                lastName = "",
                phone = "",
                emailModificationToken = null
            )

            every { mockPasswordHasher.hash(command.newPassword!!) } returns "new-hashed-password"

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(user.email).isEqualTo(command.email)
            assertThat(user.firstName).isNull()
            assertThat(user.lastName).isNull()
            assertThat(user.phone).isNull()
            assertThat(user.issueTransitionNotificationActivated).isFalse()
            assertThat(user.password).isEqualTo("new-hashed-password")
        }

        @Test
        fun `should change without checking token if email stays the same`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = user.email,
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                issueTransitionNotificationActivated = true,
                newPassword = null,
                emailModificationToken = null
            )

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(user.email).isEqualTo(command.email)
            assertThat(user.firstName).isEqualTo(command.firstName)
            assertThat(user.lastName).isEqualTo(command.lastName)
            assertThat(user.phone).isEqualTo(command.phone)
            assertThat(user.issueTransitionNotificationActivated).isEqualTo(command.issueTransitionNotificationActivated)
            assertThat(user.password).isEqualTo("hashed-password")
        }

        @Test
        fun `should throw if token is invalid`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = "jane@mail.com",
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                newPassword = null,
                emailModificationToken = "theToken"
            )

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, command.emailModificationToken!!) } returns null

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw if token is valid but for a different email address`() {
            val emailVerification = EmailVerification(
                email = "not-jane@mail.com",
                user = user,
                secretToken = "theToken",
                validityInstant = Instant.now()
            )

            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = "jane@mail.com",
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                newPassword = null,
                emailModificationToken = emailVerification.secretToken
            )

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, command.emailModificationToken!!) } returns emailVerification

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw if token is null`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = "jane@mail.com",
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                newPassword = null,
                emailModificationToken = null
            )

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw if new email is already used by other user`() {
            val emailVerification = EmailVerification(
                email = "jane@mail.com",
                user = user,
                secretToken = "theToken",
                validityInstant = Instant.now()
            )

            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = "jane@mail.com",
                firstName = "",
                lastName = "",
                phone = "",
                newPassword = null,
                emailModificationToken = emailVerification.secretToken
            )

            every { mockEmailVerificationDao.findValidByUserAndToken(user.id!!, command.emailModificationToken!!) } returns emailVerification
            every { mockUserDao.findByVerifiedEmail(command.email) } returns User()

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should throw if current password is incorrect`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = user.email,
                firstName = "Jane",
                lastName = "Dean",
                phone = "0687654321",
                newPassword = null,
                emailModificationToken = null
            )

            every { mockPasswordHasher.verify(command.currentPassword, user.password) } returns false

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }

        @Test
        fun `should not throw if new email is same as existing one`() {
            val command = CurrentUserCommandDTO(
                currentPassword = "password",
                email = user.email,
                firstName = "",
                lastName = "",
                phone = "",
                newPassword = null,
                emailModificationToken = null
            )

            every { mockUserDao.findByVerifiedEmail(command.email) } returns user

            mockMvc.perform(
                put("/api/users/me")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)
        }
    }

    @Nested
    inner class DeleteCurrentUser {
        val government = Government()
        val org = PoolingOrganization()
        val user = User().apply {
            id = USER_ID
            emailVerified = true
            password = "hashed-password"
            addAdministeredGovernment(government)
            addAdministeredPoolingOrganization(org)
        }.also {
            government.addAdministrator(it)
            org.addAdministrator(it)
        }

        @BeforeEach
        fun prepare() {
            every { mockAuthorizations.currentUser } returns user
            every { mockPasswordHasher.verify(any(), user.password) } returns true
            every { mockUserDao.findByVerifiedEmail(any()) } returns null
        }

        @Test
        fun `should delete the current user`() {
            val command = CurrentUserDeletionCommandDTO("password")

            mockMvc.perform(
                put("/api/users/me/deletion")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isNoContent)

            assertThat(government.administrators).doesNotContain(user)
            assertThat(org.administrators).doesNotContain(user)
            verify {
                mockEmailVerificationDao.deleteByUser(user)
                mockIssueDao.anonymizeByCreator(user)
                mockUserDao.delete(user)
            }
        }

        @Test
        fun `should throw if current password is incorrect`() {
            val command = CurrentUserDeletionCommandDTO("password")

            every { mockPasswordHasher.verify(command.password, user.password) } returns false

            mockMvc.perform(
                put("/api/users/me/deletion")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsBytes(command))
            )
                .andExpect(status().isBadRequest)
        }
    }
}
