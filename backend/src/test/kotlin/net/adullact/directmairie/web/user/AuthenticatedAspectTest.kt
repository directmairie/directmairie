package net.adullact.directmairie.web.user

import net.adullact.directmairie.web.exception.UnauthorizedException
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test

/**
 * Unit test for [AuthenticatedAspect]
 * @author JB Nizet
 */
class AuthenticatedAspectTest {
    @Test
    fun `should do nothing if user is authenticated`() {
        val currentUser = CurrentUser(1000L)
        AuthenticatedAspect(currentUser).checkUserIsAuthenticated(null)
    }

    @Test
    fun `should throw if user is not authenticated`() {
        val currentUser = CurrentUser(null)
        assertThatExceptionOfType(UnauthorizedException::class.java).isThrownBy {
            AuthenticatedAspect(currentUser).checkUserIsAuthenticated(null)
        }
    }
}
