package net.adullact.directmairie.web.issue

import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.Picture
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.user.Authorizations
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.byLessThan
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.core.io.Resource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * MVC tests for [IssuePictureController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(IssuePictureController::class)
class IssuePictureControllerMvcTest(@Autowired val mockMvc: MockMvc) {

    @MockkBean
    lateinit var mockIssueDao: IssueDao

    @MockkBean
    lateinit var mockPictureFileService: PictureFileService

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @BeforeEach
    fun prepare() {
        // may update issue by default
        every { mockAuthorizations.mayUpdateIssue(any()) } returns true
    }

    @Nested
    inner class GetPictureBytes {

        lateinit var issue: Issue
        lateinit var picture: Picture

        @BeforeEach
        fun prepare() {
            picture = Picture().apply {
                id = 421L
                path = "0/1/2/3.jpg"
                contentType = MediaType.IMAGE_JPEG_VALUE
            }
            issue = Issue().apply {
                id = 42L
                addPicture(picture)
            }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
            every { mockPictureFileService.toResource(picture.path) } returns ByteArrayResource("hello".toByteArray())
        }

        @Test
        fun `should get the bytes with the content type of a picture`() {
            mockMvc.perform(get("/api/issues/{issueId}/pictures/{pictureId}/bytes", issue.id, picture.id))
                .andExpect(status().isOk)
                .andExpect(content().bytes("hello".toByteArray()))
                .andExpect(content().contentType(MediaType.IMAGE_JPEG_VALUE))
                .andExpect(header().longValue("Content-Length", "hello".length.toLong()))
        }

        @Test
        fun `should not get the bytes if picture is absent from issue`() {
            issue.removePicture(picture)
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue

            mockMvc.perform(get("/api/issues/{issueId}/pictures/{pictureId}/bytes", issue.id, 42L))
                .andExpect(status().isNotFound)
        }

        @Test
        fun `should not get the bytes if file is not present`() {
            val absentResource = mockk<Resource>()
            every { absentResource.exists() } returns false
            every { mockPictureFileService.toResource(picture.path) } returns absentResource

            mockMvc.perform(get("/api/issues/{issueId}/pictures/{pictureId}/bytes", issue.id, picture.id))
                .andExpect(status().isNotFound)
        }
    }

    @Nested
    inner class Delete {
        lateinit var issue: Issue
        lateinit var picture: Picture

        @BeforeEach
        fun prepare() {
            picture = Picture().apply {
                id = 421L
                path = "0/1/2/3.jpg"
            }
            issue = Issue().apply {
                id = 42L
                addPicture(picture)
            }
            every { mockIssueDao.findByIdOrNull(issue.id!!) } returns issue
        }

        @Test
        fun `should delete picture and its file`() {
            mockMvc.perform(delete("/api/issues/{issueId}/pictures/{pictureId}", issue.id, picture.id))
                .andExpect(status().isNoContent)

            verify { mockPictureFileService.delete(picture.path) }
            verify { mockAuthorizations.mayUpdateIssue(issue) }

            assertThat(issue.pictures).isEmpty()
        }

        @Test
        fun `should throw when deleting picture on non-DRAFT issue`() {
            issue.status = IssueStatus.CREATED

            mockMvc.perform(delete("/api/issues/{issueId}/pictures/{pictureId}", issue.id, picture.id))
                .andExpect(status().isBadRequest)

            verify(inverse = true) { mockPictureFileService.delete(picture.path) }
            assertThat(issue.pictures).isNotEmpty()
        }

        @Test
        fun `should throw when user may not update issue`() {
            every { mockAuthorizations.mayUpdateIssue(issue) } returns false

            mockMvc.perform(delete("/api/issues/{issueId}/pictures/{pictureId}", issue.id, picture.id))
                .andExpect(status().isForbidden)

            verify(inverse = true) { mockPictureFileService.delete(picture.path) }

            assertThat(issue.pictures).isNotEmpty()
        }
    }

    @Nested
    inner class Upload {

        lateinit var issue: Issue

        @BeforeEach
        fun prepare() {
            issue = Issue().apply { id = 42L }
            every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
        }

        @Test
        fun `should upload a picture`() {
            every { mockIssueDao.flush() } answers { issue.pictures.first().id = 12345L }
            every { mockPictureFileService.idToRelativePath(12345L, "jpeg") } returns "0/1/2/12345.jpeg"
            mockMvc.perform(
                multipart("/api/issues/{issueId}/pictures", issue.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            MediaType.IMAGE_JPEG_VALUE,
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isCreated)
                .andExpect(jsonPath("$.id").value(12345L))

            verify { mockAuthorizations.mayUpdateIssue(issue) }

            val picture = issue.pictures.first()
            assertThat(picture.creationInstant).isCloseTo(Instant.now(), byLessThan(1, ChronoUnit.MINUTES))
            assertThat(picture.path).isEqualTo("0/1/2/12345.jpeg")
            assertThat(picture.contentType).isEqualTo(MediaType.IMAGE_JPEG_VALUE)
        }

        @Test
        fun `should throw if issue is not DRAFT`() {
            issue.status = IssueStatus.CREATED

            mockMvc.perform(
                multipart("/api/issues/{issueId}/pictures", issue.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            MediaType.IMAGE_JPEG_VALUE,
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isBadRequest)

            assertThat(issue.pictures).isEmpty()
        }

        @Test
        fun `should throw when uploading a picture to an issue that the user may not access`() {
            every { mockAuthorizations.mayUpdateIssue(issue) } returns false

            mockMvc.perform(
                multipart("/api/issues/{issueId}/pictures", issue.id)
                    .file(
                        MockMultipartFile(
                            "file",
                            "blob",
                            MediaType.IMAGE_JPEG_VALUE,
                            "hello".toByteArray()
                        )
                    )
            )
                .andExpect(status().isForbidden)

            assertThat(issue.pictures).isEmpty()
        }
    }
}
