package net.adullact.directmairie.web.logo

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Picture
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import net.adullact.directmairie.web.user.AuthenticationController
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.user.CurrentUser
import net.adullact.directmairie.web.user.PasswordHasher
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.context.annotation.Import
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.get
import java.time.Instant

/**
 * MVC tests for [BrandingController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(BrandingController::class)
@Import(PasswordHasher::class)
class BrandingControllerMvcTest(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @Test
    fun `should get no government if anonymous`() {
        every {
            mockAuthorizations.isAnonymous
        } returns true

        mockMvc.get("/api/branding").andExpect {
            status { isOk() }
            jsonPath("$.government") { isEmpty() }
        }
    }

    @Test
    fun `should get no government if not exactly 1 administered government`() {
        every {
            mockAuthorizations.isAnonymous
        } returns false

        val user = User()

        every {
            mockAuthorizations.currentUser
        } returns user

        mockMvc.get("/api/branding").andExpect {
            status { isOk() }
            jsonPath("$.government") { isEmpty() }
        }
    }

    @Test
    fun `should get government if exactly 1 administered government`() {
        every {
            mockAuthorizations.isAnonymous
        } returns false

        val government = Government().apply {
            id = 12L
            code = "34567"
            osmId = 4567L
            name = "Loire"
            url = "http://loire.fr"
            logo = Picture().apply {
                id = 4567L
                creationInstant = Instant.now()
            }
        }
        val user = User().apply {
            addAdministeredGovernment(government)
        }

        every {
            mockAuthorizations.currentUser
        } returns user

        mockMvc.get("/api/branding").andDo { print() }.andExpect {
            status { isOk() }
            jsonPath("$.government.logo") { isNotEmpty() }
            jsonPath("$.government.url") { isNotEmpty() }
        }
    }
}
