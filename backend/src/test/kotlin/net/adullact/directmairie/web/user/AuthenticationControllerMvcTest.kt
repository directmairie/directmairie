package net.adullact.directmairie.web.user

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.test.web.DirectMairieMvcTest
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * MVC tests for [AuthenticationController]
 * @author JB Nizet
 */
@DirectMairieMvcTest(AuthenticationController::class)
@Import(PasswordHasher::class)
class AuthenticationControllerMvcTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper,
    @Autowired val passwordHasher: PasswordHasher
) {

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockJwtService: JwtService

    @Test
    fun `should authenticate`() {
        val command = AuthenticationCommandDTO("john@mail.com", "passw0rd")

        val user = User().apply {
            id = 1000
            password = passwordHasher.hash(command.password)
        }

        val token = "jwtTojen"
        every { mockUserDao.findByVerifiedEmail(command.email) } returns user
        every { mockJwtService.buildToken(user.id!!) } returns token

        mockMvc.perform(MockMvcRequestBuilders.post("/api/authentications")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsBytes(command)))
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.token").value(token))
    }

    @Test
    fun `should fail if password is wrong`() {
        val command = AuthenticationCommandDTO("john@mail.com", "passw0rd")

        val user = User().apply {
            id = 1000
            password = passwordHasher.hash("otherPassword")
        }

        every { mockUserDao.findByVerifiedEmail(command.email) } returns user

        mockMvc.perform(MockMvcRequestBuilders.post("/api/authentications")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsBytes(command)))
            .andExpect(status().isUnauthorized)
    }

    @Test
    fun `should fail if user is not found`() {
        val command = AuthenticationCommandDTO("john@mail.com", "passw0rd")

        every { mockUserDao.findByVerifiedEmail(command.email) } returns null

        mockMvc.perform(MockMvcRequestBuilders.post("/api/authentications")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsBytes(command)))
            .andExpect(status().isUnauthorized)
    }
}
