package net.adullact.directmairie.web.issuecategory

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.parameterWithName
import org.springframework.restdocs.request.RequestDocumentation.pathParameters
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * Documentation tests for [IssueCategoryController]
 * @author JB Nizet
 */
@DirectMairieDocTest(IssueCategoryController::class)
class IssueCategoryControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {
    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    val roadsGroup = issueGroupWithId(ROADS_GROUP_ID)
    val gardensGroup = IssueGroup().apply {
        id = 1000
        label = "Gardens"
    }

    val fallenTreeId = 1004L

    val descriptionRequired = fieldWithPath("descriptionRequired").description("true if a description must be entered by the user when creating an issue with that category, false if the description can be left blank")

    @BeforeEach
    fun prepare() {
        every { mockIssueGroupDao.findByIdOrThrow(roadsGroup.id!!) } returns roadsGroup
        every { mockIssueGroupDao.findByIdOrThrow(gardensGroup.id!!) } returns gardensGroup
    }

    @Test
    fun create() {
        val command = IssueCategoryCommandDTO(label = "Fallen tree", descriptionRequired = false)
        every { mockIssueCategoryDao.findByLabel(command.label) } returns null

        every { mockIssueGroupDao.flush() } answers {
            gardensGroup.categories.find { it.label == command.label }?.id = fallenTreeId
        }

        mockMvc.perform(
            docPost("/api/issue-groups/{groupId}/categories", gardensGroup.id!!)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "issue-categories/create",
                    pathParameters(
                        parameterWithName("groupId").description("The ID of the issue group which must own the created category")
                    ),
                    requestFields(
                        fieldWithPath("label").description("The label of the issue category to create"),
                        descriptionRequired
                    ),
                    responseFields(
                        fieldWithPath("id").description("The ID of the issue category"),
                        fieldWithPath("label").description("The label of the issue category"),
                        descriptionRequired
                    )
                )
            )
    }

    @Test
    fun update() {
        val command = IssueCategoryCommandDTO(label = "Broken central barrier", descriptionRequired = false)
        every { mockIssueCategoryDao.findByLabel(command.label) } returns null

        mockMvc.perform(
            docPut("/api/issue-groups/{groupId}/categories/{categoryId}", roadsGroup.id!!, BROKEN_BARRIER_CATEGORY_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issue-categories/update",
                    pathParameters(
                        parameterWithName("groupId").description("The ID of the issue group which owns the category to update"),
                        parameterWithName("categoryId").description("The ID of the issue category to update")
                    ),
                    requestFields(
                        fieldWithPath("label").description("The new label of the issue category"),
                        descriptionRequired
                    )
                )
            )
    }

    @Test
    fun delete() {
        mockMvc.perform(
            docDelete("/api/issue-groups/{groupId}/categories/{categoryId}", gardensGroup.id!!, fallenTreeId)
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issue-categories/delete",
                    pathParameters(
                        parameterWithName("groupId").description("The ID of the issue group which owns the category to delete"),
                        parameterWithName("categoryId").description("The ID of the issue category to delete")
                    )
                )
            )
    }
}
