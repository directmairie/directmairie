package net.adullact.directmairie.web.government

import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.*
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import net.adullact.directmairie.web.issue.PictureFileService
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.image.ACCEPTED_IMAGE_MEDIA_TYPES
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ByteArrayResource
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.restdocs.headers.HeaderDocumentation
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant

/**
 * Documentation tests for [GovernmentController]
 * @author JB Nizet
 */
@DirectMairieDocTest(GovernmentController::class, GovernmentLogoController::class)
class GovernmentControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockIssueGroupDao: IssueGroupDao

    @MockkBean
    lateinit var mockUserDao: UserDao

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockPoolingOrganizationDao: PoolingOrganizationDao

    @MockkBean
    lateinit var mockPictureFileService: PictureFileService

    val superAdmin = User().apply { superAdmin = true }

    val grandLyonPoolingOrganization = PoolingOrganization().apply {
        id = GRAND_LYON_POOLING_ORGANIZATION_ID
    }

    val government
        get() = Government().apply {
            id = GIVORS_GOVERNMENT_ID
            name = "Givors"
            code = "12345"
            url = "https://givors.fr"
            osmId = 139203L
            poolingOrganization = grandLyonPoolingOrganization
            contactEmail = "contact@givors.fr"
            logo = Picture().apply {
                id = 83016L
                creationInstant = Instant.parse("2019-02-20T14:53:06.324Z")
                path = "1/2/3.jpg"
                contentType = MediaType.IMAGE_JPEG_VALUE
            }
            addIssueCategory(issueCategoryWithId(POTHOLE_CATEGORY_ID))
            addIssueCategory(issueCategoryWithId(GRAFFITI_CATEGORY_ID))
            addIssueCategory(issueCategoryWithId(OTHER_CATEGORY_ID))
            addContact(GovernmentContact().apply {
                group = issueGroupWithId(ROADS_GROUP_ID)
                email = "roads@givors.fr"
            })
            addContact(GovernmentContact().apply {
                group = issueGroupWithId(CLEANLINESS_GROUP_ID)
                email = "cleanliness@givors.fr"
            })
            addAdministrator(User().apply {
                id = 4567L
                email = "admin@givors.fr"
                lastName = "Faure"
            })
        }

    val createdGovernmentId = 1874L
    val createdGovernmentAdministrator = User().apply {
        id = 1452L
        email = "admin@bron.fr"
    }
    val createdLogoId = 95291L

    fun basicResponseFields(includeLogoFields: Boolean, logoDescription: String): List<FieldDescriptor> {
        val result = mutableListOf(
            fieldWithPath("id").description("The ID of the government"),
            fieldWithPath("name").description("The name of the government"),
            fieldWithPath("osmId").description("The osm_id of the relation identifying the government in OpenStreetMap"),
            fieldWithPath("code").description("The code (SIREN in France) of the government"),
            fieldWithPath("url").description("The URL of the government"),
            fieldWithPath("logo").description(logoDescription)
        )

        if (includeLogoFields) {
            result.addAll(listOf(
                fieldWithPath("logo.id").optional().description("The ID of the picture for the logo of the government"),
                fieldWithPath("logo.creationInstant").optional().description("The instant when the logo was created (i.e. uploaded) by the administrator")
            ))
        }

        return result
    }

    val governmentBasicResponseFields = basicResponseFields(
        includeLogoFields = true,
        logoDescription = "The logo of the government (if any)"
    )

    fun detailedResponseFields(afterCreation: Boolean): List<FieldDescriptor> {
        val basicList = if (afterCreation) {
            basicResponseFields(
                includeLogoFields = false,
                logoDescription = "The logo of the government (null since the government has just been created)"
            )
        } else {
            governmentBasicResponseFields
        }

        return basicList + listOf(
            fieldWithPath("contactEmail").description("The email used to send a notification when an issue is submitted for an issue group which has no configured contact"),
            subsectionWithPath("issueGroups").description("The issue groups and categories that have been selected for the government. It has the same structure as the response returned by the _Search for issue groups_ service, but only contains the categories that have been selected for the government"),
            subsectionWithPath("administrators").description("The administrators of the government"),
            fieldWithPath("contacts").description("The contacts that have been configured for the government"),
            subsectionWithPath("contacts[].issueGroup").description("The issue group of the contact"),
            fieldWithPath("contacts[].email").description("The email to contact when an issue is submitted for the group of the contact"),
            fieldWithPath("poolingOrganizationId").description("The ID of the pooling organization which the government belongs to")
        )
    }

    val imageContentTypeValues = ACCEPTED_IMAGE_MEDIA_TYPES.joinAsCodeValues()

    @BeforeEach
    fun prepare() {
        every { mockAuthorizations.atLeastPoolingOrganizationAdmin(grandLyonPoolingOrganization) } returns true
        every { mockAuthorizations.atLeastGovernmentAdmin(any()) } returns true
        every { mockUserDao.findVerifiedByIdOrThrow(createdGovernmentAdministrator.id!!, any()) } returns createdGovernmentAdministrator
        government.administrators.forEach { administrator ->
            every { mockUserDao.findVerifiedByIdOrThrow(administrator.id!!, any()) } returns administrator
        }

        every { mockPoolingOrganizationDao.findByIdOrThrow(grandLyonPoolingOrganization.id!!, any()) } returns grandLyonPoolingOrganization
        allIssueGroups.flatMap { it.categories }.forEach { category ->
            every { mockIssueCategoryDao.findByIdOrThrow(category.id!!, any()) } returns category
        }
        allIssueGroups.forEach { group ->
            every { mockIssueGroupDao.findByIdOrThrow(group.id!!, any()) } returns group
        }
        every { mockGovernmentDao.findByIdOrThrow(government.id!!) } returns government
    }

    @Test
    fun search() {
        val pageRequest = PageRequest.of(1, PAGE_SIZE)
        every { mockAuthorizations.currentUser } returns superAdmin
        val query = "givo"
        every { mockGovernmentDao.findByQuery(query, pageRequest) } returns PageImpl(
            listOf(government),
            pageRequest,
            PAGE_SIZE.toLong() + 1
        )

        mockMvc.perform(
            get("/api/governments")
                .param("query", query)
                .param("page", "1")
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "governments/search",
                    queryParameters(
                        parameterWithName("query").description("The query used to filter governments"),
                        pageParameter
                    ),
                    paginatedResponseFields(
                        fieldWithPath("content").description("The list of governments of the page")
                    ).andWithPrefix("content[].", governmentBasicResponseFields)
                )
            )
            .andDo(
                document(
                    "pagination",
                    responseFields(
                        subsectionWithPath("content").description("The elements of the page. The nature of the objects in this array depends on the service being called"),
                        fieldWithPath("number").description("The number of the page, starting at 0"),
                        fieldWithPath("size").description("The size of a page (which can be more than the actual number of elements in the page if the page is the last one)"),
                        fieldWithPath("totalElements").description("The total number of elements"),
                        fieldWithPath("totalPages").description("The total number of pages")
                    )
                )
            )
    }

    @Test
    fun get() {
        mockMvc.perform(
            docGet("/api/governments/{governmentId}", government.id!!)
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "governments/get",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government to get")
                    ),
                    responseFields(detailedResponseFields(afterCreation = false))
                )
            )
    }

    @Test
    fun create() {
        every { mockGovernmentDao.saveAndFlush(any<Government>()) } returnsModifiedFirstArg { id = createdGovernmentId }

        val command = GovernmentCommandDTO(
            name = "Bron",
            code = "54321",
            url = "https://bron.fr",
            osmId = 164214L,
            poolingOrganizationId = grandLyonPoolingOrganization.id!!,
            issueCategoryIds = setOf(BROKEN_LAMP_CATEGORY_ID, GRAFFITI_CATEGORY_ID),
            administratorIds = setOf(createdGovernmentAdministrator.id!!),
            contactEmail = "contact@bron.fr",
            contacts = setOf(
                GovernmentContactCommandDTO(
                    issueGroupId = LIGHTING_GROUP_ID,
                    email = "lighting@bron.fr"
                )
            )
        )

        every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
        every { mockGovernmentDao.findByCode(command.code) } returns null

        mockMvc.perform(
            docPost("/api/governments")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "governments/create",
                    requestFields(
                        fieldWithPath("name").description("The name of the government to create"),
                        fieldWithPath("code").description("The code (SIREN in France) of the government to create"),
                        fieldWithPath("url").description("The URL of the government to create"),
                        fieldWithPath("osmId").description("The osm_id of the relation identifying the government in OpenStreetMap"),
                        fieldWithPath("poolingOrganizationId").description("The ID of the pooling organization to which the government belongs. If the user is not a super-admin, then it must be one of the pooling organizations administered by the user."),
                        fieldWithPath("issueCategoryIds").description("The set of issue category IDs selected for the government. If the category ID `1`, identifying the _Other_ category, is not present, then it's automatically added to this set."),
                        fieldWithPath("administratorIds").description("The IDs of the administrators of the government"),
                        fieldWithPath("contactEmail").description("The email used to send a notification when an issue is submitted for an issue group which has no configured contact"),
                        fieldWithPath("contacts").description("The contacts (group + email) of the government"),
                        fieldWithPath("contacts[].issueGroupId").description("The ID of the issue group"),
                        fieldWithPath("contacts[].email").description("The email to contact when an issue is submitted for the group of the contact")
                    ),
                    responseFields(detailedResponseFields(afterCreation = true))
                )
            )
    }

    @Test
    fun update() {
        val command = GovernmentCommandDTO(
            name = "City of Givors",
            osmId = government.osmId,
            code = government.code,
            url = government.url!!,
            poolingOrganizationId = government.poolingOrganization.id!!,
            issueCategoryIds = setOf(POTHOLE_CATEGORY_ID, GRAFFITI_CATEGORY_ID, GARBAGE_CATEGORY_ID),
            administratorIds = government.administrators.map { it.id!! }.toSet(),
            contactEmail = "issues@givors.fr",
            contacts = setOf(
                GovernmentContactCommandDTO(
                    issueGroupId = CLEANLINESS_GROUP_ID,
                    email = "clean@givors.fr"
                )
            )
        )

        every { mockGovernmentDao.findByOsmId(command.osmId) } returns null
        every { mockGovernmentDao.findByCode(command.code) } returns null

        mockMvc.perform(
            docPut("/api/governments/{governmentId}", government.id!!)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "governments/update",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government to update")
                    ),
                    requestFields(
                        fieldWithPath("name").description("The new name of the government"),
                        fieldWithPath("code").description("The new code (SIREN in France) of the government"),
                        fieldWithPath("url").description("The new URL of the government"),
                        fieldWithPath("osmId").description("The osm_id of the new relation identifying the government in OpenStreetMap"),
                        fieldWithPath("poolingOrganizationId").description("The ID of the new pooling organization to which the government belongs. If the user is not a super-admin, both the old and the new pooling organization must be one of the pooling organizations administered by the user."),
                        fieldWithPath("issueCategoryIds").description("The new set of issue category IDs selected for the government. If the category ID `1`, identifying the _Other_ category, is not present, then it's automatically added to this set."),
                        fieldWithPath("administratorIds").description("The IDs of the new administrators of the government"),
                        fieldWithPath("contactEmail").description("The new email used to send a notification when an issue is submitted for an issue group which has no configured contact"),
                        fieldWithPath("contacts").description("The new contacts (group + email) of the government"),
                        fieldWithPath("contacts[].issueGroupId").description("The ID of the issue group"),
                        fieldWithPath("contacts[].email").description("The email to contact when an issue is submitted for the group of the contact")
                    )
                )
            )
    }

    @Test
    fun delete() {
        every { mockGovernmentDao.findByIdOrNull(createdGovernmentId) } returns null
        mockMvc.perform(
            docDelete("/api/governments/{governmentId}", createdGovernmentId)
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "governments/delete",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government to delete")
                    )
                )
            )
    }

    @Test
    fun getLogoBytes() {
        every { mockPictureFileService.toResource(government.logo!!.path) } returns ByteArrayResource("<the bytes of the image>".toByteArray())

        mockMvc.perform(
            docGet("/api/governments/{governmentId}/logo/{pictureId}/bytes", government.id!!, government.logo!!.id!!)
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "government-logo/bytes",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government"),
                        parameterWithName("pictureId").description("The ID of the logo to get")
                    ),
                    HeaderDocumentation.responseHeaders(
                        HeaderDocumentation.headerWithName(HttpHeaders.CONTENT_TYPE).description("The content type of the image. One of $imageContentTypeValues")
                    )
                )
            )
    }

    @Test
    fun uploadLogo() {
        val newGovernment = Government().apply {
            id = createdGovernmentId
            poolingOrganization = PoolingOrganization()
        }
        every { mockGovernmentDao.findByIdOrThrow(newGovernment.id!!) } returns newGovernment

        every { mockGovernmentDao.flush() } answers { newGovernment.logo!!.id = createdLogoId }
        every { mockPictureFileService.idToRelativePath(createdLogoId, "jpeg") } returns "0/1/2/$createdLogoId.jpeg"
        every { mockAuthorizations.atLeastPoolingOrganizationAdmin(newGovernment.poolingOrganization) } returns true

        mockMvc.perform(
            docMultipart("/api/governments/{governmentId}/logo", newGovernment.id!!)
                .file(
                    MockMultipartFile(
                        "file",
                        "whatever.jpg",
                        MediaType.IMAGE_JPEG_VALUE,
                        "<the image bytes>".toByteArray()
                    )
                )
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "government-logo/upload",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government for which a logo is uploaded")
                    ),
                    requestParts(
                        partWithName("file").description("The uploaded file. Its content type must be one of $imageContentTypeValues. The file name doesn't matter, but the part name must be `file`.")
                    ),
                    responseFields(
                        fieldWithPath("id").description("The ID of the logo"),
                        fieldWithPath("creationInstant").description("The instant when the logo was created (i.e. uploaded) by the administrator")
                    )
                )
            )
    }

    @Test
    fun deleteLogo() {
        val newGovernment = Government().apply {
            id = createdGovernmentId
            poolingOrganization = PoolingOrganization()
            logo = Picture().apply {
                id = createdLogoId
                path = "0/1/2/3.jpg"
            }
        }
        every { mockGovernmentDao.findByIdOrNull(newGovernment.id!!) } returns newGovernment
        every { mockAuthorizations.atLeastPoolingOrganizationAdmin(newGovernment.poolingOrganization) } returns true

        mockMvc.perform(
            docDelete("/api/governments/{governmentId}/logo/{pictureId}", newGovernment.id!!, createdLogoId)
                .basicAuthSuperAdmin()
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "government-logo/delete",
                    pathParameters(
                        parameterWithName("governmentId").description("The ID of the government"),
                        parameterWithName("pictureId").description("The ID of the logo to delete")
                    )
                )
            )
    }

    fun Iterable<*>.joinAsCodeValues() = joinToString(
        separator = ", ",
        prefix = "`",
        postfix = "`"
    )
}
