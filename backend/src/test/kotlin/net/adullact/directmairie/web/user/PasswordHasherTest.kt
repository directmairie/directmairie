package net.adullact.directmairie.web.user

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

/**
 * Unit tests for [PasswordHasher]
 * @author JB Nizet
 */
class PasswordHasherTest {
    @Test
    fun `should salt and hash and verify`() {
        val passwordHasher = PasswordHasher()
        val password = "password"
        val otherPassword = "otherPassword"

        val hashedPassword1 = passwordHasher.hash(password)
        val hashedPassword2 = passwordHasher.hash(password)
        val hashedOtherPassword = passwordHasher.hash(otherPassword)

        assertThat(hashedPassword2).isNotEqualTo(hashedPassword1) // because different hash
        assertThat(hashedOtherPassword).isNotEqualTo(hashedPassword1) //because different hash and different password

        assertThat(passwordHasher.verify(password, hashedPassword1)).isTrue()
        assertThat(passwordHasher.verify(password, hashedPassword2)).isTrue()
        assertThat(passwordHasher.verify(otherPassword, hashedOtherPassword)).isTrue()

        assertThat(passwordHasher.verify(otherPassword, hashedPassword1)).isFalse()
        assertThat(passwordHasher.verify(otherPassword, hashedPassword2)).isFalse()
        assertThat(passwordHasher.verify(password, hashedOtherPassword)).isFalse()
    }
}
