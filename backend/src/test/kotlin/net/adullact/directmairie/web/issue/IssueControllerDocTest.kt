package net.adullact.directmairie.web.issue

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.IssueSearchCriteria
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.service.nominatim.DetailsAddress
import net.adullact.directmairie.service.nominatim.DetailsResult
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.service.nominatim.OsmType
import net.adullact.directmairie.test.*
import net.adullact.directmairie.test.web.DirectMairieDocTest
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.user.ISSUE_ACCESS_TOKEN_HEADER
import net.adullact.directmairie.web.util.image.ACCEPTED_IMAGE_MEDIA_TYPES
import org.hamcrest.BaseMatcher
import org.hamcrest.Description
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.core.io.ByteArrayResource
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.restdocs.headers.HeaderDocumentation.*
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document
import org.springframework.restdocs.operation.OperationResponse
import org.springframework.restdocs.operation.OperationResponseFactory
import org.springframework.restdocs.operation.preprocess.OperationResponsePreprocessor
import org.springframework.restdocs.payload.PayloadDocumentation.*
import org.springframework.restdocs.request.RequestDocumentation.*
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.Instant
import java.time.ZoneId
import java.time.temporal.ChronoUnit

val generatedAccessToken = generateSecretToken()

/**
 * Documentation tests for [IssueController] and [IssuePictureController]
 * @author JB Nizet
 */
@DirectMairieDocTest(IssueController::class, IssuePictureController::class)
@Import(EventPublisher::class, IssueCsvExporter::class)
class IssueControllerDocTest(
    @Autowired val mockMvc: MockMvc,
    @Autowired val objectMapper: ObjectMapper
) {

    @MockkBean
    lateinit var mockIssueDao: IssueDao

    @MockkBean
    lateinit var mockIssueCategoryDao: IssueCategoryDao

    @MockkBean
    lateinit var mockGovernmentDao: GovernmentDao

    @MockkBean
    lateinit var mockNominatimService: NominatimService

    @MockkBean
    lateinit var mockAuthorizations: Authorizations

    @MockkBean
    lateinit var mockPictureFileService: PictureFileService

    val issueStatusValues = IssueStatus.values().toList().joinAsCodeValues()
    val imageContentTypeValues = ACCEPTED_IMAGE_MEDIA_TYPES.joinAsCodeValues()
    val validUpdatedStatusValues = IssueStatus.values().filter { it != IssueStatus.DRAFT }.joinAsCodeValues()

    val createdIssueId = 10984L
    val createdPictureId = 132734L

    val responseFields = listOf(
        fieldWithPath("id").description("The ID of the issue"),
        fieldWithPath("status").description("The status of the issue. Possible values are $issueStatusValues."),
        subsectionWithPath("coordinates").description("The geo coordinates of the issue, as submitted by the user"),
        subsectionWithPath("category").description("The category of the issue, as described in the <<issue-groups,section about issue groups>>"),
        fieldWithPath("description").description("The description, as provided by the user, of the issue"),
        fieldWithPath("pictures").description("The pictures of the issue, sorted by creation instant"),
        fieldWithPath("pictures[].id").optional().description("The ID of the picture"),
        fieldWithPath("pictures[].creationInstant").optional().description("The instant when the picture was created (i.e. uploaded) by the user"),
        fieldWithPath("creationInstant").description("The instant when the issue was created by the user"),
        subsectionWithPath("assignedGovernment").description("The government that the issue is assigned to (see <<local-governments, Local governments>>)"),
        fieldWithPath("transitions").description("The status transitions that the issue went through"),
        fieldWithPath("transitions[].transitionInstant").optional().description("The instant when the transition happened"),
        fieldWithPath("transitions[].beforeStatus").optional().description("The status before the transition"),
        fieldWithPath("transitions[].afterStatus").optional().description("The status after the transition"),
        fieldWithPath("transitions[].message").optional().description("The custom message that the administrator chose to add to the message sent to the creator of the issue, if any")
    )

    val commandFields = listOf(
        fieldWithPath("status").description(
            "The status of the new issue. Can only be `${IssueStatus.DRAFT}` or `${IssueStatus.CREATED}`"
        ),
        subsectionWithPath("coordinates").description(
            "The geo coordinates where the issue is located. Required."
        ),
        fieldWithPath("categoryId").description(
            "The ID of the issue category. Required if the status is `${IssueStatus.CREATED}`. If the status is `${IssueStatus.CREATED}`, must be one of the valid issue category IDs for given coordinates"),
        fieldWithPath("description").description(
            "The description of the issue. Required if the status is `${IssueStatus.CREATED}` and the chosen categoryId requires a description"
        )
    )

    val accessTokenHeaders = requestHeaders(
        headerWithName(ISSUE_ACCESS_TOKEN_HEADER).description(
            """
                The access token received when creating the issue anonymously.
                It is used to make sure that the original anonymous creator of the issue is the only person that can update it.
                Absent if the user is authenticated, and thus not anonymous.
            """.trimIndent()
        )
    )

    val government = Government().apply {
        id = GIVORS_GOVERNMENT_ID
        name = "Givors"
        code = "12345"
        url = "https://givors.fr"
        osmId = 139203L
        poolingOrganization = PoolingOrganization().apply {
            id = GRAND_LYON_POOLING_ORGANIZATION_ID
            name = "Grand Lyon"
        }
        logo = Picture().apply {
            id = 83016L
            creationInstant = Instant.parse("2019-02-20T14:53:06.324Z")
        }
    }

    val issues = listOf(
        Issue().apply {
            id = 10201
            status = IssueStatus.CREATED
            coordinates = GeoCoordinates(45.586021, 4.771513)
            category = issueCategoryWithId(GRAFFITI_CATEGORY_ID)
            val submissionInstant = Instant.now().minus(15, ChronoUnit.MINUTES)
            creationInstant = submissionInstant
            addPicture(Picture().apply {
                id = 102011
                creationInstant = submissionInstant.minus(95, ChronoUnit.SECONDS)
                contentType = MediaType.IMAGE_JPEG_VALUE
                path = "images/10201.jpg"
            })
            addPicture(Picture().apply {
                id = 102012
                creationInstant = submissionInstant.minus(20, ChronoUnit.SECONDS)
            })
            assignedGovernment = government
        },
        Issue().apply {
            id = 10288
            status = IssueStatus.REJECTED
            coordinates = GeoCoordinates(45.584885, 4.769480)
            category = issueCategoryWithId(POTHOLE_CATEGORY_ID)
            creationInstant = Instant.now().minus(10, ChronoUnit.MINUTES)
            assignedGovernment = government
            addTransition(IssueTransition().apply() {
                afterStatus = IssueStatus.REJECTED
                beforeStatus = IssueStatus.CREATED
                message = "sorry, we didn't find anything wrong"
                transitionInstant = Instant.now().plus(49, ChronoUnit.HOURS)
            })
        }
    )

    val governmentAdministratorEmail = GIVORS_ADMIN_EMAIL

    @BeforeEach
    fun prepare() {
        issues.forEach {
            every { mockIssueDao.findByIdOrThrow(it.id!!) } returns it
        }
        allIssueGroups.flatMap { it.categories }.forEach { category ->
            every { mockIssueCategoryDao.findByIdOrThrow(category.id!!, any()) } returns category
        }
    }

    @Test
    fun search() {
        val pageRequest = PageRequest.of(0, PAGE_SIZE)
        val user = User()
        every { mockAuthorizations.currentUser } returns user
        every { mockAuthorizations.atLeastAnyPoolingOrganizationAdmin() } returns true
        every {
            mockIssueDao.findByCriteria(
                IssueSearchCriteria(
                    user = user,
                    statuses = setOf(IssueStatus.CREATED, IssueStatus.REJECTED),
                    poolingOrganizationIds = setOf(1L, 2L),
                    governmentIds = setOf(3L, government.id!!)
                ),
                pageRequest
            )
        } returns
            PageImpl(issues, pageRequest, issues.size.toLong())

        mockMvc.perform(
            docGet("/api/issues")
                .param("status", IssueStatus.CREATED.name, IssueStatus.REJECTED.name)
                .param("org", "1", "2")
                .param("gov", "3", government.id.toString())
                .param("page", "0")
                .basicAuth(governmentAdministratorEmail)
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issues/search",
                    queryParameters(
                        parameterWithName("status")
                            .description("The set of accepted statuses of the issues. Valid values are $issueStatusValues. If no status is provided, then all non-draft issues matching the other parameters are returned."),
                        parameterWithName("org")
                            .description("A set of pooling organization IDs. If provided, then issues assigned to a government that is handled by any of these pooling organizations are returned."),
                        parameterWithName("gov")
                            .description("A set of government IDs. If provided, then issues assigned to any of these governments are returned."),
                        pageParameter
                    ),
                    paginatedResponseFields(
                        fieldWithPath("content").description("The matched issues of the page")
                    ).andWithPrefix("content[].", responseFields)
                )
            )
    }

    @Test
    fun mine() {
        val pageRequest = PageRequest.of(0, PAGE_SIZE)
        val user = User()
        every { mockAuthorizations.currentUser } returns user
        every {
            mockIssueDao.findByCreatorAndStatuses(user, setOf(IssueStatus.CREATED, IssueStatus.REJECTED), pageRequest)
        } returns
                PageImpl(issues, pageRequest, issues.size.toLong())

        mockMvc.perform(
            docGet("/api/issues/mine")
                .param("status", IssueStatus.CREATED.name, IssueStatus.REJECTED.name)
                .param("page", "0")
                .basicAuth(governmentAdministratorEmail)
        )
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issues/mine",
                    queryParameters(
                        parameterWithName("status")
                            .description("The set of accepted statuses of the issues. Valid values are $issueStatusValues. If no status is provided, then all non-draft created by the authenticated user are returned."),
                        pageParameter
                    ),
                    paginatedResponseFields(
                        fieldWithPath("content").description("The matched issues of the page")
                    ).andWithPrefix("content[].", responseFields)
                )
            )
    }

    @Test
    fun get() {
        val issue = issues[0]

        mockMvc.perform(docGet("/api/issues/{issueId}", issue.id!!))
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issues/get",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue to get")
                    ),
                    responseFields(responseFields.filter { !it.path.startsWith("transitions[]")})
                )
            )
    }

    @Test
    fun getPictureBytes() {
        val issue = issues[0]
        val picture = issue.pictures.minByOrNull(Picture::creationInstant)!!

        every { mockPictureFileService.toResource(picture.path) } returns ByteArrayResource("<the bytes of the image>".toByteArray())

        mockMvc.perform(docGet("/api/issues/{issueId}/pictures/{pictureId}/bytes", issue.id!!, picture.id!!))
            .andExpect(status().isOk)
            .andDo(
                document(
                    "issue-pictures/bytes",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue"),
                        parameterWithName("pictureId").description("The ID of the picture to get")
                    ),
                    responseHeaders(
                        headerWithName(HttpHeaders.CONTENT_TYPE).description("The content type of the image. One of $imageContentTypeValues")
                    )
                )
            )
    }

    @Test
    fun create() {
        val command = IssueCommandDTO(
            status = IssueStatus.DRAFT,
            coordinates = GeoCoordinates(45.584844, 4.770714),
            categoryId = OTHER_CATEGORY_ID,
            description = "Horrible smell coming from the sewers"
        )

        every { mockIssueDao.saveAndFlush(any<Issue>()) } returnsModifiedFirstArg { id = 10984 }
        every { mockAuthorizations.isAnonymous } returns true

        mockMvc.perform(
            docPost("/api/issues")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "issues/create",
                    ReplaceAccessTokenPreprocessor(objectMapper, generatedAccessToken),
                    requestFields(commandFields),
                    responseFields(
                        responseFields.filter { !it.path.startsWith("pictures[]") && !it.path.startsWith("transitions[]") } +
                            fieldWithPath("accessToken").description(
                                """
                                    A random access token, only present if the issue was created anonymously,
                                    and if it was created with the status `${IssueStatus.DRAFT}`.
                                    Every subsequent request to update the issue (or upload/delete pictures for
                                    that issue) must include this token in the $ISSUE_ACCESS_TOKEN_HEADER header.
                                """.trimIndent()
                            )
                    )
                )
            )
    }

    @Test
    fun update() {
        val command = IssueCommandDTO(
            status = IssueStatus.CREATED,
            coordinates = GeoCoordinates(45.584844, 4.770714),
            categoryId = OTHER_CATEGORY_ID,
            description = "Horrible smell coming from the sewers in front of the bar"
        )

        every { mockIssueDao.findByIdOrThrow(createdIssueId) } returns Issue().apply {
            id = createdIssueId
        }
        every { mockNominatimService.details(any()) } returns
            DetailsResult(listOf(DetailsAddress(osmId = 1234L, osmType = OsmType.RELATION, adminLevel = 4)))
        every { mockGovernmentDao.findByOsmIds(setOf(1234L)) } returns listOf(Government().apply {
            addIssueCategory(mockIssueCategoryDao.findByIdOrThrow(OTHER_CATEGORY_ID))
            // we need to use the exact same object, instead of a copy returned by
            // issueCategoryWithId, because the code relies on equality
        })
        every { mockAuthorizations.mayUpdateIssue(any()) } returns true

        mockMvc.perform(
            docPut("/api/issues/{issueId}", createdIssueId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .header(ISSUE_ACCESS_TOKEN_HEADER, generatedAccessToken)
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issues/update",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue to update")
                    ),
                    requestHeaders(
                        headerWithName(ISSUE_ACCESS_TOKEN_HEADER).description(
                            """
                                The access token received when creating the issue anonymously.
                                It is used to make sure that the original anonymous creator of the issue is
                                the only person that can update it.
                                Absent if the user is authenticated, and thus not anonymous.
                            """.trimIndent()
                        )
                    ),
                    requestFields(commandFields)
                )
            )
    }

    @Test
    fun uploadPicture() {
        val issue = Issue().apply {
            id = createdIssueId
            status = IssueStatus.DRAFT
        }
        every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue

        every { mockIssueDao.flush() } answers { issue.pictures.first().id = createdPictureId }
        every { mockPictureFileService.idToRelativePath(createdPictureId, "jpeg") } returns "0/1/2/$createdPictureId.jpeg"
        every { mockAuthorizations.mayUpdateIssue(any()) } returns true

        mockMvc.perform(
            docMultipart("/api/issues/{issueId}/pictures", issue.id!!)
                .file(
                    MockMultipartFile(
                        "file",
                        "whatever.jpg",
                        MediaType.IMAGE_JPEG_VALUE,
                        "<the image bytes>".toByteArray()
                    )
                )
                .header(ISSUE_ACCESS_TOKEN_HEADER, generatedAccessToken)
        )
            .andExpect(status().isCreated)
            .andDo(
                document(
                    "issue-pictures/upload",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue for which a picture is uploaded")
                    ),
                    accessTokenHeaders,
                    requestParts(
                        partWithName("file").description("The uploaded file. Its content type must be one of $imageContentTypeValues. The file name doesn't matter, but the part name must be `file`.")
                    ),
                    responseFields(
                        fieldWithPath("id").description("The ID of the picture"),
                        fieldWithPath("creationInstant").description("The instant when the picture was created (i.e. uploaded) by the user")
                    )
                )
            )
    }

    @Test
    fun deletePicture() {
        val issue = Issue().apply {
            id = createdIssueId
            status = IssueStatus.DRAFT
            addPicture(Picture().apply {
                id = createdPictureId
                path = "0/1/2/3.jpg"
            })
        }
        every { mockIssueDao.findByIdOrNull(issue.id!!) } returns issue
        every { mockAuthorizations.mayUpdateIssue(any()) } returns true

        mockMvc.perform(
            docDelete("/api/issues/{issueId}/pictures/{pictureId}", issue.id!!, createdPictureId)
                .header(ISSUE_ACCESS_TOKEN_HEADER, generatedAccessToken)
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issue-pictures/delete",
                    accessTokenHeaders,
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue for which a picture is deleted"),
                        parameterWithName("pictureId").description("The ID of the picture to delete")
                    )
                )
            )
    }

    @Test
    fun updateStatus() {
        val issue = Issue().apply {
            id = createdIssueId
            status = IssueStatus.CREATED
            assignedGovernment = government
        }
        every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
        every { mockAuthorizations.atLeastGovernmentAdmin(government)} returns true
        every { mockIssueDao.flush() } answers { issue.transitions[0].id = 4321 }

        val command = IssueStatusCommandDTO(
            status = IssueStatus.RESOLVED,
            message = "Thank you. Something was indeed very wrong."
        )

        mockMvc.perform(
            docPut("/api/issues/{issueId}/status", issue.id!!)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(command))
                .basicAuth(governmentAdministratorEmail)
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issues/update-status",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue to update")
                    ),
                    requestFields(
                        fieldWithPath("status").description("The new status of the issue. Must be one of $validUpdatedStatusValues"),
                        fieldWithPath("message").optional().description("The custom message added to the message sent to the creator of the issue (if any)")
                    )
                )
            )
    }

    @Test
    fun delete() {
        val issue = Issue().apply {
            id = createdIssueId
            assignedGovernment = government
        }
        every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
        every { mockAuthorizations.atLeastGovernmentAdmin(government)} returns true

        mockMvc.perform(
            docDelete("/api/issues/{issueId}", issue.id!!)
                .basicAuth(governmentAdministratorEmail)
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issues/delete",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue to delete")
                    )
                )
            )
    }

    @Test
    fun export() {
        every { mockAuthorizations.currentUser } returns User().apply { superAdmin = true }
        every { mockIssueDao.findNonDraftByInterval(any(), any()) } returns
            issues.sortedByDescending { it.creationInstant }.stream()

        val timezone = ZoneId.of("Europe/Paris")

        mockMvc.perform(
            docGet("/api/issues")
                .param("export", "CSV")
                .param("from", Instant.now().atZone(timezone).toLocalDate().minusDays(2).toString())
                .param("to", Instant.now().atZone(timezone).toLocalDate().toString())
                .param("timezone", timezone.toString())
                .basicAuthSuperAdmin()
        )
            .andExpect(MockMvcResultMatchers.request().asyncStarted())
            .andDo { it.getAsyncResult() }
            .andExpect(status().isOk)
            .andExpect(content().string(HasLines(issues.size + 2))) // header + empty line at the end
            .andDo(
                document(
                    "issues/export",
                    queryParameters(
                        parameterWithName("export").description("The type of export. Must be set to `CSV`."),
                        parameterWithName("from").description("The start date, inclusive, in ISO format"),
                        parameterWithName("to").description("The end date, in ISO format"),
                        parameterWithName("timezone").description(
                            """The time zone. Optional, defaults to the server time zone.
                                | This time zone is used to transform the dates into instants, and to format
                                | the creation instants in the generated CSV records.
                            """.trimMargin()
                        )
                    )
                )
            )
    }

    @Test
    fun deanonymize() {
        val issue = Issue().apply {
            id = createdIssueId
            accessToken = generatedAccessToken
        }
        every { mockIssueDao.findByIdOrThrow(issue.id!!) } returns issue
        every { mockAuthorizations.currentUser } returns User()

        mockMvc.perform(
            docPut("/api/issues/{issueId}/creator", createdIssueId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(DeanonymizationCommandDTO(issue.accessToken!!)))
                .basicAuth("jane.doe@mail.com")
        )
            .andExpect(status().isNoContent)
            .andDo(
                document(
                    "issues/deanonymize",
                    pathParameters(
                        parameterWithName("issueId").description("The ID of the issue to deanonymize")
                    ),
                    requestFields(
                        fieldWithPath("accessToken").description("The access token of the issue, received when it was created anonymously.")
                    )
                )
            )

    }

    fun Iterable<*>.joinAsCodeValues() = joinToString(
        separator = ", ",
        prefix = "`",
        postfix = "`"
    )

    inner class HasLines(val expectedLineCount: Int) : BaseMatcher<String>() {
        override fun describeTo(description: Description) {
            description.appendText("should have $expectedLineCount lines")
        }

        override fun matches(item: Any?): Boolean {
            return (item as String).lines().size == expectedLineCount
        }
    }
}

class ReplaceAccessTokenPreprocessor(val objectMapper: ObjectMapper, val alternateToken: String) : OperationResponsePreprocessor {
    override fun preprocess(response: OperationResponse): OperationResponse {
        val body =
            objectMapper.readValue<MutableMap<String, Any?>>(response.content, object : TypeReference<MutableMap<String, Any?>>() {})
        body["accessToken"] = alternateToken
        return OperationResponseFactory().createFrom(response, objectMapper.writeValueAsBytes(body))
    }
}
