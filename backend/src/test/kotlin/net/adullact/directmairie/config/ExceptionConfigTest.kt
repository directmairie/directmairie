package net.adullact.directmairie.config

import io.mockk.every
import io.mockk.spyk
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.web.error.ErrorAttributeOptions
import org.springframework.mock.web.MockHttpServletRequest
import org.springframework.web.context.request.ServletWebRequest

/**
 * Unit tests for [ExceptionConfig]
 * @author JB Nizet
 */
class ExceptionConfigTest {
    @Test
    fun `should include functional error`() {
        val result = spyk(ExceptionConfig().errorAttributes())
        val request = MockHttpServletRequest()
        val webRequest = ServletWebRequest(request)

        val exception = BadRequestException(ErrorCode.ALREADY_REGISTERED)
        every { result.getError(webRequest) } returns exception

        val errorAttributes = result.getErrorAttributes(webRequest, ErrorAttributeOptions.defaults())

        Assertions.assertThat(errorAttributes["functionalError"]).isEqualTo(exception.errorCode)
    }

    @Test
    fun `should not include functional error if not present`() {
        val result = spyk(ExceptionConfig().errorAttributes())
        val request = MockHttpServletRequest()
        val webRequest = ServletWebRequest(request)

        val exception = BadRequestException("foo")
        every { result.getError(webRequest) } returns exception

        val errorAttributes = result.getErrorAttributes(webRequest, ErrorAttributeOptions.defaults())

        Assertions.assertThat(errorAttributes).doesNotContainKey("functionalError")
    }

    @Test
    fun `should not include functional error if not bad request exception`() {
        val result = spyk(ExceptionConfig().errorAttributes())
        val request = MockHttpServletRequest()
        val webRequest = ServletWebRequest(request)

        val exception = IllegalStateException("foo")
        every { result.getError(webRequest) } returns exception

        val errorAttributes = result.getErrorAttributes(webRequest, ErrorAttributeOptions.defaults())

        Assertions.assertThat(errorAttributes).doesNotContainKey("functionalError")
    }
}
