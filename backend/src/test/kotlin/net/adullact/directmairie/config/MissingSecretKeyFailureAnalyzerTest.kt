package net.adullact.directmairie.config

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.diagnostics.FailureAnalysis
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.context.annotation.Configuration

/**
 * Unit test for [MissingSecretKeyFailureAnalyzer]
 * @author JB Nizet
 */
class MissingSecretKeyFailureAnalyzerTest {
    @Test
    fun `should fail loading the context with a meaningful error message`() {
        val analysis = performAnalysis()
        assertThat(analysis).isNotNull()

        assertThat(analysis.cause).isNotNull()
        assertThat(analysis.description).contains("The directmairie.security.secret-key property must be explicitly provided")
        assertThat(analysis.action).contains("If you need to generate a key")
    }

    fun performAnalysis(): FailureAnalysis {
        val failure = createFailure()
        assertThat(failure).isNotNull()
        return MissingSecretKeyFailureAnalyzer().analyze(failure)
    }

    fun createFailure(): Exception? {
        try {
            val context = AnnotationConfigApplicationContext()
            context.register(AlternateConfiguration::class.java)
            context.refresh()
            context.close()
            return null
        } catch (e: Exception) {
            return e
        }

    }

    @Configuration
    @EnableConfigurationProperties(SecurityProperties::class)
    class AlternateConfiguration
}
