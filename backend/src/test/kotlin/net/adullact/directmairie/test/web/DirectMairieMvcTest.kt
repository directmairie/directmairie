package net.adullact.directmairie.test.web

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.annotation.AliasFor
import kotlin.reflect.KClass

/**
 * Meta-annotation used to simplify the creation of MVC tests.
 *
 * @author JB Nizet
 */
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@WebMvcTest
annotation class DirectMairieMvcTest(
    @get:AliasFor(annotation = WebMvcTest::class, attribute = "value") vararg val value: KClass<*> = []
)
