package net.adullact.directmairie.test.web

import net.adullact.directmairie.doc.DocumentationConfig
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.Import
import org.springframework.core.annotation.AliasFor
import org.springframework.restdocs.RestDocumentationExtension
import kotlin.reflect.KClass

/**
 * Meta-annotation used to simplify the creation of Spring REST Doc tests.
 *
 * @author JB Nizet
 */
@Target(AnnotationTarget.ANNOTATION_CLASS, AnnotationTarget.CLASS)
@ExtendWith(
    RestDocumentationExtension::class
)
@Import(DocumentationConfig::class)
@AutoConfigureRestDocs
@WebMvcTest
annotation class DirectMairieDocTest(
    @get:AliasFor(annotation = WebMvcTest::class, attribute = "value") vararg val value: KClass<*> = []
)
