package net.adullact.directmairie.test

import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import net.adullact.directmairie.domain.OTHER_ISSUE_GROUP_ID
import org.springframework.restdocs.payload.FieldDescriptor
import org.springframework.restdocs.payload.PayloadDocumentation
import org.springframework.restdocs.request.ParameterDescriptor
import org.springframework.restdocs.request.RequestDocumentation

val ROADS_GROUP_ID = 2L
val LIGHTING_GROUP_ID = 3L
val CLEANLINESS_GROUP_ID = 4L

val POTHOLE_CATEGORY_ID = 21L
val BROKEN_BARRIER_CATEGORY_ID = 22L

val INSUFFICIENT_LIGHTING_CATEGORY_ID = 31L
val BROKEN_LAMP_CATEGORY_ID = 32L

val GARBAGE_CATEGORY_ID = 41L
val GRAFFITI_CATEGORY_ID = 42L

val allIssueGroups
    get() = listOf(
        IssueGroup().apply {
            id = OTHER_ISSUE_GROUP_ID
            label = "Other"
            addCategory(IssueCategory().apply {
                id = OTHER_CATEGORY_ID
                label = "Other"
                descriptionRequired = true
            })
        },
        IssueGroup().apply {
            id = ROADS_GROUP_ID
            label = "Roads"
            addCategory(IssueCategory().apply {
                id = POTHOLE_CATEGORY_ID
                label = "Pothole"
            })
            addCategory(IssueCategory().apply {
                id = BROKEN_BARRIER_CATEGORY_ID
                label = "Broken Barrier"
            })
        },
        IssueGroup().apply {
            id = LIGHTING_GROUP_ID
            label = "Lighting"
            addCategory(IssueCategory().apply {
                id = INSUFFICIENT_LIGHTING_CATEGORY_ID
                label = "Insufficient lighting"
                descriptionRequired = true
            })
            addCategory(IssueCategory().apply {
                id = BROKEN_LAMP_CATEGORY_ID
                label = "Broken lamp"
            })
        },
        IssueGroup().apply {
            id = CLEANLINESS_GROUP_ID
            label = "Cleanliness"
            addCategory(IssueCategory().apply {
                id = GARBAGE_CATEGORY_ID
                label = "Garbage"
                descriptionRequired = true
            })
            addCategory(IssueCategory().apply {
                id = GRAFFITI_CATEGORY_ID
                label = "Graffiti"
            })
        }
    )

fun issueGroupWithId(groupId: Long) = allIssueGroups.find { it.id == groupId }!!
fun issueCategoryWithId(categoryId: Long) = allIssueGroups.flatMap { it.categories }.find { it.id == categoryId }!!

val pageParameter: ParameterDescriptor =
    RequestDocumentation.parameterWithName("page")
        .optional()
        .description("the page number, optional, starting at 0")

val GRAND_LYON_POOLING_ORGANIZATION_ID = 1007L

val USER_EMAIL = "john@mail.com"
val USER_PASSWORD = "passw0rd"
val USER_ID = 8527L

fun paginatedResponseFields(vararg descriptors: FieldDescriptor) =
    PayloadDocumentation.responseFields(
        PayloadDocumentation.fieldWithPath("number").ignored(),
        PayloadDocumentation.fieldWithPath("size").ignored(),
        PayloadDocumentation.fieldWithPath("totalPages").ignored(),
        PayloadDocumentation.fieldWithPath("totalElements").ignored(),
        *descriptors
    )

val GIVORS_GOVERNMENT_ID = 1054L
val GIVORS_ADMIN_EMAIL = "admin@givors.fr"

