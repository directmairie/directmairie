package net.adullact.directmairie.test

import io.mockk.MockKStubScope
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import java.util.*

/**
 * Tells the stubbed method that it must return its first argument, modified by the given consumer.
 * Useful to mock a DAO save method that returns its argument with a generated ID.
 * For example:
 *
 * ```
 * every { issueDao.save(issue) } returnsModifiedFirstArg { id = 42L }
 * ```
 */
inline infix fun <reified T, B> MockKStubScope<T, B>.returnsModifiedFirstArg(crossinline block: T.() -> Unit) =
    answers {
        firstArg<T>().block()
        firstArg()
    }

fun docGet(urlTemplate: String, vararg urlVariables: Any) =
    RestDocumentationRequestBuilders.get(urlTemplate, *urlVariables)
fun docPost(urlTemplate: String, vararg urlVariables: Any) =
    RestDocumentationRequestBuilders.post(urlTemplate, *urlVariables)
fun docPut(urlTemplate: String, vararg urlVariables: Any) =
    RestDocumentationRequestBuilders.put(urlTemplate, *urlVariables)
fun docDelete(urlTemplate: String, vararg urlVariables: Any) =
    RestDocumentationRequestBuilders.delete(urlTemplate, *urlVariables)
fun docMultipart(urlTemplate: String, vararg urlVariables: Any) =
    RestDocumentationRequestBuilders.multipart(urlTemplate, *urlVariables)

fun MockHttpServletRequestBuilder.basicAuth(email: String)
    = this.header("Authorization", "Basic " + Base64.getEncoder().encodeToString("$email:password".toByteArray()))
fun MockHttpServletRequestBuilder.basicAuthSuperAdmin() = basicAuth("superadmin@adullact.org")
