package net.adullact.directmairie

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource

/**
 * Integration test checking that the [Application] starts correctly
 * @author JB Nizet
 */
@SpringBootTest
@TestPropertySource("/test.properties")
class ApplicationTest {

    @Test
    fun `context should load`() {
    }
}
