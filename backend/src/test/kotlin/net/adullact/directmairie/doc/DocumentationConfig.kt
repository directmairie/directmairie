package net.adullact.directmairie.doc

import org.springframework.boot.test.autoconfigure.restdocs.RestDocsMockMvcConfigurationCustomizer
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer
import org.springframework.restdocs.operation.OperationRequest
import org.springframework.restdocs.operation.OperationRequestFactory
import org.springframework.restdocs.operation.OperationResponse
import org.springframework.restdocs.operation.OperationResponseFactory
import org.springframework.restdocs.operation.preprocess.OperationPreprocessor
import org.springframework.restdocs.operation.preprocess.Preprocessors.*

/**
 * Additional configuration class allowing to set defaults regarding the REST documentation
 * @author JB Nizet
 */
@TestConfiguration
class DocumentationConfig : RestDocsMockMvcConfigurationCustomizer {
    override fun customize(configurer: MockMvcRestDocumentationConfigurer) {
        configurer.operationPreprocessors()
            .withRequestDefaults(
                modifyUris()
                    .scheme("https")
                    .host("directmairie.adullact.org")
                    .removePort(),
                prettyPrint(),
                modifyHeaders().remove(
                    HttpHeaders.CONTENT_LENGTH
                    // the JSON content type must not be removed from requests, otherwise curl snippets don't include
                    // it, and the generated curl commands don't work
                )
            )
            .withResponseDefaults(
                prettyPrint(),
                modifyHeaders().remove(
                    HttpHeaders.CONTENT_LENGTH
                ),
                JsonContentTypeRemovingHeaderPreprocessor()
            )
    }
}

/**
 * A preprocessor that removes the content-type header if its value is compatible with application/json, since that's
 * the default that doesn't need to be documented.
 */
class JsonContentTypeRemovingHeaderPreprocessor : OperationPreprocessor {
    val operationRequestFactory = OperationRequestFactory()
    val operationResponseFactory = OperationResponseFactory()

    override fun preprocess(request: OperationRequest) =
        operationRequestFactory.createFrom(request, newHeaders(request.headers))

    override fun preprocess(response: OperationResponse) =
        operationResponseFactory.createFrom(response, newHeaders(response.headers))

    fun newHeaders(headers: HttpHeaders): HttpHeaders {
        return headers.contentType?.takeIf { it.isCompatibleWith(MediaType.APPLICATION_JSON) }
            ?.let {
                val newHeaders = HttpHeaders()
                headers.forEach {
                    if (it.key != HttpHeaders.CONTENT_TYPE) {
                        newHeaders.put(it.key, it.value)
                    }
                }
                newHeaders
            }
            ?: headers
    }
}

