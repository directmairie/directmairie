package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.RegistrationProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.UserRegistered
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant

/**
 * Unit tests for [RegistrationMailer]
 * @author JB Nizet
 */
class RegistrationMailerTest {
    @Test
    fun `should send registration email using mustache templates`() {
        val mockMailer = mockk<Mailer>()
        val mockEmailVerificationDao = mockk<EmailVerificationDao>()

        val registrationProperties = RegistrationProperties(
            from = "noreply@directmairie.adullact.org",
            subject = "Your registration"
        )
        val hostingProperties = HostingProperties(
            host = "directmairie.adullact.org"
        )

        val registrationMailer = RegistrationMailer(
            mockMailer,
            mockEmailVerificationDao,
            registrationProperties,
            hostingProperties
        ).apply {
            initializeTemplates()
        }

        val emailVerification = EmailVerification(
            user = User().apply {
                id = 1000L
            },
            email = "jenny@gmail.com",
            secretToken = "theSecret",
            validityInstant = Instant.now()
        )
        val event = UserRegistered(42)
        every { mockEmailVerificationDao.findByIdOrNull(event.emailVerificationId) } returns emailVerification

        registrationMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.from).isEqualTo(registrationProperties.from)
                assertThat(it.to).isEqualTo(emailVerification.email)
                assertThat(it.subject).isEqualTo(registrationProperties.subject)
                assertThat(it.plainText)
                    .contains(emailVerification.user.id.toString())
                    .contains(emailVerification.secretToken)
                    .contains("https://directmairie.adullact.org/registration")
                assertThat(it.htmlText)
                    .contains(emailVerification.user.id.toString())
                    .contains(emailVerification.secretToken)
                    .contains("https://directmairie.adullact.org/registration")
            })
        }
    }
}
