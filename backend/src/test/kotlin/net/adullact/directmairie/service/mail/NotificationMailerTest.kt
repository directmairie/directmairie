package net.adullact.directmairie.service.mail

import io.mockk.Ordering
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.TIMEZONE
import net.adullact.directmairie.config.mail.IssueTransitionProperties
import net.adullact.directmairie.config.mail.NotificationProperties
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.IssueTransitionDao
import net.adullact.directmairie.dao.Recipient
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.IssueTransition
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.IssueTransitioned
import net.adullact.directmairie.event.NotificationRequested
import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.notification.NotificationCommandDTO
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

/**
 * Unit tests for [NotificationMailer]
 * @author JB Nizet
 */
class NotificationMailerTest {
    lateinit var mockMailer: Mailer
    lateinit var government: GovernmentDTO
    lateinit var notificationMailer: NotificationMailer
    lateinit var event: NotificationRequested

    val notificationProperties = NotificationProperties(
        from = "noreply@directmairie.adullact.org"
    )
    val hostingProperties = HostingProperties(
        host = "directmairie.adullact.org"
    )

    @BeforeEach
    fun prepare() {
        mockMailer = mockk()
        val mockFetcher = mockk<NotificationDataFetcher>()

        notificationMailer = NotificationMailer(
            mockMailer,
            notificationProperties,
            hostingProperties,
            mockFetcher
        ).apply {
            initializeTemplates()
        }

        government = GovernmentDTO(
            id = 1234,
            name = "Lyon",
            code = "lyon",
            url = null,
            osmId = 69L,
            logo = null
        )
        val command = NotificationCommandDTO(
            governmentId = government.id,
            subject = "Test",
            message = "Test Message",
            url = "https://lyon.fr"
        )
        event = NotificationRequested(command)
        every { mockFetcher.getGovernment(command.governmentId) } returns government

        every { mockFetcher.batchOfRecipients(command.governmentId, null) } returns
                (0 until RECIPIENT_BATCH_SIZE).map { Recipient(it.toLong(), "user${it}@mail.com") }
        every { mockFetcher.batchOfRecipients(command.governmentId, (RECIPIENT_BATCH_SIZE - 1).toLong()) } returns
                (RECIPIENT_BATCH_SIZE until RECIPIENT_BATCH_SIZE + 1).map { Recipient(it.toLong(), "user${it}@mail.com") }
    }

    @Test
    fun `should send email to all recipients`() {
        notificationMailer.sendNotificationEmail(event)

        verify {
            (0 until RECIPIENT_BATCH_SIZE + 1).forEach { num ->
                mockMailer.send(withArg {
                    assertThat(it.from).isEqualTo(notificationProperties.from)
                    assertThat(it.to).isEqualTo("user${num}@mail.com")
                    assertThat(it.subject).isEqualTo("Test")
                    assertThat(it.plainText)
                        .contains("Test Message")
                        .contains("https://lyon.fr")
                        .contains("https://directmairie.adullact.org/me/subscriptions")
                        .contains("Lyon")
                    assertThat(it.htmlText)
                        .contains("Test Message")
                        .contains("https://lyon.fr")
                        .contains("https://directmairie.adullact.org/me/subscriptions")
                        .contains("Lyon")
                })
            }
        }
    }
}
