package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.LostPasswordProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.PasswordLost
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant

/**
 * Unit tests for [LostPasswordMailer]
 * @author JB Nizet
 */
class LostPasswordMailerTest {
    @Test
    fun `should send lost password email`() {
        val mockMailer = mockk<Mailer>()
        val mockEmailVerificationDao = mockk<EmailVerificationDao>()

        val lostPasswordProperties = LostPasswordProperties(
            from = "noreply@directmairie.adullact.org",
            subject = "Your password reset request"
        )
        val hostingProperties = HostingProperties(
            host = "directmairie.adullact.org"
        )

        val lostPasswordMailer = LostPasswordMailer(
            mockMailer,
            mockEmailVerificationDao,
            lostPasswordProperties,
            hostingProperties
        ).apply {
            initializeTemplates()
        }

        val emailVerification = EmailVerification(
            user = User().apply {
                id = 1000L
            },
            email = "jenny@gmail.com",
            secretToken = "theSecret",
            validityInstant = Instant.now()
        )
        val event = PasswordLost(42)
        every { mockEmailVerificationDao.findByIdOrNull(event.emailVerificationId) } returns emailVerification

        lostPasswordMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.from).isEqualTo(lostPasswordProperties.from)
                assertThat(it.to).isEqualTo(emailVerification.email)
                assertThat(it.subject).isEqualTo(lostPasswordProperties.subject)
                assertThat(it.plainText)
                    .contains(emailVerification.user.id.toString())
                    .contains(emailVerification.secretToken)
                    .contains("https://directmairie.adullact.org/lost-password")
                assertThat(it.htmlText)
                    .contains(emailVerification.user.id.toString())
                    .contains(emailVerification.secretToken)
                    .contains("https://directmairie.adullact.org/lost-password")
            })
        }
    }
}
