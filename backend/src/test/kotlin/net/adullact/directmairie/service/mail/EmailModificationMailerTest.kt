package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.EmailModificationProperties
import net.adullact.directmairie.config.mail.LostPasswordProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.EmailModificationRequested
import net.adullact.directmairie.event.PasswordLost
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.Instant

/**
 * Unit tests for [EmailModificationMailer]
 * @author JB Nizet
 */
class EmailModificationMailerTest {
    @Test
    fun `should send email modification email`() {
        val mockMailer = mockk<Mailer>()
        val mockEmailVerificationDao = mockk<EmailVerificationDao>()

        val emailModificationProperties = EmailModificationProperties(
            from = "noreply@directmairie.adullact.org",
            subject = "Your email modification request"
        )

        val emailModificationMailer = EmailModificationMailer(
            mockMailer,
            mockEmailVerificationDao,
            emailModificationProperties
        ).apply {
            initializeTemplates()
        }

        val emailVerification = EmailVerification(
            user = User().apply {
                id = 1000L
            },
            email = "jenny@gmail.com",
            secretToken = "theSecret",
            validityInstant = Instant.now()
        )
        val event = EmailModificationRequested(42)
        every { mockEmailVerificationDao.findByIdOrNull(event.emailVerificationId) } returns emailVerification

        emailModificationMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.from).isEqualTo(emailModificationProperties.from)
                assertThat(it.to).isEqualTo(emailVerification.email)
                assertThat(it.subject).isEqualTo(emailModificationProperties.subject)
                assertThat(it.plainText)
                    .contains(emailVerification.secretToken)
                assertThat(it.htmlText)
                    .contains(emailVerification.secretToken)
            })
        }
    }
}
