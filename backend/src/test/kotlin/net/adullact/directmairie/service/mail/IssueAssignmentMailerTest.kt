package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.NewIssueProperties
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.GovernmentContact
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.event.IssueCreated
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

/**
 * Unit tests for [IssueAssignmentMailer]
 * @author JB Nizet
 */
class IssueAssignmentMailerTest {

    lateinit var mockMailer: Mailer
    lateinit var issue: Issue
    lateinit var issueAssignmentMailer: IssueAssignmentMailer
    lateinit var event: IssueCreated

    val issueGroup = IssueGroup().apply {
        id = 42
    }

    val newIssueProperties = NewIssueProperties(
        from = "noreply@directmairie.adullact.org",
        subject = "New issue for category %s"
    )

    val hostingProperties = HostingProperties(
        host = "directmairie.adullact.org"
    )

    @BeforeEach
    fun prepare() {
        mockMailer = mockk()
        val mockIssueDao = mockk<IssueDao>()

        issueAssignmentMailer = IssueAssignmentMailer(
            mockMailer,
            mockIssueDao,
            newIssueProperties,
            hostingProperties
        ).apply {
            initializeTemplates()
        }

        issue = Issue().apply {
            id = 1000
            category = IssueCategory().apply {
                label = "Graffiti"
                group = issueGroup
            }
            description = "<b> is painted on the wall"
            assignedGovernment = Government()
        }
        event = IssueCreated(issue.id!!)
        every { mockIssueDao.findByIdOrNull(event.issueId) } returns issue
    }

    @Test
    fun `should send new issue mail to group contact if government has contact email for the issue group`() {
        issue.assignedGovernment!!.addContact(GovernmentContact().apply {
            group = issueGroup
            email = "contact@mail.com"
        })

        issueAssignmentMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.from).isEqualTo(newIssueProperties.from)
                assertThat(it.to).isEqualTo("contact@mail.com")
                assertThat(it.subject).isEqualTo("New issue for category Graffiti")
                assertThat(it.plainText)
                    .contains(issue.category!!.label)
                    .contains(issue.id.toString())
                    .contains(issue.description)
                    .contains("https://directmairie.adullact.org/admin/issues")
                assertThat(it.htmlText)
                    .contains(issue.category!!.label)
                    .contains(issue.id.toString())
                    .contains("&lt;b&gt; is painted on the wall")
                    .contains("https://directmairie.adullact.org/admin/issues")
            })
        }
    }

    @Test
    fun `should send new issue mail to default contact email if government has no contact email for the issue group`() {
        issue.assignedGovernment!!.contactEmail = "contact@mail.com"

        issueAssignmentMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.to).isEqualTo("contact@mail.com")
            })
        }
    }
}
