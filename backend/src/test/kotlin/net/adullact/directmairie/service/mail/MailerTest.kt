package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import jakarta.mail.internet.MimeMessage

class TestMailer(javaMailSender: JavaMailSender, val mockHelper: MimeMessageHelper) : Mailer(javaMailSender) {
    override fun createHelper(mimeMessage: MimeMessage) = mockHelper
}

/**
 * Unit test for [Mailer]
 * @author JB Nizet
 */
class MailerTest {
    @Test
    fun `should send a mime message`() {
        val mockJavaMailSender = mockk<JavaMailSender>()
        val mockMimeMessage = mockk<MimeMessage>()
        every { mockJavaMailSender.createMimeMessage() } returns mockMimeMessage

        val mockHelper = mockk<MimeMessageHelper>()
        val mailer = TestMailer(mockJavaMailSender, mockHelper)

        val mailMessage = MailMessage(
            from = "john@directmairie.com",
            to = "jenny@gmail.com",
            subject = "Hello",
            plainText = "This is plain text",
            htmlText = "<html><body>This is html text</body></html>"
        )

        mailer.send(mailMessage)

        verify {
            mockJavaMailSender.send(mockMimeMessage)
            with(mockHelper) {
                setFrom(mailMessage.from)
                setTo(mailMessage.to)
                setSubject(mailMessage.subject)
                setText(mailMessage.plainText, mailMessage.htmlText)
            }
        }
    }
}
