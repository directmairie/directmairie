package net.adullact.directmairie.service.nominatim

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import net.adullact.directmairie.config.Nominatim
import net.adullact.directmairie.config.NominatimConfig
import net.adullact.directmairie.domain.GeoCoordinates
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.context.annotation.Import
import org.springframework.web.reactive.function.client.WebClient

/**
 * Unit tests for Nominatim
 * @author JB Nizet
 */
@RestClientTest
@Import(NominatimConfig::class)
class NominatimServiceTest(
    @Autowired val objectMapper: ObjectMapper,
    @Autowired @Nominatim val webClient: WebClient
) {

    @Nested
    inner class Deserialization {
        @Test
        fun `should deserialize reverse result`() {
            // from https://nominatim.openstreetmap.org/reverse?format=json&lat=48.8582602&lon=2.29449905431968
            val json = """
                {
                  "place_id": "69121935",
                  "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
                  "osm_type": "way",
                  "osm_id": "5013364",
                  "lat": "48.8582602",
                  "lon": "2.29449905431968",
                  "display_name": "Tour Eiffel, 5, Avenue Anatole France, Quartier du Gros-Caillou, Paris 7e Arrondissement, Paris, Île-de-France, France métropolitaine, 75007, France",
                  "address": {
                    "attraction": "Tour Eiffel",
                    "house_number": "5",
                    "pedestrian": "Avenue Anatole France",
                    "suburb": "Quartier du Gros-Caillou",
                    "city_district": "Paris 7e Arrondissement",
                    "city": "Paris",
                    "county": "Paris",
                    "state": "Île-de-France",
                    "country": "France",
                    "postcode": "75007",
                    "country_code": "fr"
                  },
                  "boundingbox": [
                    "48.8574753",
                    "48.8590465",
                    "2.2933084",
                    "2.2956897"
                  ]
                }
            """.trimIndent()

            val result = objectMapper.readValue<ReverseResult>(json)

            assertThat(result).isEqualTo(ReverseResult(5013364L, OsmType.WAY))
        }

        @Test
        fun `should deserialize error result`() {
            // from https://nominatim.openstreetmap.org/reverse?format=json&lat=1&lon=1
            val json = """
                {"error":"Unable to geocode"}
            """.trimIndent()

            val result = objectMapper.readValue<ReverseResult>(json)

            assertThat(result).isEqualTo(ReverseResult(null, null))
        }

        @Test
        fun `should deserialize details result`() {
            // from https://nominatim.openstreetmap.org/details?osmtype=W&osmid=5013364&format=json&hierarchy=0&addressdetails=1
            val json = """
                {
                  "place_id": 69122003,
                  "parent_place_id": 167433492,
                  "osm_type": "W",
                  "osm_id": 5013364,
                  "category": "man_made",
                  "type": "tower",
                  "admin_level": "15",
                  "localname": "Tour Eiffel",
                  "names": {
                    "alt_name:sr": "Ајфелов торањ",
                    "alt_name:zh-Hans": "巴黎铁塔",
                    "alt_name:zh-Hant": "巴黎鐵塔",
                    "name": "Tour Eiffel",
                    "name:af": "Eiffel-toring",
                    "name:ar": "برج إيفل",
                    "name:ast": "Torrne Eiffel",
                    "name:ba": "Эйфель башняһы",
                    "name:be": "Вежа Эйфеля",
                    "name:cs": "Eiffelova věž",
                    "name:da": "Eiffeltårnet",
                    "name:de": "Eiffelturm",
                    "name:el": "Πύργος του Άιφελ",
                    "name:en": "Eiffel Tower",
                    "name:eo": "Eiffel-Turo",
                    "name:es": "Torre Eiffel",
                    "name:et": "Eiffeli torn",
                    "name:fa": "برج ایفل",
                    "name:fi": "Eiffel-torni",
                    "name:fr": "Tour Eiffel",
                    "name:hr": "Eiffelov toranj",
                    "name:hu": "Eiffel-torony",
                    "name:ia": "Turre Eiffel",
                    "name:id": "Menara Eiffel",
                    "name:io": "Turmo Eiffel",
                    "name:it": "Torre Eiffel",
                    "name:ja": "エッフェル塔",
                    "name:ko": "에펠탑",
                    "name:ku": "Barûya Eyfelê",
                    "name:la": "Turris Eiffelia",
                    "name:lb": "Eiffeltuerm",
                    "name:nl": "Eiffeltoren",
                    "name:pl": "Wieża Eiffla",
                    "name:pt": "Torre Eiffel",
                    "name:ru": "Эйфелева башня",
                    "name:sk": "Eiffelova veža",
                    "name:sl": "Eifflov stolp",
                    "name:sr": "Ајфелова кула",
                    "name:sv": "Eiffeltornet",
                    "name:ta": "ஈபெல் கோபுரம்",
                    "name:tr": "Eyfel Kulesi",
                    "name:tt": "Эйфель манарасы",
                    "name:uk": "Ейфелева вежа",
                    "name:vi": "Tháp Eiffel",
                    "name:vo": "Tüm di Eiffel",
                    "name:zh": "埃菲尔铁塔",
                    "name:zh-Hant": "艾菲爾鐵塔"
                  },
                  "addresstags": {
                    "city": "Paris",
                    "housenumber": "5",
                    "postcode": "75007",
                    "street": "Avenue Anatole France"
                  },
                  "housenumber": "5",
                  "calculated_postcode": "75007",
                  "country_code": "fr",
                  "indexed_date": "2019-01-02T07:30:15+00:00",
                  "importance": 0.553772102971417,
                  "calculated_importance": 0.553772102971417,
                  "extratags": {
                    "fee": "10-25€",
                    "image": "http://upload.wikimedia.org/wikipedia/commons/a/a8/Tour_Eiffel_Wikimedia_Commons.jpg",
                    "opening_hours": "09:30-23:45; Jun 21-Sep 02: 09:00-00:45; Jul 14,Jul 15 off",
                    "website": "http://toureiffel.paris",
                    "wheelchair": "yes",
                    "wikidata": "Q243",
                    "wikipedia": "fr:Tour Eiffel"
                  },
                  "calculated_wikipedia": "fr:Tour_Eiffel",
                  "rank_address": 30,
                  "rank_search": 30,
                  "isarea": true,
                  "centroid": {
                    "type": "Point",
                    "coordinates": [
                      2.29449905431968,
                      48.8582602
                    ]
                  },
                  "geometry": {
                    "type": "Point",
                    "coordinates": [
                      2.29449905431968,
                      48.8582602
                    ]
                  },
                  "address": [
                    {
                      "localname": "Tour Eiffel",
                      "place_id": 69122003,
                      "osm_id": null,
                      "osm_type": null,
                      "class": "man_made",
                      "type": "tower",
                      "admin_level": null,
                      "rank_address": 29,
                      "distance": 0
                    },
                    {
                      "localname": "5",
                      "place_id": 69122003,
                      "osm_id": null,
                      "osm_type": null,
                      "class": "place",
                      "type": "house_number",
                      "admin_level": null,
                      "rank_address": 28,
                      "distance": 0
                    },
                    {
                      "localname": "Avenue Anatole France",
                      "place_id": 167433492,
                      "osm_id": 416858377,
                      "osm_type": "W",
                      "class": "highway",
                      "type": "pedestrian",
                      "admin_level": 15,
                      "rank_address": 26,
                      "distance": 0
                    },
                    {
                      "localname": "Quartier du Gros-Caillou",
                      "place_id": 198418039,
                      "osm_id": 2188569,
                      "osm_type": "R",
                      "class": "boundary",
                      "type": "administrative",
                      "admin_level": 10,
                      "rank_address": 20,
                      "distance": 0.00393396142790994
                    },
                    {
                      "localname": "Paris 7e Arrondissement",
                      "place_id": 197979395,
                      "osm_id": 9521,
                      "osm_type": "R",
                      "class": "boundary",
                      "type": "administrative",
                      "admin_level": 9,
                      "rank_address": 18,
                      "distance": 0.0140678145528286
                    },
                    {
                      "localname": "Paris",
                      "place_id": 198006226,
                      "osm_id": 7444,
                      "osm_type": "R",
                      "class": "place",
                      "type": "city",
                      "admin_level": 8,
                      "rank_address": 16,
                      "distance": 0.044753238640682
                    },
                    {
                      "localname": "Paris",
                      "place_id": 198480347,
                      "osm_id": 1641193,
                      "osm_type": "R",
                      "class": "boundary",
                      "type": "administrative",
                      "admin_level": 7,
                      "rank_address": 14,
                      "distance": 0.044753238640682
                    },
                    {
                      "localname": "Paris",
                      "place_id": 198683960,
                      "osm_id": 71525,
                      "osm_type": "R",
                      "class": "boundary",
                      "type": "administrative",
                      "admin_level": 6,
                      "rank_address": 12,
                      "distance": 0.044753238640682
                    },
                    {
                      "localname": "Île-de-France",
                      "place_id": 198254379,
                      "osm_id": 8649,
                      "osm_type": "R",
                      "class": "place",
                      "type": "state",
                      "admin_level": 4,
                      "rank_address": 8,
                      "distance": 0.252487784129359
                    },
                    {
                      "localname": "France métropolitaine",
                      "place_id": 198451286,
                      "osm_id": 1403916,
                      "osm_type": "R",
                      "class": "place",
                      "type": "country",
                      "admin_level": 3,
                      "rank_address": 6,
                      "distance": 2.32633690656113
                    },
                    {
                      "localname": "75007",
                      "place_id": null,
                      "osm_id": null,
                      "osm_type": null,
                      "class": "place",
                      "type": "postcode",
                      "admin_level": null,
                      "rank_address": 5,
                      "distance": 0
                    },
                    {
                      "localname": "France",
                      "place_id": 198319371,
                      "osm_id": 2202162,
                      "osm_type": "R",
                      "class": "place",
                      "type": "country",
                      "admin_level": 2,
                      "rank_address": 4,
                      "distance": 30.2994196383546
                    },
                    {
                      "localname": "fr",
                      "place_id": null,
                      "osm_id": null,
                      "osm_type": null,
                      "class": "place",
                      "type": "country_code",
                      "admin_level": null,
                      "rank_address": 4,
                      "distance": 0
                    }
                  ]
                }
            """.trimIndent()

            val result = objectMapper.readValue<DetailsResult>(json)

            assertThat(result).isEqualTo(DetailsResult(listOf(
                DetailsAddress(null, null, null),
                DetailsAddress(null, null, null),
                DetailsAddress(416858377L, OsmType.WAY, 15),
                DetailsAddress(2188569L, OsmType.RELATION, 10),
                DetailsAddress(9521L, OsmType.RELATION, 9),
                DetailsAddress(7444L, OsmType.RELATION, 8),
                DetailsAddress(1641193L, OsmType.RELATION, 7),
                DetailsAddress(71525L, OsmType.RELATION, 6),
                DetailsAddress(8649L, OsmType.RELATION, 4),
                DetailsAddress(1403916L, OsmType.RELATION, 3),
                DetailsAddress(null, null, null),
                DetailsAddress(2202162L, OsmType.RELATION, 2),
                DetailsAddress(null, null, null)
            )))
        }
    }

    @Test
    @Disabled("This test is disabled because Nominatim sometimes returns a 403 error")
    fun `should get details of coordinates from nominatim`() {
        val nominatimService = NominatimService(webClient)
        val coordinates = GeoCoordinates(48.8582602, 2.29449905431968)

        val details = nominatimService.details(coordinates)
        assertThat(details.address).isNotEmpty
        assertThat(details.address.filter { it.isPotentialGovernment }).isNotEmpty
    }

    @Test
    fun `should get details of coordinates from nominatim when reverse fails`() {
        val nominatimService = NominatimService(webClient)
        val coordinates = GeoCoordinates(3.0, 3.0)

        val details = nominatimService.details(coordinates)
        assertThat(details.address).isEmpty()
    }
}
