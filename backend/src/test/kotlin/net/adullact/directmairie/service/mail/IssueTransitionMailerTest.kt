package net.adullact.directmairie.service.mail

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.TIMEZONE
import net.adullact.directmairie.config.mail.IssueTransitionProperties
import net.adullact.directmairie.config.mail.NewIssueProperties
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.IssueTransitionDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.GovernmentContact
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.IssueTransition
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.event.IssueCreated
import net.adullact.directmairie.event.IssueTransitioned
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDate

/**
 * Unit tests for [IssueTransitionMailer]
 * @author JB Nizet
 */
class IssueTransitionMailerTest {
    lateinit var mockMailer: Mailer
    lateinit var issueTransition: IssueTransition
    lateinit var issueTransitionMailer: IssueTransitionMailer
    lateinit var event: IssueTransitioned

    val issueTransitionProperties = IssueTransitionProperties(
        from = "noreply@directmairie.adullact.org",
        subject = "Issue status changed"
    )
    val hostingProperties = HostingProperties(
        host = "directmairie.adullact.org"
    )

    @BeforeEach
    fun prepare() {
        mockMailer = mockk()
        val mockIssueTransitionDao = mockk<IssueTransitionDao>()

        issueTransitionMailer = IssueTransitionMailer(
            mockMailer,
            mockIssueTransitionDao,
            issueTransitionProperties,
            hostingProperties
        ).apply {
            initializeTemplates()
        }

        issueTransition = IssueTransition().apply {
            id = 1234
            afterStatus = IssueStatus.RESOLVED
            message = "Well done"
            issue = Issue().apply {
                category = IssueCategory().apply {
                    label = "Graffiti"
                }
                description = "<b> is painted on the wall"
                creator = User().apply {
                    email = "john@mail.com"
                    issueTransitionNotificationActivated = true
                }
                creationInstant = LocalDate.of(2023, 3, 9).atStartOfDay(TIMEZONE).toInstant()
                assignedGovernment = Government().apply {
                    name = "Saint-Etienne"
                }
            }
        }
        event = IssueTransitioned(issueTransition.id!!)
        every { mockIssueTransitionDao.findByIdOrNull(event.issueTransitionId) } returns issueTransition
    }

    @Test
    fun `should send transition mail to creator if notifications activated`() {
        issueTransitionMailer.sendEmail(event)

        verify {
            mockMailer.send(withArg {
                assertThat(it.from).isEqualTo(issueTransitionProperties.from)
                assertThat(it.to).isEqualTo("john@mail.com")
                assertThat(it.subject).isEqualTo("Issue status changed")
                assertThat(it.plainText)
                    .contains("9 mars 2023")
                    .contains(issueTransition.issue.category!!.label)
                    .contains(issueTransition.issue.description)
                    .contains(issueTransition.message)
                    .contains("résolue")
                    .contains(issueTransition.issue.assignedGovernment!!.name)
                    .contains("https://directmairie.adullact.org/me/account/edit")
                assertThat(it.htmlText)
                    .contains("9 mars 2023")
                    .contains(issueTransition.issue.category!!.label)
                    .contains("&lt;b&gt; is painted on the wall")
                    .contains(issueTransition.message)
                    .contains("résolue")
                    .contains(issueTransition.issue.assignedGovernment!!.name)
                    .contains("https://directmairie.adullact.org/me/account/edit")
            })
        }
    }

    @Test
    fun `should not send transition mail if notifications not activated`() {
        issueTransition.issue.creator!!.issueTransitionNotificationActivated = false

        issueTransitionMailer.sendEmail(event)

        verify(inverse = true) {
            mockMailer.send(any())
        }
    }

    @Test
    fun `should not send transition mail if no creator`() {
        issueTransition.issue.creator = null

        issueTransitionMailer.sendEmail(event)

        verify(inverse = true) {
            mockMailer.send(any())
        }
    }

    @Test
    fun `should not send transition mail if new status is not RESOLVED or REJECTED`() {
        issueTransition.afterStatus = IssueStatus.CREATED

        issueTransitionMailer.sendEmail(event)

        verify(inverse = true) {
            mockMailer.send(any())
        }
    }
}
