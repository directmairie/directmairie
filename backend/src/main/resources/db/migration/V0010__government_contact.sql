CREATE TABLE government_contact (
  id            BIGINT PRIMARY KEY,
  email         VARCHAR NOT NULL,
  government_id BIGINT NOT NULL,
  group_id      BIGINT NOT NULL
);

CREATE SEQUENCE government_contact_seq START WITH 1000;

ALTER TABLE government_contact ADD CONSTRAINT government_contact_un1 UNIQUE (government_id, group_id);

ALTER TABLE government_contact
  ADD CONSTRAINT government_contact_fk1
  FOREIGN KEY (government_id)
  REFERENCES government (id);

ALTER TABLE government_contact
  ADD CONSTRAINT government_contact_fk2
  FOREIGN KEY (group_id)
  REFERENCES issue_group (id);
