CREATE TABLE issue_group (
  id    BIGINT PRIMARY KEY,
  label VARCHAR NOT NULL
);
ALTER TABLE issue_group ADD CONSTRAINT issue_group_un1 UNIQUE (label);

CREATE SEQUENCE issue_group_seq START WITH 1000;

-- ID 1 is the "OTHER" group. The code uses this ID to put this group at the bottom of the list
INSERT INTO issue_group (id, label) VALUES (1, 'Autre');

CREATE TABLE issue_category (
  id                   BIGINT PRIMARY KEY,
  label                VARCHAR NOT NULL,
  description_required BOOLEAN NOT NULL,
  group_id             BIGINT NOT NULL
);

ALTER TABLE issue_category ADD CONSTRAINT issue_category_fk1 FOREIGN KEY (group_id) REFERENCES issue_group (id);

CREATE SEQUENCE issue_category_seq START WITH 1000;

ALTER TABLE issue ADD COLUMN category_id BIGINT;
ALTER TABLE issue ADD CONSTRAINT issue_fk1 FOREIGN KEY (category_id) REFERENCES issue_category (id);

-- ID 1 is the "OTHER" category. The code uses this ID to put this category at the bottom of the list
INSERT INTO issue_category (id, label, description_required, group_id) VALUES (1, 'Autre', true, 1);
