ALTER TABLE auser ADD COLUMN issue_transition_notification_activated BOOLEAN;

UPDATE auser set issue_transition_notification_activated = false;

ALTER TABLE auser ALTER COLUMN issue_transition_notification_activated SET NOT NULL;
