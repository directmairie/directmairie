CREATE TABLE auser (
  id                          BIGINT PRIMARY KEY,
  email                       VARCHAR NOT NULL,
  password                    VARCHAR NOT NULL,
  super_admin                 BOOLEAN NOT NULL,
  email_verified              BOOLEAN NOT NULL
);

CREATE SEQUENCE auser_seq START WITH 1000;

CREATE TABLE government_administrator (
  government_id        BIGINT NOT NULL,
  user_id              BIGINT NOT NULL,
  PRIMARY KEY (government_id, user_id)
);

ALTER TABLE government_administrator
  ADD CONSTRAINT government_administrator_fk1
  FOREIGN KEY (government_id)
  REFERENCES government (id);
ALTER TABLE government_administrator
  ADD CONSTRAINT government_administrator_fk2
  FOREIGN KEY (user_id)
  REFERENCES auser (id);
