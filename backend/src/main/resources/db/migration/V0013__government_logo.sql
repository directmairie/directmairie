ALTER TABLE government ADD COLUMN logo_id BIGINT;

ALTER TABLE government ADD CONSTRAINT government_fk2 FOREIGN KEY (logo_id) REFERENCES picture (id);
