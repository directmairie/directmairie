CREATE TABLE government (
  id                   BIGINT PRIMARY KEY,
  name                 VARCHAR NOT NULL,
  osm_id               BIGINT NOT NULL
);

CREATE SEQUENCE government_seq START WITH 1000;

ALTER TABLE government ADD CONSTRAINT government_un1 UNIQUE (osm_id);

ALTER TABLE issue ADD COLUMN assigned_government_id BIGINT;
ALTER TABLE issue ADD CONSTRAINT issue_fk2 FOREIGN KEY (assigned_government_id) REFERENCES government (id);

ALTER TABLE issue ADD COLUMN leaf_government_id BIGINT;
ALTER TABLE issue ADD CONSTRAINT issue_fk3 FOREIGN KEY (leaf_government_id) REFERENCES government (id);

CREATE TABLE government_issue_category (
  government_id        BIGINT NOT NULL,
  category_id          BIGINT NOT NULL,
  PRIMARY KEY (government_id, category_id)
);

ALTER TABLE government_issue_category
  ADD CONSTRAINT government_issue_category_fk1
  FOREIGN KEY (government_id)
  REFERENCES government (id);
ALTER TABLE government_issue_category
  ADD CONSTRAINT government_issue_category_fk2
  FOREIGN KEY (category_id)
  REFERENCES issue_category (id);
