CREATE TABLE picture (
  id               BIGINT PRIMARY KEY,
  path             VARCHAR NOT NULL,
  content_type     VARCHAR NOT NULL,
  creation_instant TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE SEQUENCE picture_seq START WITH 1000;

CREATE TABLE issue_picture (
  issue_id         BIGINT NOT NULL,
  picture_id       BIGINT NOT NULL,
  PRIMARY KEY (issue_id, picture_id)
);

ALTER TABLE issue_picture ADD CONSTRAINT issue_picture_fk1 FOREIGN KEY (issue_id) REFERENCES issue (id);
ALTER TABLE issue_picture ADD CONSTRAINT issue_picture_fk2 FOREIGN KEY (picture_id) REFERENCES picture (id);
