ALTER TABLE government ADD COLUMN contact_email VARCHAR;

UPDATE government SET contact_email = 'invalid@adullact.org';

ALTER TABLE government ALTER COLUMN contact_email SET NOT NULL;
