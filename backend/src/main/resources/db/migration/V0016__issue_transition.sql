CREATE TABLE issue_transition
(
    id                 BIGINT                   NOT NULL PRIMARY KEY,
    before_status      VARCHAR                  NOT NULL,
    after_status       VARCHAR                  NOT NULL,
    transition_instant TIMESTAMP WITH TIME ZONE NOT NULL,
    message            VARCHAR,
    issue_id           BIGINT                   NOT NULL
);

ALTER TABLE issue_transition
    ADD CONSTRAINT issue_transition_fk1 FOREIGN KEY (issue_id) REFERENCES issue (id);

CREATE SEQUENCE issue_transition_seq START WITH 1000;
