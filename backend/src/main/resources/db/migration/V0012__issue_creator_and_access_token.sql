ALTER TABLE issue ADD COLUMN creator_id BIGINT;
ALTER TABLE issue ADD COLUMN access_token VARCHAR;

ALTER TABLE issue ADD CONSTRAINT issue_fk4 FOREIGN KEY (creator_id) REFERENCES auser (id);
