CREATE TABLE subscription
(
    id            BIGINT NOT NULL PRIMARY KEY,
    user_id       BIGINT NOT NULL,
    government_id BIGINT NOT NULL
);

ALTER TABLE subscription
    ADD CONSTRAINT subscription_fk1 FOREIGN KEY (user_id) REFERENCES auser (id);
ALTER TABLE subscription
    ADD CONSTRAINT subscription_fk2 FOREIGN KEY (government_id) REFERENCES government (id);
ALTER TABLE subscription
    ADD CONSTRAINT subscription_un1 UNIQUE (user_id, government_id);

CREATE INDEX subscription_idx_user_id on subscription(user_id);
CREATE INDEX subscription_idx_government_id on subscription(government_id);

CREATE SEQUENCE subscription_seq START WITH 1000;
