CREATE TABLE pooling_organization (
  id                   BIGINT PRIMARY KEY,
  name                 VARCHAR NOT NULL
);

CREATE SEQUENCE pooling_organization_seq START WITH 1000;

ALTER TABLE pooling_organization ADD CONSTRAINT pooling_organization_un1 UNIQUE (name);

ALTER TABLE government ADD COLUMN pooling_organization_id BIGINT NOT NULL;
ALTER TABLE government
  ADD CONSTRAINT government_fk1
  FOREIGN KEY (pooling_organization_id)
  REFERENCES pooling_organization (id);

CREATE TABLE pooling_organization_administrator (
  pooling_organization_id BIGINT NOT NULL,
  user_id                 BIGINT NOT NULL,
  PRIMARY KEY (pooling_organization_id, user_id)
);

ALTER TABLE pooling_organization_administrator
  ADD CONSTRAINT pooling_organization_administrator_fk1
  FOREIGN KEY (pooling_organization_id)
  REFERENCES pooling_organization (id);
ALTER TABLE pooling_organization_administrator
  ADD CONSTRAINT pooling_organization_administrator_fk2
  FOREIGN KEY (user_id)
  REFERENCES auser (id);
