CREATE TABLE email_verification (
  id                          BIGINT PRIMARY KEY,
  email                       VARCHAR NOT NULL,
  secret_token                VARCHAR NOT NULL,
  validity_instant            TIMESTAMP WITH TIME ZONE NOT NULL,
  user_id                     BIGINT NOT NULL
);

ALTER TABLE email_verification
  ADD CONSTRAINT email_verification_fk1
  FOREIGN KEY (user_id)
  REFERENCES auser (id);

CREATE SEQUENCE email_verification_seq START WITH 1000;
