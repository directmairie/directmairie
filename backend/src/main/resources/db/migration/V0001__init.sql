CREATE TABLE issue (
  id               BIGINT PRIMARY KEY,
  status           VARCHAR NOT NULL,
  description      VARCHAR,
  latitude         DOUBLE PRECISION NOT NULL,
  longitude        DOUBLE PRECISION NOT NULL,
  creation_instant TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE SEQUENCE issue_seq START WITH 1000;
