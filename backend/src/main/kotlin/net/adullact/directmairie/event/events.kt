package net.adullact.directmairie.event

import net.adullact.directmairie.web.notification.NotificationCommandDTO

/**
 * Event that is published when the status of an issue becomes [CREATED][net.adullact.directmairie.domain.IssueStatus.CREATED]
 * for the first time (i.e. at the end of the submission of the issue)
 */
data class IssueCreated(val issueId: Long)

/**
 * Event that is published when a user asks to register
 */
data class UserRegistered(val emailVerificationId: Long)

/**
 * Event that is published when a user signals that they lost his/her password
 */
data class PasswordLost(val emailVerificationId: Long)

/**
 * Event that is published when a user signals that they want to change their email
 */
data class EmailModificationRequested(val emailVerificationId: Long)

/**
 * Event that is published when the status of an issue is updated by an administrator
 */
data class IssueTransitioned(val issueTransitionId: Long)

/**
 * Event that is published when an issue is deleted.
 */
data class IssueDeleted(val issueId: Long, val picturePaths: Set<String>)

/**
 * Event sent when a notification must be sent by email to all the subscribers of a government
 */
data class NotificationRequested(val command: NotificationCommandDTO)
