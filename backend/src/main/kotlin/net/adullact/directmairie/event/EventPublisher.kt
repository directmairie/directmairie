package net.adullact.directmairie.event

import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component

/**
 * Component which abstracts away the Spring `ApplicationEventPublisher` and makes it easier to mock with MockK
 * @author JB Nizet
 */
@Component
class EventPublisher(private val publisher: ApplicationEventPublisher) {
    fun publish(event: Any) = publisher.publishEvent(event)
}
