package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * DAO for [User]
 * @author JB Nizet
 */
interface UserDao : BaseRepository<User, Long> {

    /**
     * Finds the unique user having the given email (case-insensitive), or null if there is no such user.
     * Note that a user can be returned by this method even if the email is not verified.
     */
    @Query("select user from User user where lower(user.email) = lower(:email)")
    fun findByEmail(@Param("email") email: String): User?

    /**
     * Finds a page of verified users having an email containing (case-insensitive) the given query.
     * @return the page of users, ordered by their email (case-insensitive)
     */
    @Query(
        """
            select user from User user
            where lower(user.email) like concat('%', lower(:query), '%')
            and user.emailVerified = true
            order by lower(user.email)
        """
    )
    fun searchByVerifiedEmail(@Param("query") query: String, page: Pageable): Page<User>

    /**
     * Finds the unique verified user having the given email (case-insensitive), or null if there is no such user.
     */
    @Query("select user from User user where lower(user.email) = lower(:email) and user.emailVerified = true")
    fun findByVerifiedEmail(@Param("email") email: String): User?

    /**
     * Gets the verified user with the given ID, or throws an exception is there is no such user.
     * @param userId: the ID of the verified user to get
     * @param exceptionSupplier the function creating the exception thrown if there is no such user.
     */
    fun findVerifiedByIdOrThrow(userId: Long, exceptionSupplier: () -> Exception): User =
        findByIdOrNull(userId)
            ?.takeIf { it.emailVerified }
            ?: throw exceptionSupplier()
}
