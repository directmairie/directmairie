package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.IssueGroup

/**
 * The DAO for [IssueGroup]
 * @author JB Nizet
 */
interface IssueGroupDao : BaseRepository<IssueGroup, Long> {

    /**
     * Finds the unique issue group with the given label, or null if there is none.
     */
    fun findByLabel(label: String): IssueGroup?
}
