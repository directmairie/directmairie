package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.domain.User
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * DAO for [PoolingOrganization]
 * @author JB Nizet
 */
interface PoolingOrganizationDao : BaseRepository<PoolingOrganization, Long> {
    /**
     * Finds the pooling organizations which contain (case-insensitive) the given substring in their name.
     * If the query is empty, all the organizations are returned.
     * @return the found organizations, ordered by name (in case-insensitive order)
     */
    @Query(
        """
            select org from PoolingOrganization org
            where lower(org.name) like concat('%', lower(:query), '%')
            order by lower(org.name)""" // don't add a newline otherwise the count query isn't correctly created
    )
    fun findByQuery(@Param("query") query: String): List<PoolingOrganization>

    /**
     * Finds the pooling organizations which contain (case-insensitive) the given substring in their name
     * and are administered by the given user.
     * @return the found organizations, ordered by name (in case-insensitive order)
     */
    @Query(
        """
            select org from PoolingOrganization org
            where lower(org.name) like concat('%', lower(:query), '%')
            and :user member of org.administratorSet
            order by lower(org.name)
        """
    )
    fun findByQueryAndAdministrator(@Param("query") query: String, @Param("user") user: User): List<PoolingOrganization>

    /**
     * Finds the unique pooling organization with the given name, or null if there is no such organization
     */
    fun findByName(name: String): PoolingOrganization?
}
