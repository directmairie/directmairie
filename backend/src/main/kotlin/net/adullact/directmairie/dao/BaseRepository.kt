package net.adullact.directmairie.dao

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.support.JpaEntityInformation
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import org.springframework.data.repository.NoRepositoryBean
import java.io.Serializable
import jakarta.persistence.EntityManager


/**
 * Custom base repository that makes it more Kotlin friendly and adds the standard method that will be added in the
 * next version of Spring data JPA (see https://jira.spring.io/browse/DATACMNS-1346)
 * @author JB Nizet
 */
@NoRepositoryBean
interface BaseRepository<T, ID: Serializable> : JpaRepository<T, ID> {
    /**
     * Returns the entity identified by the given ID, or null if not found
     */
    fun findByIdOrNull(id: ID): T?

    /**
     * Returns the entity identified by the given ID, or throws an exception
     * @param id: the ID used to find the entity
     * @param exceptionSupplier the function supplying the exception to find. If absent, a [NotFoundException] is thrown
     */
    fun findByIdOrThrow(id: ID, exceptionSupplier: () -> Exception = { NotFoundException() }): T
}


open class BaseRepositoryImpl<T, ID : Serializable>(
    entityInformation: JpaEntityInformation<T, *>,
    private val entityManager: EntityManager
) : SimpleJpaRepository<T, ID>(entityInformation, entityManager), BaseRepository<T, ID> {

    override fun findByIdOrNull(id: ID): T? = findById(id).orElse(null)

    override fun findByIdOrThrow(id: ID, exceptionSupplier: () -> Exception): T =
        findByIdOrNull(id) ?: throw exceptionSupplier()
}
