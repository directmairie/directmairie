package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import java.time.Instant
import java.util.*
import java.util.stream.Stream
import jakarta.persistence.EntityManager

/**
 * The DAO for [Issue]
 * @author JB Nizet
 */
interface IssueDao : BaseRepository<Issue, Long>, IssueDaoCustom {

    /**
     * Finds the issues in the given instant interval, that can be handled by the current user:
     *
     * - if pooling organization admin: sees every issue of the governments of the administered pooling organizations
     * - if government admin: sees every issue of the administered governments
     *
     * If the user is a super-admin, then [findByStatus] should be used instead
     */
    @Query(
        """
            select issue from Issue issue
            join issue.assignedGovernment government
            join government.poolingOrganization organization
            left join fetch issue.assignedGovernment fetchedGovernment
            left join fetch fetchedGovernment.poolingOrganization
            left join fetch issue.category fetchedCategory
            left join fetch fetchedCategory.group
            where issue.creationInstant >= :from
            and issue.creationInstant < :to
            and issue.status <> net.adullact.directmairie.domain.IssueStatus.DRAFT
            and (
                :user member of government.administratorSet
                or
                :user member of organization.administratorSet
            )
            order by issue.creationInstant desc""" // don't add a newline otherwise the count query isn't correctly created
    )
    fun findNonDraftByIntervalAndUser(
        @Param("from") from: Instant,
        @Param("to") to: Instant,
        @Param("user") user: User
    ): Stream<Issue>

    /**
     * Finds all the issues in the given interval (used for super-admins)
     */
    @Query(
        """
            select issue from Issue issue
            left join fetch issue.assignedGovernment fetchedGovernment
            left join fetch fetchedGovernment.poolingOrganization
            left join fetch issue.category fetchedCategory
            left join fetch fetchedCategory.group
            where issue.creationInstant >= :from
            and issue.creationInstant < :to
            and issue.status <> net.adullact.directmairie.domain.IssueStatus.DRAFT
            order by issue.creationInstant desc""" // don't add a newline otherwise the count query isn't correctly created
    )
    fun findNonDraftByInterval(@Param("from") from: Instant, @Param("to") to: Instant): Stream<Issue>

    /**
     * Finds the issues created by the given user and having one of the given statuses
     */
    @Query(
        """
            select issue from Issue issue
            where issue.creator = :creator
            and issue.status in :statuses
            order by issue.creationInstant desc
        """
    )
    fun findByCreatorAndStatuses(
        @Param("creator") creator: User,
        @Param("statuses") statuses: Set<IssueStatus>,
        page: Pageable
    ): Page<Issue>

    @Query("update Issue issue set issue.creator = null where issue.creator  = :creator")
    @Modifying
    fun anonymizeByCreator(@Param("creator") creator: User);
}

interface IssueDaoCustom {
    /**
     * Finds the issues obeying the given search criteria
     *
     * - if pooling organization admin: sees every issue of the governments of the administered pooling organizations
     * - if government admin: sees every issue of the administered governments
     *
     * If the user is a super-admin, then [findByStatus] should be used instead
     */
    fun findByCriteria(criteria: IssueSearchCriteria, page: Pageable): Page<Issue>
}

class IssueDaoCustomImpl(entityManager: EntityManager) : BaseCustomRepository<Issue, Long>(entityManager, Issue::class.java),
    IssueDaoCustom {
    override fun findByCriteria(criteria: IssueSearchCriteria, page: Pageable): Page<Issue> {
        val jpql = buildString {
            append(
                """
                select issue from Issue issue
                join issue.assignedGovernment government
                join government.poolingOrganization organization
                where issue.status in :statuses
            """.trimIndent()
            )
            if (!criteria.user.superAdmin) {
                append(
                    """
                    
                    and (
                        :user member of government.administratorSet
                        or
                        :user member of organization.administratorSet
                    )
                    
                """.trimIndent()
                )
            }
            if (criteria.poolingOrganizationIds.isNotEmpty() || criteria.governmentIds.isNotEmpty()) {
                val clauses = mutableListOf<String>()
                if (criteria.poolingOrganizationIds.isNotEmpty()) {
                    clauses.add("organization.id in :poolingOrganizationIds")
                }
                if (criteria.governmentIds.isNotEmpty()) {
                    clauses.add("government.id in :governmentIds")
                }
                append(" and (${clauses.joinToString(separator = " or ")}) ")
            }
            append(" order by issue.creationInstant desc")
        }
        return executePagedQuery(jpql, page) { query ->
            query.setParameter("statuses", criteria.statuses)
            if (!criteria.user.superAdmin) {
                query.setParameter("user", criteria.user)
            }
            if (criteria.poolingOrganizationIds.isNotEmpty()) {
                query.setParameter("poolingOrganizationIds", criteria.poolingOrganizationIds)
            }
            if (criteria.governmentIds.isNotEmpty()) {
                query.setParameter("governmentIds", criteria.governmentIds)
            }
        }
    }
}

data class IssueSearchCriteria(
    /**
     * if superAdmin, then all issues are candidates. Otherwise, only the issues administered by this user are retrieved
     */
    val user: User,
    /**
     * May not be empty. Only the issues with one of thoses statuses are retrieved
     */
    val statuses: Set<IssueStatus> = IssueStatus.nonDraft,
    /**
     * If not empty, only the issues belonging to one of those pooling organizations or to the given governments are retrieved
     */
    val poolingOrganizationIds: Set<Long> = emptySet(),
    /**
     * If not empty, only the issues belonging to one of those governments or to the given pooling organizations are retrieved
     */
    val governmentIds: Set<Long> = emptySet()
)
