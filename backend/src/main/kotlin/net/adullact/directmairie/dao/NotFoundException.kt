package net.adullact.directmairie.dao

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception that can be thrown to signal that an entity can't be found. Throwing this exception
 * sends a 404 error to the client.
 * @author JB Nizet
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
class NotFoundException : RuntimeException() {
}
