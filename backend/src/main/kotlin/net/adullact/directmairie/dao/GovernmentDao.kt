package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.User
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * The DAO for [Government]
 * @author JB Nizet
 */
interface GovernmentDao : BaseRepository<Government, Long> {

    /**
     * Finds all the governments which have an OSM ID present in the given set of OSM IDs. The governments
     * are not returned in any specific order.
     * @param osmIds the set of accepted OSM IDs, which may not be empty
     */
    @Query(
        """
            select government from Government government
            where government.osmId in :osmIds"""
    )
    fun findByOsmIds(@Param("osmIds") osmIds: Set<Long>): List<Government>

    /**
     * Finds the governments which contain (case-insensitive) the given substring in their name or their code
     * and which are administered by the given user or
     * which belong to one of the administered pooling organizations of the current user.
     * If the current user is a super admin, then [findByQuery] should be used instead
     */
    @Query(
        """
            select government from Government government
            join government.poolingOrganization organization
            where (
                lower(government.name) like concat('%', lower(:query), '%')
                or lower(government.code) like concat('%', lower(:query), '%')
            )
            and (
                :user member of organization.administratorSet
                or
                :user member of government.administratorSet
            )
            order by lower(government.name)""" // don't add a newline otherwise the count query isn't correctly created
    )
    fun findByQueryAndUser(@Param("query") query: String, user: User, page: Pageable): Page<Government>

    /**
     * Finds the governments which contain (case-insensitive) the given substring in their name or their code
     */
    @Query(
        """
            select government from Government government
            where lower(government.name) like concat('%', lower(:query), '%')
            or lower(government.code) like concat('%', lower(:query), '%')
            order by lower(government.name)""" // don't add a newline otherwise the count query isn't correctly created
    )
    fun findByQuery(@Param("query") query: String, page: Pageable): Page<Government>

    /**
     * Finds the unique government identified by the given OSM ID
     */
    fun findByOsmId(osmId: Long): Government?

    /**
     * Finds the unique government identified by the given code
     */
    fun findByCode(code: String): Government?
}
