package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Subscription
import jakarta.persistence.EntityManager

/**
 * The DAO for [Subscription]
 * @author JB Nizet
 */
interface SubscriptionDao : BaseRepository<Subscription, Long>, SubscriptionDaoCustom

interface SubscriptionDaoCustom {
    /**
     * Returns the batch of recipients subscribed to the given government, of the given size,
     * and starting after the given recipient ID. For the first batch, pass Long.MIN_VALUE.
     */
    fun batchOfRecipients(governmentId: Long, size: Int, startingAfterId: Long?): List<Recipient>

    /**
     * Finds the 10 first governments which contain (case-insensitive) the given substring in their name,
     * and to which the given user is not subscribed yet
     */
    fun suggestGovernments(query: String, userId: Long, size: Int): List<Government>;
}

class SubscriptionDaoCustomImpl(entityManager: EntityManager) :
    BaseCustomRepository<Subscription, Long>(entityManager, Subscription::class.java), SubscriptionDaoCustom {

    override fun batchOfRecipients(governmentId: Long, size: Int, startingAfterId: Long?): List<Recipient> {
        val jpql =
            """
            select new net.adullact.directmairie.dao.Recipient(
                user.id,
                user.email
            )
            from Subscription subscription
            inner join subscription.user user
            where subscription.government.id = :governmentId
            ${startingAfterId?.let { "and user.id > :startingAfterId" } ?: ""}
            order by user.id
            """.trimIndent()
        return entityManager.createQuery(jpql, Recipient::class.java)
            .apply {
                maxResults = size
            }
            .setParameter("governmentId", governmentId)
            .apply {
                startingAfterId?.let {
                    setParameter("startingAfterId", startingAfterId)
                }
            }
            .resultList
    }

    override fun suggestGovernments(query: String, userId: Long, size: Int): List<Government> {
        val jpql =
            """
            select government from Government government
            where lower(government.name) like concat('%', lower(:query), '%')
            and government.id not in (
                select subscription.government.id 
                from Subscription subscription 
                where subscription.user.id = :userId
            )
            order by lower(government.name)
            """
        return entityManager.createQuery(jpql, Government::class.java)
            .apply {
                maxResults = size
            }
            .setParameter("query", query)
            .setParameter("userId", userId)
            .resultList
    }
}

data class Recipient(val id: Long, val email: String)
