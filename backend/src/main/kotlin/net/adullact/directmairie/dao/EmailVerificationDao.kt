package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * DAO for [EmailVerification]
 * @author JB Nizet
 */
interface EmailVerificationDao : BaseRepository<EmailVerification, Long> {

    @Query("""
        select v from EmailVerification v
        where v.user.id = :userId
        and v.secretToken = :secretToken
        and v.validityInstant > current_timestamp
    """)
    fun findValidByUserAndToken(
        @Param("userId") userId: Long,
        @Param("secretToken") secretToken: String
    ): EmailVerification?

    @Query("delete from EmailVerification ev where ev.user = :user")
    @Modifying
    fun deleteByUser(@Param("user") user: User)
}
