package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.IssueCategory
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * The DAO for [IssueCategory]
 * @author JB Nizet
 */
interface IssueCategoryDao : BaseRepository<IssueCategory, Long> {

    /**
     * Finds all the distinct issue categories that are used by a government having one of the given OSM IDs.
     * The categories are not returned in any specific order.
     * @param osmIds the set of accepted OSM IDs, which may not be empty
     */
    @Query(
        """
        select distinct category from Government government
        inner join government.issueCategorySet category
        where government.osmId in :osmIds
        """
    )
    fun findByGovernmentOsmIds(@Param("osmIds") osmIds: Set<Long>): List<IssueCategory>

    /**
     * Finds the unique issue category with the given label, or null if there is none.
     */
    fun findByLabel(label: String): IssueCategory?
}
