package net.adullact.directmairie.dao

import net.adullact.directmairie.domain.IssueTransition

/**
 * The DAO for [IssueTransition]
 * @author JB Nizet
 */
interface IssueTransitionDao : BaseRepository<IssueTransition, Long>
