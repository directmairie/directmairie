package net.adullact.directmairie.dao

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.query.QueryUtils
import org.springframework.data.jpa.repository.support.SimpleJpaRepository
import org.springframework.data.support.PageableExecutionUtils
import jakarta.persistence.EntityManager
import jakarta.persistence.Query
import jakarta.persistence.TypedQuery

/**
 * Base class for custom repository implementations.
 * It allows delegating to a [SimpleJpaRepository] rather than just to an [EntityManager].
 * It provides protected methods that can be used to execute pageable queries based on dynamically
 * composed JPQL queries.
 * @author JB Nizet
 */
open class BaseCustomRepository<T, ID>(protected val entityManager: EntityManager, private val domainClass: Class<T>) {
    protected val delegate: SimpleJpaRepository<T, ID> = SimpleJpaRepository(domainClass, entityManager)

    /**
     * Executes a pageable JPQL query returning instances of this repository's domain class.
     * @param jpql the JPQL query
     * @param pageable the pageable
     * @param parameterBinder a lambda that sets the necessary parameters on the query, and on the derived
     * count query. Can be omitted if the query doesn't have any parameter.
     */
    protected fun executePagedQuery(
        jpql: String,
        pageable: Pageable,
        parameterBinder: (query: Query) -> Unit = { _ -> }
    ): Page<T> {
        return this.executePagedQuery(jpql, domainClass, pageable, parameterBinder)
    }

    /**
     * Executes a pageable JPQL query returning instances of the given result class.
     * @param jpql the JPQL query
     * @param pageable the pageable
     * @param parameterBinder a lambda that sets the necessary parameters on the query, and on the derived
     * count query. Can be omitted if the query doesn't have any parameter.
     */
    protected fun <U> executePagedQuery(
        jpql: String,
        resultClass: Class<U>,
        pageable: Pageable,
        parameterBinder: (query: Query) -> Unit = { _ -> }
    ): Page<U> {
        val query = entityManager.createQuery(jpql, resultClass)
        parameterBinder(query)
        if (pageable.isPaged()) {
            query.setFirstResult(pageable.getOffset().toInt())
            query.setMaxResults(pageable.getPageSize())
        }
        return PageableExecutionUtils.getPage(
            query.getResultList(),
            pageable
        ) { executeCountQuery(createCountQuery(jpql, parameterBinder)) }
    }

    @Suppress("DEPRECATION")
    private fun createCountQuery(jpql: String, parameterBinder: (query: Query) -> Unit): TypedQuery<Long> {
        val countJpql = QueryUtils.createCountQueryFor(jpql, null)
        val countQuery = entityManager.createQuery(countJpql, Long::class.javaObjectType)
        parameterBinder(countQuery)
        return countQuery
    }

    private fun executeCountQuery(query: TypedQuery<Long>): Long {
        val totals: List<Long?> = query.resultList
        var total = 0L
        for (element in totals) {
            total += element ?: 0
        }
        return total
    }
}
