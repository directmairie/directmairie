package net.adullact.directmairie.service.nominatim

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import net.adullact.directmairie.config.Nominatim
import net.adullact.directmairie.domain.GeoCoordinates
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

/**
 * The type of a Nominatim node, designed to properly deserialize it using any of the various ways the API exposes this
 * information (i.e. for example, a WAY is sometimes exposed using `"W"`, and sometimes using `"way"`. And we need to pass
 * it as a query parameter, it has to be `"W"`).
 */
enum class OsmType {
    WAY, NODE, RELATION;

    companion object {
        @JvmStatic
        @JsonCreator
        fun fromJson(jsonValue: String): OsmType {
            return when (jsonValue) {
                "W", "way" -> WAY
                "N", "node" -> NODE
                "R", "relation" -> RELATION
                else -> throw IllegalArgumentException("Invalid osm_type: $jsonValue")
            }
        }
    }

    fun toQueryParameter() : String {
        return name.substring(0, 1)
    }
}

/**
 * A data class mapping (partially) the result of a query to the "reverse" Nominatim JSON API
 * (see http://nominatim.org/release-docs/latest/api/Reverse/)
 */
data class ReverseResult(@JsonProperty("osm_id") val osmId: Long?,
                         @JsonProperty("osm_type") val osmType: OsmType?) {

}

/**
 * A data class mapping (partially) the result of a query to the "details" Nominatim JSON API
 * (see http://nominatim.org/release-docs/latest/api/Details/)
 */
data class DetailsResult(val address: List<DetailsAddress>)

/**
 * One of the addresses of the result of a query to the "details" Nominatim JSON API
 */
data class DetailsAddress(@JsonProperty("osm_id") val osmId: Long?,
                          @JsonProperty("osm_type") val osmType: OsmType?,
                          @JsonProperty("admin_level") val adminLevel: Int?) {
    /**
     * True if the address is a candidate for the OSM ID of a [government][net.adullact.directmairie.domain.Government],
     * i.e. if the address has an OSM ID, is of type [OsmType.RELATION], and has an admin level.
     */
    val isPotentialGovernment
        get() = osmId != null && osmType == OsmType.RELATION && adminLevel != null
}


/**
 * Service used to call the Nominatim OpenStreetMap API
 * @author JB Nizet
 */
@Service
class NominatimService(@Nominatim val webClient: WebClient) {

    /**
     * Gets the details for the given coordinates. This actually sends two consecutive requests:
     * - one to get the reverse lookup for the given coordinates
     * - one to get the details of the reverse lookup
     */
    fun details(coordinates: GeoCoordinates): DetailsResult {
        return loadReverse(coordinates).flatMap { loadDetails(it) }.block()!!
    }

    private fun loadReverse(coordinates: GeoCoordinates): Mono<ReverseResult> {
        return webClient.get().uri { builder ->
            with(builder) {
                path("/reverse")
                queryParam("format", "json")
                queryParam("lat", coordinates.latitude.toString())
                queryParam("lon", coordinates.longitude.toString())
                build()
            }
        }.retrieve().bodyToMono()
    }

    private fun loadDetails(reverseResult: ReverseResult): Mono<DetailsResult> {
        if (reverseResult.osmId == null || reverseResult.osmType == null) {
            return DetailsResult(emptyList()).toMono()
        }
        return webClient.get().uri { builder ->
            with(builder) {
                path("/details")
                queryParam("format", "json")
                queryParam("osmtype", reverseResult.osmType.toQueryParameter())
                queryParam("osmid", reverseResult.osmId.toString())
                queryParam("hierarchy", "0")
                queryParam("addressdetails", "1")
                build()
            }
        }.retrieve().bodyToMono()
    }
}
