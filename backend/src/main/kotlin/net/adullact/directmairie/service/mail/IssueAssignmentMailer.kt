package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.NewIssueProperties
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.event.IssueCreated
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import jakarta.transaction.Transactional

/**
 * The context exposed to the template used to generate the issue assignment email body
 */
data class NewIssueContext(
    val id: Long,
    val category: String,
    val description: String?,
    val host: String
)

/**
 * Mailer used to send mails when an issue is assigned to a government
 * @author JB Nizet
 */
@Service
@Transactional
class IssueAssignmentMailer(
    mailer: Mailer,
    private val issueDao: IssueDao,
    private val newIssueProperties: NewIssueProperties,
    private val hostingProperties: HostingProperties
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/new-issue/new-issue-text.mustache",
    htmlResourcePath = "/mail/new-issue/new-issue-html.mustache"
) {

    /**
     * Sends, asynchronously, an email to the contact email associated to the group of the issue, if any, or to the
     * government contact email if none.
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendEmail(issueCreated: IssueCreated) {
        fun String?.blankToNull() = if (this.isNullOrBlank()) null else this

        issueDao.findByIdOrNull(issueCreated.issueId)?.let { issue ->
            val government = issue.assignedGovernment!!
            val contactEmail = government.contactForGroup(issue.category!!.group)?.email ?: government.contactEmail
            sendEmail(
                from = newIssueProperties.from,
                to = contactEmail,
                subject = newIssueProperties.subject.format(issue.category!!.label),
                context = NewIssueContext(
                    id = issue.id!!,
                    category = issue.category!!.label,
                    description = issue.description.blankToNull(),
                    host = hostingProperties.host
                )
            )
        }
    }
}
