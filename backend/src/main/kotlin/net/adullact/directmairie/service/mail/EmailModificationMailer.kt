package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.mail.EmailModificationProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.event.EmailModificationRequested
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import jakarta.transaction.Transactional

/**
 * The context exposed to the template used to generate the lost password email body
 */
data class EmailModificationContext(
    val token: String
)

/**
 * Service used to send emails allowing to verify the new email when the user changes it
 * @author JB Nizet
 */
@Service
@Transactional
class EmailModificationMailer(
    mailer: Mailer,
    private val emailVerificationDao: EmailVerificationDao,
    private val emailModificationProperties: EmailModificationProperties
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/emailmodification/emailmodification-text.mustache",
    htmlResourcePath = "/mail/emailmodification/emailmodification-html.mustache"
) {

    /**
     * Sends, asynchronously, an email to the user who requested an email modification in order to verify the new email
     * address belongs to them
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendEmail(emailModification: EmailModificationRequested) {
        emailVerificationDao.findByIdOrNull(emailModification.emailVerificationId)?.let { emailVerification ->
            sendEmail(
                from = emailModificationProperties.from,
                to = emailVerification.email,
                subject = emailModificationProperties.subject,
                context = EmailModificationContext(
                    token = emailVerification.secretToken
                )
            )
        }
    }
}
