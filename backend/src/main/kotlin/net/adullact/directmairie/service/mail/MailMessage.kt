package net.adullact.directmairie.service.mail

/**
 * A mail message
 * @author JB Nizet
 */
data class MailMessage(
    val from: String,
    val to: String,
    val subject: String,
    val plainText: String,
    val htmlText: String
)
