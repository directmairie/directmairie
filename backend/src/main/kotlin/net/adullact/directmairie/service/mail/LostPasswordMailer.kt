package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.LostPasswordProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.event.PasswordLost
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import jakarta.transaction.Transactional

/**
 * The context exposed to the template used to generate the lost password email body
 */
data class LostPasswordContext(
    val userId: Long,
    val token: String,
    val host: String
)

/**
 * Service used to send lost password emails
 * @author JB Nizet
 */
@Service
@Transactional
class LostPasswordMailer(
    mailer: Mailer,
    private val emailVerificationDao: EmailVerificationDao,
    private val lostPasswordProperties: LostPasswordProperties,
    private val hostingProperties: HostingProperties
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/lostpassword/lostpassword-text.mustache",
    htmlResourcePath = "/mail/lostpassword/lostpassword-html.mustache"
) {

    /**
     * Sends, asynchronously, an email to the user who signalled a lost password in order to let him/her reset his/her
     * password.
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendEmail(passwordLost: PasswordLost) {
        emailVerificationDao.findByIdOrNull(passwordLost.emailVerificationId)?.let { emailVerification ->
            sendEmail(
                from = lostPasswordProperties.from,
                to = emailVerification.email,
                subject = lostPasswordProperties.subject,
                context = LostPasswordContext(
                    userId = emailVerification.user.id!!,
                    token = emailVerification.secretToken,
                    host = hostingProperties.host
                )
            )
        }
    }
}
