package net.adullact.directmairie.service.mail

import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.stereotype.Component
import jakarta.mail.internet.MimeMessage

/**
 * Component that offers a higher-level abstraction than Spring's MimeMessageHelper in order to make it testable
 * @author JB Nizet
 */
@Component
class Mailer(private val mailSender: JavaMailSender) {

    /**
     * Sends an email message
     */
    fun send(message: MailMessage) {
        val mimeMessage = mailSender.createMimeMessage()
        val helper = createHelper(mimeMessage)
        with(helper) {
            setFrom(message.from)
            setTo(message.to)
            setSubject(message.subject)
            setText(message.plainText, message.htmlText)
        }

        mailSender.send(mimeMessage)
    }

    protected fun createHelper(mimeMessage: MimeMessage) = MimeMessageHelper(mimeMessage, true)
}
