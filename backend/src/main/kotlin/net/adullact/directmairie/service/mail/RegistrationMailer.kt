package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.RegistrationProperties
import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.event.UserRegistered
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import jakarta.transaction.Transactional

/**
 * The context exposed to the template used to generate the registration email body
 */
data class RegistrationContext(
    val userId: Long,
    val token: String,
    val host: String
)

/**
 * Service used to send registration emails
 * @author JB Nizet
 */
@Service
@Transactional
class RegistrationMailer(
    mailer: Mailer,
    private val emailVerificationDao: EmailVerificationDao,
    private val registrationProperties: RegistrationProperties,
    private val hostingProperties: HostingProperties
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/registration/registration-text.mustache",
    htmlResourcePath = "/mail/registration/registration-html.mustache"
) {

    /**
     * Sends, asynchronously, an email to the user who registered in order to let him/her prove that he/she owns the
     * email address used to register
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendEmail(userRegistered: UserRegistered) {
        emailVerificationDao.findByIdOrNull(userRegistered.emailVerificationId)?.let { emailVerification ->
            sendEmail(
                from = registrationProperties.from,
                to = emailVerification.email,
                subject = registrationProperties.subject,
                context = RegistrationContext(
                    userId = emailVerification.user.id!!,
                    token = emailVerification.secretToken,
                    host = hostingProperties.host
                )
            )
        }
    }
}
