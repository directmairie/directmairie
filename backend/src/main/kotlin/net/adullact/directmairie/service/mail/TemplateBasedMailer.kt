package net.adullact.directmairie.service.mail

import com.samskivert.mustache.Mustache
import com.samskivert.mustache.Template
import org.springframework.core.io.ClassPathResource
import jakarta.annotation.PostConstruct

/**
 * Base class for mailers which send an email based on mustache templates
 * @author JB Nizet
 */
open class TemplateBasedMailer(
    protected val mailer: Mailer,
    private val textResourcePath: String,
    private val htmlResourcePath: String
) {

    protected lateinit var textTemplate: Template
    protected lateinit var htmlTemplate: Template

    @PostConstruct
    internal fun initializeTemplates() {
        ClassPathResource(textResourcePath, TemplateBasedMailer::class.java).inputStream.bufferedReader().use { reader ->
            textTemplate = Mustache.compiler().escapeHTML(false).compile(reader)
        }

        ClassPathResource(htmlResourcePath, TemplateBasedMailer::class.java).inputStream.bufferedReader().use { reader ->
            htmlTemplate = Mustache.compiler().escapeHTML(true).compile(reader)
        }
    }

    protected fun sendEmail(from: String, to: String, subject: String, context: Any) {
        val plainText = textTemplate.execute(context)
        val htmlText = htmlTemplate.execute(context)

        val message = MailMessage(
            from = from,
            to = to,
            subject = subject,
            plainText = plainText,
            htmlText = htmlText
        )

        mailer.send(message)
    }
}
