package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.mail.NotificationProperties
import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.Recipient
import net.adullact.directmairie.dao.SubscriptionDao
import net.adullact.directmairie.event.NotificationRequested
import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.government.toDTO
import org.slf4j.LoggerFactory
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import java.util.NoSuchElementException

const val RECIPIENT_BATCH_SIZE = 100

/**
 * The context exposed to the template used to generate the lost password email body
 */
data class NotificationContext(
    val message: String,
    val url: String?,
    val governmentName: String,
    val host: String
)

/**
 * Event listener which sends, asynchronously, a notification email to all subscribed users
 * @author JB Nizet
 */
@Service
class NotificationMailer(
    mailer: Mailer,
    private val notificationProperties: NotificationProperties,
    private val hostingProperties: HostingProperties,
    private val notificationDataFetcher: NotificationDataFetcher
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/notification/notification-text.mustache",
    htmlResourcePath = "/mail/notification/notification-html.mustache"
) {

    private val logger = LoggerFactory.getLogger(NotificationMailer::class.java)

    /**
     * Sends, asynchronously, an email to the user who requested an email modification in order to verify the new email
     * address belongs to them
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendNotificationEmail(event: NotificationRequested) {
        val batcher = Batcher(event.command.governmentId)

        val government = notificationDataFetcher.getGovernment(event.command.governmentId)
            ?: throw IllegalStateException("No government with ID ${event.command.governmentId}")
        val context = NotificationContext(
            message = event.command.message,
            url = event.command.url,
            governmentName = government.name,
            host = hostingProperties.host
        )
        val plainText = textTemplate.execute(context)
        val htmlText = htmlTemplate.execute(context)

        while (batcher.hasMore()) {
            val batch = batcher.nextBatch()
            sendNotificationEmailToBatch(
                subject = event.command.subject,
                plainText = plainText,
                htmlText = htmlText,
                batch = batch
            )
        }
    }

    private fun sendNotificationEmailToBatch(subject: String, plainText: String, htmlText: String, batch: List<Recipient>) {
        batch.forEach { recipient ->
            try {
                mailer.send(
                    MailMessage(
                        from = notificationProperties.from,
                        to = recipient.email,
                        subject = subject,
                        plainText = plainText,
                        htmlText = htmlText
                    )
                )
            } catch (e: Exception) {
                logger.error("Failed sending mail to recipient ${recipient.email}", e);
            }
        }

    }

    private inner class Batcher(private val governmentId: Long) {
        private var lastBatch: List<Recipient>? = null

        fun hasMore() = lastBatch.let { it == null || it.size == RECIPIENT_BATCH_SIZE }

        fun nextBatch(): List<Recipient> {
            if (!hasMore()) {
                throw NoSuchElementException()
            }
            return notificationDataFetcher.batchOfRecipients(governmentId, lastBatch?.last()?.id).also {
                this.lastBatch = it
            }
        }
    }
}

/**
 * Service which fetches government data, and recipients by batches of N, to avoid a long transaction running while emails are
 * being sent, and to avoid loading too many recipients in memory
 */
@Service
class NotificationDataFetcher(
    private val subscriptionDao: SubscriptionDao,
    private val governmentDao: GovernmentDao
) {
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun batchOfRecipients(governmentId: Long, startingAfterId: Long? = null): List<Recipient> =
        subscriptionDao.batchOfRecipients(governmentId, RECIPIENT_BATCH_SIZE, startingAfterId)

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    fun getGovernment(governmentId: Long): GovernmentDTO? = governmentDao.findByIdOrNull(governmentId)?.toDTO()
}
