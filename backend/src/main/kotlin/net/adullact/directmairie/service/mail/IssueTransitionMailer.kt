package net.adullact.directmairie.service.mail

import net.adullact.directmairie.config.HostingProperties
import net.adullact.directmairie.config.LOCALE
import net.adullact.directmairie.config.TIMEZONE
import net.adullact.directmairie.config.mail.IssueTransitionProperties
import net.adullact.directmairie.dao.IssueTransitionDao
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.event.IssueTransitioned
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import jakarta.transaction.Transactional

/**
 * The context exposed to the template used to generate the issue transition email body
 */
data class IssueTransitionContext(
    val creationDate: String,
    val category: String,
    val description: String?,
    private val status: IssueStatus,
    val message: String?,
    val governmentName: String,
    val host: String
) {
    val resolved = status == IssueStatus.RESOLVED
    val rejected = status == IssueStatus.REJECTED
}

/**
 * Mailer used to send mails to the issue creator when an issue status is updated by an administrator
 * @author JB Nizet
 */
@Service
@Transactional
class IssueTransitionMailer(
    mailer: Mailer,
    private val issueTransitionDao: IssueTransitionDao,
    private val issueTransitionProperties: IssueTransitionProperties,
    private val hostingProperties: HostingProperties
) : TemplateBasedMailer(
    mailer = mailer,
    textResourcePath = "/mail/issue-transition/issue-transition-text.mustache",
    htmlResourcePath = "/mail/issue-transition/issue-transition-html.mustache"
) {

    /**
     * Sends, asynchronously, an email to the creator of an issue (if any, and if they have requested to be notified).
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun sendEmail(issueTransitioned: IssueTransitioned) {
        fun String?.blankToNull() = if (this.isNullOrBlank()) null else this

        issueTransitionDao.findByIdOrNull(issueTransitioned.issueTransitionId)
            ?.takeIf { it.afterStatus == IssueStatus.RESOLVED || it.afterStatus == IssueStatus.REJECTED }
            ?.let { transition ->
                val issue = transition.issue
                issue.creator?.takeIf { it.issueTransitionNotificationActivated }?.let { creator ->
                    sendEmail(
                        from = issueTransitionProperties.from,
                        to = creator.email,
                        subject = issueTransitionProperties.subject,
                        context = IssueTransitionContext(
                            creationDate = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
                                .withLocale(LOCALE)
                                .format(issue.creationInstant.atZone(TIMEZONE)),
                            category = issue.category!!.label,
                            description = issue.description.blankToNull(),
                            status = transition.afterStatus,
                            message = transition.message.blankToNull(),
                            governmentName = issue.assignedGovernment!!.name,
                            host = hostingProperties.host
                        )
                    )
                }
        }
    }
}
