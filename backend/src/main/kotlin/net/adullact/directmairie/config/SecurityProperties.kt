package net.adullact.directmairie.config

import net.adullact.directmairie.web.user.JwtService
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.bind.validation.BindValidationException
import org.springframework.boot.context.properties.source.ConfigurationPropertyName
import org.springframework.boot.diagnostics.AbstractFailureAnalyzer
import org.springframework.boot.diagnostics.FailureAnalysis
import org.springframework.stereotype.Component
import org.springframework.validation.FieldError
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.NotEmpty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

private const val SECURITY_PREFIX = "directmairie.security"

/**
 * Properties related to security
 *
 * @property secretKey The secret key used to sign JWT tokens. It must stay secret and thus not be stored in the
 * public git repo
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = SECURITY_PREFIX)
@Validated
data class SecurityProperties(@field:NotEmpty val secretKey: String?)

@Configuration
@EnableConfigurationProperties(SecurityProperties::class)
class SecurityPropertiesConfig

/**
 * Failure analyzer that displays a meaningful and actionable error message if the secret key hasn't been provided
 */
class MissingSecretKeyFailureAnalyzer : AbstractFailureAnalyzer<BindValidationException>() {
    override fun analyze(rootFailure: Throwable, cause: BindValidationException): FailureAnalysis? {
        if (cause.validationErrors.name == ConfigurationPropertyName.of(SECURITY_PREFIX) &&
            cause.validationErrors.allErrors.any { it is FieldError && it.field == "secretKey" }) {
            return FailureAnalysis(
                """
                    The $SECURITY_PREFIX.secret-key property must be explicitly provided.
                    Indeed, since the key is supposed to be secret, we can't store it in the source code, which is public.
                """.trimIndent(),
                """
                    If you need to generate a key, run the 'generateSecretKey' gradle task, or execute
                    the class ${JwtService::class}.
                    Then use one of the standard Spring Boot ways to define the property. For example, use the
                    command line argument
                        --$SECURITY_PREFIX.secret-key=<the generated secret key>
                """.trimIndent(),
                rootFailure
            )
        }
        return null
    }
}
