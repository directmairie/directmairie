package net.adullact.directmairie.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.NotEmpty
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * Properties related to the hosting of the application.
 * Currently it only contains the host, because we assume that the protocol is always https and the port is always the
 * standard port.
 * The host is used in the mailing templates, and is also interpolated in the mailing from addresses
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.hosting")
@Validated
data class HostingProperties(@field:NotEmpty val host: String)

@Configuration
@EnableConfigurationProperties(HostingProperties::class)
class HostingConfig
