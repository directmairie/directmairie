package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * POJO used to read issue transition mail related properties
 *
 * @property from The from email address of emails sent to the creator when an issue status is updated.
 * @property subject The subject of emails sent to the creator when an issue status is updated.
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.mail.issue-transition")
@Validated
data class IssueTransitionProperties(
    @field:NotEmpty @field:Email val from: String,
    @field:NotEmpty val subject: String
)
