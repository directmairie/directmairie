package net.adullact.directmairie.config

import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableAsync

/**
 * Configuration class for asynchronous tasks handling
 * @author JB Nizet
 */
@Configuration
@EnableAsync
class AsyncConfig
