package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * POJO used to read notification mail related properties
 *
 * @property from The from email address of notification emails
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.mail.notification")
@Validated
data class NotificationProperties(
    @field:NotEmpty @field:Email val from: String
)
