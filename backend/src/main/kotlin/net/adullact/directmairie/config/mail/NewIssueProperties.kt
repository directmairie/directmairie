package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * POJO used to read new issue mail related properties
 *
 * @property from The from email address of emails sent when a new issue is assigned to a government
 * @property subject The subject pattern (with %s identifying the placeholder for the issue category) of emails sent
 * when a new issue is assigned to a government
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.mail.new-issue")
@Validated
data class NewIssueProperties(
    @field:NotEmpty @field:Email val from: String,
    @field:NotEmpty val subject: String
)
