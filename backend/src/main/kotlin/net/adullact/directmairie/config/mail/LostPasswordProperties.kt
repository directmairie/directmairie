package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * POJO used to read lost password mail related properties
 *
 * @property from The from email address of emails sent to verify an email of a lost password request
 * @property subject The subject of emails sent to verify an email of a lost password request
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.mail.lost-password")
@Validated
data class LostPasswordProperties(
    @field:NotEmpty @field:Email val from: String,
    @field:NotEmpty val subject: String
)
