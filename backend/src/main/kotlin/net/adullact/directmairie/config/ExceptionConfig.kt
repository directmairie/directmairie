package net.adullact.directmairie.config

import net.adullact.directmairie.config.ExceptionConfig.CustomErrorAttributes
import net.adullact.directmairie.web.exception.BadRequestException
import org.springframework.boot.web.error.ErrorAttributeOptions
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes
import org.springframework.boot.web.servlet.error.ErrorAttributes
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.context.request.WebRequest

/**
 * Spring Configuration class that adds a custom [ErrorAttributes][CustomErrorAttributes] bean to the context, in order to
 * add a `functionalError` attribute to the JSON body of error responses when the exception
 * is a [BadRequestException]
 * @author JB Nizet
 */
@Configuration
class ExceptionConfig {
    @Bean
    fun errorAttributes(): ErrorAttributes {
        return CustomErrorAttributes()
    }

    open class CustomErrorAttributes : DefaultErrorAttributes() {
        override fun getErrorAttributes(webRequest: WebRequest, options: ErrorAttributeOptions): Map<String, Any> {
            val result = super.getErrorAttributes(webRequest, options)
            val exception = getError(webRequest)
            if (exception is BadRequestException) {
                if (exception.errorCode != null) {
                    result["functionalError"] = exception.errorCode
                }
            }
            return result
        }
    }
}
