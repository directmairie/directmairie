package net.adullact.directmairie.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.io.File
import java.nio.file.Path
import jakarta.validation.constraints.NotNull
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * POJO used to read picture-related properties
 *
 * @property rootDirectory The root directory where uploaded pictures are stored
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.pictures")
@Validated
data class PictureProperties(val rootDirectory: File) {
    val rootPath: Path
        get() = rootDirectory.toPath()
}

@Configuration
@EnableConfigurationProperties(PictureProperties::class)
class PictureConfig
