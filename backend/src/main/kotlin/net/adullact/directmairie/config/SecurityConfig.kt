package net.adullact.directmairie.config

import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.web.actuator.ActuatorFilter
import net.adullact.directmairie.web.user.AuthenticationFilter
import net.adullact.directmairie.web.user.CurrentUser
import net.adullact.directmairie.web.user.JwtService
import net.adullact.directmairie.web.user.PasswordHasher
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Configuration of the security. It registers the authentication filter, as well as the actuator filter
 * checking that a user is a super-admin to access the actuator
 * @author JB Nizet
 */
@Configuration
class SecurityConfig {

    @Bean
    fun authenticationFilter(
        jwtService: JwtService,
        currentUser: CurrentUser,
        userDao: UserDao,
        passwordHasher: PasswordHasher
    ) = AuthenticationFilter(jwtService, currentUser, userDao, passwordHasher)

    @Bean
    fun authenticationFilterRegistration(
        authenticationFilter: AuthenticationFilter
    ): FilterRegistrationBean<AuthenticationFilter> {
        val filterRegistrationBean = FilterRegistrationBean(authenticationFilter)
        filterRegistrationBean.urlPatterns = listOf("/api/*", "/actuator", "/actuator/*")
        return filterRegistrationBean
    }

    @Bean
    fun actuatorFilter(userDao: UserDao, currentUser: CurrentUser) = ActuatorFilter(userDao, currentUser)

    @Bean
    fun actuatorFilterRegistration(actuatorFilter: ActuatorFilter): FilterRegistrationBean<ActuatorFilter> {
        val filterRegistrationBean = FilterRegistrationBean(actuatorFilter)
        filterRegistrationBean.urlPatterns = listOf("/actuator", "/actuator/*")
        return filterRegistrationBean
    }
}
