package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Configuration

/**
 * Configuration class to enable mail properties
 * @author JB Nizet
 */
@Configuration
@EnableConfigurationProperties(value = [
    EmailModificationProperties::class,
    IssueTransitionProperties::class,
    LostPasswordProperties::class,
    NewIssueProperties::class,
    NotificationProperties::class,
    RegistrationProperties::class
])
class MailConfig
