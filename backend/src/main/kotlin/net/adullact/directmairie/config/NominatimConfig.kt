package net.adullact.directmairie.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

@Qualifier
annotation class Nominatim

/**
 * Configuration class defining the beans used to communicate with the Nominatim API
 * @author JB Nizet
 */
@Configuration
class NominatimConfig {

    /**
     * Creates a WebClient, identified by the [Nominatim] qualifier meta-annotation, used to get data from the
     * Nominatim OpenStreetMap API.
     *
     * Note: the http client connector is not pooled, because using the default pooled one causes exceptions
     * of type `reactor.netty.http.client.HttpClientOperations$PrematureCloseException` when sending two consecutive
     * requests
     */
    @Bean
    @Nominatim
    fun nominatimWebClient(builder: WebClient.Builder): WebClient {
        return builder
            .baseUrl("https://nominatim.openstreetmap.org")
            .clientConnector(ReactorClientHttpConnector(HttpClient.newConnection().compress(true)))
            .build()
    }
}
