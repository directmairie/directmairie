package net.adullact.directmairie.config.mail

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * POJO used to read email modification mail related properties
 *
 * @property from The from email address of emails sent to verify an email of an email modification request
 * @property subject The subject of emails sent to verify an email of an email modification request
 *
 * @author JB Nizet
 */
@ConfigurationProperties(prefix = "directmairie.mail.email-modification")
@Validated
data class EmailModificationProperties(
    @field:NotEmpty @field:Email val from: String,
    @field:NotEmpty val subject: String
)
