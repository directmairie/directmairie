package net.adullact.directmairie.config

import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Locale

val LOCALE = Locale.FRANCE
val TIMEZONE = ZoneId.of("Europe/Paris")
