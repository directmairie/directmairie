package net.adullact.directmairie.config

import net.adullact.directmairie.dao.BaseRepository
import net.adullact.directmairie.dao.BaseRepositoryImpl
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

/**
 * Configuration class for the persistence
 * @author JB Nizet
 */
@Configuration
@EnableJpaRepositories(
    basePackageClasses = [BaseRepository::class],
    repositoryBaseClass = BaseRepositoryImpl::class
)
class PersistenceConfig {
}
