package net.adullact.directmairie.domain

import org.hibernate.annotations.BatchSize
import java.time.Instant
import java.util.*
import jakarta.persistence.*

private const val ISSUE_GENERATOR = "IssueGenerator"

/**
 * The status of an issue
 */
enum class IssueStatus {

    /**
     * The issue has been created with the minimal information necessary (coordinates, category???), but it's not
     * finalized yet.
     * While the issue is in DRAFT status, the creator can add and remove pictures to the issue.
     */
    DRAFT,

    /**
     * The user has terminated the creation of the issue, and the issue can be visible by its administrators
     */
    CREATED,

    /**
     * The administrator has marked the issue as resolved
     */
    RESOLVED,

    /**
     * The administrator has marked the issue as rejected
     */
    REJECTED;

    companion object {
        val nonDraft: Set<IssueStatus> = EnumSet.complementOf(EnumSet.of(IssueStatus.DRAFT))
    }
}

/**
 * The issue entity.
 * @author JB Nizet
 */
@Entity
class Issue {

    /**
     * The ID of the issue, null if the issue isn't persistent yet.
     */
    @Id
    @SequenceGenerator(name = ISSUE_GENERATOR, sequenceName = "ISSUE_SEQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(generator = ISSUE_GENERATOR)
    var id: Long? = null

    /**
     * The status of the issue
     */
    @Enumerated(EnumType.STRING)
    var status: IssueStatus = IssueStatus.DRAFT

    /**
     * The category of the issue. Can only be null when the status is DRAFT.
     */
    @ManyToOne
    @JoinColumn(name = "category_id")
    var category: IssueCategory? = null

    /**
     * The description of the issue, that the creator entered when submitting the issue.
     * Null if the creator didn't enter anything, because the category and/or the pictures
     * are sufficient to describe the issue
     */
    var description: String? = null

    /**
     * The geographic coordinates of the issue
     */
    lateinit var coordinates: GeoCoordinates

    /**
     * The user who created this issue (null if it was created anonymously)
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "creator_id")
    var creator: User? = null

    /**
     * The random access token generated and stored in the issue when it's created anonymously with a DRAFT status. This
     * token is sent back to the creator, which must send it with every request modifying, or adding/removing pictures
     * to the issue. This prevents a random hacker to modify draft issues that he/she didn't create.
     * If the issue is created by an authenticated user, this property is null.
     */
    var accessToken: String? = null

    /**
     * The instant when the issue was created (i.e. entered in the system)
     */
    lateinit var creationInstant: Instant

    /**
     * The set of pictures attached to this issue
     */
    @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinTable(
        name = "issue_picture",
        joinColumns = [JoinColumn(name = "issue_id")],
        inverseJoinColumns = [JoinColumn(name = "picture_id")]
    )
    @BatchSize(size = 10)
    private val pictureSet: MutableSet<Picture> = mutableSetOf()

    /**
     * The government that this issue is assigned to, i.e. which is supposed to fix the issue. This can change during
     * the life of the issue. For example, an issue could first be assigned to a small government, but then the
     * administrator of this small government might decide to assign it to one of the ancestor governments because
     * the road where the issue is located is managed by that ancestor government
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "assigned_government_id")
    var assignedGovernment: Government? = null

    /**
     * The smallest local government that this issue belongs to, found when the issue is created, from its coordinates.
     * Storing this information is helpful to be able to reassign the issue to a smaller government that is still part
     * of the correct branch
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "leaf_government_id")
    var leafGovernment: Government? = null

    /**
     * The set of status transitions that this issue went through
     */
    @OneToMany(mappedBy = "issue", cascade = [CascadeType.ALL], orphanRemoval = true)
    private val transitionSet: MutableSet<IssueTransition> = mutableSetOf()

    val pictures: Set<Picture>
        get() = pictureSet

    fun addPicture(picture: Picture) = pictureSet.add(picture)
    fun removePicture(picture: Picture) = pictureSet.remove(picture)

    val transitions: List<IssueTransition>
        get() = transitionSet.sortedBy { it.transitionInstant }

    fun addTransition(transition: IssueTransition) {
        transition.issue = this;
        transitionSet.add(transition)
    }
}
