package net.adullact.directmairie.domain

import jakarta.persistence.Embeddable

/**
 * Immutable geographic coordinates, typically used to know the localization of an issue
 * @author JB Nizet
 */
@Embeddable
data class GeoCoordinates(val latitude: Double, val longitude: Double)
