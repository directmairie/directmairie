package net.adullact.directmairie.domain

import jakarta.persistence.*

private const val USER_GENERATOR = "UserGenerator"

/**
 * A user of the application
 * @author JB Nizet
 */
@Entity
@Table(name = "auser")
class User {
    /**
     * The ID of the user, null if the user isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = USER_GENERATOR,
        sequenceName = "AUSER_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = USER_GENERATOR)
    var id: Long? = null

    /**
     * The email of the user, also used as login, and thus unique
     */
    lateinit var email: String

    /**
     * Flag indicating if the email has been verified. If it's not verified yet, then the user can't log in.
     */
    var emailVerified: Boolean = false

    /**
     * The first name of the user, which can be null if the user chooses not to provide it
     */
    var firstName: String? = null

    /**
     * The first name of the user, which can be null if the user chooses not to provide it
     */
    var lastName: String? = null

    /**
     * The phone number of the user, which can be null if the user chooses not to provide it
     */
    var phone: String? = null

    /**
     * The salted and hashed password of the user
     */
    lateinit var password: String

    /**
     * Indicates if the user is a super-administrator of the application (i.e. manages other users, creates governments,
     * issue categories, etc.)
     */
    var superAdmin: Boolean = false

    /**
     * Indicates whether the user wants to be notified of issue transitions
     */
    var issueTransitionNotificationActivated = false

    /**
     * The governments that this user is an administrator for (i.e. lists issues of these governments, solves/rejects
     * issues, etc.).
     */
    @ManyToMany(mappedBy = "administratorSet")
    private val administeredGovernmentSet: MutableSet<Government> = mutableSetOf()

    /**
     * The pooling organizations that this user is an administrator for.
     */
    @ManyToMany(mappedBy = "administratorSet")
    private val administeredPoolingOrganizationSet: MutableSet<PoolingOrganization> = mutableSetOf()

    /**
     * The subscriptions of this user.
     */
    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], orphanRemoval = true)
    private val subscriptionSet: MutableSet<Subscription> = mutableSetOf()

    val administeredGovernments: Set<Government>
        get() = administeredGovernmentSet
    fun addAdministeredGovernment(government: Government) = administeredGovernmentSet.add(government)
    fun removeAdministeredGovernment(government: Government) = administeredGovernmentSet.remove(government)

    val administeredPoolingOrganizations: Set<PoolingOrganization>
        get() = administeredPoolingOrganizationSet
    fun addAdministeredPoolingOrganization(organization: PoolingOrganization) =
        administeredPoolingOrganizationSet.add(organization)
    fun removeAdministeredPoolingOrganization(organization: PoolingOrganization) =
        administeredPoolingOrganizationSet.remove(organization)

    val subscriptions: Set<Subscription>
        get() = subscriptionSet
    fun addSubscription(subscription: Subscription): Unit {
        subscription.user = this
        subscriptionSet.add(subscription)
    }
    fun removeSubscription(subscription: Subscription) = subscriptionSet.remove(subscription)
}
