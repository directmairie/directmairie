package net.adullact.directmairie.domain

import java.time.Instant
import jakarta.persistence.ManyToOne

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;

private const val ISSUE_TRANSITION_GENERATOR = "IssueTransitionGenerator"

/**
 * An entity tracing the status transitions of an issue, with the messages sent to the issuer if any
 * @author JB Nizet
 */
@Entity
class IssueTransition {
    /**
     * The ID of the transition, null if the transition isn't persistent yet.
     */
    @Id
    @SequenceGenerator(name = ISSUE_TRANSITION_GENERATOR, sequenceName = "ISSUE_TRANSITION_SEQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(generator = ISSUE_TRANSITION_GENERATOR)
    var id: Long? = null

    /**
     * The status that the issue had before the transition
     */
    lateinit var beforeStatus: IssueStatus

    /**
     * The status that the issue had after the transition
     */
    lateinit var afterStatus: IssueStatus

    /**
     * The instant when the transition happened
     */
    lateinit var transitionInstant: Instant

    /**
     * The message sent to the creator of the issue, if any
     */
    var message: String? = null

    /**
     * The issue that this transition belongs to
     */
    @ManyToOne(optional = false)
    lateinit var issue: Issue
}
