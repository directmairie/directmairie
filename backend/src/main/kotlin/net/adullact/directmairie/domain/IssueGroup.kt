package net.adullact.directmairie.domain

import jakarta.persistence.*

const val OTHER_ISSUE_GROUP_ID = 1L;
private const val ISSUE_GROUP_GENERATOR = "IssueGroupGenerator"

/**
 * A group of issue categories. A category belongs to one and only one group.
 * The special group "Other", identified by [OTHER_ISSUE_GROUP_ID], contains the special category "Other".
 * @author JB Nizet
 */
@Entity
class IssueGroup {
    /**
     * The ID of the group, null if the group isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = ISSUE_GROUP_GENERATOR,
        sequenceName = "ISSUE_GROUP_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = ISSUE_GROUP_GENERATOR)
    var id: Long? = null

    /**
     * The short label of the group, unique
     */
    lateinit var label: String

    /**
     * The set of categories of the group
     */
    @OneToMany(mappedBy = "group", cascade = [CascadeType.ALL], orphanRemoval = true)
    private val categorySet: MutableSet<IssueCategory> = mutableSetOf()

    val categories: Set<IssueCategory>
        get() = categorySet

    fun addCategory(category: IssueCategory) {
        category.group = this
        categorySet.add(category)
    }
    fun removeCategory(category: IssueCategory) {
        categorySet.remove(category)
    }
}
