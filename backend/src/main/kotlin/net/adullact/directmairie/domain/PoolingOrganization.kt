package net.adullact.directmairie.domain

import jakarta.persistence.*

private const val POOLING_ORGANIZATION_GENERATOR = "PoolingOrganizationGenerator"

/**
 * A pooling organization, whose role is to administer [local governments][Government]. A pooling organization is a
 * group of [users][User] who have the right to create and configure local governments.
 * A local government, once created by a pooling organization, can only be modified by an administrator of that
 * pooling organization, or by a super-admin.
 *
 * Pooling organizations are themselves created by a super-admin
 *
 * @author JB Nizet
 */
@Entity
class PoolingOrganization {
    /**
     * The ID of the organization, null if the organization isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = POOLING_ORGANIZATION_GENERATOR,
        sequenceName = "POOLING_ORGANIZATION_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = POOLING_ORGANIZATION_GENERATOR)
    var id: Long? = null

    /**
     * The name of the organization.
     */
    lateinit var name: String

    /**
     * The administrators of this pooling organization, who can list their governments and configure them.
     */
    @ManyToMany
    @JoinTable(
        name = "pooling_organization_administrator",
        joinColumns = [JoinColumn(name = "pooling_organization_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    private val administratorSet: MutableSet<User> = mutableSetOf()

    val administrators: Set<User>
        get() = administratorSet

    fun clearAdministrators() = administratorSet.clear()
    fun addAdministrator(administrator: User) = administratorSet.add(administrator)
    fun removeAdministrator(administrator: User) = administratorSet.remove(administrator)
}
