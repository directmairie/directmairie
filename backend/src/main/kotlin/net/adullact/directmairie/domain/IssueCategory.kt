package net.adullact.directmairie.domain

import jakarta.persistence.*

const val OTHER_CATEGORY_ID = 1L
private const val CATEGORY_GENERATOR = "IssueCategoryGenerator"

/**
 * A category of issue. Each [government][Government] defines the list of categories that are acceptable for an issue
 * assigned to that government.
 * When creating an issue, one of these categories must be chosen by the user. For some categories,
 * a description is required because the category is too vague.
 * A category belongs to one and only one [IssueGroup] group.
 * One of the categories is a bit special: "Other", identified with the ID [OTHER_CATEGORY_ID], and belonging to a
 * special group "Other", which is supposed to exist on every government.
 * @author JB Nizet
 */
@Entity
class IssueCategory {
    /**
     * The ID of the category, null if the category isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = CATEGORY_GENERATOR,
        sequenceName = "ISSUE_CATEGORY_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = CATEGORY_GENERATOR)
    var id: Long? = null

    /**
     * The short label of the category
     */
    lateinit var label: String

    /**
     * Determines if a description is required for issues created in this category
     */
    var descriptionRequired: Boolean = false

    /**
     * The group of the category
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    lateinit var group: IssueGroup
}
