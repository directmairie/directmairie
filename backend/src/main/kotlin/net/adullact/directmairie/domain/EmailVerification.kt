package net.adullact.directmairie.domain

import java.security.SecureRandom
import java.time.Instant
import java.util.*
import jakarta.persistence.*

private const val EMAIL_VERIFICATION_GENERATOR = "EmailVerificationGenerator"

/**
 * Entity used to store the temporary information needed for an email verification.
 *
 * When a user registers or asks to change his/her email or his/her password, a secret token is generated and stored
 * here, along with a short validity period and the email to verify. An email is then sent to this new email address.
 * If the user receiving the mail accepts the change, he/she sends back the secret code. If the code matches and is
 * still valid, then the email is stored in the user and marked as verified, or the password is changed.
 * @author JB Nizet
 */
@Entity
class EmailVerification(
    val email: String,
    val secretToken: String,
    val validityInstant: Instant,
    @ManyToOne @JoinColumn(name = "userId") val user: User
) {
    /**
     * The ID of the verification, null if it isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = EMAIL_VERIFICATION_GENERATOR,
        sequenceName = "EMAIL_VERIFICATION_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = EMAIL_VERIFICATION_GENERATOR)
    var id: Long? = null
}

private val random = SecureRandom.getInstance("SHA1PRNG")

/**
 * Generates an 8-bytes secret token using a secure random generator, and encodes it using a Base64 URL encoder so
 * that it can be used easily and safely in URLs, headers, etc.
 */
fun generateSecretToken(): String {
    val bytes = ByteArray(8)
    random.nextBytes(bytes)
    return Base64.getUrlEncoder().encodeToString(bytes)
}
