package net.adullact.directmairie.domain

import jakarta.persistence.*

private const val GOVERNMENT_CONTACT_GENERATOR = "GovernmentContactGenerator"

/**
 * Entity containing, for a given [Government] and a given [IssueGroup], a contact email that is used to send a mail
 * every time an issue of that group is assigned to that government.
 * @author JB Nizet
 */
@Entity
class GovernmentContact {
    /**
     * The ID of the contact, null if the contact isn't persistent yet.
     */
    @Id
    @SequenceGenerator(name = GOVERNMENT_CONTACT_GENERATOR, sequenceName = "GOVERNMENT_CONTACT_SEQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(generator = GOVERNMENT_CONTACT_GENERATOR)
    var id: Long? = null

    /**
     * The contact email address
     */
    lateinit var email: String;

    /**
     * The government to which this contact belongs
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "government_id")
    lateinit var government: Government

    /**
     * The issue group for which this contact is registered
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    lateinit var group: IssueGroup
}
