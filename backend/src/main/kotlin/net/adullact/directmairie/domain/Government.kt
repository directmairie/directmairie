package net.adullact.directmairie.domain

import jakarta.persistence.*
import jakarta.validation.constraints.Min

private const val GOVERNMENT_GENERATOR = "GovernmentGenerator"

/**
 * A (local) government (in French: collectivité). DirectMairie only stores the governments which have decided to use DirectMairie.
 * A government is linked to an Open Street Map place identified by its `osm_id`.
 *
 * Governments are hierarchical, i.e. a small government (like a municipality) can be part of a larger
 * government (like a department), which can itself be part of an even larger government (like a region), etc.
 *
 * When an issue is created, its coordinates are used to get the address hierarchy using the OpenStreetMap
 * Nominatim API, and then every place in the address hierarchy that has an `admin_level` is looked up, in order,
 * to find the smallest local government which includes the coordinates. If found, the issue is attached
 * to this government.
 *
 * @author JB Nizet
 */
@Entity
class Government {
    /**
     * The ID of the government, null if the government isn't persistent yet.
     */
    @Id
    @SequenceGenerator(
        name = GOVERNMENT_GENERATOR,
        sequenceName = "GOVERNMENT_SEQ",
        initialValue = 1000,
        allocationSize = 1
    )
    @GeneratedValue(generator = GOVERNMENT_GENERATOR)
    var id: Long? = null

    /**
     * The name of the government, which can be more descriptive than the name stored in OpenStreetMap.
     */
    lateinit var name: String

    /**
     * The functional code uniquely identifying the government. In France, it's the SIREN code.
     */
    lateinit var code: String

    /**
     * The URL of the government (can be null).
     */
    var url: String? = null

    /**
     * The OpenStreetMap ID (named `osm_id` in the Nominatim terminology) of this government
     */
    @Min(1)
    var osmId: Long = 0

    /**
     * The issue categories configured for this government. They should always contain the category "Other",
     * identified by [OTHER_CATEGORY_ID]
     */
    @ManyToMany
    @JoinTable(
        name = "government_issue_category",
        joinColumns = [JoinColumn(name = "government_id")],
        inverseJoinColumns = [JoinColumn(name = "category_id")]
    )
    private val issueCategorySet: MutableSet<IssueCategory> = mutableSetOf()

    /**
     * The administrators of this government, who can list their issues, solve or reject them, etc.
     */
    @ManyToMany
    @JoinTable(
        name = "government_administrator",
        joinColumns = [JoinColumn(name = "government_id")],
        inverseJoinColumns = [JoinColumn(name = "user_id")]
    )
    private val administratorSet: MutableSet<User> = mutableSetOf()

    /**
     * The pooling organization which created this government, and which is thus allowed to configure it.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pooling_organization_id")
    lateinit var poolingOrganization: PoolingOrganization;

    /**
     * The contact email of the government. When an issue is assigned to the government, if there is no
     * contact email set for the issue group of the issue, then an email is sent to this email address
     */
    lateinit var contactEmail: String;

    /**
     * The set of contacts of this governent (at most one per [IssueGroup])
     */
    @OneToMany(mappedBy = "government", cascade = [CascadeType.ALL], orphanRemoval = true)
    private val contactSet: MutableSet<GovernmentContact> = mutableSetOf()

    /**
     * The picture of the logo of the government
     */
    @OneToOne(fetch = FetchType.LAZY, cascade = [CascadeType.ALL], orphanRemoval = true)
    @JoinColumn(name = "logo_id")
    var logo: Picture? = null

    val issueCategories: Set<IssueCategory>
        get() = issueCategorySet
    fun clearIssueCategories() = issueCategorySet.clear()
    fun addIssueCategory(category: IssueCategory) = issueCategorySet.add(category)
    fun removeIssueCategory(category: IssueCategory) = issueCategorySet.remove(category)

    val administrators: Set<User>
        get() = administratorSet
    fun clearAdministrators() = administratorSet.clear()
    fun addAdministrator(administrator: User) = administratorSet.add(administrator)
    fun removeAdministrator(administrator: User) = administratorSet.remove(administrator)

    val contacts: Set<GovernmentContact>
        get() = contactSet
    fun clearContacts() = contactSet.clear()
    fun addContact(contact: GovernmentContact) {
        contact.government = this
        contactSet.add(contact)
    }
    fun contactForGroup(group: IssueGroup): GovernmentContact? = contactSet.find { group == it.group }
}
