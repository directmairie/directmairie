package net.adullact.directmairie.domain

import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.ManyToOne
import jakarta.persistence.SequenceGenerator

private const val SUBSCRIPTION_GENERATOR = "SubscriptionGenerator"

/**
 * A subscription of a user to the notifications of a government
 * @author JB Nizet
 */
@Entity
class Subscription {

    /**
     * The ID of the subscription, null if the subscription isn't persistent yet.
     */
    @Id
    @SequenceGenerator(name = SUBSCRIPTION_GENERATOR, sequenceName = "SUBSCRIPTION_SEQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(generator = SUBSCRIPTION_GENERATOR)
    var id: Long? = null

    /**
     * The user who is subscribed
     */
    @ManyToOne(optional = false)
    lateinit var user: User

    /**
     * The government which the user is susbcribed to
     */
    @ManyToOne(optional = false)
    lateinit var government: Government
}
