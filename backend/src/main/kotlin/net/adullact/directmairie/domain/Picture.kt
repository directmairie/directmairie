package net.adullact.directmairie.domain

import java.time.Instant
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import jakarta.persistence.SequenceGenerator

private const val PICTURE_GENERATOR = "PictureGenerator"

/**
 * A picture (attached to an issue, or used to contain the logo of a government).
 * It doesn't contain the bytes of the picture, but only a file system path
 * to the picture (relative to a root directory configured for the application).
 * @author JB Nizet
 */
@Entity
class Picture {

    /**
     * The ID of the picture, null if the picture isn't persistent yet.
     */
    @Id
    @SequenceGenerator(name = PICTURE_GENERATOR, sequenceName = "PICTURE_SEQ", initialValue = 1000, allocationSize = 1)
    @GeneratedValue(generator = PICTURE_GENERATOR)
    var id: Long? = null

    /**
     * The path to the picture, relative to the root directory configured in the application
     */
    lateinit var path: String;

    /**
     * The content type (typically image/jpeg) of the picture
     */
    lateinit var contentType: String;

    /**
     * The instant when the picture was created (i.e. uploaded)
     */
    lateinit var creationInstant: Instant;
}
