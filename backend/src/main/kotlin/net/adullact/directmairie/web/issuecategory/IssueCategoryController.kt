package net.adullact.directmairie.web.issuecategory

import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.dao.NotFoundException
import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.user.SuperAdminOnly
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import jakarta.transaction.Transactional

/**
 * REST controller to handle [IssueCategory]
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/issue-groups/{groupId}/categories")
@SuperAdminOnly
class IssueCategoryController(
    private val issueGroupDao: IssueGroupDao,
    private val issueCategoryDao: IssueCategoryDao
) {
    /**
     * Creates an issue category under a given issue group
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    fun create(
        @PathVariable groupId: Long,
        @RequestBody @Validated command: IssueCategoryCommandDTO
    ): IssueCategoryDTO {
        issueCategoryDao.findByLabel(command.label)?.let { throwSameLabelExists() }

        val group = issueGroupDao.findByIdOrThrow(groupId)
        val category = IssueCategory().apply { fromCommand(command) }
        group.addCategory(category)
        issueGroupDao.flush()
        return category.toDTO()
    }

    /**
     * Updates an issue category
     */
    @PutMapping("/{categoryId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(
        @PathVariable groupId: Long,
        @PathVariable categoryId: Long,
        @RequestBody @Validated command: IssueCategoryCommandDTO
    ) {
        val group = issueGroupDao.findByIdOrThrow(groupId)
        val category = group.categories.find { it.id == categoryId } ?: throw NotFoundException()
        category.apply {
            issueCategoryDao.findByLabel(command.label)?.takeUnless { it == this }?.let { throwSameLabelExists() }
            fromCommand(command)
        }
    }

    /**
     * Deletes an issue category
     */
    @DeleteMapping("/{categoryId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(
        @PathVariable groupId: Long,
        @PathVariable categoryId: Long
    ) {
        val group = issueGroupDao.findByIdOrThrow(groupId)
        if (categoryId == OTHER_CATEGORY_ID) {
            throw BadRequestException("The 'Other' category may not be deleted")
        }
        group.categories.find { it.id == categoryId }?.let { group.removeCategory(it) }
    }

    private fun IssueCategory.fromCommand(command: IssueCategoryCommandDTO) {
        label = command.label
        descriptionRequired = command.descriptionRequired
    }

    private fun throwSameLabelExists() {
        throw BadRequestException(ErrorCode.ISSUE_CATEGORY_WITH_SAME_LABEL_ALREADY_EXISTS)
    }
}
