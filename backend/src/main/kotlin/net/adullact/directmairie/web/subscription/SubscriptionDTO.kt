package net.adullact.directmairie.web.subscription

import net.adullact.directmairie.domain.Subscription
import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.government.toDTO

/**
 * DTO for [Subscription]
 * @author JB Nizet
 */
data class SubscriptionDTO(val id: Long, val government: GovernmentDTO)

fun Subscription.toDTO() = SubscriptionDTO(id!!, government.toDTO())
