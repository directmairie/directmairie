package net.adullact.directmairie.web.issuecategory

import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID

/**
 * DTO containing information about a category
 * @author JB Nizet
 */
data class IssueCategoryDTO(
    val id: Long,
    val label: String,
    val descriptionRequired: Boolean
)

/**
 * Transforms an [IssueCategory] into an [IssueCategoryDTO]
 */
fun IssueCategory.toDTO() = IssueCategoryDTO(id!!, label, descriptionRequired)

/**
 * Transforms a collection of [IssueCategory] into a list of [IssueCategoryDTO], sorted by label, except for the
 * "other" category, which always goes last.
 */
fun Collection<IssueCategory>.toSortedDTOs() =
    sortedWith(compareBy<IssueCategory> { if (it.id == OTHER_CATEGORY_ID) 1 else 0 }
                   .thenBy(String.CASE_INSENSITIVE_ORDER) { it.label })
        .map(IssueCategory::toDTO)
