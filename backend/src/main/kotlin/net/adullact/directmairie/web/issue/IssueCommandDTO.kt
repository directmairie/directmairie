package net.adullact.directmairie.web.issue

import net.adullact.directmairie.domain.GeoCoordinates
import net.adullact.directmairie.domain.IssueStatus

/**
 * Command DTO sent to create or update an issue
 * @author JB Nizet
 */
data class IssueCommandDTO(
    val coordinates: GeoCoordinates,
    val categoryId: Long? = null,
    val description: String? = null,
    val status: IssueStatus = IssueStatus.DRAFT
)
