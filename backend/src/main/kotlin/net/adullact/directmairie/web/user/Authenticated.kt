package net.adullact.directmairie.web.user

/**
 * Annotation allowing to install the aspect [AuthenticatedAspect] on a method.
 * @author JB Nizet
 */
@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
annotation class Authenticated
