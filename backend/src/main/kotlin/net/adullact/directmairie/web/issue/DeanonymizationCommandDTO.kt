package net.adullact.directmairie.web.issue

/**
 * Command sent to deanonymize an issue, i.e. make of the current user the creator of an issue, identified by
 * its ID and secured by its access token (to prove that the current user was indeed the creator of the issue)
 * @author JB Nizet
 */
data class DeanonymizationCommandDTO(val accessToken: String)
