package net.adullact.directmairie.web.user

import net.adullact.directmairie.domain.User

/**
 * DTO of a User
 * @author JB Nizet
 */
data class UserDTO(
    val id: Long,
    val email: String,
    val firstName: String?,
    val lastName: String?
)

/**
 * Transforms a [User] into a [UserDTO]
 */
fun User.toDTO() = UserDTO(id = id!!, email = email, firstName = firstName, lastName = lastName)

/**
 * Transforms a collection of [User] into a list of [UserDTO], sorted by their email (case insensitive)
 */
fun Collection<User>.toSortedDTOs(): List<UserDTO> =
    sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER) { it.email }).map { it.toDTO() }
