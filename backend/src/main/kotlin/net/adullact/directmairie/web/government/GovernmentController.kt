package net.adullact.directmairie.web.government

import net.adullact.directmairie.dao.*
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.GovernmentContact
import net.adullact.directmairie.domain.OTHER_CATEGORY_ID
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.page.PageDTO
import net.adullact.directmairie.web.util.page.toDTO
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import jakarta.transaction.Transactional

const val PAGE_SIZE = 20

/**
 * REST controller used to deal with the CRUD of local governments.
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/governments")
@Authenticated
class GovernmentController(
    private val governmentDao: GovernmentDao,
    private val issueCategoryDao: IssueCategoryDao,
    private val issueGroupDao: IssueGroupDao,
    private val userDao: UserDao,
    private val authorizations: Authorizations,
    private val poolingOrganizationDao: PoolingOrganizationDao
) {

    /**
     * Searches for a page of governments, ordered by their name, and filtered by an optional query.
     * The returned governments are the ones that can be administered by the current user.
     */
    @GetMapping
    fun search(
        @RequestParam(required = false) query: String?,
        @RequestParam(defaultValue = "0") page: Int
    ): PageDTO<GovernmentDTO> {
        val actualQuery = query ?: ""
        val pageRequest = PageRequest.of(page, PAGE_SIZE)
        val governments = if (authorizations.currentUser.superAdmin) {
            governmentDao.findByQuery(actualQuery, pageRequest)
        } else {
            governmentDao.findByQueryAndUser(actualQuery, authorizations.currentUser, pageRequest)
        }

        return governments.toDTO { it.toDTO() }
    }

    /**
     * Gets a detailed view of the government identified by the given ID.
     */
    @GetMapping("/{governmentId}")
    fun get(@PathVariable governmentId: Long): GovernmentDetailedDTO {
        val government = governmentDao.findByIdOrThrow(governmentId)
        if (!authorizations.atLeastGovernmentAdmin(government)) {
            throw ForbiddenException()
        }
        return government.toDetailedDTO()
    }

    /**
     * Creates a new government (without logo)
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody @Validated command: GovernmentCommandDTO): GovernmentDetailedDTO {
        val poolingOrganization = poolingOrganizationDao.findByIdOrThrow(command.poolingOrganizationId) {
            BadRequestException("No pooling organization with ID ${command.poolingOrganizationId}")
        }
        if (!authorizations.atLeastPoolingOrganizationAdmin(poolingOrganization)) {
            throw BadRequestException(
                "The user is not an administrator of the pooling organization ${command.poolingOrganizationId}"
            )
        }

        governmentDao.findByOsmId(command.osmId)?.let { throwSameOsmIdExists() }
        governmentDao.findByCode(command.code)?.let { throwSameCodeExists() }

        val government = Government().apply { fromCommand(command) }
        return governmentDao.saveAndFlush(government).toDetailedDTO()
    }

    /**
     * Updates a government (except for its logo which is updated using [GovernmentLogoController])
     */
    @PutMapping("/{governmentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(
        @PathVariable governmentId: Long,
        @RequestBody @Validated command: GovernmentCommandDTO
    ) {
        val government = governmentDao.findByIdOrThrow(governmentId)
        val currentPoolingOrganization = government.poolingOrganization
        if (!authorizations.atLeastPoolingOrganizationAdmin(currentPoolingOrganization)) {
            throw ForbiddenException()
        }

        val newPoolingOrganization = poolingOrganizationDao.findByIdOrThrow(command.poolingOrganizationId) {
            BadRequestException("no pooling organization with ID ${command.poolingOrganizationId}")
        }
        if (!authorizations.atLeastPoolingOrganizationAdmin(newPoolingOrganization)) {
            throw BadRequestException(
                "The user is not an administrator of the pooling organization ${command.poolingOrganizationId}"
            )
        }

        governmentDao.findByOsmId(command.osmId)?.takeUnless { it == government }?.let { throwSameOsmIdExists() }
        governmentDao.findByCode(command.code)?.takeUnless { it == government }?.let { throwSameCodeExists() }

        government.apply { fromCommand(command) }
    }

    /**
     * Deletes the government identified by the given ID
     */
    @DeleteMapping("/{governmentId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable governmentId: Long) {
        governmentDao.findByIdOrNull(governmentId)?.let {
            if (!authorizations.atLeastPoolingOrganizationAdmin(it.poolingOrganization)) {
                throw ForbiddenException()
            }
            governmentDao.delete(it)
        }
    }

    private fun Government.fromCommand(command: GovernmentCommandDTO) {
        name = command.name
        osmId = command.osmId
        code = command.code
        url = command.url
        poolingOrganization = poolingOrganizationDao.findByIdOrThrow(command.poolingOrganizationId) {
            BadRequestException("no pooling organization with ID ${command.poolingOrganizationId}")
        }
        contactEmail = command.contactEmail

        clearIssueCategories()
        command.issueCategoryIds.forEach { issueCategoryId ->
            addIssueCategory(
                issueCategoryDao.findByIdOrThrow(issueCategoryId) {
                    throw BadRequestException("No issue category with ID $issueCategoryId")
                }
            )
        }
        addIssueCategory(issueCategoryDao.findByIdOrThrow(OTHER_CATEGORY_ID)) // can't possibly throw

        clearAdministrators()
        command.administratorIds.forEach { userId ->
            addAdministrator(userDao.findVerifiedByIdOrThrow(userId) {
                BadRequestException("No verified user with the ID $userId")
            })
        }

        clearContacts()
        // to make sure old contacts are deleted before adding new ones, otherwise the unique constraint fails
        governmentDao.flush()
        command.contacts.forEach { contactCommand ->
            addContact(GovernmentContact().apply {
                email = contactCommand.email
                group = issueGroupDao.findByIdOrThrow(contactCommand.issueGroupId) {
                    throw BadRequestException("No issue group with ID ${contactCommand.issueGroupId}")
                }
            })
        }
    }

    private fun throwSameOsmIdExists() {
        throw BadRequestException(ErrorCode.GOVERNMENT_WITH_SAME_OSM_ID_ALREADY_EXISTS)
    }

    private fun throwSameCodeExists() {
        throw BadRequestException(ErrorCode.GOVERNMENT_WITH_SAME_CODE_ALREADY_EXISTS)
    }
}
