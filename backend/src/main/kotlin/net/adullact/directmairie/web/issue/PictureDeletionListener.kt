package net.adullact.directmairie.web.issue

import net.adullact.directmairie.event.IssueDeleted
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Component
import org.springframework.transaction.event.TransactionPhase
import org.springframework.transaction.event.TransactionalEventListener
import java.io.IOException
import java.lang.RuntimeException

/**
 * Listener invoked when an issue has been deleted, in order to delete the pictures after the commit
 * (to make sure the pictures still exist on disk if the issue deletion fails and is thus rollbacked)
 * @author JB Nizet
 */
@Component
class PictureDeletionListener(private val pictureFileService: PictureFileService) {
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @Async
    fun deletePicturesOfDeletedIssue(event: IssueDeleted) {
        event.picturePaths.forEach { path ->
            try {
                pictureFileService.delete(path)
            } catch (e: Exception) {
                // ignore
            }
        }
    }
}
