package net.adullact.directmairie.web.government

import com.fasterxml.jackson.annotation.JsonUnwrapped
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.GovernmentContact
import net.adullact.directmairie.web.issue.PictureDTO
import net.adullact.directmairie.web.issue.toDTO
import net.adullact.directmairie.web.issuecategory.IssueGroupDTO
import net.adullact.directmairie.web.issuecategory.toSortedIssueGroupDTOs
import net.adullact.directmairie.web.user.UserDTO
import net.adullact.directmairie.web.user.toSortedDTOs

/**
 * Simple DTO containing the basic information about a government, as returned by the search method
 * and as referenced by subscriptions
 * @author JB Nizet
 */
data class GovernmentDTO(
    val id: Long,
    val name: String,
    val code: String,
    val url: String?,
    val osmId: Long,
    val logo: PictureDTO?
)

/**
 * Detailed DTO containing the basic information about a government along with the list of its issue categories,
 * as returned by the get method
 * @author JB Nizet
 */
data class GovernmentDetailedDTO(
    @field:JsonUnwrapped val government: GovernmentDTO,
    val contactEmail: String,
    val issueGroups: List<IssueGroupDTO>,
    val administrators: List<UserDTO>,
    val contacts: List<GovernmentContactDTO>,
    val poolingOrganizationId: Long
)

/**
 * DTO allowing to display a reference to an issue group (i.e. containing just its ID and its name)
 */
data class IssueGroupRefDTO(
    val id: Long,
    val label: String
)

/**
 * DTO for the contact of a government, i.e. an email address for a given issue group
 */
data class GovernmentContactDTO(
    val issueGroup: IssueGroupRefDTO,
    val email: String
)

/**
 * Transforms a [Government] into a [GovernmentDTO]
 */
fun Government.toDTO() = GovernmentDTO(id = id!!, name = name, code = code, url = url, osmId = osmId, logo = logo?.toDTO())

/**
 * Transforms a [Government] into a [GovernmentDetailedDTO]
 */
fun Government.toDetailedDTO() = GovernmentDetailedDTO(
    government = toDTO(),
    contactEmail = contactEmail,
    issueGroups = issueCategories.toSortedIssueGroupDTOs(),
    administrators = administrators.toSortedDTOs(),
    contacts = contacts.sortedBy { it.group.id }.map(GovernmentContact::toDTO),
    poolingOrganizationId = poolingOrganization.id!!
)

/**
 * Transforms a [GovernmentContact] into a [GovernmentContactDTO]
 */
fun GovernmentContact.toDTO() = GovernmentContactDTO(
    issueGroup = IssueGroupRefDTO(group.id!!, group.label),
    email = email
)
