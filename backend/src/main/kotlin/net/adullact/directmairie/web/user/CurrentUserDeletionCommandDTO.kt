package net.adullact.directmairie.web.user

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent by a user in order to delete their account
 * @property password the current password, which must be correct to accept the deletion
 * @author JB Nizet
 */
data class CurrentUserDeletionCommandDTO(
    @field:NotEmpty
    val password: String
)
