package net.adullact.directmairie.web.notification

import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.NotificationRequested
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

/**
 * Controller allowing government administrators to send email notifications to all subscribed users
 * @author JB Nizet
 */
@RestController
@RequestMapping("/api/notifications")
@Transactional
@Authenticated
class NotificationController(
    private val governmentDao: GovernmentDao,
    private val eventPublisher: EventPublisher,
    private val authorizations: Authorizations
) {
    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun sendNotification(@Validated @RequestBody command: NotificationCommandDTO) {
        val government = governmentDao.findByIdOrThrow(command.governmentId) {
            BadRequestException("No government with ID ${command.governmentId}")
        }
        if (!authorizations.atLeastGovernmentAdmin(government)) {
            throw ForbiddenException()
        }

        eventPublisher.publish(NotificationRequested(command))
    }
}
