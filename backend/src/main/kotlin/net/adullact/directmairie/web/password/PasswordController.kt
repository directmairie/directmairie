package net.adullact.directmairie.web.password

import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.generateSecretToken
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.PasswordLost
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.user.AuthenticationResultDTO
import net.adullact.directmairie.web.user.JwtService
import net.adullact.directmairie.web.user.PasswordHasher
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.time.temporal.ChronoUnit
import jakarta.transaction.Transactional

/**
 * REST controller used to handle password lost/change requests
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/passwords")
class PasswordController(
    private val userDao: UserDao,
    private val emailVerificationDao: EmailVerificationDao,
    private val eventPublisher: EventPublisher,
    private val passwordHasher: PasswordHasher,
    private val jwtService: JwtService
) {

    /**
     * Asks for a password reset token to be sent by email to a user
     */
    @PostMapping("/lost")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun passwordLost(@RequestBody @Validated command: LostPasswordCommandDTO) {
        val user = userDao.findByVerifiedEmail(command.email)
            ?: throw BadRequestException("There is no registered user with the given email", ErrorCode.UNKNOWN_EMAIL);
        val emailVerification = EmailVerification(
            email = command.email,
            secretToken = generateSecretToken(),
            user = user,
            validityInstant = Instant.now().plus(24, ChronoUnit.HOURS)
        )
        emailVerificationDao.saveAndFlush(emailVerification)

        eventPublisher.publish(PasswordLost(emailVerification.id!!))
    }

    /**
     * Modifies the password of a user, after having verified that the user is indeed the owner of the email address
     */
    @PostMapping("/{userId}/modifications")
    @ResponseStatus(HttpStatus.CREATED)
    fun changePassword(
        @PathVariable userId: Long,
        @RequestBody @Validated command: PasswordModificationCommandDTO
    ): AuthenticationResultDTO {
        val emailVerification = emailVerificationDao.findValidByUserAndToken(userId, command.token)
            ?: throw BadRequestException(ErrorCode.INVALID_PASSWORD_MODIFICATION_TOKEN)
        emailVerification.user.password = passwordHasher.hash(command.password)
        emailVerificationDao.delete(emailVerification)

        return AuthenticationResultDTO(jwtService.buildToken(emailVerification.user.id!!))
    }
}
