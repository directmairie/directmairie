package net.adullact.directmairie.web.password

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to change the password of the user identified by the given email verification token
 * @author JB Nizet
 */
data class PasswordModificationCommandDTO(
    @field:NotEmpty val password: String,
    @field:NotEmpty var token: String
)
