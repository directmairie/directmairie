package net.adullact.directmairie.web.issue

import net.adullact.directmairie.domain.IssueStatus

/**
 * Command DTO sent to update the status of an existing issue
 */
data class IssueStatusCommandDTO(
    val status: IssueStatus,
    val message: String? = null
)
