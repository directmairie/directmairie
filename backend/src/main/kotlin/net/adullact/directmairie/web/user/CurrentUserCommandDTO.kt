package net.adullact.directmairie.web.user

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * Command sent by a user in order to change their personal information
 * @property currentPassword the current password, which must be correct to accept the modification
 * @property newPassword if not blank, changes the password of the current user. Otherwise, leave it
 * @property emailModificationToken: if the email being submitted is different from the current one;
 * then this token must be present to prove that the new email belongs to the current user
 * as it is.
 * @author JB Nizet
 */
data class CurrentUserCommandDTO(
    @field:NotEmpty @field:Email val email: String,
    val firstName: String?,
    val lastName: String?,
    val phone: String?,
    val issueTransitionNotificationActivated: Boolean = false,
    @field:NotEmpty
    val currentPassword: String,
    val newPassword: String?,
    val emailModificationToken: String?
)
