package net.adullact.directmairie.web.government

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty
import jakarta.validation.constraints.Pattern

/**
 * Command sent to create or update a government
 * @author JB Nizet
 */
data class GovernmentCommandDTO(
    @field:NotEmpty val name: String,
    @field:NotEmpty val code: String,
    // validate that the URL is starting with http or https
    // and contains no spaces
    @field:Pattern(regexp = "^http(s?)://(\\S+)") val url: String?,
    val osmId: Long,
    val poolingOrganizationId: Long,
    val issueCategoryIds: Set<Long> = emptySet(),
    val administratorIds: Set<Long> = emptySet(),
    @field:Email val contactEmail: String,
    val contacts: Set<GovernmentContactCommandDTO> = emptySet()
)

data class GovernmentContactCommandDTO(
    @field:NotEmpty val issueGroupId: Long,
    @field:Email val email: String
)
