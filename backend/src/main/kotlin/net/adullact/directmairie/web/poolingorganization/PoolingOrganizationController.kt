package net.adullact.directmairie.web.poolingorganization

import net.adullact.directmairie.dao.PoolingOrganizationDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.user.SuperAdminOnly
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import jakarta.transaction.Transactional

/**
 * REST controller to handle [pooling organizations][PoolingOrganization]
 * @author JB Nizet
 */
@RestController
@RequestMapping("/api/pooling-organizations")
@Transactional
class PoolingOrganizationController(
    private val poolingOrganizationDao: PoolingOrganizationDao,
    private val userDao: UserDao,
    private val authorizations: Authorizations
) {

    /**
     * Searches for pooling organization by an optional query (all organizations are returned if no query)
     * The returned organizations are the ones administered by the current user, or all of them if the user
     * is a super-admin.
     */
    @Authenticated
    @GetMapping
    fun search(@RequestParam(required = false) query: String?): List<PoolingOrganizationDTO> {
        // either the user is super admin, and we search everything
        // or not, and we filter by the pooling organizations he/she administers
        return if (authorizations.currentUser.superAdmin) {
            poolingOrganizationDao.findByQuery(query ?: "").map { it.toDTO() }
        } else {
            poolingOrganizationDao.findByQueryAndAdministrator(query ?: "", authorizations.currentUser).map { it.toDTO() }
        }
    }

    /**
     * Gets a detailed pooling organization, containing its administrators
     */
    @Authenticated
    @GetMapping("/{organizationId}")
    fun get(@PathVariable organizationId: Long): PoolingOrganizationDetailedDTO {
        val organization = poolingOrganizationDao.findByIdOrThrow(organizationId)
        if (!authorizations.atLeastPoolingOrganizationAdmin(organization)) {
            throw ForbiddenException()
        }
        return organization.toDetailedDTO()
    }

    /**
     * Creates a pooling organization
     */
    @SuperAdminOnly
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody @Validated command: PoolingOrganizationCommandDTO): PoolingOrganizationDetailedDTO {
        poolingOrganizationDao.findByName(command.name)?.let { throwSameNameExists() }
        val org = PoolingOrganization().apply { fromCommand(command) }
        return poolingOrganizationDao.saveAndFlush(org).toDetailedDTO()
    }

    /**
     * Updates a pooling organization
     */
    @SuperAdminOnly
    @PutMapping("/{organizationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(
        @PathVariable organizationId: Long,
        @RequestBody @Validated command: PoolingOrganizationCommandDTO
    ) {
        val organization = poolingOrganizationDao.findByIdOrThrow(organizationId)
        poolingOrganizationDao.findByName(command.name)
            ?.takeUnless { it == organization }
            ?.let { throwSameNameExists() }

        organization.fromCommand(command)
    }

    /**
     * Deletes a pooling organization
     */
    @SuperAdminOnly
    @DeleteMapping("/{organizationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable organizationId: Long) {
        poolingOrganizationDao.findByIdOrNull(organizationId)?.let { poolingOrganizationDao.delete(it) }
    }

    private fun PoolingOrganization.fromCommand(command: PoolingOrganizationCommandDTO) {
        name = command.name
        clearAdministrators()
        command.administratorIds.forEach { userId ->
            addAdministrator(userDao.findVerifiedByIdOrThrow(userId) {
                BadRequestException("No verified user with the ID $userId")
            })
        }
    }

    private fun throwSameNameExists() {
        throw BadRequestException(ErrorCode.POOLING_ORGANIZATION_WITH_SAME_NAME_ALREADY_EXISTS)
    }
}
