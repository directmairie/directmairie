package net.adullact.directmairie.web.user

/**
 * Annotation allowing to install the aspect [SuperAdminOnlyAspect] on a method.
 * @author JB Nizet
 */
@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
annotation class SuperAdminOnly
