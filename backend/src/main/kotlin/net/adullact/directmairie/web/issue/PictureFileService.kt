package net.adullact.directmairie.web.issue

import net.adullact.directmairie.config.PictureProperties
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Service used to read and save pictures
 * @author JB Nizet
 */
@Service
class PictureFileService(private val pictureProperties: PictureProperties) {

    /**
     * Computes the relative picture path based on its ID, and on the given file extension.
     * In order to avoid having directories with thousands of files, making it slow and hard to manage, a 3-level
     * directory structure is used:
     *
     *  - the first level is the ID divided by 1 million. So for ID between 0 and 999,999, the first level is 0;
     *    for IDs between 1,000,000 and 1,999,999, the first level is 1; etc.
     *  - the second level is the remaining of th division, divided by 10,000. So for IDs between 1,000,000 and
     *    1,009,999, the second level is 0; for IDs between 1,010,000 and 1,019,999, the first level is 1; etc.
     *  - the third level is the remaining of the difivioon by 10,000, divided by 100. So for IDs between 1,000,000 and
     *    1,000,099, the third level is 0; for IDs between 1,000,100 and 1,000,199, the first level is 1; etc.
     */
    fun idToRelativePath(pictureId: Long, fileExtension: String): String {
        val root = (pictureId / 1_000_000L).toString()
        val subDir = ((pictureId % 1_000_000L) / 10_000L).toString()
        val subSubDir = ((pictureId % 10_000L) / 100L).toString()
        val fileName = "${pictureId}.${fileExtension}"
        return Paths.get(root, subDir, subSubDir, fileName).toString();
    }

    /**
     * Writes the content of the given multipart file to the path obtained from the configured
     * picture root directory and the given relative path.
     */
    fun write(file: MultipartFile, relativePath: String) {
        val completePath = pictureProperties.rootPath.resolve(relativePath)
        Files.createDirectories(completePath.parent)
        file.transferTo(completePath.toFile())
    }

    /**
     * Transforms the given relative path into a file system resource
     */
    fun toResource(relativePath: String): Resource = FileSystemResource(pictureProperties.rootPath.resolve(relativePath))

    /**
     * Deletes the file located at the given relative path
     */
    fun delete(relativePath: String) {
        Files.delete(pictureProperties.rootPath.resolve(relativePath))
    }
}
