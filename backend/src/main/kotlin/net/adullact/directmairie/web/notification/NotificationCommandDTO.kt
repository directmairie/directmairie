package net.adullact.directmairie.web.notification

import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.URL
import jakarta.validation.constraints.NotBlank

const val MAX_SUBJECT_LENGTH = 80;
const val MAX_MESSAGE_LENGTH = 250;

/**
 * A notification command sent to all subscribed users
 * @author JB Nizet
 */
data class NotificationCommandDTO(
    val governmentId: Long,
    @field:NotBlank @field:Length(max = MAX_SUBJECT_LENGTH) val subject: String,
    @field:NotBlank @field:Length(max = MAX_MESSAGE_LENGTH) val message: String,
    @field:URL val url: String?
) {
}
