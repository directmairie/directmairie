package net.adullact.directmairie.web.government

import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.NotFoundException
import net.adullact.directmairie.domain.Picture
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.issue.PictureDTO
import net.adullact.directmairie.web.issue.PictureFileService
import net.adullact.directmairie.web.issue.toDTO
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.image.imageExtension
import net.adullact.directmairie.web.util.image.requireValidImage
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.time.Instant
import jakarta.transaction.Transactional

/**
 * REST controller used to get and update the logo of a government
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/governments/{governmentId}/logo")
class GovernmentLogoController(
    private val governmentDao: GovernmentDao,
    private val pictureFileService: PictureFileService,
    private val authorizations: Authorizations
) {

    /**
     * Gets the bytes of the logo of the given government
     */
    @GetMapping("/{pictureId}/bytes")
    fun getLogoBytes(@PathVariable governmentId: Long, @PathVariable pictureId: Long): ResponseEntity<Resource> {
        val picture = governmentDao.findByIdOrThrow(governmentId).logo ?: throw NotFoundException()
        if (picture.id != pictureId) {
            throw NotFoundException()
        }

        val resource = pictureFileService.toResource(picture.path)
        if (!resource.exists()) {
            throw NotFoundException()
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(picture.contentType))
            .contentLength(resource.contentLength())
            .body(resource)
    }

    /**
     * Deletes the given logo of the given government (and its bytes on the disk)
     */
    @DeleteMapping("/{pictureId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable governmentId: Long, @PathVariable pictureId: Long) {
        governmentDao.findByIdOrNull(governmentId)?.let { government ->
            if (!authorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization)) {
                throw ForbiddenException()
            }
            government.logo?.let { logo ->
                if (logo.id == pictureId) {
                    try {
                        pictureFileService.delete(logo.path)
                    } catch (e: IOException) {
                        // ignore
                    }

                    government.logo = null
                }
            }
        }
    }

    /**
     * Uploads a new logo for the given government (and deletes the old one)
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Authenticated
    fun upload(@PathVariable governmentId: Long, @RequestPart file: MultipartFile): PictureDTO {
        file.requireValidImage()
        val government = governmentDao.findByIdOrThrow(governmentId)
        if (!authorizations.atLeastPoolingOrganizationAdmin(government.poolingOrganization)) {
            throw ForbiddenException()
        }

        val oldLogo = government.logo

        // generate the picture ID, and then use it to determine where to store the file
        val picture = Picture().apply {
            contentType = file.contentType!!
            creationInstant = Instant.now()
            path = file.name // temporary value
        }
        government.logo = picture
        governmentDao.flush()

        picture.path = pictureFileService.idToRelativePath(picture.id!!, file.imageExtension)
        pictureFileService.write(file, picture.path)
        oldLogo?.let {
            try {
                pictureFileService.delete(it.path)
            } catch (e: IOException) {
                // ignore
            }
        }

        return picture.toDTO()
    }
}
