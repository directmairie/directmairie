package net.adullact.directmairie.web.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception used to signal an authentication error (401 error)
 * @author JB Nizet
 */
@ResponseStatus(HttpStatus.UNAUTHORIZED)
class UnauthorizedException() : RuntimeException()
