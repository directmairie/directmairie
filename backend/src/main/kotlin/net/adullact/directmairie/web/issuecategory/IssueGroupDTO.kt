package net.adullact.directmairie.web.issuecategory

import net.adullact.directmairie.domain.IssueCategory
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.OTHER_ISSUE_GROUP_ID

/**
 * DTO containing the information about an issue group, and all of some of its categories (depending on the API used
 * to list them)
 * @author JB Nizet
 */
data class IssueGroupDTO(
    val id: Long,
    val label: String,
    val categories: List<IssueCategoryDTO>
)

/**
 * Transforms an [IssueGroup] into an [IssueGroupDTO], by only including the categories of the group which are
 * contained in the given filtered categories collection, if present. If not present, all the categories of the group
 * are present in the created [IssueGroupDTO].
 */
fun IssueGroup.toDTO(filteredCategories: Collection<IssueCategory> = categories): IssueGroupDTO = IssueGroupDTO(
    id!!,
    label,
    filteredCategories.toSortedDTOs()
)

/**
 * Transforms a collection of [IssueGroup] into a list of [IssueGroupDTO], sorted by label (except for the "other"
 * group which always goes last). Only the categories accepted by the given predicate (if present) are included in the
 * created DTOs. If no predicate is provided, all the categories are included in the groups.
 */
fun Collection<IssueGroup>.toSortedDTOs(categoryPredicate: (IssueCategory) -> Boolean = { true }): List<IssueGroupDTO> =
    sortedWith(compareBy<IssueGroup> { if (it.id == OTHER_ISSUE_GROUP_ID) 1 else 0 }
                   .thenBy(String.CASE_INSENSITIVE_ORDER) { it.label })
        .map { it.toDTO(it.categories.filter(categoryPredicate)) }

/**
 * Transforms a collection of [IssueCategory] into a sorted list of [IssueGroupDTO] containing only the categories
 * and the group of categories that are present in the collection.
 */
fun Collection<IssueCategory>.toSortedIssueGroupDTOs(): List<IssueGroupDTO> {
    val groups = map { it.group }.toSet()
    val categorySet = toSet()
    return groups.toSortedDTOs { categorySet.contains(it) }
}
