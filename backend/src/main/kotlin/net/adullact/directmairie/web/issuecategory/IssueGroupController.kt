package net.adullact.directmairie.web.issuecategory

import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueGroupDao
import net.adullact.directmairie.domain.GeoCoordinates
import net.adullact.directmairie.domain.IssueGroup
import net.adullact.directmairie.domain.OTHER_ISSUE_GROUP_ID
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.user.SuperAdminOnly
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import jakarta.transaction.Transactional

/**
 * Controller used to list, create, update and delete issue groups
 * @author JB Nizet
 */
@RestController
@RequestMapping("/api/issue-groups")
@Transactional
class IssueGroupController(
    private val nominatimService: NominatimService,
    private val issueGroupDao: IssueGroupDao,
    private val issueCategoryDao: IssueCategoryDao
) {
    /**
     * Lists all the issue groups, with all their categories, sorted by labels (except for the "other" group, which
     * always goes last)
     */
    @GetMapping(params = ["!latitude", "!longitude"])
    fun list(): List<IssueGroupDTO> {
        return issueGroupDao.findAll().toSortedDTOs()
    }

    /**
     * Searches the categories for the given coordinates, by finding the details of the coordinates from Nominatim,
     * then extracting all the potential government OSM IDs from these details, and aggregating all the categories
     * belonging to the governments identified by these OSM IDs.
     * The partial groups containing the resulting categories are then returned.
     */
    @GetMapping(params = ["latitude", "longitude"])
    fun search(@RequestParam latitude: Double, @RequestParam longitude: Double): List<IssueGroupDTO> {
        val details = nominatimService.details(GeoCoordinates(latitude, longitude))

        val osmIds = details.address
            .asSequence()
            .filter { it.isPotentialGovernment }
            .map { it.osmId!! }
            .toSet()

        if (osmIds.isEmpty()) {
            return emptyList()
        }

        val categories = issueCategoryDao.findByGovernmentOsmIds(osmIds)
        return categories.toSortedIssueGroupDTOs()
    }

    /**
     * Gets the issue group identified by the given ID
     */
    @GetMapping("/{groupId}")
    fun get(@PathVariable groupId: Long): IssueGroupDTO {
        return issueGroupDao.findByIdOrThrow(groupId).toDTO()
    }

    /**
     * Creates a new issue group
     */
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    @SuperAdminOnly
    fun create(@RequestBody @Validated command: IssueGroupCommandDTO): IssueGroupDTO {
        issueGroupDao.findByLabel(command.label)?.let { throwSameLabelExists() }

        val group = IssueGroup().apply { fromCommand(command) }
        return issueGroupDao.saveAndFlush(group).toDTO()
    }

    /**
     * Updates an issue group
     */
    @PutMapping("/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @SuperAdminOnly
    fun update(@PathVariable groupId: Long, @RequestBody @Validated command: IssueGroupCommandDTO) {
        issueGroupDao.findByIdOrThrow(groupId).apply {
            issueGroupDao.findByLabel(command.label)?.takeUnless { it == this }?.let { throwSameLabelExists() }
            fromCommand(command)
        }
    }

    /**
     * Deletes an issue group
     */
    @DeleteMapping("/{groupId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @SuperAdminOnly
    fun delete(@PathVariable groupId: Long) {
        if (groupId == OTHER_ISSUE_GROUP_ID) {
            throw BadRequestException("The 'Other' group may not be deleted")
        }
        issueGroupDao.findByIdOrNull(groupId)?.let { issueGroupDao.delete(it) }
    }

    private fun IssueGroup.fromCommand(command: IssueGroupCommandDTO) {
        label = command.label
    }

    private fun throwSameLabelExists() {
        throw BadRequestException(ErrorCode.ISSUE_GROUP_WITH_SAME_LABEL_ALREADY_EXISTS)
    }
}
