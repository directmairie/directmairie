package net.adullact.directmairie.web.subscription

import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.NotFoundException
import net.adullact.directmairie.dao.SubscriptionDao
import net.adullact.directmairie.domain.Subscription
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.government.toDTO
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import jakarta.transaction.Transactional

/**
 * Controller used to handle the subscriptions of the current user
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/subscriptions/mine")
@Authenticated
class SubscriptionController(
    private val authorizations: Authorizations,
    private val governmentDao: GovernmentDao,
    private val subscriptionDao: SubscriptionDao
) {
    @GetMapping
    fun list(): List<SubscriptionDTO> =
        authorizations.currentUser.subscriptions.sortedBy { it.government.name }.map { it.toDTO() }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Validated @RequestBody command: SubscriptionCommandDTO): SubscriptionDTO {
        val subscriptionGovernment = governmentDao.findByIdOrThrow(command.governmentId) {
            BadRequestException("No government with ID ${command.governmentId}")
        }
        if (authorizations.currentUser.subscriptions.any { it.government == subscriptionGovernment }) {
            throw BadRequestException("Already subscribed to this government")
        }

        val subscription =
            Subscription().apply {
            user = authorizations.currentUser
            government = subscriptionGovernment
        }.also {
            authorizations.currentUser.addSubscription(it)
        }
        // flush to make sure the ID is generated
        subscriptionDao.flush()

        return subscription.toDTO()
    }

    @DeleteMapping("/{subscriptionId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable subscriptionId: Long) {
        val subscription = authorizations.currentUser.subscriptions.firstOrNull { it.id == subscriptionId } ?: throw NotFoundException()
        authorizations.currentUser.removeSubscription(subscription)
    }

    @GetMapping("/suggestions")
    fun suggest(@RequestParam(required = false) query: String?): List<GovernmentDTO> {
        val actualQuery = query ?: ""
        val userId = authorizations.currentUser.id!!
        return subscriptionDao.suggestGovernments(actualQuery, userId, 10).map { it.toDTO() }
    }
}
