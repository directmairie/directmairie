package net.adullact.directmairie.web.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception used to signal a forbidden error (403 error)
 * @author JB Nizet
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
class ForbiddenException() : RuntimeException()
