package net.adullact.directmairie.web.issue

import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.web.user.Authorizations
import org.apache.commons.csv.CSVFormat
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.io.BufferedWriter
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.nio.charset.StandardCharsets
import java.time.Instant
import java.time.ZoneId

/**
 * A Spring bean allowing to export issues for the current user as CSV
 * @author JB Nizet
 */
@Component
class IssueCsvExporter(
    private val issueDao: IssueDao,
    private val authorizations: Authorizations
) {

    /**
     * Exports the issues created between the given dates (inclusive) as CSV, by writing it to the given
     * output stream.
     * @param out the destination of the CSV rows
     * @param from the first acceptable creation date for the exported issues, inclusive.
     * @param to the last acceptable creation date for the exported issues, inclusive.
     * @param timezone the timezone used to transform the dates into instants, and to format the creation instants
     * of the issues into local date times.
     */
    @Transactional(readOnly = true)
    fun export(out: OutputStream, from: Instant, to: Instant, timezone: ZoneId) {
        getIssues(from, to).use { issueStream ->
            CSVFormat.DEFAULT
                .builder()
                .setHeader(
                    "ID",
                    "Status",
                    "Creation instant",
                    "Creator ID",
                    "Latitude",
                    "Longitude",
                    "Category ID",
                    "Category Label",
                    "Group ID",
                    "Group label",
                    "Description",
                    "Government ID",
                    "Government name",
                    "Government code",
                    "Government OSM ID",
                    "Pooling organization ID",
                    "Pooling organization name"
                )
                .build()
                .print(BufferedWriter(OutputStreamWriter(out, CHARSET))).use { csvPrinter ->

                    issueStream.forEach { issue ->
                        csvPrinter.printRecord(
                            issue.id,
                            issue.status,
                            issue.creationInstant.atZone(timezone).toLocalDateTime(),
                            issue.creator?.id,
                            issue.coordinates.latitude,
                            issue.coordinates.longitude,
                            issue.category!!.id,
                            issue.category!!.label,
                            issue.category!!.group.id,
                            issue.category!!.group.label,
                            issue.description,
                            issue.assignedGovernment!!.id,
                            issue.assignedGovernment!!.name,
                            issue.assignedGovernment!!.code,
                            issue.assignedGovernment!!.osmId,
                            issue.assignedGovernment!!.poolingOrganization.id,
                            issue.assignedGovernment!!.poolingOrganization.name
                        )
                    }
                }
        }
    }

    private fun getIssues(from: Instant, to: Instant) =
        if (authorizations.currentUser.superAdmin) {
            issueDao.findNonDraftByInterval(from, to)
        } else {
            issueDao.findNonDraftByIntervalAndUser(from, to, authorizations.currentUser)
        }

    companion object {
        val CHARSET = StandardCharsets.UTF_8
    }
}
