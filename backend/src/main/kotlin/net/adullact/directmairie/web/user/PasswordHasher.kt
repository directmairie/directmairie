package net.adullact.directmairie.web.user

import org.springframework.stereotype.Component
import java.security.SecureRandom
import java.util.*
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.PBEKeySpec

private const val SALT_LENGTH = 16
private const val ITERATION_COUNT = 10000
private const val KEY_LENGTH = 512

/**
 * Component allowing to salt and hash passwords
 * @author JB Nizet
 */
@Component
class PasswordHasher {

    private val random = SecureRandom.getInstance("SHA1PRNG")
    private val secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")

    /**
     * Salts and hashes the given password, and returns the result, base64-encoded
     */
    fun hash(password: String): String {
        val salt = ByteArray(SALT_LENGTH)
        random.nextBytes(salt)

        val hash = generateHash(password, salt)

        return Base64.getEncoder().encodeToString(salt + hash)
    }

    /**
     * Verifies if the given password matches with the given salted, hashed and base64-encoded password
     */
    fun verify(password: String, hashedPassword: String): Boolean {
        try {
            val bytes = Base64.getDecoder().decode(hashedPassword)
            val salt = bytes.sliceArray(0 until SALT_LENGTH)
            val expectedHash = bytes.sliceArray(SALT_LENGTH until bytes.size)

            val actualHash = generateHash(password, salt)

            return Arrays.equals(actualHash, expectedHash)
        } catch (e: Exception) {
            return false
        }
    }

    private fun generateHash(password: String, salt: ByteArray): ByteArray {
        val keySpec = PBEKeySpec(password.toCharArray(), salt, ITERATION_COUNT, KEY_LENGTH)
        val hash = secretKeyFactory.generateSecret(keySpec).encoded
        return hash
    }

    companion object {
        /**
         * Allows hashing a password passed as argument. This main method is executed by the `hashPassword` Gradle task.
         */
        @JvmStatic
        fun main(args: Array<String>) {
            if (args.size != 1) {
                System.err.println("Missing argument: password-to-hash");
                System.err.println("Usage when run from gradle: ./gradlew hashPassword --args <password-to-hash>");
                System.exit(-1);
            }
            println("Hashed password: ${PasswordHasher().hash(args[0])}")
        }
    }
}
