package net.adullact.directmairie.web.poolingorganization

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to create or update a pooling organization
 * @author JB Nizet
 */
data class PoolingOrganizationCommandDTO(
    @field:NotEmpty val name: String,
    val administratorIds: Set<Long> = emptySet()
)
