package net.adullact.directmairie.web.poolingorganization

import com.fasterxml.jackson.annotation.JsonUnwrapped
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.web.user.UserDTO
import net.adullact.directmairie.web.user.toSortedDTOs

/**
 * DTO containing basic, public information about a pooling organization, as returned by the list API
 * @author JB Nizet
 */
data class PoolingOrganizationDTO(
    val id: Long,
    val name: String
)

/**
 * Detailed DTO containing the basic information about a pooling organization along with the list of its administrators,
 * as returned by the get method
 * @author JB Nizet
 */
data class PoolingOrganizationDetailedDTO(
    @field:JsonUnwrapped val organization: PoolingOrganizationDTO,
    val administrators: List<UserDTO>
)

/**
 * Transforms a [PoolingOrganization] into a [PoolingOrganizationDTO]
 */
fun PoolingOrganization.toDTO() = PoolingOrganizationDTO(id!!, name)

/**
 * Transforms a [PoolingOrganization] into a [PoolingOrganizationDetailedDTO]
 */
fun PoolingOrganization.toDetailedDTO() = PoolingOrganizationDetailedDTO(
    toDTO(),
    administrators.toSortedDTOs()
)
