package net.adullact.directmairie.web.issue

import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.NotFoundException
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.Picture
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.image.imageExtension
import net.adullact.directmairie.web.util.image.requireValidImage
import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.io.IOException
import java.time.Instant
import jakarta.transaction.Transactional

/**
 * REST controller allowing to list, get add and remove pictures of/to an issue
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/issues/{issueId}/pictures")
class IssuePictureController(
    val issueDao: IssueDao,
    val pictureFileService: PictureFileService,
    val authorizations: Authorizations
) {


    /**
     * Gets the actual image bytes of the picture identified by the given issue ID and picture ID
     */
    @GetMapping("/{pictureId}/bytes")
    fun getPictureBytes(@PathVariable issueId: Long, @PathVariable pictureId: Long): ResponseEntity<Resource> {
        val picture =
            issueDao.findByIdOrThrow(issueId).pictures.find { it.id == pictureId } ?: throw NotFoundException()
        val resource = pictureFileService.toResource(picture.path)
        if (!resource.exists()) {
            throw NotFoundException()
        }

        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(picture.contentType))
            .contentLength(resource.contentLength())
            .body(resource)
    }

    /**
     * Deletes the given picture (and its bytes on the disk)
     */
    @DeleteMapping("/{pictureId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable issueId: Long, @PathVariable pictureId: Long) {
        issueDao.findByIdOrNull(issueId)?.let { issue ->
            if (!authorizations.mayUpdateIssue(issue)) {
                throw ForbiddenException()
            }
            if (issue.status != IssueStatus.DRAFT) {
                throw BadRequestException("Pictures may not be deleted on non-DRAFT issues")
            }
            issue.pictures.find { it.id == pictureId }?.let { picture ->
                try {
                    pictureFileService.delete(picture.path)
                } catch (e: IOException) {
                    // ignore
                }

                issue.removePicture(picture)
            }
        }
    }

    /**
     * Uploads a picture for the given issue
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun upload(@PathVariable issueId: Long, @RequestPart file: MultipartFile): PictureDTO {
        file.requireValidImage()
        val issue = issueDao.findByIdOrThrow(issueId)
        if (!authorizations.mayUpdateIssue(issue)) {
            throw ForbiddenException()
        }
        if (issue.status != IssueStatus.DRAFT) {
            throw BadRequestException("Pictures may not be added to non-DRAFT issues")
        }

        // generate the picture ID, and then use it to determine where to store the file
        val picture = Picture().apply {
            contentType = file.contentType!!
            creationInstant = Instant.now()
            path = file.name // temporary value
        }
        issue.addPicture(picture)
        issueDao.flush()

        picture.path = pictureFileService.idToRelativePath(picture.id!!, file.imageExtension)
        pictureFileService.write(file, picture.path)

        return picture.toDTO()
    }
}
