package net.adullact.directmairie.web.util.image

import net.adullact.directmairie.web.exception.BadRequestException
import org.springframework.http.MediaType
import org.springframework.web.multipart.MultipartFile

val IMAGE_SVG = MediaType.parseMediaType("image/svg+xml")

/**
 * The set of accepted media types for images
 */
val ACCEPTED_IMAGE_MEDIA_TYPES = setOf(
    MediaType.IMAGE_JPEG,
    MediaType.IMAGE_PNG,
    MediaType.IMAGE_GIF,
    IMAGE_SVG
)

/**
 * Validates that the given multipart file has a content type and that this content type is a valid image content type
 */
fun MultipartFile.requireValidImage() {
    this.contentType?.let { contentType ->
        val mediaType = MediaType.parseMediaType(contentType)
        if (mediaType !in ACCEPTED_IMAGE_MEDIA_TYPES) {
            throw BadRequestException("content type must be one of $ACCEPTED_IMAGE_MEDIA_TYPES")
        }
    } ?: throw BadRequestException("No content type found")
}

/**
 * Gets the image extension that is appropriate for the content type of the multipart file (which must be valid
 * according to [requireValidImage]).
 */
val MultipartFile.imageExtension: String
    get() = MediaType.parseMediaType(contentType!!).imageExtension

/**
 * Gets the image extension that is appropriate for the given content type (which must be valid
 * according to [requireValidImage]).
 */
val MediaType.imageExtension: String
    get() = if (IMAGE_SVG.equals(this)) "svg" else subtype
