package net.adullact.directmairie.web.user

import net.adullact.directmairie.domain.User

/**
 * DTO containing information about the current user
 * @author JB Nizet
 */
data class CurrentUserDTO(
    val id: Long,
    val email: String,
    val firstName: String?,
    val lastName: String?,
    val phone: String?,
    val issueTransitionNotificationActivated: Boolean,
    val superAdmin: Boolean,
    val poolingOrganizationAdmin: Boolean,
    val governmentAdmin: Boolean
)

/**
 * Transforms a [User] into a [CurrentUserDTO]
 */
fun User.toCurrentUserDTO() = CurrentUserDTO(
    id = id!!,
    email = email,
    firstName = firstName,
    lastName = lastName,
    phone = phone,
    issueTransitionNotificationActivated = issueTransitionNotificationActivated,
    superAdmin = superAdmin,
    poolingOrganizationAdmin = !administeredPoolingOrganizations.isEmpty(),
    governmentAdmin = !administeredGovernments.isEmpty()
)
