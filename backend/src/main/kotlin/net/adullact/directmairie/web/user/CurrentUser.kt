package net.adullact.directmairie.web.user

import net.adullact.directmairie.web.exception.UnauthorizedException
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext

/**
 * Request-scoped bean containing the ID of the current user, populated by the authentication filter
 * @author JB Nizet
 */
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
class CurrentUser @JvmOverloads constructor(userId: Long? = null) {

    /**
     * Gets/Sets the current user ID. Setter intentionally only visible to classes in the same package.
     */
    var userId: Long? = userId
        internal set

    /**
     * Gets the current user ID if present, or throws an [UnauthorizedException].
     * This method is useful for methods that can only be called if the user is authenticated. If the
     * controller method is annotated with [Authenticated], then this method is guaranteed to not throw
     * since the aspect has only made that check.
     */
    val userIdOrThrow: Long
        get() = userId ?: throw UnauthorizedException()
}
