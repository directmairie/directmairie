package net.adullact.directmairie.web.user

/**
 * The result of a successful authentication
 * @author JB Nizet
 */
class AuthenticationResultDTO(val token: String)
