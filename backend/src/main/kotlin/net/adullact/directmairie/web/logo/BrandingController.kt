package net.adullact.directmairie.web.logo

import net.adullact.directmairie.web.government.toDTO
import net.adullact.directmairie.web.user.Authorizations
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import jakarta.transaction.Transactional

/**
 * Controller used to return the government to use for branding (if any) in the navbar and in the home page.
 * It currently only returns a government if the user is authenticated and if the user is the admin of a unique government. In that case,
 * the government information is returned
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/branding")
class BrandingController(private val authorizations: Authorizations) {
    @GetMapping
    fun get(): BrandingDTO {
        if (authorizations.isAnonymous || authorizations.currentUser.administeredGovernments.size != 1) {
            return BrandingDTO(null)
        }
        return BrandingDTO(authorizations.currentUser.administeredGovernments.first().toDTO())
    }
}
