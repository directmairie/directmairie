package net.adullact.directmairie.web.subscription

/**
 * Command sent to create a new subscription
 * @author JB Nizet
 */
data class SubscriptionCommandDTO(val governmentId: Long)
