package net.adullact.directmairie.web.issuecategory

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to create or update an issue category
 * @author JB Nizet
 */
data class IssueCategoryCommandDTO(
    @field:NotEmpty val label: String,
    val descriptionRequired: Boolean
)
