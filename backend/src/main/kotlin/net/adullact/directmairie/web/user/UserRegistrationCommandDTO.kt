package net.adullact.directmairie.web.user

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to start a new registration
 * @author JB Nizet
 */
data class UserRegistrationCommandDTO(
    @field:NotEmpty @field:Email val email: String,
    @field:NotEmpty val password: String,
    val firstName: String?,
    val lastName: String?,
    val phone: String?,
    val issueTransitionNotificationActivated: Boolean = false
)
