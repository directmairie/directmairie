package net.adullact.directmairie.web.issuecategory

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to create or update an issue group
 * @author JB Nizet
 */
data class IssueGroupCommandDTO(@field:NotEmpty val label: String)
