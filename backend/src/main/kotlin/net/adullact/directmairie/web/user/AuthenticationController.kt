package net.adullact.directmairie.web.user

import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.web.exception.UnauthorizedException
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import jakarta.transaction.Transactional


/**
 * REST controller to handle authentications
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/authentications")
class AuthenticationController(
    private val userDao: UserDao,
    private val jwtService: JwtService,
    private val passwordHasher: PasswordHasher
) {
    /**
     * Authenticates a user by password.
     * @return the [AuthenticationResultDTO] containing the authentication token
     * @throws UnauthorizedException (401 status code) if the authentication fails
     */
    @PostMapping
    fun authenticate(@RequestBody @Validated command: AuthenticationCommandDTO): AuthenticationResultDTO {
        val user = userDao.findByVerifiedEmail(command.email) ?: throw UnauthorizedException()

        if (!passwordHasher.verify(command.password, user.password)) {
            throw UnauthorizedException()
        }

        val token = jwtService.buildToken(user.id!!)
        return AuthenticationResultDTO(token)
    }
}

