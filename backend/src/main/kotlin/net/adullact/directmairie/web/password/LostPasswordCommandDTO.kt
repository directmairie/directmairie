package net.adullact.directmairie.web.password

import jakarta.validation.constraints.Email

/**
 * Command sent to request a password reset token to be sent by email
 * @author JB Nizet
 */
data class LostPasswordCommandDTO(@field:Email val email: String)
