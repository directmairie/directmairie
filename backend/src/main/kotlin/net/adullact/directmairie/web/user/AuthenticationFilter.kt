package net.adullact.directmairie.web.user

import net.adullact.directmairie.dao.UserDao
import org.springframework.http.HttpHeaders
import java.nio.charset.StandardCharsets
import java.util.*
import jakarta.servlet.Filter
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletRequest

private const val BEARER_PREFIX = "Bearer "
private const val BASIC_PREFIX = "Basic "

/**
 * The authentication filter, which gets the authentication token out of the request, validates it,
 * extracts the user ID, and initializes the [CurrentUser] bean.
 *
 * It also supports basic authentication, to make it easier to use the API using the command line.
 *
 * @author JB Nizet
 */
class AuthenticationFilter(
    private val jwtService: JwtService,
    private val currentUser: CurrentUser,
    private val userDao: UserDao,
    private val passwordHasher: PasswordHasher
) : Filter {

    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        request as HttpServletRequest
        currentUser.userId = jwtAuth(request) ?: basicAuth(request)
        chain.doFilter(request, response)
    }

    private fun jwtAuth(request: HttpServletRequest): Long? {
        return request.getHeader(HttpHeaders.AUTHORIZATION)
            ?.takeIf { it.startsWith(BEARER_PREFIX) }
            ?.let { it.substring(BEARER_PREFIX.length).trim() }
            ?.let { token ->
                try {
                    jwtService.extractUserId(token)
                } catch (e: Exception) {
                    null
                }
            }
    }

    private fun basicAuth(request: HttpServletRequest): Long? {
        return request.getHeader(HttpHeaders.AUTHORIZATION)
            ?.takeIf { it.startsWith(BASIC_PREFIX) }
            ?.let { basicAuth(it) }
        }

    private fun basicAuth(header: String): Long? {
        try {
            val decodedBytes = Base64.getDecoder().decode(header.substring(BASIC_PREFIX.length).trim())
            val decodedString = decodedBytes.toString(StandardCharsets.UTF_8)
            val colonIndex = decodedString.indexOf(':')
            if (colonIndex <= 0) {
                return null
            }

            val email = decodedString.substring(0, colonIndex)
            val password = decodedString.substring(colonIndex + 1)
            return userDao.findByVerifiedEmail(email)?.takeIf { user ->
                passwordHasher.verify(password, user.password)
            }?.id
        } catch (e: Exception) {
            return null
        }
    }
}
