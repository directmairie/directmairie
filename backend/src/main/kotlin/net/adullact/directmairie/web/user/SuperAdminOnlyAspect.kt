package net.adullact.directmairie.web.user

import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.exception.UnauthorizedException
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.stereotype.Component

/**
 * Aspect allowing to check that the current user is a super admin before invoking a controller method.
 * This aspect is installed by annotating a method with [SuperAdminOnly]
 *
 * If the user is not authenticated or doesn't exist, this aspect throws an [UnauthorizedException].
 * If the user is not a super-admin, this aspect throws an [ForbiddenException].
 *
 * @author JB Nizet
 */
@Aspect
@Component
class SuperAdminOnlyAspect(private val currentUser: CurrentUser, private val userDao: UserDao) {

    @Before("@annotation(superAdminOnly) || @within(superAdminOnly)")
    fun checkUserIsSuperAdmin(superAdminOnly: SuperAdminOnly?) {
        val userId = currentUser.userId ?: throw UnauthorizedException()
        val user = userDao.findByIdOrNull(userId)

        if (user == null) {
            throw UnauthorizedException()
        }

        if (!user.superAdmin) {
            throw ForbiddenException()
        }
    }
}
