package net.adullact.directmairie.web.issue

import net.adullact.directmairie.dao.GovernmentDao
import net.adullact.directmairie.dao.IssueCategoryDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.IssueSearchCriteria
import net.adullact.directmairie.domain.*
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.IssueCreated
import net.adullact.directmairie.event.IssueDeleted
import net.adullact.directmairie.event.IssueTransitioned
import net.adullact.directmairie.service.nominatim.NominatimService
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.user.Authenticated
import net.adullact.directmairie.web.user.Authorizations
import net.adullact.directmairie.web.util.page.PageDTO
import net.adullact.directmairie.web.util.page.toDTO
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import jakarta.transaction.Transactional

const val PAGE_SIZE = 10

/**
 * REST controller to manage issues
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/issues")
class IssueController(
    private val issueDao: IssueDao,
    private val issueCategoryDao: IssueCategoryDao,
    private val nominatimService: NominatimService,
    private val governmentDao: GovernmentDao,
    private val eventPublisher: EventPublisher,
    private val authorizations: Authorizations,
    private val issueCsvExporter: IssueCsvExporter
) {

    /**
     * Lists the issues, page by page, for the governments administered by the current user, or for the governments
     * which are inside the given governmentIds (if provided) or which belong to any of the given pooling organizations (if provided)
     *
     * - if super admin: sees every issue
     * - if pooling organization admin: sees every issue of the governments of the administered pooling organizations
     * - if government admin: sees every issue of the administered governments
     */
    @GetMapping
    @Authenticated
    fun search(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(name = "status", required = false) statuses: Set<IssueStatus>?,
        @RequestParam(name = "org", required = false) poolingOrganizationIds: Set<Long>?,
        @RequestParam(name = "gov", required = false) governmentIds: Set<Long>?
    ): PageDTO<IssueDTO> {
        // find the user that requested the search
        // and filter the issues by the governments administered
        val pageRequest = PageRequest.of(page, PAGE_SIZE)
        val issues = issueDao.findByCriteria(
            IssueSearchCriteria(
                user = authorizations.currentUser,
                statuses = statuses?.takeIf { it.isNotEmpty() } ?: IssueStatus.nonDraft,
                poolingOrganizationIds = poolingOrganizationIds.orEmpty(),
                governmentIds = governmentIds.orEmpty()
            ),
            pageRequest
        )
        return issues.toDTO(Issue::toDTO)
    }

    /**
     * Lists the issues, page by page, created by the current user.
     * By default, only the non-draft issues are returned, unless statues are specified
     */
    @GetMapping("/mine")
    @Authenticated
    fun mine(
        @RequestParam(defaultValue = "0") page: Int,
        @RequestParam(name = "status", required = false) statuses: Set<IssueStatus>?
    ): PageDTO<IssueDTO> {
        val pageRequest = PageRequest.of(page, PAGE_SIZE)
        val issues = issueDao.findByCreatorAndStatuses(authorizations.currentUser, statuses?.takeIf { it.isNotEmpty() } ?: IssueStatus.nonDraft, pageRequest)
        return issues.toDTO(Issue::toDTO)
    }

    /**
     * Gets an issue.
     */
    @GetMapping("/{issueId}")
    fun get(@PathVariable issueId: Long): IssueDTO {
        return issueDao.findByIdOrThrow(issueId).toDTO()
    }

    /**
     * Creates a new issue.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@Validated @RequestBody command: IssueCommandDTO): IssueDTO {
        validateStatus(newStatus = command.status)
        val issue = Issue().apply {
            creationInstant = Instant.now()
            fromCommand(command)
        }
        if (!authorizations.isAnonymous) {
            issue.creator = authorizations.currentUser
        } else if (issue.status == IssueStatus.DRAFT) {
            issue.accessToken = generateSecretToken()
        }

        issueDao.saveAndFlush(issue)

        publishEventIfCreated(issue);

        return issue.toDTOWithAccessToken()
    }

    /**
     * Updates an existing issue.
     */
    @PutMapping("/{issueId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun update(@PathVariable issueId: Long, @Validated @RequestBody command: IssueCommandDTO) {
        val issue = issueDao.findByIdOrThrow(issueId)
        if (!authorizations.mayUpdateIssue(issue)) {
            throw ForbiddenException()
        }

        validateStatus(oldStatus = issue.status, newStatus = command.status)
        issue.fromCommand(command)

        publishEventIfCreated(issue);
    }

    /**
     * Deletes an existing issue.
     * The issue can be deleted if the user is an administrator of the government of the issue,
     * or the administrator of the pooling organization of the government of the issue, or a superadmin.
     * If the user doesn't have the right to do so, throws [ForbiddenException]
     */
    @DeleteMapping("/{issueId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authenticated
    fun delete(@PathVariable issueId: Long) {
        val issue = issueDao.findByIdOrThrow(issueId)

        issue.assignedGovernment.let { government ->
            if (government == null && !authorizations.currentUser.superAdmin) {
                throw ForbiddenException()
            }
            if (government != null && !authorizations.atLeastGovernmentAdmin(government)) {
                throw ForbiddenException()
            }
        }

        eventPublisher.publish(IssueDeleted(issue.id!!, issue.pictures.map(Picture::path).toSet()))

        issueDao.delete(issue)
    }

    private fun publishEventIfCreated(issue: Issue) {
        if (issue.status == IssueStatus.CREATED) {
            eventPublisher.publish(IssueCreated(issue.id!!))
        }
    }

    /**
     * Disallows statuses other than DRAFT and CREATED, and ensures that an update is not from CREATED to DRAFT
     */
    private fun validateStatus(oldStatus: IssueStatus = IssueStatus.DRAFT, newStatus: IssueStatus) {
        if (newStatus != IssueStatus.CREATED && newStatus != IssueStatus.DRAFT) {
            throw BadRequestException("Status ${newStatus.name} is not allowed")
        }
        if (oldStatus == IssueStatus.CREATED && newStatus == IssueStatus.DRAFT) {
            throw BadRequestException("Status ${newStatus.name} is not allowed after status ${oldStatus.name}")
        }
    }

    /**
     * Updates the status of an existing issue.
     * The status can be modified if the user is an administrator of the government of the issue,
     * or the administrator of the pooling organization of the government of the issue, or a superadmin.
     * If the user doesn't have the right to do so, throws [ForbiddenException]
     * If the existing status, or new status, is DRAFT, then throws [BadRequestException]
     */
    @PutMapping("/{issueId}/status")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authenticated
    fun updateStatus(@PathVariable issueId: Long, @RequestBody command: IssueStatusCommandDTO) {
        val issue = issueDao.findByIdOrThrow(issueId)
        issue.assignedGovernment?.let { government ->
            if (!authorizations.atLeastGovernmentAdmin(government)) {
                throw ForbiddenException()
            }
        }

        if (issue.status == IssueStatus.DRAFT) {
            throw BadRequestException("Issue with status ${issue.status.name} can't be updated to another status")
        }
        if (command.status == IssueStatus.DRAFT) {
            throw BadRequestException("Issue can't be updated to status ${command.status.name}")
        }

        val statusBefore = issue.status
        issue.status = command.status

        val transition = IssueTransition().apply {
            this.issue = issue
            message = command.message
            transitionInstant = Instant.now()
            beforeStatus = statusBefore
            afterStatus = issue.status
        }
        issue.addTransition(transition)

        // flush to make sure the issue transition ID is generated
        issueDao.flush();

        eventPublisher.publish(IssueTransitioned(transition.id!!))
    }

    /**
     * Exports the issues created between the two given local dates, inclusive, in CSV format, for the governments
     * administered by the current user:
     *
     * - if super admin: sees every issue
     * - if pooling organization admin: sees every issue of the governments of the administered pooling organizations
     * - if government admin: sees every issue of the administered governments
     *
     * The local dates are transformed into Instants using the provided timezone. If no timezone is provided, the
     * timezone of the server is used.
     */
    @GetMapping(params = ["export=CSV"])
    @Authenticated
    fun export(
        @RequestParam from: LocalDate,
        @RequestParam to: LocalDate,
        @RequestParam(required = false) timezone: ZoneId?
    ): ResponseEntity<StreamingResponseBody> {
        val body = StreamingResponseBody { outputStream ->
            val actualTimezone = timezone ?: ZoneId.systemDefault()
            issueCsvExporter.export(
                outputStream,
                from.atStartOfDay(actualTimezone).toInstant(),
                to.plusDays(1).atStartOfDay(actualTimezone).toInstant(),
                actualTimezone
            )
        }
        return ResponseEntity.ok().contentType(MediaType.parseMediaType("text/csv")).body(body)
    }

    /**
     * Deanonymizes an issue, i.e. sets the current user as the creator of an issue, if he/she can prove that he/she's
     * the actual creator of the anonymous issue by providing its access token.
     */
    @PutMapping("/{issueId}/creator")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Authenticated
    fun deanonymize(@PathVariable issueId: Long, @Validated @RequestBody command: DeanonymizationCommandDTO) {
        val issue = issueDao.findByIdOrThrow(issueId)
        if (issue.creator == null) {
            // nominal case: the issue is indeed anonymous
            if (issue.accessToken == command.accessToken) {
                issue.creator = authorizations.currentUser
            } else {
                throw ForbiddenException()
            }
        } else {
            // in the sake of idempotency, if the creator of the issue is the current user, do nothing. Otherwise,
            // throw a ForbiddenException
            if (issue.creator != authorizations.currentUser) {
                throw ForbiddenException()
            }
        }
    }

    private fun Issue.fromCommand(command: IssueCommandDTO) {
        coordinates = command.coordinates
        category = command.categoryId?.let { categoryId ->
            issueCategoryDao.findByIdOrThrow(categoryId) {
                throw BadRequestException("No issue category with ID $categoryId")
            }
        }
        description = command.description
        status = command.status

        if (status == IssueStatus.CREATED) {
            assignGovernments()
            val issueCategory = category!! // the category can't be null since assignGovernments has checked it
            if (issueCategory.descriptionRequired && description.isNullOrBlank()) {
                throw BadRequestException("A description is required for issues of the category ${issueCategory.id}")
            }
        }
    }

    private fun Issue.assignGovernments() {
        val issueCategory = category ?: throw BadRequestException("category is mandatory")

        val governments = findGovernments(coordinates)
        if (governments.isEmpty()) {
            throw BadRequestException("No government for coordinates $coordinates")
        }

        leafGovernment = governments[0]

        assignedGovernment = findGovernmentWithCategory(governments, issueCategory)
            ?: throw BadRequestException("No government for coordinates $coordinates and category ${issueCategory.id}")
    }

    private fun findGovernments(coordinates: GeoCoordinates): List<Government> {
        val sortedOsmIds = nominatimService.details(coordinates).address
            .filter { it.isPotentialGovernment }
            .sortedByDescending { it.adminLevel }
            .map { it.osmId!! }

        if (sortedOsmIds.isEmpty()) {
            return emptyList()
        }

        return governmentDao.findByOsmIds(sortedOsmIds.toSet()).sortedBy { sortedOsmIds.indexOf(it.osmId) }
    }

    private fun findGovernmentWithCategory(governments: List<Government>, category: IssueCategory): Government? {
        return governments.first { category in it.issueCategories }
    }
}
