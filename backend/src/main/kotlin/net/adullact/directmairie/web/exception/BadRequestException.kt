package net.adullact.directmairie.web.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

/**
 * Exception used to signal a bad request (400 error).
 *
 * Some errors are technical (i.e. the error should never happen if the client correctly validates the data it sends).
 *
 * Some are functional (i.e. the client has no way to know that the error will happen). In that case, the exception
 * should be constructed with an [ErrorCode] as argument, so that the client can programmatically know what the error
 * is and display it the way it wants to.
 * @author JB Nizet
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
class BadRequestException(message: String, val errorCode: ErrorCode? = null) : RuntimeException(message) {

    /**
     * Constructs a functional BadRequestException with a functional error code, and a message which is obtained by
     * replacing underscores in the code by spaces and then lowercasing the result.
     */
    constructor(errorCode: ErrorCode): this(errorCode.name.replace('_', ' ').lowercase(), errorCode)
}
