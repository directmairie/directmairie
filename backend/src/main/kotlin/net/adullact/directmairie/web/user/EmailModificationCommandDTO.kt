package net.adullact.directmairie.web.user

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to request an email verification code for a new email address
 * @author JB Nizet
 */
data class EmailModificationCommandDTO(
    @field:NotEmpty val password: String,
    @field:NotEmpty @field:Email val email: String
)
