package net.adullact.directmairie.web.user

import net.adullact.directmairie.dao.EmailVerificationDao
import net.adullact.directmairie.dao.IssueDao
import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.EmailVerification
import net.adullact.directmairie.domain.User
import net.adullact.directmairie.domain.generateSecretToken
import net.adullact.directmairie.event.EmailModificationRequested
import net.adullact.directmairie.event.EventPublisher
import net.adullact.directmairie.event.UserRegistered
import net.adullact.directmairie.web.exception.BadRequestException
import net.adullact.directmairie.web.exception.ErrorCode
import net.adullact.directmairie.web.exception.ForbiddenException
import net.adullact.directmairie.web.util.page.PageDTO
import net.adullact.directmairie.web.util.page.toDTO
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import java.time.Instant
import java.time.temporal.ChronoUnit
import jakarta.transaction.Transactional

const val PAGE_SIZE = 20

/**
 * REST controller used to register new users, authenticate them
 * @author JB Nizet
 */
@RestController
@Transactional
@RequestMapping("/api/users")
class UserController(
    private val userDao: UserDao,
    private val emailVerificationDao: EmailVerificationDao,
    private val passwordHasher: PasswordHasher,
    private val eventPublisher: EventPublisher,
    private val jwtService: JwtService,
    private val authorizations: Authorizations,
    private val issueDao: IssueDao
) {

    /**
     * Starts a user registration: creates the user with an unverified email address, generates an email verification
     * and sends the secret token by email to the email.
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun register(@RequestBody @Validated command: UserRegistrationCommandDTO): UserDTO {
        val existingUser = userDao.findByEmail(command.email)
        val user = if (existingUser?.emailVerified ?: false) {
            throw BadRequestException(ErrorCode.ALREADY_REGISTERED)
        } else if (existingUser != null) {
            existingUser.email = command.email
            existingUser.password = passwordHasher.hash(command.password)
            existingUser.firstName = command.firstName?.ifBlank { null }
            existingUser.lastName = command.lastName?.ifBlank { null }
            existingUser.phone = command.phone?.ifBlank { null }
            existingUser.issueTransitionNotificationActivated = command.issueTransitionNotificationActivated
            existingUser
        } else {
            User().apply {
                email = command.email
                password = passwordHasher.hash(command.password)
                firstName = command.firstName?.ifBlank { null }
                lastName = command.lastName?.ifBlank { null }
                phone = command.phone?.ifBlank { null }
                issueTransitionNotificationActivated = command.issueTransitionNotificationActivated
                emailVerified = false

                userDao.saveAndFlush(this)
            }
        }

        val emailVerification = EmailVerification(
            email = command.email,
            secretToken = generateSecretToken(),
            validityInstant = Instant.now().plus(1, ChronoUnit.DAYS),
            user = user
        )

        emailVerificationDao.saveAndFlush(emailVerification)

        eventPublisher.publish(UserRegistered(emailVerification.id!!))

        return user.toDTO()
    }

    /**
     * Gets the information about the current user
     */
    @Authenticated
    @GetMapping("/me")
    fun getCurrentUser() = authorizations.currentUser.toCurrentUserDTO()

    /**
     * Request that an email verification code be sent by email to a new email in order to change
     * the current user email. This is required before updating the current user if the new email
     * is different from the current one.
     */
    @Authenticated
    @PostMapping("/me/email-modification")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun requestEmailVerificationToken(@RequestBody @Validated command: EmailModificationCommandDTO) {
        val currentUser = checkEmailVerifiedAndPasswordCorrect(command.password)
        checkEmailNotAlreadyUsed(command.email)

        val emailVerification = EmailVerification(
            email = command.email,
            secretToken = generateSecretToken(),
            validityInstant = Instant.now().plus(1, ChronoUnit.DAYS),
            user = currentUser
        )

        emailVerificationDao.saveAndFlush(emailVerification)

        eventPublisher.publish(EmailModificationRequested(emailVerification.id!!))
    }

    /**
     * Updates the information of the current user
     */
    @Authenticated
    @PutMapping("/me")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun updateCurrentUser(@RequestBody @Validated command: CurrentUserCommandDTO) {
        val currentUser = checkEmailVerifiedAndPasswordCorrect(command.currentPassword)
        checkEmailNotAlreadyUsed(command.email)

        if (command.email != currentUser.email) {
            val tokenCorrect = command.emailModificationToken?.let { token ->
                emailVerificationDao.findValidByUserAndToken(currentUser.id!!, token)?.takeIf { it.email == command.email } != null
            } ?: false
            if (!tokenCorrect) {
                throw BadRequestException(ErrorCode.INVALID_EMAIL_MODIFICATION_TOKEN)
            }
        }

        currentUser.email = command.email
        currentUser.firstName = command.firstName?.ifBlank { null }
        currentUser.lastName = command.lastName?.ifBlank { null }
        currentUser.phone = command.phone?.ifBlank { null }
        currentUser.issueTransitionNotificationActivated = command.issueTransitionNotificationActivated

        command.newPassword?.let { password ->
            if (password.isNotBlank()) {
                currentUser.password = passwordHasher.hash(password)
            }
        }
    }

    /**
     * Deletes the current user and anonymizes their issues
     */
    @Authenticated
    @PutMapping("/me/deletion")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteCurrentUser(@RequestBody @Validated command: CurrentUserDeletionCommandDTO) {
        val currentUser = checkEmailVerifiedAndPasswordCorrect(command.password)
        currentUser.administeredGovernments.forEach { government ->
            government.removeAdministrator(currentUser)
        }
        currentUser.administeredPoolingOrganizations.forEach { org ->
            org.removeAdministrator(currentUser)
        }
        emailVerificationDao.deleteByUser(currentUser)
        issueDao.anonymizeByCreator(currentUser)
        userDao.delete(currentUser)
    }

    /**
     * Searches for a page of users whose emails match the given query. The users are returned sorted by their email
     * (case-insensitive)
     */
    @Authenticated
    @GetMapping
    fun searchByEmail(
        @RequestParam query: String,
        @RequestParam(defaultValue = "0") page: Int
    ): PageDTO<UserDTO> {
        if (!authorizations.atLeastAnyPoolingOrganizationAdmin()) {
            throw ForbiddenException()
        }
        return userDao.searchByVerifiedEmail(query, PageRequest.of(page, PAGE_SIZE)).toDTO { it.toDTO() }
    }

    /**
     * Submits the email verification token sent by email after a registration, to mark the given user as
     * verified, and thus allow to authenticate later. This also authenticates the user, and thus returns
     * the authentication result, containing the authentication token.
     */
    @PostMapping("/{userId}/email-verifications")
    @ResponseStatus(HttpStatus.CREATED)
    fun verifyEmail(
        @PathVariable userId: Long,
        @RequestBody @Validated command: EmailVerificationCommandDTO
    ): AuthenticationResultDTO {
        val emailVerification =
            emailVerificationDao.findValidByUserAndToken(userId, command.token)
                ?: throw BadRequestException(ErrorCode.INVALID_REGISTRATION_TOKEN)

        emailVerification.user.apply {
            email = emailVerification.email
            emailVerified = true
        }

        emailVerificationDao.delete(emailVerification)

        return AuthenticationResultDTO(jwtService.buildToken(emailVerification.user.id!!))
    }

    private fun checkEmailVerifiedAndPasswordCorrect(password: String): User {
        val currentUser = authorizations.currentUser
        if (!currentUser.emailVerified) {
            throw ForbiddenException()
        }
        if (!passwordHasher.verify(password, currentUser.password)) {
            throw BadRequestException(ErrorCode.INCORRECT_PASSWORD)
        }
        return authorizations.currentUser
    }

    private fun checkEmailNotAlreadyUsed(
        email: String
    ) {
        val userWithSameEmail = userDao.findByVerifiedEmail(email)
        if (userWithSameEmail != null && userWithSameEmail != authorizations.currentUser) {
            throw BadRequestException(ErrorCode.EMAIL_ALREADY_USED)
        }
    }
}
