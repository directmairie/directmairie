package net.adullact.directmairie.web.user

import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.domain.Government
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.PoolingOrganization
import net.adullact.directmairie.web.exception.UnauthorizedException
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import org.springframework.web.context.WebApplicationContext
import jakarta.servlet.http.HttpServletRequest

const val ISSUE_ACCESS_TOKEN_HEADER = "X-Issue-Access"

/**
 * Request-scoped component used to check authorizations. Unless indicated otherwise, all methods assume that there
 * is a current user, and throw an [UnauthorizedException] if there is not
 * @author JB Nizet
 */
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
@Component
class Authorizations(
    private val currentUserService: CurrentUser,
    private val userDao: UserDao,
    private val request: HttpServletRequest
) {
    /**
     * Gets the current user or throws an [UnauthorizedException].
     */
    val currentUser by lazy(LazyThreadSafetyMode.NONE) {
        userDao.findByIdOrThrow(currentUserService.userIdOrThrow) { UnauthorizedException() }
    }
    // Note: since the bean is request-scoped, only one thread can access this property, and synchronizing it (which
    // is the default for lazy properties) is thus unnecessary

    /**
     * Tests if the request is anonymous, i.e. if there is no current user
     */
    val isAnonymous: Boolean
        get() = this.currentUserService.userId == null

    /**
     * Tests if the current user is at least a pooling organization administrator (of any pooling organization)
     * @return true if the user is an administrator of at least one pooling organization or is a super admin. false
     * otherwise
     */
    fun atLeastAnyPoolingOrganizationAdmin() =
        currentUser.superAdmin || !currentUser.administeredPoolingOrganizations.isEmpty()

    /**
     * Tests if the current user is at least an administrator of the given government
     * @return true if the user is an administrator of the given government, or of the pooling organization
     * of the given government, or is a super admin. false otherwise
     */
    fun atLeastGovernmentAdmin(government: Government): Boolean {
        return currentUser.superAdmin ||
            government in currentUser.administeredGovernments ||
            government.poolingOrganization in currentUser.administeredPoolingOrganizations
    }

    /**
     * Tests if the current user is at least an administrator of the given pooling organization
     * @return true if the user is an administrator of the given pooling organization or is a super admin. false
     * otherwise
     */
    fun atLeastPoolingOrganizationAdmin(poolingOrganization: PoolingOrganization): Boolean {
        return currentUser.superAdmin || poolingOrganization in currentUser.administeredPoolingOrganizations
    }

    /**
     * Tests if the current user, if there is one, is the creator of the given issue,
     * or if the given access token in the request, if any, is equal to the access token stored in the issue
     */
    fun mayUpdateIssue(issue: Issue): Boolean {
        if (this.isAnonymous) {
            val accessToken = extractAccessTokenFromRequest()
            return accessToken != null && accessToken == issue.accessToken
        }
        return this.currentUser == issue.creator
    }

    private fun extractAccessTokenFromRequest(): String? {
        return this.request.getHeader(ISSUE_ACCESS_TOKEN_HEADER)
    }
}
