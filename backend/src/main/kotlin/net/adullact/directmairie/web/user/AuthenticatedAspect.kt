package net.adullact.directmairie.web.user

import net.adullact.directmairie.web.exception.UnauthorizedException
import org.aspectj.lang.annotation.Aspect
import org.aspectj.lang.annotation.Before
import org.springframework.stereotype.Component

/**
 * Aspect allowing to check that there is a current user. If there is none, this aspect throws an
 * [UnauthorizedException].
 *
 * @author JB Nizet
 */
@Aspect
@Component
class AuthenticatedAspect(private val currentUser: CurrentUser) {

    @Before("@annotation(authenticated) || @within(authenticated)")
    fun checkUserIsAuthenticated(authenticated: Authenticated?) {
        currentUser.userIdOrThrow
    }
}
