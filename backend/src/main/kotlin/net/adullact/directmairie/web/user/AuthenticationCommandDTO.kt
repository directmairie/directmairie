package net.adullact.directmairie.web.user

import jakarta.validation.constraints.Email
import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to authenticate
 * @author JB Nizet
 */
data class AuthenticationCommandDTO(
    @field:NotEmpty @field:Email val email: String,
    @field:NotEmpty val password: String
)
