package net.adullact.directmairie.web.user

import jakarta.validation.constraints.NotEmpty

/**
 * Command sent to verify an email
 * @author JB Nizet
 */
data class EmailVerificationCommandDTO(@field:NotEmpty val token: String)
