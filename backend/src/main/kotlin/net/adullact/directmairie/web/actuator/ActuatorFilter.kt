package net.adullact.directmairie.web.actuator

import net.adullact.directmairie.dao.UserDao
import net.adullact.directmairie.web.user.CurrentUser
import org.springframework.http.HttpStatus
import jakarta.servlet.Filter
import jakarta.servlet.FilterChain
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.HttpServletResponse

/**
 * Filter checking that requests to the actuator are only made by super-admin users
 * @author JB Nizet
 */
class ActuatorFilter(
    private val userDao: UserDao,
    private val currentUser: CurrentUser
) : Filter {
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        response as HttpServletResponse

        val userId = currentUser.userId
        if (userId == null) {
            response.sendError(HttpStatus.UNAUTHORIZED.value())
            return
        }

        val user = userDao.findByIdOrNull(userId)
        if (user == null) {
            response.sendError(HttpStatus.UNAUTHORIZED.value())
            return
        }

        if (!user.superAdmin) {
            response.sendError(HttpStatus.FORBIDDEN.value())
            return
        }

        chain.doFilter(request, response)
    }
}
