package net.adullact.directmairie.web.issue

import com.fasterxml.jackson.annotation.JsonInclude
import net.adullact.directmairie.domain.GeoCoordinates
import net.adullact.directmairie.domain.Issue
import net.adullact.directmairie.domain.IssueStatus
import net.adullact.directmairie.domain.IssueTransition
import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.government.toDTO
import net.adullact.directmairie.web.issuecategory.IssueCategoryDTO
import net.adullact.directmairie.web.issuecategory.toDTO
import java.time.Instant

/**
 * DTO containing basic, public information about an issue
 * @author JB Nizet
 */
data class IssueDTO(
    val id: Long,
    val status: IssueStatus,
    val coordinates: GeoCoordinates,
    val category: IssueCategoryDTO?,
    val description: String?,
    val pictures: List<PictureDTO>?,
    val creationInstant: Instant,
    val assignedGovernment: GovernmentDTO?,
    val transitions: List<IssueTransitionDTO>,
    @JsonInclude(JsonInclude.Include.NON_NULL) val accessToken: String?
)

/**
 * DTO containing the information about the transitions of an issue
 */
data class IssueTransitionDTO(
    val transitionInstant: Instant,
    val beforeStatus: IssueStatus,
    val afterStatus: IssueStatus,
    val message: String?
)

/**
 * Transforms an [Issue] into an [IssueDTO], without any access token
 */
fun Issue.toDTO(): IssueDTO = toDTO(withAccessToken = false)

private fun IssueTransition.toDTO() = IssueTransitionDTO(
    transitionInstant,
    beforeStatus,
    afterStatus,
    message
)

/**
 * Transforms an [Issue] into an [IssueDTO], containing the access token. Such a DTO can only be sent to the creator
 * of the issue, because only the creator is supposed to know the access token, which allows him/her to update and
 * deanonymize the issue.
 */
fun Issue.toDTOWithAccessToken(): IssueDTO = toDTO(withAccessToken = true)

private fun Issue.toDTO(withAccessToken: Boolean) = IssueDTO(
    id = id!!,
    status = status,
    coordinates = coordinates,
    category = category?.toDTO(),
    description = description,
    pictures = pictures.toSortedDTOs(),
    creationInstant = creationInstant,
    assignedGovernment = assignedGovernment?.toDTO(),
    transitions = transitions.map { it.toDTO() },
    accessToken = accessToken?.takeIf { withAccessToken }
)
