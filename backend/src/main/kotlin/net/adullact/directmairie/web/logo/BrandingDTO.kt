package net.adullact.directmairie.web.logo

import net.adullact.directmairie.web.government.GovernmentDTO
import net.adullact.directmairie.web.issue.PictureDTO

/**
 * The information about the branding to be used (or not)
 * @author JB Nizet
 */
data class BrandingDTO(
    /**
     * The information about the government that must be used to brand the application (i.e logo, url),
     * or null if no branding must be done.
     */
    val government: GovernmentDTO?
)
