package net.adullact.directmairie.web.user

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import net.adullact.directmairie.config.SecurityProperties
import org.springframework.stereotype.Service
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.crypto.KeyGenerator

private val SIGNATURE_ALGORITHM = Jwts.SIG.HS256.key().build().algorithm

/**
 * A service used to create and parse/validate JWT tokens, used by the authentication mechanism
 * @author JB Nizet
 */
@Service
class JwtService(securityProperties: SecurityProperties) {

    val secretKey = Keys.hmacShaKeyFor(Base64.getDecoder().decode(securityProperties.secretKey!!))

    /**
     * Builds a token for the given user ID
     * @return a JWT token as a String
     */
    fun buildToken(userId: Long): String {
        return Jwts.builder()
            .subject(userId.toString())
            .expiration(Date.from(Instant.now().plus(30, ChronoUnit.DAYS)))
            .signWith(secretKey)
            .compact()
    }

    /**
     * Extracts the user ID from the given JWT token
     * @param token - token to analyze
     * @return the user ID contained in the token
     */
    fun extractUserId(token: String): Long {
        return Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token).payload.subject.toLong()
    }

    companion object {
        internal fun generateSecretKey(): String {
            val keyGenerator = KeyGenerator.getInstance(SIGNATURE_ALGORITHM)
            val secretKey = keyGenerator.generateKey()
            return Base64.getEncoder().encodeToString(secretKey.encoded)
        }

        /**
         * Allows generating a new secret key used to sign the JWT tokens. This main method is executed by the
         * `generateSecretKey` Gradle task.
         */
        @JvmStatic
        fun main(args: Array<String>) {
            println(generateSecretKey())
        }
    }
}

