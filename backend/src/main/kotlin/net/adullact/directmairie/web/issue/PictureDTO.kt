package net.adullact.directmairie.web.issue

import net.adullact.directmairie.domain.Picture
import java.time.Instant

/**
 * A DTO of a picture
 * @author JB Nizet
 */
data class PictureDTO(val id: Long, val creationInstant: Instant)

/**
 * Transforms a [Picture] into a [PictureDTO]
 */
fun Picture.toDTO() = PictureDTO(id!!, creationInstant)

/**
 * Transforms a collection of [Picture] into a list of [PictureDTO] sorted by creation instant (most recent last)
 */
fun Collection<Picture>.toSortedDTOs() = sortedBy(Picture::creationInstant).map(Picture::toDTO)
