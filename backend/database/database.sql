-- creates the directmairie user and database
-- this script must be executed by an admin user. It can typically be executed from the root of the project using
-- psql -h localhost -U postgres -f backend/database/database.sql

create user directmairie password 'directmairie';
create database directmairie owner directmairie
  encoding 'UTF8'
  lc_collate 'fr_FR.UTF-8'
  lc_ctype 'fr_FR.UTF-8'
  template=template0;

create database directmairie_test owner directmairie
encoding 'UTF8'
lc_collate 'fr_FR.UTF-8'
lc_ctype 'fr_FR.UTF-8'
template=template0;
