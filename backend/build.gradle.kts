import org.flywaydb.gradle.task.FlywayCleanTask
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.springframework.boot.gradle.tasks.buildinfo.BuildInfo

buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("org.postgresql:postgresql:42.7.5")
    }
}

plugins {
    val kotlinVersion = "2.1.10"

    java
    jacoco
    kotlin("jvm") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.spring") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.jpa") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.noarg") version kotlinVersion
    id("org.jetbrains.kotlin.plugin.allopen") version kotlinVersion
    id("org.jetbrains.kotlin.kapt") version kotlinVersion
    id("org.springframework.boot") version "3.4.3"
    id("io.spring.dependency-management") version "1.1.7"
    id("org.flywaydb.flyway") version "11.3.4"
    id("com.gorylenko.gradle-git-properties") version "2.4.2"
    id("org.jetbrains.dokka") version "2.0.0"
    id("org.asciidoctor.jvm.convert") version "4.0.4"
}

java {
    sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
    mavenCentral()
}

allOpen {
    annotation("javax.persistence.Entity")
}

gitProperties {
    // filter out the properties that change at every build (like the build date), because
    // they change the output of the task at every build, and thus cause all the downstream tasks (tests, etc.)
    // to be re-executed at each build instead of being up-to-date.
    keys = keys.filter { !(it as String).startsWith("git.build") }
}

flyway {
    cleanDisabled = false
}


val snippetsDir = file("build/generated-snippets")
val docTestsPattern = "**/*DocTest.class"
val springBootVersion = dependencyManagement.managedVersions["org.springframework.boot:spring-boot"]

tasks {
    withType<KotlinCompile> {
        compilerOptions {
            freeCompilerArgs.set(listOf("-Xjsr305=strict", "-Xjvm-default=all"))
            jvmTarget.set(JvmTarget.JVM_17)
        }
    }

    // this task is always out-of-date because it generates a properties file with the build time inside
    // so the bootJar task is also always out of date, too, since it depends on it
    // but it's better to do that than using the bootInfo() method of the springBoot closure, because that
    // makes the test task out of date, which makes the build much longer.
    // See https://github.com/spring-projects/spring-boot/issues/13152
    val buildInfo by registering(BuildInfo::class) {
        destinationDir.set(file("${layout.buildDirectory}/buildInfo"))
    }

    bootJar {
        archiveFileName.set("directmairie.jar")
        dependsOn(":frontend:assemble")
        dependsOn(buildInfo)

        bootInf {
            into("classes/static") {
                from("${project(":frontend").projectDir}/dist/directmairie-frontend/browser")
            }
            into("classes/META-INF") {
                from(buildInfo.map { it.destinationDir })
            }
        }
    }

    bootRun {
        args("--directmairie.security.secret-key=Dauwf0wOh/UMa2pa8ZWJ37Za98yz7JdwxTBm1gIjbmA=")
    }

    test {
        useJUnitPlatform()
        // On CI, Gitlab will spin a Postgres service on host "postgres"
        if (System.getenv("CI") != null) {
            systemProperty("directmairie.database.host-and-port", "postgres:5432")
        }
        exclude(docTestsPattern)
    }

    jacocoTestReport {
        reports {
            xml.required.set(true)
            html.required.set(true)
        }
    }

    // disable default tasks added by flyway plugin
    matching { it.name.startsWith("flyway") }.forEach { it.enabled = false }

    val flywayCleanInteg by registering(FlywayCleanTask::class) {
        url = "jdbc:postgresql://localhost:5432/directmairie"
    }
    val flywayCleanTest by registering(FlywayCleanTask::class) {
        url = "jdbc:postgresql://localhost:5432/directmairie_test"
    }

    listOf(flywayCleanInteg, flywayCleanTest).forEach {
        it.configure {
            user = "directmairie"
            password = "directmairie"
            (this as DefaultTask).group = "Database"
        }
    }

    register<JavaExec>("generateSecretKey") {
        group = "DirectMairie"
        description = "Generates a secret key that can then be used as the value of the directmairie.secret-key property"
        mainClass.set("net.adullact.directmairie.web.user.JwtService")
        classpath = sourceSets.main.get().runtimeClasspath
    }

    register<JavaExec>("hashPassword") {
        group = "DirectMairie"
        description = """
            Hashes a password, ready to be used as the password of the first administrator inserted in the database.
            Usage: ./gradlew hashPassword --args <password-to-hash>
        """.trimIndent()
        mainClass.set("net.adullact.directmairie.web.user.PasswordHasher")
        classpath = sourceSets.main.get().runtimeClasspath
    }


    val docTest by registering(Test::class) {
        val test by testing.suites.existing(JvmTestSuite::class)
        testClassesDirs = files(test.map { it.sources.output.classesDirs })
        classpath = files(test.map { it.sources.runtimeClasspath })

        useJUnitPlatform()
        include(docTestsPattern)
        outputs.dir(snippetsDir)
    }

    asciidoctor {
        configurations("asciidoctorExt")
        inputs.dir(snippetsDir)
        dependsOn(docTest)
        sourceDir("src/main/asciidoc")
        attributes = mapOf(
            "stylesheet" to "directmairie.css",
            "stylesdir" to "src/main/asciidoc/styles",
            "springbootversion" to springBootVersion
        )
    }

    check {
        dependsOn(docTest)
    }

    build {
        dependsOn(asciidoctor)
    }
}

val jwtVersion = "0.12.6"

configurations.create("asciidoctorExt")

dependencies {
    implementation("org.springframework.boot:spring-boot-starter-data-jpa")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-webflux")
    implementation("org.springframework.boot:spring-boot-starter-mail")
    implementation("org.springframework.boot:spring-boot-starter-validation")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("com.samskivert:jmustache")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("io.jsonwebtoken:jjwt-api:$jwtVersion")
    implementation("org.apache.commons:commons-csv:1.13.0")

    kapt("org.springframework.boot:spring-boot-configuration-processor")

    runtimeOnly("org.postgresql:postgresql")
    runtimeOnly("org.flywaydb:flyway-core")
    runtimeOnly("org.flywaydb:flyway-database-postgresql")
    runtimeOnly("io.jsonwebtoken:jjwt-impl:$jwtVersion")
    runtimeOnly("io.jsonwebtoken:jjwt-jackson:$jwtVersion")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("com.ninja-squad:DbSetup:2.1.0")
    testImplementation("com.ninja-squad:DbSetup-kotlin:2.1.0")
    testImplementation("io.mockk:mockk:1.13.17")
    testImplementation("com.ninja-squad:springmockk:4.0.2")
    testImplementation("org.springframework.restdocs:spring-restdocs-mockmvc")

    "asciidoctorExt"("org.springframework.restdocs:spring-restdocs-asciidoctor")
}
